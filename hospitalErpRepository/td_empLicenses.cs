//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hospitalErpRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class td_empLicenses
    {
        public int Lid { get; set; }
        public int LName { get; set; }
        public int LPlace { get; set; }
        public System.DateTime LStartDate { get; set; }
        public System.DateTime LEndDate { get; set; }
        public bool LValidity { get; set; }
        public string Limg { get; set; }
        public int empId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<bool> Notifications { get; set; }
    
        public virtual td_selectionDetails td_selectionDetails { get; set; }
        public virtual td_employee td_employee { get; set; }
        public virtual td_selectionDetails td_selectionDetails1 { get; set; }
    }
}
