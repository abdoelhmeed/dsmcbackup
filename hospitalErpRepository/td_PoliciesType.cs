//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hospitalErpRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class td_PoliciesType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public td_PoliciesType()
        {
            this.td_Policies = new HashSet<td_Policies>();
            this.td_PolicyPlan = new HashSet<td_PolicyPlan>();
        }
    
        public int id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public string description { get; set; }
        public Nullable<int> MId { get; set; }
        public Nullable<System.DateTime> EnterDate { get; set; }
        public Nullable<int> Oid { get; set; }
        public string TImg { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<td_Policies> td_Policies { get; set; }
        public virtual tdMang_Organization tdMang_Organization { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<td_PolicyPlan> td_PolicyPlan { get; set; }
        public virtual tdManag_Management tdManag_Management { get; set; }
    }
}
