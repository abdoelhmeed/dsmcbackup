//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hospitalErpRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class td_IncidentAffectedStaff
    {
        public int AffectedId { get; set; }
        public Nullable<int> empId { get; set; }
        public Nullable<int> IncidentId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    
        public virtual td_employee td_employee { get; set; }
        public virtual td_Incident td_Incident { get; set; }
        public virtual tdMang_Organization tdMang_Organization { get; set; }
    }
}
