//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hospitalErpRepository
{
    using System;
    using System.Collections.Generic;
    
    public partial class td_PoliciesRead
    {
        public int id { get; set; }
        public string policiesNumbers { get; set; }
        public Nullable<int> empId { get; set; }
        public Nullable<bool> isRead { get; set; }
        public Nullable<System.DateTime> RaedDate { get; set; }
    
        public virtual td_employee td_employee { get; set; }
        public virtual td_Policies td_Policies { get; set; }
    }
}
