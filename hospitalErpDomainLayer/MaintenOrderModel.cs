﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public class MaintenOrderModel
    {
        [Display(Name = "Id")]
        public int id { get; set; }
        [Display(Name = "asstes")]
        public Nullable<int> asstesId { get; set; }
        [Display(Name = "empId")]
        public Nullable<int> empId { get; set; }
        public Nullable<System.DateTime> MaintenanceOrderDate { get; set; }
        [Display(Name = "Order Stuts")]
        public string OrderStuts { get; set; }
        [Display(Name = "Order Notes")]
        public string OrderNotes { get; set; }
        [Display(Name = "Order Done")]
        public Nullable<bool> Done { get; set; }
        [Display(Name = "Order Type")]
        public string OrderType { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }

    public class MaintenOrderViewModel
    {
        [Display(Name = "Id")]
        public int id { get; set; }
        [Display(Name = "asstes")]
        public Nullable<int> asstesId { get; set; }

        [Display(Name = "Asstes Name")]
        public string asstesName { get; set; }

        [Display(Name = "empId")]
        public Nullable<int> empId { get; set; }
        [Display(Name = "Employee Name")]
        public string employeeNameEn { get; set; }

        [Display(Name = "Employee Name")]
        public string employeeNameAr { get; set; }

        [Display(Name = "Maintenance Order Date")]
        public DateTime MOrderDate { get; set; }
        [Display(Name = "Order Date")]
        public string strMaintenanceOrderDate { get; set; }
        [Display(Name = "Order Stuts")]
        public string OrderStuts { get; set; }
        [Display(Name = "Order Notes")]
        public string OrderNotes { get; set; }
        [Display(Name = "Order Done")]
        public Nullable<bool> Done { get; set; }
        [Display(Name = "Order Type")]
        public string OrderType { get; set; }
    }



}