﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public class IncidentAffectedStaffModel
    {
        public int AffectedId { get; set; }
        public Nullable<int> empId { get; set; }
        public Nullable<int> IncidentId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }

    public class IncidentAffectedStaffViewModel
    {
        [Display(Name = "Id")]
        public int AffectedId { get; set; }
        [Display(Name = "Emp Id")]
        public Nullable<int> empId { get; set; }
        [Display(Name = "E Name Arabic")]
        public string empNameAr { get; set; }
        [Display(Name = "E Name English")]
        public string empNameEn { get; set; }
        public Nullable<int> IncidentId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }

    public class IncidentAffectedVisitorModel
    {
        [Display(Name ="Id")]
        public int Vid { get; set; }
        [Display(Name = "Name")]
        public string VName { get; set; }
        [Display(Name = "Address")]
        public string VAddress { get; set; }
        [Display(Name = "Phone")]
        public string VPhone { get; set; }
        public Nullable<int> IncidentId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }
}
