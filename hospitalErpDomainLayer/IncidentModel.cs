﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace hospitalErpDomainLayer
{
    public class IncidentsModel
    {
        [Display(Name ="Id")]
        public int IncidentId { get; set; }
        [Display(Name = "Occurrence Date")]
        [Required]
        
        public DateTime OccurrenceDate { get; set; }
        [Display(Name = "Discover Date")]
        [Required]
        public DateTime DiscoverDate { get; set; }
        [Display(Name = "Report Date")]
        public Nullable<System.DateTime> ReportDate { get; set; }
        [Display(Name = "Description")]
        [Required]
        public string Description { get; set; }
        [Display(Name = "Hide MyName")]
        public bool HideMyName { get; set; }
        [Display(Name = "Self Reporting")]
        public bool SelfReporting { get; set; }
        [Display(Name = "Complete")]
        public Nullable<int> Complete { get; set; }
        [Display(Name = "Status")]
        public string status { get; set; }
        public Nullable<int> CreateByEmpId { get; set; }
        [Display(Name = "Create By")]
        public string CreatorName { get; set; }
        public Nullable<bool> isClosed { get; set; }
        [Display(Name = "Location Of Incident")]
        [Required]
        public Nullable<int> LId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
        public string InciImages { get; set; }
        public HttpPostedFileBase ImagesFile { get; set; }
     
    }
    public class IncidentViewModel
    {
        [Display(Name = "Id")]
        public int IncidentId { get; set; }
        [Display(Name = "Occurrence Date")]
        [Required]
        public DateTime OccurrenceDate { get; set; }
        [Display(Name = "Discover Date")]
        [Required]
        public DateTime DiscoverDate { get; set; }
        [Display(Name = "Report Date")]
        [Required]
        public DateTime ReportDate { get; set; }
        [Display(Name = "Description")]
        [Required]
        public string Description { get; set; }
        [Display(Name = "Hide MyName")]
        public Nullable<bool> HideMyName { get; set; }
        [Display(Name = "Self Reporting")]
        public Nullable<bool> SelfReporting { get; set; }
        [Display(Name = "Complete")]
        public Nullable<int> Complete { get; set; }
        [Display(Name = "Status")]
        public string status { get; set; }
        public Nullable<int> CreateByEmpId { get; set; }
        [Display(Name = "Create By")]
        public string CreatorName { get; set; }
        public Nullable<bool> isClosed { get; set; }
        [Display(Name = "Location Of Incident")]
        [Required]
        public int LId { get; set; }
        public string LNameAr { get; set; }
        public string LNameEn { get; set; }
        public Nullable<bool> Visible { get; set; }
        public int OId { get; set; }
        public string InciImages { get; set; }
        public HttpPostedFileBase ImagesFile { get; set; }
     
    }
}
