﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public  class InvestigatorModel
    {
        [Display(Name ="Id")]
        public int InvId { get; set; }
        [Display(Name = "Actions")]
        [Required]
        public string Actions { get; set; }
        [Required]
        [Display(Name = "Recement")]
        public string Recement { get; set; }
        [Display(Name = "investigation Date")]
        public Nullable<System.DateTime> InvestigationDate { get; set; }
        public Nullable<int> assId { get; set; }
        public Nullable<int> IncidentId { get; set; }
        public Nullable<int> CFId { get; set; }
        public Nullable<int> Oid { get; set; }
    }

    public class InvestigatorViewModel
    {
        [Display(Name = "Id")]
        public int InvId { get; set; }
        [Display(Name = "Actions")]
        [Required]
        public string Actions { get; set; }
        [Required]
        [Display(Name = "Recement")]
        public string Recement { get; set; }
        [Display(Name = "investigation Date")]
        public Nullable<System.DateTime> InvestigationDate { get; set; }
        public Nullable<int> assId { get; set; }
        public Nullable<int> IncidentId { get; set; }
        public Nullable<int> CFId { get; set; }
        public Nullable<int> Oid { get; set; }
        public string CFNameEn { get; set; }
        public string CFNameAr { get; set; }
    }
}
