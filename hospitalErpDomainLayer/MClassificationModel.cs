﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
   public class MClassificationModel
    {
        public int id { get; set; }
        public string NameArabic { get; set; }
        public string NameEnglish { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }

    public class MClassificationViewModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        [Display(Name = "Name Arabic")]
        public string NameArabic { get; set; }
        [Display(Name = "Name English")]
        public string NameEnglish { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
        [Display(Name = "Org Name Arabic")]
        public string ONameArabic { get; set; }
        [Display(Name = "Org Name English")]
        public string ONameEnglish { get; set; }

    }
}
