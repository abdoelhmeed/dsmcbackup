﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpDomainLayer
{
    public  class IncidentAnalystToCloseModel
     {
        public int ARId { get; set; }
        [Required]
        public string Comment { get; set; }
        public bool Near { get; set; }
        public bool Valid { get; set; }
        public bool SentinelEvent { get; set; }
        public bool Miss { get; set; }
        public bool DOfUE_O { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<int> IncidentId { get; set; }

        public Nullable<int> RPMId { get; set; }
        public Nullable<int> RSSId { get; set; }
        public Nullable<int> DHId { get; set; }
        public Nullable<System.DateTime> CreatDate { get; set; }
        public Nullable<int> CreateByempId { get; set; }
     }
    public class AnalystToCloseViewModel
    {
        [Display(Name = "Id")]
        public int IncidentId { get; set; }
        [Display(Name = "Occurrence Date")]
        public DateTime OccurrenceDate { get; set; }
        [Display(Name = "Discover Date")]
        public DateTime DiscoverDate { get; set; }
        [Display(Name = "Report Date")]
        public DateTime ReportDate { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "status")]
        public string status { get; set; }
        [Display(Name = "Id")]
        public int InvestigId { get; set; }
        [Display(Name = "AssingId")]
        public Nullable<int> assId { get; set; }

        public Nullable<int> RPMId { get; set; }
        public Nullable<int> RSSId { get; set; }
    }

    public class ColseReport 
    {
        public string DegreeHarmNameAr { get; set; }
        public string DegreeHarmNameEn { get; set; }


        public string NameRiskProbabilityAR { get; set; }
        public string NameRiskProbabilityEn { get; set; }


        public string NameRiskSeventyAR { get; set; }
        public string NameRiskSeventyEn { get; set; }
        public Nullable<int> RiskRating { get; set; }


        public string Comment { get; set; }
        public Nullable<System.DateTime> CreatDate { get; set; }
    }

}
