﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
  public  class AsstesEmployeeModel
  {
        public int id { get; set; }
        public Nullable<int> AsId { get; set; }
        public Nullable<System.DateTime> AsEmpReceivedDate { get; set; }
        public Nullable<int> AsEmpId { get; set; }
        public Nullable<System.DateTime> AsempEntryDate { get; set; }
        public Nullable<System.DateTime> AsempMaintenanceDateReq { get; set; }
        public string StatusConsumption { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<int> NotificationBefore { get; set; }

    }

    public class AsstesEmployeeViewModel
    {
        public int id { get; set; }
        public int AsId { get; set; }
        public string AsName { get; set; }
        public string AsDescription { get; set; }
        public DateTime AsEmpReceivedDate { get; set; }
        public string strAsEmpReceivedDate { get; set; }
        public int AsEmpId { get; set; }
        public string EmpNameEnglis { get; set; }
        public string EmpNameArabic { get; set; }
        public DateTime AsempEntryDate { get; set; }
        public DateTime AsempMaintenanceDateReq { get; set; }
        public string strAsempEntryDate { get; set; }
        public string strAsempMaintenanceDateReq { get; set; }
        public string StatusConsumption { get; set; }
        public bool Visible { get; set; }

    }
}
