﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public  class IncidentRiskProbabilityModel
    {
        [Display(Name ="Id")]
        public int RPMId { get; set; }
        [Display(Name = "Arabic Name")]
        public string RPMNameAr { get; set; }
        [Display(Name = "English Name")]
        public string RPMINameEn { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }
}
