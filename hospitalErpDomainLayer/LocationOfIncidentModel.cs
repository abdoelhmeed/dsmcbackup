﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public  class LocationOfIncidentModel
    {
        [Display(Name ="Id")]
        public int LId { get; set; }
        [Display(Name = "L Name Arabic")]
        public string LNAr { get; set; }
        [Display(Name = "L Name English")]
        public string LNEn { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }
}
