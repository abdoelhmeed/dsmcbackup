﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace hospitalErpDomainLayer
{
 public class EmployeeModel
    {

        [Required]
        [Display(Name ="Number")]
        public int empid { get; set; }
        [Required]
        [Display(Name = "Farst Name Arabic")]
        public string empFNameAr { get; set; }
        [Required]
        [Display(Name = "second  Name Arabic")]
        public string empSNameAr { get; set; }
        [Required]
        [Display(Name = "Third   Name Arabic")]
        public string empTNameAr { get; set; }
        [Required]
        [Display(Name = "Farst Name English")]
        public string empFNameEn { get; set; }
        [Required]
        [Display(Name = "Second Name English")]
        public string empSNameEn { get; set; }
        [Required]
        [Display(Name = "Third Name English")]
        public string empTNameEn { get; set; }
        public string empImg { get; set; }
        public HttpPostedFileBase empImgFile { get; set; }
        [Required]
        [Display(Name = "Family English")]
        public string empFamilyNameEn { get; set; }
        [Required]
        [Display(Name = "Family Arabic")]
        public string empFamilyNameAr { get; set; }
        [Required]
        [Display(Name = "Nick Name")]
        public string empNickName { get; set; }
        [Required]
        [Display(Name = "Title")]
        public int? empTitle { get; set; }
        [Required]
        [Display(Name = "Gender")]
        public int? empGender { get; set; }
        [Required]
        [Display(Name = "Nationality")]
        public int? empNationality { get; set; }
        [Required]
        [Display(Name = "Religion")]
        public int? empReligion { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Birth Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime empBirthDate { get; set; }
        [Required]
        [Display(Name = "Birth Place")]
        public string empBirthblace { get; set; }
        [Required]
        [Display(Name = "Blood Group")]
        public string empBloodGroup { get; set; }
        [Required]
        [Display(Name = "Marital Status")]
        public int? empMaritalStatus { get; set; }
        [Required]
        [Display(Name = "Career Level")]
        public int? empCareerLevel { get; set; }
        [Required]
        [Display(Name = "Activation Status")]
        public bool empActivationStatus { get; set; }
        public string UserId { get; set; }
        public int? OId { get; set; }
    }
  public class EmployeeViewModel
    {
        [Display(Name = "Number")]
        public int? empid { get; set; }
        [Display(Name = "Farst Name Arabic")]
        public string empFNameAr { get; set; }
        [Display(Name = "second  Name Arabic")]
        public string empSNameAr { get; set; }
        [Display(Name = "Third   Name Arabic")]
        public string empTNameAr { get; set; }
        [Display(Name = "Farst Name English")]
        public string empFNameEn { get; set; }
        [Display(Name = "Second Name English")]
        public string empSNameEn { get; set; }
        [Display(Name = "Third Name English")]
        public string empTNameEn { get; set; }
        [Display(Name = "Picture")]
        public string empImg { get; set; }
        [Display(Name = "Family Name English")]
        public string empFamilyNameEn { get; set; }
        [Display(Name = "Family Name Arabic")]
        public string empFamilyNameAr { get; set; }
        [Display(Name = "Nick Name")]
        public string empNickName { get; set; }
        [Display(Name = "Title Id")]
        public int? empTitle { get; set; }
        [Display(Name = "Title Name")]
        public string NameTitleAr { get; set; }
        public string NameTitleEn { get; set; }
        [Display(Name = "Gender Id")]
        public int? empGender { get; set; }
        [Display(Name = "Gender")]
        public string NameGenderEn { get; set; }
        public string NameGenderAr { get; set; }
        [Display(Name = "Nationality Id")]
        public int? empNationality { get; set; }
        [Display(Name = "Nationality ")]
        public string NameNationalityAr { get; set; }
        public string NameNationalityEn { get; set; }
        [Display(Name = "Religion Id")]
        public int? empReligion { get; set; }
        [Display(Name = "Religion")]
        public string NameReligionEn { get; set; }
        public string NameReligionAr { get; set; }
        [Display(Name = "Birth Date")]
        public DateTime? empBirthDate { get; set; }
        [Display(Name = "Birth Place")]
        public string empBirthblace { get; set; }
        [Display(Name = "Blood Group")]
        public string empBloodGroup { get; set; }
        [Display(Name = "MaritalStatus Id")]
        public int? empMaritalStatus { get; set; }
        [Display(Name = "MaritalStatus")]
        public string NameMaritalStatusAr { get; set; }
        public string NameMaritalStatusEn { get; set; }
        [Display(Name = "Career Level Id")]
        public int? empCareerLevel { get; set; }
        [Display(Name = "Career Level ")]
        public string NameCareerLevelAr { get; set; }
        public string NameCareerLevelEn { get; set; }
        [Display(Name = "Activation Status")]
        public bool? empActivationStatus { get; set; }
        public string UserId { get; set; }
        public int? OId { get; set; }
    }
  public class EmployeeSelectionModel
  {
        public int? empid { get; set; }
        public string NameArabic { get; set; }
        public string NameEnglish { get; set; }
        public string CareerLevelEnglish { get; set; }
        public string CareerLevelArabic { get; set; }
        public int? OId { get; set; }

    }
}
