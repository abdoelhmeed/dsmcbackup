﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpDomainLayer
{
    public class EmployeeRequestsNodesModel
    {
        public int id { get; set; }
        public Nullable<int> empId { get; set; }
        public Nullable<int> requestsId { get; set; }
        public string nodes { get; set; }
        public string Status { get; set; }
        public Nullable<bool> Approve { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> Arrangement { get; set; }
    }
    public   class EmployeeRequestsNodesViewModel
    {
        public int id { get; set; }
        public Nullable<int> empId { get; set; }
        public string empNameEn { get; set; }
        public string empNameAr { get; set; }
        public Nullable<int> requestsId { get; set; }
        public string requestDetails { get; set; }
        public string nodes { get; set; }
        public string Status { get; set; }
        public Nullable<bool> Approve { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> Arrangement { get; set; }

    }
}
