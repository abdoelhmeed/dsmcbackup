﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace hospitalErpDomainLayer
{
  public  class OrganizationLicensesModel
    {
        public int OLid { get; set; }
        public int OLName { get; set; }
        public int OLPlace { get; set; }
        public System.DateTime OLStartDate { get; set; }
        public System.DateTime OLEndDate { get; set; }
        public System.DateTime OLEnterDate { get; set; }
        public bool OLValidity { get; set; }
        public string OLimg { get; set; }
        public int OId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public HttpPostedFileBase OLimgFile { get; set; }

    }
  public class OrganizationLicensesViewsModel
    {
        [Display(Name ="OLId")]
        public int OLid { get; set; }
        [Display(Name = "OLIdName")]
        public int OLName { get; set; }
        [Display(Name = "OLIdPlace")]
        public int OLPlace { get; set; }
        [Display(Name = "Name Arabic")]
        public string OLNameArabic { get; set; }
        [Display(Name = "Place Arabic")]
        public string OLPlaceArabic { get; set; }
        [Display(Name = "Name English")]
        public string OLNameEnglish { get; set; }
        [Display(Name = "Place English")]
        public string OLPlaceEnglish { get; set; }
        [Display(Name = "Start Date")]
        public System.DateTime OLStartDate { get; set; }
        [Display(Name = "End Date")]
        public System.DateTime OLEndDate { get; set; }
        [Display(Name = "Enter Date")]
        public System.DateTime OLEnterDate { get; set; }


        public string strOLStartDate { get; set; }
        public string strOLEndDate { get; set; }
        public string strOLEnterDate { get; set; }

        public bool OLValidity { get; set; }
        public string OLimg { get; set; }
        public int OId { get; set; }
        public string OName { get; set; }
        public Nullable<bool> Visible { get; set; }

    }
}
