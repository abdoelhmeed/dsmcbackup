﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
  public  class employeeDepartmentManagmentModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        public Nullable<int> empid { get; set; }
        [Display(Name = " Department")]
        public Nullable<int> DepartmentId { get; set; }
        [Display(Name = " Managment")]
        public Nullable<int> ManagmentId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }


    public class employeeDepartmentManagmentViewModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        [Display(Name ="Employee Id")]
        public Nullable<int> empid { get; set; }
        [Display(Name = "Employee Name")]
        public string empNameArabic { get; set; }
        [Display(Name = "Employee Name")]
        public string empNameEnglish { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        [Display(Name = "Department Name Arabic")]
        public string DeparNameArabic { get; set; }
        [Display(Name = "Department Name English")]
        public string DeparNameEnglish { get; set; }
        [Display(Name = "Managment Name Arabic")]
        public string ManagArabic { get; set; }
        [Display(Name = "Managment Name English")]
        public string ManagEnglish { get; set; }
        public Nullable<int> ManagmentId { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<bool> Visible { get; set; }

    }

    public class employeePositionModel
    {
        [Display(Name = "Number")]
        public int empid { get; set; }


        [Display(Name = "Name Arabic")]
        public string empNameAr { get; set; }


        [Display(Name = "Name Emglish")]
        public string empNameEn { get; set; }

        [Display(Name = "Picture")]
        public string empImg { get; set; }
        
       
      
      
        [Display(Name = "Title Name")]
        public string NameTitleEn { get; set; }

      
        [Display(Name = "Gender")]
        public string NameGenderEn { get; set; }

       
        [Display(Name = "Nationality ")]
        public string NameNationalityEn { get; set; }
      
        [Display(Name = "Religion")]
        public string NameReligionEn { get; set; }

        [Display(Name = "Birth Place")]
        public string empBirthblace { get; set; }


        [Display(Name = "Marital Status")]
        public string NameMaritalStatusEn { get; set; }

        [Display(Name = "Career Level ")]
        public string NameCareerLevelEn { get; set; }








        [Display(Name = "Title Name")]
        public string NameTitleAr { get; set; }


        [Display(Name = "Gender")]
        public string NameGenderAr { get; set; }


        [Display(Name = "Nationality ")]
        public string NameNationalityAr { get; set; }

        [Display(Name = "Religion")]
        public string NameReligionAr { get; set; }

       

        [Display(Name = "Marital Status")]
        public string NameMaritalStatusAr { get; set; }

        [Display(Name = "Career Level ")]
        public string NameCareerLevelAr { get; set; }





        [Display(Name = "Birth Date")]
        public DateTime empBirthDate { get; set; }


        
        [Display(Name = "Blood Group")]
        public string empBloodGroup { get; set; }

        [Display(Name = "Department Name Arabic")]
        public string DeparNameArabic { get; set; }

        [Display(Name = "Department Name English")]
        public string DeparNameEnglish { get; set; }

        [Display(Name = "Managment Name Arabic")]
        public string ManagArabic { get; set; }

        [Display(Name = "Managment Name English")]
        public string ManagEnglish { get; set; }

        public bool empActivationStatus { get; set; }
        public string UserId { get; set; }
       
        public int OId { get; set; }

    }
}
