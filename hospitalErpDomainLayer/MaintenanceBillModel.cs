﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;



namespace hospitalErpDomainLayer
{
   public class MaintenanceBillModel
    {
        public int id { get; set; }
        public Nullable<int> AssetsId { get; set; }
        public string AssetsName { get; set; }
        public Nullable<int> MaintenanceHandlingId { get; set; }
        public string MaintenanceDetails { get; set; }
        public string AspiratDetails { get; set; }
        public string InvoiceImage { get; set; }
        public Nullable<bool> GeneralDirector { get; set; }
        public HttpPostedFileBase InvoiceFill { get; set; }
        public string Nots { get; set; }
        public Nullable<double> TotalPrice { get; set; }
        public Nullable<bool> Financial { get; set; }
        public Nullable<bool> Visible { get; set; }
        public string MStuts { get; set; }
        public Nullable<System.DateTime> MAspiratDate { get; set; }

    }
   public class MaintenanceBillViewModel
    {
        [Display(Name ="ID")]
        public int id { get; set; }
        public Nullable<int> AssetsId { get; set; }
        [Display(Name = "Assets Name")]
        public string AssetsName { get; set; }
        public Nullable<int> MaintenanceHandlingId { get; set; }
        [Display(Name = "Maintenance Details")]
        public string MaintenanceDetails { get; set; }
        [Display(Name = "Aspirat Details")]
        public string AspiratDetails { get; set; }
        public string InvoiceImage { get; set; }
        public Nullable<bool> GeneralDirector { get; set; }
        [Display(Name = "Total Price")]
        public Nullable<double> TotalPrice { get; set; }
        [Display(Name = "Stuts")]
        public string MStuts { get; set; }
        public string Nots { get; set; }
        public Nullable<bool> Financial { get; set; }
        public Nullable<bool> Visible { get; set; }
        [Display(Name = "Date")]
        public Nullable<System.DateTime> MAspiratDate { get; set; }
        public string strMAspiratDate { get; set; }
    }
}
