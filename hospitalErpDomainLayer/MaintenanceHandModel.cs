﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
   public class MaintenanceHandModel
   {
        public int id { get; set; }
        public Nullable<int> OrderMaintenanceIId { get; set; }
        public Nullable<int> asstesId { get; set; }
        public string MaintenanceDetails { get; set; }
        public string AspiratDetails { get; set; }
        public Nullable<System.DateTime> MaintenanceStartDate { get; set; }
        public string dMaintenanceStartDate { get; set; }
        public Nullable<System.DateTime> MaintenanceEndDate { get; set; }
        public Nullable<bool> Finished { get; set; }
        public string MStuts { get; set; }
        public Nullable<bool> Visible { get; set; }
   }
   public class MaintenanceHandViewModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        [Display(Name = "Order Id")]
        public Nullable<int> OrderMaintenanceIId { get; set; }
        [Display(Name = "Order Data")]
        public string OrderMaintenanceDate { get; set; }
        [Display(Name = "Emp Name")]
        public string OrderMaintenanceEmployeeEn { get; set; }
        [Display(Name = "Emp Name")]
        public string OrderMaintenanceEmployeeAr { get; set; }
        public Nullable<int> asstesId { get; set; }
        [Display(Name = "Asstes Name")]
        public string asstesName { get; set; }
        [Display(Name = " Maintenance Details")]
        public string MaintenanceDetails { get; set; }
        [Display(Name = " Aspirat Details")]
        public string AspiratDetails { get; set; }
        [Display(Name = " M Start Date")]
        public Nullable<System.DateTime> MaintenanceStartDate { get; set; }
        [Display(Name = " M End Date")]
        public Nullable<System.DateTime> MaintenanceEndDate { get; set; }
        public string MStuts { get; set; }
        public Nullable<bool> Finished { get; set; }
        public Nullable<bool> Visible { get; set; }
    }
}
