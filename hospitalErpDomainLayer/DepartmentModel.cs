﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
  public  class DepartmentModel
    {
        [Display(Name = "Id")]
        public int deptId { get; set; }
        [Display(Name = "Name Arabic")]
        public string deptNameAr { get; set; }
        [Display(Name = "Name English")]
        public string deptNameEn { get; set; }
        public Nullable<int> deptManager { get; set; }
        public Nullable<int> deptVicManager { get; set; }
        public Nullable<int> Mid { get; set; }
        public int OId { get; set; }

    }
  public  class DepartmentViewModel
    {
        [Display(Name = "Id")]
        public int deptId { get; set; }
        [Display(Name = "Name Arabic")]
        public string deptNameAr { get; set; }
        [Display(Name = "Name English")]
        public string deptNameEn { get; set; }
        public Nullable<int> deptManager { get; set; }
        public Nullable<int> deptVicManager { get; set; }
        [Display(Name = "Name Manager")]
        public string NameManager { get; set; }
        [Display(Name = "Name Vic Manager")]
        public string NameVicManager { get; set; }
        public Nullable<int> Mid { get; set; }
        [Display(Name = "Management")]
        public string NameMidArbic { get; set; }
        public string NameMidEnglis { get; set; }

        public string NameVicManagerAr { get; set; }
        public string NameManagerAr { get; set; }
    }
}
