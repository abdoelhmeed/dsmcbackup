﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpDomainLayer
{
   public class MaintenanceDetailsModel
   {
        public int HId { get; set; }
        [Display(Name = "OId")]
        public int OId { get; set; }
        public int BId { get; set; }
        public int assetsId { get; set; }
        public string AssetsName { get; set; }
        public string HMaintenanceDetails { get; set; }
        public string HAspiratDetails { get; set; }
        public Nullable<System.DateTime> HMaintenanceStartDate { get; set; }
        public Nullable<System.DateTime> HMaintenanceEndDate { get; set; }
        public string HStuts { get; set; }
        public Nullable<System.DateTime> OMaintenanceOrderDate { get; set; }
        [Display(Name = "Order Stuts")]
        public string OrderStuts { get; set; }
        [Display(Name = "Order Notes")]
        public string OrderNotes { get; set; }
        public string MaintenanceDetails { get; set; }
        public string AspiratDetails { get; set; }
        public string InvoiceImage { get; set; }
        public string Nots { get; set; }
        public Nullable<double> TotalPrice { get; set; }
        public string MStuts { get; set; }
        
    }
}
