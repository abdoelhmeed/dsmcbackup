﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace hospitalErpDomainLayer
{
    public class EmployeeLicensesModel
    {
        [Display(Name ="Id")]
        [Required]
        public int Lid { get; set; }
        [Display(Name = "Name")]
        [Required]
        public int LName { get; set; }
        [Display(Name = "Place")]
        [Required]
        public int LPlace { get; set; }
        [Display(Name = "Start Date")]
        [Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy}",ApplyFormatInEditMode = true)]
        public DateTime LStartDate { get; set; }
        [Display(Name = "End Date")]
        [Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy}",ApplyFormatInEditMode = true)]
        public DateTime LEndDate { get; set; }
        [Display(Name = "Validity")]
        [Required]
        public bool LValidity { get; set; }
        [Display(Name = "picture")]
        [Required]
        public string Limg { get; set; }
        public HttpPostedFileBase LimgFile { get; set; }
        public int empId { get; set; }
        public bool Visible { get; set; }
    }
    public class EmployeeLicensesViewsModel
    {
        [Display(Name = "Id")]
        [Required]
        public int Lid { get; set; }
        [Display(Name = "Name")]
        [Required]
        public int LName { get; set; }

        [Display(Name = "Name Arbic")]
        public string NameAr { get; set; }

        [Display(Name = "Name English")]
        public string NameEn { get; set; }


        [Display(Name = "Place")]
        public int LPlace { get; set; }

        [Display(Name = "Place Arbic")]
        public string PlaceAr { get; set; }
        [Display(Name = "Place English")]
        public string PlaceEn { get; set; }

        [Display(Name = "Start Date")]
        [Required]
        public DateTime LStartDate { get; set; }
        [Display(Name = "End Date")]
        [Required]
        public DateTime LEndDate { get; set; }
        [Display(Name = "Validity")]
        [Required]
        public bool LValidity { get; set; }
        [Display(Name = "picture")]
        [Required]
        public string Limg { get; set; }
        public int empId { get; set; }
        public string empNameAr { get; set; }
        public string empNameEn { get; set; }

        public bool Visible { get; set; }
    }

    public class customEmpLicenses
    {
        public int id { get; set; }
        public string NameEn { get; set; }
        public string PlaceEn { get; set; }
        public string NameAr { get; set; }
        public string PlaceAr { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Img { get; set; }


    }
}
