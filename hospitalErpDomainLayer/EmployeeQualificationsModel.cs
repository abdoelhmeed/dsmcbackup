﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace hospitalErpDomainLayer
{
    public class EmployeeQualificationsModel
    {
        public int Qid { get; set; }
        public int Qualifications { get; set; }
        public int QPlace { get; set; }
        public int QGrade { get; set; }
        public DateTime QDate { get; set; }
        public string QImg { get; set; }
        public HttpPostedFileBase QimgFile { get; set; }
        public int empId { get; set; }
        public bool Visible { get; set; }
    }

   public class EmployeeQualificationsViewModel
    {
        public int Qid { get; set; }
        public int Qualifications { get; set; }
        public int QPlace { get; set; }
        public string QNameArbic { get; set; }
        public string QNamePlaceArbic { get; set; }
        public string QNameEnglish { get; set; }
        public string QNamePlaceEnglish { get; set; }
        public int QGrade { get; set; }
        public DateTime QDate { get; set; }
        public string QImg { get; set; }
        public int empId { get; set; }
        public bool Visible { get; set; }
    }

    public class CustomEmployeeQualifications
    {
        public string QNameArbic { get; set; }
        public string QNamePlaceArbic { get; set; }
        public string QNameEnglish { get; set; }
        public string QNamePlaceEnglish { get; set; }
        public int Qid { get; set; }
        public string QDate { get; set; }
        public int QGrade { get; set; }
        public string QImg { get; set; }

    }

}
