﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
   public class NotificationsSettingMode
    {
        public int Id { get; set; }
        public int deptId { get; set; }
        public int empId { get; set; }
        public int Type { get; set; }
        public int OId { get; set; }
    }

    public class NotificationsViewSettingMode
    {
        [Display(Name ="Id")]
        public int Id { get; set; }
        [Display(Name = "Emp Id")]
        public int empId { get; set; }
        [Display(Name = "Department Name Arabic")]
        public string deptNameAr { get; set; }
        [Display(Name = "Department Name English")]
        public string deptNameEn { get; set; }
        [Display(Name = "Employees  Name English")]
        public string empNameEn { get; set; }
        [Display(Name = "Employees  Name Arabic")]
        public string empNameAr { get; set; }
        public int Type { get; set; }
        public int OId { get; set; }
    }
}
