﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpDomainLayer
{
    public  class AsstesDepartmentModel
    {
        public int id { get; set; }
        public Nullable<int> AsId { get; set; }
        public Nullable<System.DateTime> DeptReceivedDate { get; set; }
        public Nullable<int> DeptId { get; set; }
        public Nullable<System.DateTime> DeptEntryDate { get; set; }
        public Nullable<System.DateTime> DeptMaintenanceDateReq { get; set; }
        public string StatusConsumption { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> NotificationBefore { get; set; }
        public Nullable<int> ManagementId { get; set; }



    }

    public class AsstesDepartmentViewModel
    {
        public int id { get; set; }
        public Nullable<int> AsId { get; set; }
        public string AsName { get; set; }
        public Nullable<System.DateTime> DeptReceivedDate { get; set; }
        public Nullable<int> DeptId { get; set; }
        public string DeptNameEnglish { get; set; }
        public string DeptNameArabic { get; set; }
        public string MagaNameArabic { get; set; }
        public string MagaNameEnglish { get; set; }
        public Nullable<System.DateTime> DeptEntryDate { get; set; }
        public Nullable<System.DateTime> DeptMaintenanceDateReq { get; set; }
        public string StatusConsumption { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> NotificationBefore { get; set; }

    }
}
