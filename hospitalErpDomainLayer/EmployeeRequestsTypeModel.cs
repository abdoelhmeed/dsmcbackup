﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace hospitalErpDomainLayer
{
    public  class EmployeeRequestsTypeModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        [Display(Name = "Request Name Arabic")]
        public string requestNameArabic { get; set; }
        [Display(Name = "Request Name Arabic")]
        public string requestNameEnglish { get; set; }
        [Display(Name = "DepartmentId")]
        public Nullable<int> DepartmentId { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<bool> Visible { get; set; }
    }


    public class EmployeeRequestsTypeViewModel
    {
        [Display(Name = "Id")]
        public int id { get; set; }
        [Display(Name = "Request Name Arabic")]
        public string requestNameArabic { get; set; }
        [Display(Name = "Request Name Arabic")]
        public string requestNameEnglish { get; set; }
        [Display(Name = "DepartmentId")]
        public Nullable<int> DepartmentId { get; set; }
        [Display(Name = "Department Name")]
        public string DepartmentNameAr { get; set; }
        public string DepartmentNameEn { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<bool> Visible { get; set; }
    }
}
