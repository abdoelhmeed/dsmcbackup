﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace hospitalErpDomainLayer
{
   public class PoliciesModel
   {
        [Required]
        [Display(Name = "Policy Numbers")]
        public string policiesNumbers { get; set; }
        [Required]
        [Display(Name = "Policy Title")]
        public string polTitle { get; set; }
        [Required]
        [Display(Name = "Policy Body")]
        public string PolBody { get; set; }
        [Display(Name = "Policy Pdf")]
        public string PolPdf { get; set; }
        [Display(Name = "Policy Pdf")]
        public HttpPostedFileBase PolFile { get; set; }
        [Display(Name = "Policy Effective Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> PolEffectiveDate { get; set; }
        [Display(Name = "Policy Effective Date")]
        public string strPolEffectiveDate { get; set; }
        public Nullable<int> OId { get; set; }
        [Display(Name = "Policy Status")]
        public string Status { get; set; }
        public Nullable<System.DateTime> EnterDate { get; set; }
        [Display(Name = "Department")]
        public List<int> DeptId { get; set; }

        public int TId { get; set; }

        [Display(Name = "Policy For")]
        public string Type { get; set; }
        public Nullable<int> CreateByEmpId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<bool> isPublish { get; set; }
    }


    public class PoliciesUpdateModel
    {
        [Display(Name = "Policy Numbers")]
        [Required]
        public string policiesNumbers { get; set; }
        [Required]
        [Display(Name = "Policy Title")]
        public string polTitle { get; set; }
       
        [Display(Name = "Policy Body")]
        public string PolBody { get; set; }
        [Display(Name = "Policy Pdf")]
        public string PolPdf { get; set; }
        [Required]
        [Display(Name = "Policy Effective Date")]
        public Nullable<System.DateTime> PolEffectiveDate { get; set; }
        [Display(Name = "Policy Pdf")]
        public HttpPostedFileBase PolFile { get; set; }
    }
    public class PoliciesViewModel
    {
        [Display(Name = "Policy Numbers")]
        public string policiesNumbers { get; set; }
        [Display(Name = "Policy Title")]
        public string polTitle { get; set; }
        [Display(Name = "Policy Body")]
        public string PolBody { get; set; }
        [Display(Name = "Policy Pdf")]
        public string PolPdf { get; set; }
        [Display(Name = "Policy Effective Date")]
        public Nullable<System.DateTime> PolEffectiveDate { get; set; }
        public Nullable<int> OId { get; set; }
        [Display(Name = "Policy Status")]
        public string Status { get; set; }
        [Display(Name = "Policy Enter Date")]
        public Nullable<System.DateTime> EnterDate { get; set; }
        [Display(Name = "Policy Type")]
        public string polTypeAr { get; set; }
        [Display(Name = "Policy Type")]
        public string polTypeEn { get; set; }
        public Nullable<int> TId { get; set; }
        public Nullable<int> CreateByEmpId { get; set; }
        [Display(Name = "Policy Created By")]
        public string CreateByEmpNameEn { get; set; }
        [Display(Name = "Policy Created By")]
        public string CreateByEmpNameAr { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<bool> isPublish { get; set; }
    }
}
