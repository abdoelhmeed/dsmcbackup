﻿namespace hospitalErpDomainLayer
{
    // for chart 
    // get Incident for Current Years By RiskProbability
    public class GetCharIncidentByRiskProbabilityModel
    {
        public string RPMName { get; set; }
        public int RPMNumber { get; set; }
    }

}
