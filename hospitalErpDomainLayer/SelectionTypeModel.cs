﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
   public class SelectionTypeModel
   {
        [Display(Name ="ID")]
        public int id { get; set; }
        [Required]
        [Display(Name = "Arabic Name")]
        public string ArabicName { get; set; }
        [Required]
        [Display(Name = "English Name")]
        public string EnglishName { get; set; }
    }
}
