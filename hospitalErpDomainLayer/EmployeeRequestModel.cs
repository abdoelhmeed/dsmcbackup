﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
  public  class EmployeeRequestModel
  {
        public int requestId { get; set; }
        public Nullable<int> empId { get; set; }
        public Nullable<int> requestTypeId { get; set; }
        public string requestDetails { get; set; }
        public string Status { get; set; }
        public Nullable<int> Oid { get; set; }
        public Nullable<bool> Visible { get; set; }

    }
  public class EmployeeRequestViweModel
    {
        [Display(Name ="Id")]
        public int requestId { get; set; }
        public Nullable<int> empId { get; set; }
        [Display(Name = "Emp Name Arabic")]
        public string empNameAr { get; set; }
        [Display(Name = "Emp Name English")]
        public string empNameEn { get; set; }
        [Display(Name = "Type Id")]
        public Nullable<int> requestTypeId { get; set; }
        public string requestTypeNameAr { get; set; }
        [Display(Name = "Type Name")]
        public string requestTypeNameEn { get; set; }
        [Display(Name = "Request Details")]
        public string requestDetails { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
        public Nullable<int> Oid { get; set; }
        public Nullable<bool> Visible { get; set; }

    }
}
