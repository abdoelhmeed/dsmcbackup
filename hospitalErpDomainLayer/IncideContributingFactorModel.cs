﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public class IncideContributingFactorModel
    {
        [Display(Name ="CFId")]
        public int CFId { get; set; }
        [Display(Name = "CF Arabic Name" )] 
        public string CFNameAr { get; set; }
        [Display(Name = "CF English Name")]
        public string CFNameEn { get; set; }
        public Nullable<int> OId { get; set; }
    }
}
