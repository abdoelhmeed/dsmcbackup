﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
   public class SelectionDetailsModel
    {
        [Display(Name ="ID")]
        public int id { get; set; }
        [Display(Name = "Arabic Name")]
        [Required]
        public string selNameArabic { get; set; }
        [Display(Name = "English Name")]
        [Required]
        public string selNameEnglish { get; set; }
        [Display(Name = "Selection Type")]
        public int sTId { get; set; }
    }
}
