﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public class IncidentAssignModel
    {
        public int assId { get; set; }
        public Nullable<int> assDeptResDept { get; set; }
        public Nullable<int> IMTId { get; set; }
        public Nullable<int> ISTId { get; set; }
        public Nullable<int> IncidentDeptId { get; set; }
        public Nullable<int> investigatorEmpId { get; set; }
        public string Comment { get; set; }
        public Nullable<int> IncidentId { get; set; }
        public Nullable<int> CreateByEmpId { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public string status { get; set; }
    }

    public class IncidentAssignViewModel
    {
        [Display(Name ="Assing Id")]
        public int assId { get; set; }
        public Nullable<int> assDeptResDept { get; set; }
        [Display(Name = "D N English Responsible")]
        public string assDeptResDeptNameAr { get; set; }
        [Display(Name = "D N Arabic Responsible")]
        public string assDeptResDeptNameEn { get; set; }

        public Nullable<int> IMTId { get; set; }
        public Nullable<int> ISTId { get; set; }
        [Display(Name = " English Type")]
        public string IMTNameEn { get; set; }
        [Display(Name = " English Sub Type")]
        public string ISTNameEn { get; set; }
        [Display(Name = " Arabic Type")]
        public string IMTNameAr { get; set; }
        [Display(Name = " Arabic Sub Type")]
        public string ISTNameAr { get; set; }
        public Nullable<int> IncidentDeptId { get; set; }
        [Display(Name = "D N Arabic investigation ")]
        public string  IncidentDeptNameAr { get; set; }
        [Display(Name = "D N English investigation ")]
        public string IncidentDeptNameEn { get; set; }

        public Nullable<int> investigatorEmpId { get; set; }
        [Display(Name = "investigator Name English")]
        public string investigatorEmpIdNameEn { get; set; }

        [Display(Name = "investigator Name Arabic")]
        public string investigatorEmpIdNameAr { get; set; }

        public string Comment { get; set; }
        public Nullable<int> IncidentId { get; set; }
        public Nullable<int> CreateByEmpId { get; set; }
        [Display(Name = "Create By Arabic ")]
        public string CreateByAr { get; set; }
        [Display(Name = "Create By English ")]
        public string CreateByEn { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<bool> Visible { get; set; }
    }

    public class IncidentAssignForViewModel
    {
        public int assId { get; set; }
        public Nullable<int> IncidentId { get; set; }
        [Display(Name = "Discover Date")]
        public DateTime OccurrenceDate { get; set; }
        [Display(Name = "Discover Date")]
        public DateTime DiscoverDate { get; set; }
        [Display(Name = "Report Date")]
        public Nullable<System.DateTime> ReportDate { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Hide MyName")]
        public bool HideMyName { get; set; }
        [Display(Name = "Status")]
        public string status { get; set; }
    }
}
