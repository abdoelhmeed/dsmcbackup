﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
  public  class PolicyApprovModel
  {
        public int id { get; set; }
        public string policiesNumbers { get; set; }
        public Nullable<int> empId { get; set; }
        public Nullable<bool> Approv { get; set; }
        public Nullable<int> empgroup { get; set; }
        public string Status { get; set; }
  }


    public class PolicyApproveViewModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        [Display(Name = "policy Number")]
        public string policiesNumbers { get; set; }
        public Nullable<int> empId { get; set; }
        [Display(Name = " AR Name")]
        public string empNameAr { get; set; }
        [Display(Name = " EN Name")]
        public string empNameEn { get; set; }
        [Display(Name = " Approve ")]
        public bool Approv { get; set; }
        [Display(Name = " Approve Arrangement")]
        public int empgroup { get; set; }
        public string Status { get; set; }
    }

    public class PolicyForApproveViewModel
    {
        [Display(Name = "Id")]
        public int id { get; set; }
        [Display(Name = "policyTitel")]
        public string Titel { get; set; }
        [Display(Name = "policy Number")]
        public string policiesNumbers { get; set; }
        public Nullable<int> empId { get; set; }
        [Display(Name = " AR Name")]
        public string empNameAr { get; set; }
        [Display(Name = " EN Name")]
        public string empNameEn { get; set; }
        [Display(Name = " Approve ")]
        public bool Approv { get; set; }
        [Display(Name = " Approve Arrangement")]
        public int empgroup { get; set; }
        public string Status { get; set; }
        [Display(Name = "Policy Pdf")]
        public string PolPdf { get; set; }
        [Display(Name = "Policy Effective Date")]
        public DateTime PolEffectiveDate { get; set; }
       
    }
}
