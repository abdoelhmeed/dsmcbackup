﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
   public class OrganizationModel
   {
        [Display(Name ="Id")]
        public int OId { get; set; }
        [Display(Name = "Name English")]
        public string ONameEn { get; set; }
        [Display(Name = "Name Arabic")]
        public string ONameAr { get; set; }
        [Display(Name = "Address")]
        public string OAddress { get; set; }
        [Display(Name = "Phone Number")]
        public string OPhoneNumbers { get; set; }
        [Display(Name = "Support Number")]
        public string OSupportNumbers { get; set; }
        [Display(Name = "Email")]
        public string OEmail { get; set; }
        [Display(Name = "Manager")]
        public Nullable<int> OManager { get; set; }
        [Display(Name = "VicManager")]
        public Nullable<int> OVicManager { get; set; }
        
    }

    public class OrganizationViewModel
    {
        [Display(Name = "Id")]
        public int OId { get; set; }
        [Display(Name = "Name English")]
        public string ONameEn { get; set; }
        [Display(Name = "Name Arabic")]
        public string ONameAr { get; set; }
        [Display(Name = "Address")]
        public string OAddress { get; set; }
        [Display(Name = "Phone Number")]
        public string OPhoneNumbers { get; set; }
        [Display(Name = "Support Number")]
        public string OSupportNumbers { get; set; }
        [Display(Name = "Email")]
        public string OEmail { get; set; }
        [Display(Name = "Manager")]
        public string NameOManager { get; set; }
        [Display(Name = "VicManager")]
        public string NameOVicManager { get; set; }
        public Nullable<int> OVicManager { get; set; }
        public Nullable<int> OManager { get; set; }

    }
}
