﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public class IncidentDegreeOfHarmModel
    {
        [Display(Name ="Id")]
        public int DHId { get; set; }
        [Display(Name = "Arabic Degree Harm")]
        public string DHNameAr { get; set; }
        [Display(Name = "English Degree Harm")]
        public string DHNameEn { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<bool> Visible { get; set; }
    }
}
