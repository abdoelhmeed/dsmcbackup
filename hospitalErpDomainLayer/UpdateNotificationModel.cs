﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpDomainLayer
{
   public class UpdateNotificationModel
    {
        public int id { get; set; }
        public System.DateTime UpdatesNotification { get; set; }
    }
}
