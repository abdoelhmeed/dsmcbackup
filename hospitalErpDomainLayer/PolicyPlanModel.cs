﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpDomainLayer
{
  public  class PolicyPlanModel
  {
        public int id { get; set; }
        public Nullable<int> TId { get; set; }
        public Nullable<int> Mid { get; set; }
        public Nullable<int> Arrangement { get; set; }
  }


    public class PolicyPlanViweModel
    {
        public int id { get; set; }
        public Nullable<int> TId { get; set; }
        public Nullable<int> Mid { get; set; }
        public string MNameAr { get; set; }
        public string MNameEn { get; set; }
        public Nullable<int> Arrangement { get; set; }
    }
}
