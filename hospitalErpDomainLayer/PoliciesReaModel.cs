﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpDomainLayer
{
   public class PoliciesReaModel
   {
        public int id { get; set; }
        public string policiesNumbers { get; set; }
        public Nullable<int> empId { get; set; }
        public Nullable<bool> isRead { get; set; }
        public Nullable<System.DateTime> RaedDate { get; set; }
    }



    public class PoliciesReaViewModel
    {
        public int id { get; set; }
        public string policiesNumbers { get; set; }
        public Nullable<int> empId { get; set; }
        public Nullable<bool> isRead { get; set; }
        public Nullable<System.DateTime> RaedDate { get; set; }



      
        [Display(Name = "Policy Title")]
        public string polTitle { get; set; }
        [Display(Name = "Policy Body")]
        public string PolBody { get; set; }
        [Display(Name = "Policy Pdf")]
        public string PolPdf { get; set; }
        [Display(Name = "Policy Effective Date")]
        public Nullable<System.DateTime> PolEffectiveDate { get; set; }
        public Nullable<int> OId { get; set; }
        [Display(Name = "Policy Status")]
        public string Status { get; set; }
        [Display(Name = "Policy Enter Date")]
        public Nullable<System.DateTime> EnterDate { get; set; }
        [Display(Name = "Policy Type")]
        public string polTypeAr { get; set; }
        [Display(Name = "Policy Type")]
        public string polTypeEn { get; set; }
        public Nullable<int> TId { get; set; }
        public Nullable<int> CreateByEmpId { get; set; }
        [Display(Name = "Policy Created By")]
        public string CreateByEmpNameEn { get; set; }
        [Display(Name = "Policy Created By")]
        public string CreateByEmpNameAr { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<bool> isPublish { get; set; }
        public string TImg { get; set; }

    }
}
