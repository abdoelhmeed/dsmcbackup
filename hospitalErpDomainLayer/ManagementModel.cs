﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public class ManagementModel
    {
        [Display(Name = "ID")]
        public int Mid { get; set; }
        [Display(Name = "Name Arabic")]
        public string MNameAr { get; set; }
        [Display(Name = "Name Englis")]
        public string MName { get; set; }
        [Display(Name = "Manager")]
        public Nullable<int> deptManager { get; set; }
        [Display(Name = "Vic Manager")]
        public Nullable<int> deptVicManager { get; set; }
        [Display(Name = "Organization")]
        public Nullable<int> OId { get; set; }
    }
    public class ManagementViewModel
    {
        [Display(Name = "ID")]
        public int Mid { get; set; }
        [Display(Name = "Name Arabic")]
        public string MNameAr { get; set; }
        [Display(Name = "Name English")]
        public string MName { get; set; }
        public Nullable<int> deptManager { get; set; }
        [Display(Name = "Manager")]
        public string NameManagerEnglish { get; set; }
        [Display(Name = "Manager")]
        public string NameManagerArabic { get; set; }
        [Display(Name = "Vic Manager")]
        public string NameVicManagerEnglish { get; set; }
        [Display(Name = "Vic Manager")]
        public string NameVicManagerArbic { get; set; }
        public Nullable<int> deptVicManager { get; set; }
        [Display(Name = "Organizationr")]
        public Nullable<int> OId { get; set; }
        [Display(Name = "Org Arabic Name")]
        public string ONameArabic { get; set; }
        [Display(Name = "Org English Name")]
        public string ONameEnglish { get; set; }
    }
    public class ManagementelectionModel
    {
        public int Mid { get; set; }
        public string MNameArabic { get; set; }
        public string MNameEnglish { get; set; }
        public Nullable<int> OId { get; set; }
    }

   
}
