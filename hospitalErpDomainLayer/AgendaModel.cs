﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace hospitalErpDomainLayer
{
  public  class AgendaModel
  {
        public int id { get; set; }
        public string Title { get; set; }
        public string nUrl { get; set; }
        public Nullable<System.DateTime> History { get; set; }

        public string Subject { get; set; }
        public Nullable<int> FromemployeeId { get; set; }
        public Nullable<int> ToEmployeeId { get; set; }
        public Nullable<int> Oid { get; set; }
        public Nullable<int> DepartmentId { get; set; }

        public Nullable<int> Type { get; set; }
        public Nullable<bool> isRead { get; set; }
    }
  public class AgendaViewModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        public string Title { get; set; }
        [Display(Name = "History")]
        public Nullable<System.DateTime> History { get; set; }
        public string strHistory { get; set; }
        [Display(Name = "Subject")]
        public string Subject { get; set; }
        public Nullable<int> FromemployeeId { get; set; }
        [Display(Name = "From")]
        public string EmpNameArabicFor { get; set; }
        [Display(Name = "From")]
        public string EmpNameEnglishFor { get; set; }
        public Nullable<int> ToEmployeeId { get; set; }
        [Display(Name = "TO")]
        public string EmplNameEnglishTo { get; set; }
        [Display(Name = "TO")]
        public string EmplNameArabicTo { get; set; }
        public Nullable<int> Oid { get; set; }
        public Nullable<bool> isRead { get; set; }
    }

    public class AgendaViewEmployeeModel
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string Subject { get; set; }
        public Nullable<int> FromemployeeId { get; set; }
    }

}
