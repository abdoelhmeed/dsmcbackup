﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
  public  class SClassificationModel
  {
        public int id { get; set; }
        public string NameArbic { get; set; }
        public string NameEnglish { get; set; }
        public Nullable<int> MainClassificationId { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }

    public class SClassificationViewModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        [Display(Name = "Name Arabic")]
        public string NameArbic { get; set; }
        [Display(Name = "Name English")]
        public string NameEnglish { get; set; }
        public Nullable<int> MainClassificationId { get; set; }
        [Display(Name = " Main Classification Arabic")]
        public string  MainNameClassificationArabic { get; set; }

        [Display(Name = " Main Classification English")]
        public string MainNameClassificationEnglish { get; set; }

        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }

    }
}
