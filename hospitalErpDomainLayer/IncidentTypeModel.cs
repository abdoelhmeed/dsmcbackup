﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public class IncidentMainTypeModel
    {
        [Display(Name ="Id")]
        public int IMTId { get; set; }
        [Display(Name ="Arabic Name")]
        public string IMTNameAr { get; set; }
        [Display(Name = "English Name")]
        public string IMTNameEr { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }

    public class IncidentSubTypeModel
    {
        [Display(Name = "Id")]
        public int ISTId { get; set; }
        [Display(Name = "Arabic Name")]
        public string ISTNameAr { get; set; }
        [Display(Name = "English Name")]
        public string ISTNameEn { get; set; }
        [Display(Name = "Main Type")]
        public Nullable<int> IMTId { get; set; }
        [Display(Name = "M Arabic Name")]
        public string IMTNameAr { get; set; }
        [Display(Name = "M English Name ")]
        public string IMTNameEn { get; set; }
        public Nullable<bool> Visible { get; set; }
        public Nullable<int> OId { get; set; }
    }
}
