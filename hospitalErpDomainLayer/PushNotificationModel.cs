﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpDomainLayer
{
   public class PushNotificationModel
   {
        public int id { get; set; }
        public string nTitel { get; set; }
        public string nBody { get; set; }
        public string nUrl { get; set; }
        public Nullable<bool> isRead { get; set; }
        public Nullable<System.DateTime> ReadDate { get; set; }
        public string UserId { get; set; }
        public Nullable<int> empId { get; set; }
        public string NType { get; set; }
    }
}
