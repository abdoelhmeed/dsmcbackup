﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
    public  class IncidentRiskSeventySubModel
    {
        [Display(Name ="Id")]
        public int RSSId { get; set; }
        [Display(Name = "Arabic Name")]
        public string RSSNameAr { get; set; }
        [Display(Name = "Arabic Name")]
        public string RSSNameEn { get; set; }
        public Nullable<int> RiskRating { get; set; }
        public Nullable<int> RPMId { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<bool> Visible { get; set; }
    }

    public class IncidentRiskSeventySubViewModel
    {
        [Display(Name = "Id")]
        public int RSSId { get; set; }
        [Display(Name = "Arabic Name")]
        public string RSSNameAr { get; set; }
        [Display(Name = "Arabic Name")]
        public string RSSNameEn { get; set; }
        [Display(Name = "Risk Rating")]
        public Nullable<int> RiskRating { get; set; }
        public Nullable<int> RPMId { get; set; }
        [Display(Name = "Risk Probability Arabic")]
        public  string RiskProbabilityArabic { get; set; }
        [Display(Name = "Risk Probability English")]
        public string RiskProbabilityEnglish { get; set; }
        public Nullable<int> OId { get; set; }
        public Nullable<bool> Visible { get; set; }
    }


    

}
