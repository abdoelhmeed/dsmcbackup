﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpDomainLayer
{
   public class PoliciesAppliesToModel
   {
        public int id { get; set; }
        public Nullable<int> DeptId { get; set; }
        public string policiesNumbers { get; set; }
    }

    public class PoliciesAppliesToViewModel
    {
        public int id { get; set; }
        public Nullable<int> DeptId { get; set; }
        public string DeptNameAr { get; set; }
        public string DeptNameEn { get; set; }
        public string policiesNumbers { get; set; }
        public string polTitel { get; set; }
    }
}
