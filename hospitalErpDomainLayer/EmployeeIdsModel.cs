﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace hospitalErpDomainLayer
{
   public class EmployeeIdsModel
    {
        public int id { get; set; }
        public string idNumber { get; set; }
        public int idType { get; set; }
        public System.DateTime idIssued { get; set; }
        public System.DateTime idexpiredDate { get; set; }
        public string idPlaceIssue { get; set; }
        public string idImg { get; set; }
        public HttpPostedFileBase idImgFile { get; set; }
        public int empId { get; set; }
        public Nullable<bool> Visible { get; set; }
    }


    public class EmployeeIdsViewsModel
    {
        public int id { get; set; }
        public string idNumber { get; set; }
        public int idType { get; set; }
        public string NameTypeAr { get; set; }
        public string NameTypeEn { get; set; }
        public System.DateTime idIssued { get; set; }
        public System.DateTime idexpiredDate { get; set; }
        public string stridIssued { get; set; }
        public string stridexpiredDate { get; set; }
        public string idPlaceIssue { get; set; }
        public string idImg { get; set; }
        public int empId { get; set; }
        public string empNameArbic { get; set; }
        public string empNameEnglish { get; set; }
        public bool Visible { get; set; }
    }
}
