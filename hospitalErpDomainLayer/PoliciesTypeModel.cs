﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace hospitalErpDomainLayer
{
  public  class PoliciesTypeModel
  {
        public int id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public string description { get; set; }
        public Nullable<int> MId { get; set; }
        public Nullable<System.DateTime> EnterDate { get; set; }
        public Nullable<int> Oid { get; set; }
        public string TImg { get; set; }
        public HttpPostedFileBase TImgFile { get; set; }
    }

    public class PoliciesTypeViewModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        [Display(Name = "Name Arabic")]
        public string NameAr { get; set; }
        [Display(Name = "Name English")]
        public string NameEn { get; set; }
        [Display(Name = "Name Description")]
        public string description { get; set; }
        public Nullable<int> MId { get; set; }
        [Display(Name = "M Name Arabic")]
        public string MNameAr { get; set; }
        [Display(Name = "M Name English")]
        public string MNameEr { get; set; }
        [Display(Name = "Enter Date")]
        public Nullable<System.DateTime> EnterDate { get; set; }
        public Nullable<int> Oid { get; set; }
        public string TImg { get; set; }
    }
}
