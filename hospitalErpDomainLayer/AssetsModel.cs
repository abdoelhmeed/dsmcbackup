﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace hospitalErpDomainLayer
{
   public class AssetsModel
    {
        public int id { get; set; }
        public Nullable<int> AsOId { get; set; }
        public string AsName { get; set; }
        public Nullable<double> AsPrice { get; set; }
        public Nullable<double> AnnualDepreciation { get; set; }
        public Nullable<double> StopDeeperPrint { get; set; }
        public string AsDescription { get; set; }
        public Nullable<int> SubType { get; set; }
        [Display(Name = "AsWarrantyPeriodDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> AsWarrantyPeriodDate { get; set; }
        [Display(Name = "PurchaseDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> PurchaseDate { get; set; }
        public string ASBill { get; set; }
        public string AsWarrantyImage { get; set; }
        public string AsInstructionManual { get; set; }
        public string ASMaintenanceContract { get; set; }
        public string Status { get; set; }
        public int MType { get; set; }
        public Nullable<int> AsMaintenanceDuration { get; set; }
        public Nullable<bool> Availability { get; set; }
        public Nullable<int> AsUseresType { get; set; }
        [Display(Name = "EnterDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EnterDate { get; set; }
        public Nullable<int> AsMaintenanceType { get; set; }
        public Nullable<bool> Visible { get; set; }
        public HttpPostedFileBase InstructionManualFile { get; set; }
        public HttpPostedFileBase WarrantyImageFile { get; set; }
        public HttpPostedFileBase BillFile { get; set; }
        public HttpPostedFileBase MaintenanceContractFile { get; set; }
    }
    
    public class AssetsViewModel
    {
        [Display(Name ="Id")]
        public int id { get; set; }
        public Nullable<int> AsOId { get; set; }
        [Display(Name = "Name")]
        public string AsName { get; set; }
        [Display(Name = "Price")]
        public Nullable<double> AsPrice { get; set; }
        [Display(Name = "Description")]
        public string AsDescription { get; set; }
        [Display(Name = "Maintenance Contract")]
        public string ASMaintenanceContract { get; set; }
        [Display(Name = "Type")]
        public string AssetTypeNameEnglish { get; set; }
        [Display(Name = "Type")]
        public string AssetTypeNameArabic { get; set; }
     
        [Display(Name = "Bill")]
        public string ASBill { get; set; }
        [Display(Name = "Instruction Manual")]
        public string AsInstructionManual { get; set; }
        [Display(Name = "Maintenance Type")]
        public Nullable<int> AsMaintenanceType { get; set; }
       
        [Display(Name = "Status")]
        public string Status{ get; set; }
        [Display(Name = "Asstes For")]
        public Nullable<int> AsUseresType { get; set; }
        [Display(Name = "Availability")]
        public Nullable<bool> Availability { get; set; }
        [Display(Name = "Sub Type")]
        public Nullable<int> SubType { get; set; }
        [Display(Name = "W Period Date")]
        public DateTime AsWarrantyPeriodDate { get; set; }
        [Display(Name = "Purchase Date")]
        public DateTime PurchaseDate { get; set; }
   
        public Nullable<int> AsMaintenanceDuration { get; set; }
        public DateTime EnterDate { get; set; }
        public string AsWarrantyImage { get; set; }
        public Nullable<bool> Visible { get; set; }
    }
}
