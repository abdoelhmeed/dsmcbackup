﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;

namespace hospitalErpBusinessLayer
{
    public class PoliciesApplies : IPoliciesApplies
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int add(PoliciesAppliesToModel model)
        {
            td_PoliciesAppliesTo policiesAppliesTo = new td_PoliciesAppliesTo
            {
                DeptId=model.DeptId,
                policiesNumbers=model.policiesNumbers
            };
            db.td_PoliciesAppliesTo.Add(policiesAppliesTo);
           int res= db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
                
        }
        public PoliciesAppliesToViewModel GetById(int Id)
        {
            td_PoliciesAppliesTo pAppTo = db.td_PoliciesAppliesTo.Find(Id);
            PoliciesAppliesToViewModel mode = new PoliciesAppliesToViewModel {
                policiesNumbers=pAppTo.policiesNumbers,
                DeptId=pAppTo.DeptId,
                DeptNameEn=pAppTo.tdManag_Department.deptNameEn,
                DeptNameAr = pAppTo.tdManag_Department.deptNameAr,
                id=pAppTo.id,
                polTitel=pAppTo.td_Policies.polTitle,
            };
            return mode;
        }
        public List<PoliciesAppliesToViewModel> GetByPolicyNumbers(string Number)
        {
            List<PoliciesAppliesToViewModel> model = db.td_PoliciesAppliesTo.Where(x => x.policiesNumbers == Number)
                .Select(u => new PoliciesAppliesToViewModel {
                policiesNumbers = u.policiesNumbers,
                DeptId = u.DeptId,
                DeptNameEn = u.tdManag_Department.deptNameEn,
                DeptNameAr = u.tdManag_Department.deptNameAr,
                id = u.id,
                polTitel = u.td_Policies.polTitle,
            }).ToList();

            return model;
        }


    }
}
