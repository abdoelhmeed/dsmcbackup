﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class NotificationsSetting : INotificationsSetting
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(NotificationsSettingMode model)
        {
            td_NotificationsSetting setting = new td_NotificationsSetting
            {
                deptId=model.deptId,
                empId=model.empId,
                OId=model.OId,
                Type=model.Type
            };
            db.td_NotificationsSetting.Add(setting);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int Delete(int Id)
        {
            td_NotificationsSetting setting = db.td_NotificationsSetting.Find(Id);
            db.Entry(setting).State = EntityState.Deleted;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
         
        }

        public List<NotificationsViewSettingMode> GetByOId(int OId)
        {
            List<NotificationsViewSettingMode> model = db.td_NotificationsSetting.Where(u => u.OId == OId)
                .Select(x => new NotificationsViewSettingMode
                {
                    OId=x.OId,
                    deptNameAr=x.tdManag_Department.deptNameAr,
                    deptNameEn=x.tdManag_Department.deptNameEn,
                    empNameAr=x.td_employee.empFNameAr+" "+ x.td_employee.empSNameAr+" "+ x.td_employee.empTNameAr,
                    empNameEn=x.td_employee.empFNameEn+" "+ x.td_employee.empSNameEn+" "+ x.td_employee.empTNameEn,
                    Id=x.Id,
                    Type=x.Type,
                    empId=x.empId
                }).ToList();
            return model;
        }

     
    }
}
