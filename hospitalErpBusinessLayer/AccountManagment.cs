﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;

namespace hospitalErpBusinessLayer
{
    public class AccountManagment : IAccountManagment
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public List<CreateRoleModel> RolesList()
        {
            List<CreateRoleModel> model = db.AspNetRoles.Select(x=> new CreateRoleModel
            {
                Id=x.Id,
                Name=x.Name
            }).ToList();

            return model;
        }

        public List<CreateRoleModel> RolesifNotAdminList()
        {
            List<CreateRoleModel> model = db.AspNetRoles.Where(e => e.Name != "Admin").Select(x => new CreateRoleModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return model;
        }

    }
}
