﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class PoliciesPlan : IPoliciesPlan
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(PolicyPlanModel model)
        {
            int num = db.td_PolicyPlan.Where(x => x.TId == model.TId).Count();
            td_PolicyPlan PP = new td_PolicyPlan
            {
                TId = model.TId,
                Arrangement = (num+1),
                Mid=model.Mid
            };
            db.td_PolicyPlan.Add(PP);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public PolicyPlanModel GetById(int Id)
        {
            td_PolicyPlan PPT = db.td_PolicyPlan.Find(Id);
            PolicyPlanModel model = new PolicyPlanModel();
            model.Mid = PPT.Mid;
            model.TId = PPT.TId;
            model.id = PPT.id;
            model.Arrangement = PPT.Arrangement;
            return model;
        }

        public List<PolicyPlanViweModel> GetByTId(int TId)
        {
            List<PolicyPlanViweModel> model = db.td_PolicyPlan.Where(u => u.TId == TId)
                .Select(x => new PolicyPlanViweModel
                {
                    Arrangement=x.Arrangement,
                    TId=x.TId,
                    Mid=x.Mid,
                    id=x.id,
                    MNameAr=x.tdManag_Management.MNameAr,
                    MNameEn=x.tdManag_Management.MName

                }).ToList();
            return model;
        }

        public int Update(PolicyPlanModel model)
        {
            td_PolicyPlan PPT = db.td_PolicyPlan.Find(model.id);
            model.Mid = PPT.Mid;
            model.TId = PPT.TId;
            db.Entry(PPT).State = EntityState.Modified;
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
           
        }

        public List<PolicyPlanViweModel> GetByMid(int Mid)
        {
            List<PolicyPlanViweModel> model = db.td_PolicyPlan.Where(u => u.Mid == Mid)
                .Select(x => new PolicyPlanViweModel
                {
                    Arrangement = x.Arrangement,
                    TId = x.TId,
                    Mid = x.Mid,
                    id = x.id,
                    MNameAr = x.tdManag_Management.MNameAr,
                    MNameEn = x.tdManag_Management.MName

                }).ToList();
            return model;
        }

        public int Delete(int Id)
        {
            td_PolicyPlan PPLModel = db.td_PolicyPlan.Find(Id);
            if (PPLModel != null)
            {
                List<td_PolicyPlan> modelList = db.td_PolicyPlan.Where(x => x.Arrangement > PPLModel.Arrangement && x.TId==PPLModel.TId).ToList();
                foreach (var item in modelList)
                {
                    td_PolicyPlan PPT = db.td_PolicyPlan.Find(item.id);
                    PPT.Arrangement = (PPT.Arrangement - 1);
                    db.Entry(PPT).State = EntityState.Modified;
                    db.SaveChanges();
                }

                db.Entry(PPLModel).State = EntityState.Deleted;
                db.SaveChanges();

                return 1;
            }
            else
            {
                return 0;
            }
           
        }
    }
}
