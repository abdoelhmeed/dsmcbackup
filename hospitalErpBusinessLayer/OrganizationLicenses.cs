﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class OrganizationLicenses : IOrganizationLicenses
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddOrganizationLicenses(OrganizationLicensesModel model)
        {
            tdMana_OrganizationLicenses OLmodel = new tdMana_OrganizationLicenses
            {
                OId = model.OId,
                OLEndDate = model.OLEndDate,
                OLEnterDate = DateTime.Now,
                OLimg = model.OLimg,
                OLName = model.OLName,
                OLPlace = model.OLPlace,
                OLStartDate = model.OLStartDate,
                OLValidity = true,
                Visible = true
            };
            db.tdMana_OrganizationLicenses.Add(OLmodel);
            int res= db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int DeleteOrganizationLicenses(int id)
        {
            tdMana_OrganizationLicenses OLmodel = db.tdMana_OrganizationLicenses.Find(id);
            OLmodel.Visible = false;
            db.Entry(OLmodel).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<OrganizationLicensesViewsModel> GetListOrganizationLicensesByOId(int Oid)
        {
            List<OrganizationLicensesViewsModel> model = db.tdMana_OrganizationLicenses.Where(u => u.Visible == true&& u.OId==Oid).Select(x => new OrganizationLicensesViewsModel
            {
                OId=x.OId,
                OLEndDate=x.OLEndDate,
                OLEnterDate=x.OLEnterDate,
                OLid=x.OLid,
                OLimg=x.OLimg,
                OLName=x.OLName,
                OLNameArabic=x.td_selectionDetails.selNameArabic,
                OLNameEnglish=x.td_selectionDetails.selNameEnglish,
                OLPlace=x.OLPlace,
                OLPlaceArabic=x.td_selectionDetails1.selNameArabic,
                OLPlaceEnglish=x.td_selectionDetails1.selNameEnglish,
                OLStartDate=x.OLStartDate,
                OLValidity=x.OLValidity,
                OName=x.tdMang_Organization.ONameEn,
               
            }).ToList();
            return model;
        }

        public OrganizationLicensesModel GetOrganizationLicensesById(int id)
        {
            tdMana_OrganizationLicenses OL = db.tdMana_OrganizationLicenses.Find(id);
            OrganizationLicensesModel model = new OrganizationLicensesModel();
            model.OId = OL.OId;
            model.OLEndDate = OL.OLEndDate;
            model.OLEnterDate = OL.OLEnterDate;
            model.OLid = OL.OLid;
            model.OLimg = OL.OLimg;
            model.OLName = OL.OLName;
            model.OLPlace = OL.OLPlace;
            model.OLStartDate = OL.OLStartDate;
            model.OLValidity = OL.OLValidity;
            model.Visible = OL.Visible;
            return model;



        }

        public int OrganizationLicenseValidity(int Id)
        {
            tdMana_OrganizationLicenses OrganizationLicenses = db.tdMana_OrganizationLicenses.Find(Id);
            OrganizationLicenses.OLValidity = false;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }else
            {
                return 0;
            }
        }

        public int UpdateOrganizationLicenses(OrganizationLicensesModel OL)
        {
            tdMana_OrganizationLicenses model = db.tdMana_OrganizationLicenses.Find(OL.OLid);
            model.OLEndDate = OL.OLEndDate;
            model.OLEnterDate = OL.OLEnterDate;
            model.OLimg = OL.OLimg;
            model.OLName = OL.OLName;
            model.OLPlace = OL.OLPlace;
            model.OLStartDate = OL.OLStartDate;
            db.Entry(model).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }


    }
}
