﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class PoliciesRea : IPoliciesRea
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(PoliciesReaModel model)
        {
            td_PoliciesRead read = new td_PoliciesRead
            {
                empId = model.empId,
                isRead=false,
                policiesNumbers=model.policiesNumbers,
                RaedDate=DateTime.Now,
                
            };
            db.td_PoliciesRead.Add(read);
            int res=db.SaveChanges();
            return 1;
        }
        public int PolicisRead(string Number, int empId, string UserId)
        {
            List<td_PoliciesRead> read = db.td_PoliciesRead.Where(x => x.policiesNumbers == Number && x.empId == empId).ToList();
            foreach (var item in read)
            {
                item.isRead = true;
                item.RaedDate = DateTime.Now;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }
            List<td_Notifications> notif = db.td_Notifications.Where(x => x.nTitel == Number && x.UserId == UserId).ToList();
            foreach (var item in notif)
            {
                item.isRead = true;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }

            return 1;
        }
        public List<PoliciesReaViewModel> GetMyPolicy(int empId)
        {
            List<PoliciesReaViewModel> model = db.td_PoliciesRead.Where(x => x.empId == empId && x.td_Policies.isPublish==true  && x.td_Policies.Visible==true).Select(u => new PoliciesReaViewModel
            {
                isPublish=u.td_Policies.isPublish,
                CreateByEmpId=u.td_Policies.CreateByEmpId,
                CreateByEmpNameAr=u.td_employee.empFNameAr+" " +u.td_employee.empSNameAr+" "+u.td_employee.empTNameAr,
                CreateByEmpNameEn = u.td_employee.empFNameEn + " " + u.td_employee.empSNameEn + " " + u.td_employee.empTNameEn,
                empId=u.empId,
                EnterDate=u.td_Policies.EnterDate,
                isRead=u.isRead,
                id=u.id,
                OId=u.td_Policies.OId,
                PolBody=u.td_Policies.PolBody,
                PolEffectiveDate=u.td_Policies.PolEffectiveDate,
                policiesNumbers=u.policiesNumbers,
                PolPdf=u.td_Policies.PolPdf,
                polTitle=u.td_Policies.polTitle,
                polTypeAr=u.td_Policies.td_PoliciesType.NameAr,
                polTypeEn=u.td_Policies.td_PoliciesType.NameEn,
                RaedDate=u.RaedDate,
                Status=u.td_Policies.Status,
                TId=u.td_Policies.TId,
                Visible=u.td_Policies.Visible,
                TImg=u.td_Policies.td_PoliciesType.TImg
                

            }).ToList();

            return model;
        }
    }
}
