﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class SClassification : ISClassification
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(SClassificationModel model)
        {
            td_AsstesSubClassification Sub = new td_AsstesSubClassification {
                MainClassificationId =model.MainClassificationId,
                NameArbic=model.NameArbic,
                NameEnglish=model.NameEnglish,
                Visible=true,
                OId=model.OId
            };
            db.td_AsstesSubClassification.Add(Sub);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_AsstesSubClassification Sub = db.td_AsstesSubClassification.Find(Id);
            Sub.Visible = false;
            db.Entry(Sub).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<SClassificationViewModel> GetByMainId(int Id)
        {
            List<SClassificationViewModel> model = db.td_AsstesSubClassification.Where(u => u.Visible == true && u.MainClassificationId == Id).Select(x => new SClassificationViewModel {

                id=x.id,
                MainClassificationId=x.MainClassificationId,
                Visible=x.Visible,
                MainNameClassificationEnglish=x.td_AsstesMainClassification.NameEnglish,
                MainNameClassificationArabic= x.td_AsstesMainClassification.NameArabic,
                NameEnglish=x.NameEnglish,
                NameArbic=x.NameArbic,
                OId=x.OId
            }).ToList();

            return model;
        }

        public List<SClassificationViewModel> GetByOId(int Id)
        {
            List<SClassificationViewModel> model = db.td_AsstesSubClassification.Where(u => u.Visible == true && u.OId == Id).Select(x => new SClassificationViewModel
            {

                id = x.id,
                MainClassificationId = x.MainClassificationId,
                Visible = x.Visible,
                MainNameClassificationEnglish = x.td_AsstesMainClassification.NameEnglish,
                MainNameClassificationArabic = x.td_AsstesMainClassification.NameArabic,
                NameEnglish = x.NameEnglish,
                NameArbic = x.NameArbic,
                OId = x.OId
            }).ToList();

            return model;
        }

        public SClassificationModel GetBySubId(int Id)
        {
            td_AsstesSubClassification model = db.td_AsstesSubClassification.Find(Id);
            SClassificationModel Sub = new SClassificationModel
            {
                OId=model.OId,
                id=model.id,
                MainClassificationId=model.MainClassificationId,
                NameArbic=model.NameArbic,
                NameEnglish=model.NameEnglish,
                Visible=model.Visible
            };
            return Sub;
        }

        public int Update(SClassificationModel model)
        {
            td_AsstesSubClassification Sub = db.td_AsstesSubClassification.Find(model.id);
            Sub.MainClassificationId = model.MainClassificationId;
            Sub.NameArbic = model.NameArbic;
            Sub.NameEnglish = model.NameEnglish;
            db.Entry(Sub).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
}
