﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;

namespace hospitalErpBusinessLayer
{
    public class EmployeeRequest : IEmployeeRequest
    {
        erpHospitalEntities db = new erpHospitalEntities();
        EmployeesDepartmentManagment empManaDepart = new EmployeesDepartmentManagment();
        EmployeeRequestsNodes Nodes = new EmployeeRequestsNodes();
        public int Add(EmployeeRequestModel model)
        {
            td_EmployeeRequests EReq = new td_EmployeeRequests
            {
                empId=model.empId,
                Oid=model.Oid,
                requestDetails=model.requestDetails,
                requestTypeId=model.requestTypeId,
                Status="New",
                Visible=true
            };
            db.td_EmployeeRequests.Add(EReq);
            int res = db.SaveChanges();
            if (res > 0)
            {
                int empId =(int)model.empId;
                employeeDepartmentManagmentViewModel departManag= empManaDepart.GetEmployeesByEmpId(empId);
                tdManag_Department Department = db.tdManag_Department.Find(departManag.DepartmentId);
                EmployeeRequestsNodesModel enodeDepartment = new EmployeeRequestsNodesModel {
                    Arrangement = 1,
                    empId = Department.deptManager,
                    Approve = false,
                    nodes = "New",
                    requestsId=EReq.requestId,
                    Status="New",
                    Visible=true
                };
                int saved= Nodes.Add(enodeDepartment);
                if (saved > 0)
                {

                }

                tdManag_Management Management = db.tdManag_Management.Find(departManag.ManagmentId);
                EmployeeRequestsNodesModel enodeManagement = new EmployeeRequestsNodesModel
                {
                    Arrangement = 2,
                    empId = Management.deptManager,
                    Approve = false,
                    nodes = "New",
                    requestsId = EReq.requestId,
                    Status = "New",
                    Visible = true
                };
                Nodes.Add(enodeManagement);
                ///
                int? modelDeptForType = db.td_EmployeeRequestsType.Find(EReq.requestTypeId).DepartmentId;
                tdManag_Department ModelDepartment = db.tdManag_Department.Find(modelDeptForType);
                EmployeeRequestsNodesModel modelEnodeDepartment = new EmployeeRequestsNodesModel
                {
                    Arrangement = 3,
                    empId = ModelDepartment.deptManager,
                    Approve = false,
                    nodes = "New",
                    requestsId = EReq.requestId,
                    Status = "New",
                    Visible = true
                };
                Nodes.Add(modelEnodeDepartment);
               


                return 1;
            }
            else
            {
                return 0;
            }
        }
        public EmployeeRequestModel GetById(int Id)
        {
            td_EmployeeRequests EReq = db.td_EmployeeRequests.Find(Id);
            EmployeeRequestModel model = new EmployeeRequestModel();
            model.empId = EReq.empId;
            model.Oid = EReq.Oid;
            model.requestDetails = model.requestDetails;
            model.requestId = EReq.requestId;
            model.requestTypeId = model.requestTypeId;
            return model;
        }
        public EmployeeRequestViweModel GetRequestViweById(int Id)
        {
            td_EmployeeRequests eReq = db.td_EmployeeRequests.Find(Id);
            EmployeeRequestViweModel model = new EmployeeRequestViweModel
            {
                empId = eReq.empId,
                empNameAr = eReq.td_employee.empFNameAr + " " + eReq.td_employee.empSNameAr + " " + eReq.td_employee.empTNameAr,
                empNameEn = eReq.td_employee.empFNameEn + " " + eReq.td_employee.empSNameEn + " " + eReq.td_employee.empTNameEn,
                Oid = eReq.Oid,
                requestDetails=eReq.requestDetails,
                requestId=eReq.requestId,
                requestTypeId=eReq.requestTypeId,
                requestTypeNameAr=eReq.td_EmployeeRequestsType.requestNameArabic,
                requestTypeNameEn=eReq.td_EmployeeRequestsType.requestNameEnglish,
                Status=eReq.Status,
                Visible=eReq.Visible
            };
            return model;
        }
        public List<EmployeeRequestViweModel> GetByempId(int empId)
        {
            List<EmployeeRequestViweModel> model = db.td_EmployeeRequests.Where(u => u.empId == empId && u.Visible == true)
                .Select(x => new EmployeeRequestViweModel {
                    Visible=x.Visible,
                    empId=x.empId,
                    empNameAr=x.td_employee.empFNameAr+" "+x.td_employee.empSNameAr+" "+x.td_employee.empTNameAr,
                    empNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                    Oid=x.Oid,
                    requestDetails=x.requestDetails,
                    requestId=x.requestId,
                    requestTypeNameAr=x.td_EmployeeRequestsType.requestNameArabic,
                    requestTypeNameEn=x.td_EmployeeRequestsType.requestNameEnglish,
                    requestTypeId=x.requestTypeId,
                    Status=x.Status
                }).ToList();
            return model;
        }
        public int Update(EmployeeRequestModel model)
        {
            throw new NotImplementedException();
        }
    }
}
