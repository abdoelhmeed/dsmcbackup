﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
   public class IncideContributingFactor : IIncideContributingFactor
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(IncideContributingFactorModel model)
        {
            td_IncideContributingFactor CF = new td_IncideContributingFactor
            {
                CFNameAr=model.CFNameAr,
                CFNameEn=model.CFNameEn,
                OId=model.OId,
                Visible=true
            };
            db.td_IncideContributingFactor.Add(CF);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int CFId)
        {
            td_IncideContributingFactor CF = db.td_IncideContributingFactor.Find(CFId);
            CF.Visible = false;
            db.Entry(CF).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public IncideContributingFactorModel GetById(int CFId)
        {
            td_IncideContributingFactor CF = db.td_IncideContributingFactor.Find(CFId);
            IncideContributingFactorModel model = new IncideContributingFactorModel
            {
                CFId=CF.CFId,
                CFNameAr=CF.CFNameAr,
                CFNameEn=CF.CFNameEn,
                OId=CF.OId
            };
            return model;

        }

        public List<IncideContributingFactorModel> GetByOId(int OId)
        {
            List<IncideContributingFactorModel> model = db.td_IncideContributingFactor.Where(x => x.OId == OId && x.Visible==true)
                .Select(CF => new IncideContributingFactorModel
                {
                    CFId = CF.CFId,
                    CFNameAr = CF.CFNameAr,
                    CFNameEn = CF.CFNameEn,
                    OId = CF.OId
                }).ToList();
            return model;
        }

        public int Update(IncideContributingFactorModel model)
        {
            td_IncideContributingFactor CF = db.td_IncideContributingFactor.Find(model.CFId);
            CF.CFNameAr = model.CFNameAr;
            CF.CFNameEn = model.CFNameEn;
            db.Entry(CF).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
