﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class Incident : IIncident
    {
        erpHospitalEntities db = new erpHospitalEntities();
        IncidentAffected Affect = new IncidentAffected();
        public int Add(IncidentsModel model)
        {
            td_Incident incident = new td_Incident {
                Complete = 10,
                CreateByEmpId=model.CreateByEmpId,
                CreatorName=model.CreatorName,
                Description=model.Description,
                DiscoverDate=model.DiscoverDate,
                HideMyName=model.HideMyName,
                InciImages=model.InciImages,
                isClosed=false,
                LId=model.LId,
                OccurrenceDate=model.OccurrenceDate,
                ReportDate=DateTime.Now,
                OId=model.OId,
                SelfReporting=model.SelfReporting,
                status= "New incidention",
                Visible=true,
            };
            db.td_Incident.Add(incident);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return incident.IncidentId;
            }
            else
            {
                return 0;
            }
        }
        public List<IncidentViewModel> GetIncidentByOId(int OId)
        {
            List<IncidentViewModel> model = db.td_Incident.Where(u => u.OId == OId && u.Visible == true)
                .Select(x => new IncidentViewModel
                {
                    Visible=x.Visible,
                    Complete=x.Complete,
                    CreateByEmpId=x.CreateByEmpId,
                    CreatorName=x.CreatorName,
                    Description=x.Description,
                    DiscoverDate=(DateTime)x.DiscoverDate,
                    HideMyName=x.HideMyName,
                    InciImages=x.InciImages,
                    IncidentId=x.IncidentId,
                    isClosed=x.isClosed,
                    LId=(int)x.LId,
                    OccurrenceDate=(DateTime)x.OccurrenceDate,
                    OId=(int)x.OId,
                    ReportDate=(DateTime)x.ReportDate,
                    SelfReporting=x.SelfReporting,
                    status=x.status,
                    LNameAr=x.td_LocationOfIncident.LNAr,
                    LNameEn=x.td_LocationOfIncident.LNEn

                }).ToList();
            return model;
        }
        public IncidentViewModel GetById(int Id)
        {
            td_Incident Inc = db.td_Incident.Find(Id);
            IncidentViewModel model = new IncidentViewModel
            {
                Complete= Inc.Complete,
                CreateByEmpId= Inc.CreateByEmpId,
                CreatorName= Inc.CreatorName,
                Description= Inc.Description,
                DiscoverDate=(DateTime) Inc.DiscoverDate,
                HideMyName= Inc.HideMyName,
                InciImages= Inc.InciImages,
                IncidentId= Inc.IncidentId,
                isClosed= Inc.isClosed,
                LId=(int)Inc.LId,
                LNameAr= Inc.td_LocationOfIncident.LNAr,
                LNameEn= Inc.td_LocationOfIncident.LNEn,
                OccurrenceDate=(DateTime)Inc.OccurrenceDate,
                ReportDate=(DateTime)Inc.ReportDate,
                OId= (int)Inc.OId,
                SelfReporting= Inc.SelfReporting,
                status= Inc.status,
                Visible= Inc.Visible
            };
            return model;
        }
        public int SubmitIncident(int Id)
        {
            td_Incident Incident = db.td_Incident.Find(Id);
            Incident.status = "Submited";
            Incident.Complete = Incident.Complete + 20;
            db.Entry(Incident).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }


    }
}
