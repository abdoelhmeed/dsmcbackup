﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class EmployeeRequestsNodes : IEmployeeRequestsNodes
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(EmployeeRequestsNodesModel model)
        {
            td_EmployeeRequestsNodes eNode = new td_EmployeeRequestsNodes
            {
                Approve=false,
                Arrangement=model.Arrangement,
                empId=model.empId,
                nodes=model.nodes,
                requestsId=model.requestsId,
                Status=model.Status,
                Visible=true
            };
            db.td_EmployeeRequestsNodes.Add(eNode);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public EmployeeRequestsNodesModel GetById(int Id)
        {
            td_EmployeeRequestsNodes eNodes = db.td_EmployeeRequestsNodes.Find(Id);
            EmployeeRequestsNodesModel model = new EmployeeRequestsNodesModel();
            model.Approve = eNodes.Approve;
            model.Arrangement = eNodes.Arrangement;
            model.empId = eNodes.empId;
            model.id = eNodes.id;
            model.requestsId = eNodes.requestsId;
            model.Status = eNodes.Status;
            model.Visible = eNodes.Visible;
            return model;

        }

        public List<EmployeeRequestsNodesViewModel> GetByRequestId(int RequestId)
        {
            List<EmployeeRequestsNodesViewModel> model = db.td_EmployeeRequestsNodes.Where(u => u.requestsId == RequestId)
                .Select(x => new EmployeeRequestsNodesViewModel {
                    requestsId=x.requestsId,
                    Approve=x.Approve,
                    Arrangement=x.Arrangement,
                    empId=x.empId,
                    empNameAr=x.td_employee.empFNameAr+" "+x.td_employee.empSNameAr+" "+x.td_employee.empTNameAr,
                    empNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                    id=x.id,
                    nodes=x.nodes,
                    requestDetails=x.td_EmployeeRequests.requestDetails,
                    Status=x.Status,
                    Visible=x.Visible
                }).ToList();
            return model;
        }

        public List<EmployeeRequestsNodesViewModel> GetForApprove(int empId)
        {
            List<EmployeeRequestsNodesViewModel> model = new List<EmployeeRequestsNodesViewModel>();
            List<td_EmployeeRequestsNodes> eNodeList = db.td_EmployeeRequestsNodes.Where(u => u.Visible == true && u.Approve == false).ToList();
           

            foreach (var item in eNodeList)
            {
                if (item.td_EmployeeRequests.Status == "New")
                {
                    if (item.Arrangement == 1)
                    {
                        if (item.empId == empId)
                        {
                            EmployeeRequestsNodesViewModel itemNode = EmpNodes(item);
                            model.Add(itemNode);
                        }

                    }
                    else if (item.Arrangement == 2)
                    {
                        List<td_EmployeeRequestsNodes> eNodeArrangementList2 = db.td_EmployeeRequestsNodes.Where(x => x.requestsId == item.requestsId && x.Arrangement == 1 && x.Approve == true).ToList();
                        if (eNodeArrangementList2.Count > 0)
                        {
                            if (item.empId == empId)
                            {
                                EmployeeRequestsNodesViewModel itemNode = EmpNodes(item);
                                model.Add(itemNode);
                            }
                        }
                    }
                    else if (item.Arrangement == 3)
                    {
                        List<td_EmployeeRequestsNodes> eNodeArrangementList3 = db.td_EmployeeRequestsNodes.Where(x => x.requestsId == item.requestsId && x.Arrangement == 2 && x.Approve == true).ToList();
                        if (eNodeArrangementList3.Count > 0)
                        {
                            if (item.empId == empId)
                            {
                                EmployeeRequestsNodesViewModel itemNode = EmpNodes(item);
                                model.Add(itemNode);
                            }
                        }


                    }
                }
               
            }
           
            return model;
        }

        public int Approve(EmployeeRequestsNodesModel model)
        {
            td_EmployeeRequestsNodes eNode = db.td_EmployeeRequestsNodes.Find(model.id);
            eNode.Approve = true;
            eNode.Status = "Approve";
            eNode.nodes = model.nodes;
            db.Entry(eNode).State = EntityState.Modified;
            db.SaveChanges();
            if (eNode.Arrangement==3)
            {
                td_EmployeeRequests empReq = db.td_EmployeeRequests.Find(eNode.requestsId);
                empReq.Status = "Approve";
                db.Entry(empReq).State = EntityState.Modified;
                db.SaveChanges();
               
            }
            return 1;
        }

        public int Reject(EmployeeRequestsNodesModel model)
        {
            td_EmployeeRequests employeeRequests = db.td_EmployeeRequests.Find(model.requestsId);
            employeeRequests.Status = "Rejected";
            db.Entry(employeeRequests).State = EntityState.Modified;
            int res= db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        private EmployeeRequestsNodesViewModel EmpNodes(td_EmployeeRequestsNodes item)
        {
            EmployeeRequestsNodesViewModel itemModel = new EmployeeRequestsNodesViewModel
            {
                Arrangement = item.Arrangement,
                Approve = item.Approve,
                empId = item.empId,
                empNameAr = item.td_employee.empFNameAr + " " + item.td_employee.empSNameAr + " " + item.td_employee.empTNameAr,
                empNameEn = item.td_employee.empFNameEn + " " + item.td_employee.empSNameEn + " " + item.td_employee.empTNameEn,
                id = item.id,
                nodes = item.nodes,
                requestDetails = item.td_EmployeeRequests.requestDetails,
                requestsId = item.requestsId,
                Status = item.Status,
                Visible = item.Visible
            };

            return itemModel;
        }

        public int Update(EmployeeRequestsNodesModel model)
        {
            throw new NotImplementedException();
        }
    }
}
