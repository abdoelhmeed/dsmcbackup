﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class IncidentAffected : IIncidentAffected
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddStaff(IncidentAffectedStaffModel model)
        {
            td_IncidentAffectedStaff staff = new td_IncidentAffectedStaff
            {
                empId=model.empId,
                IncidentId=model.IncidentId,
                OId=model.OId,
                Visible=true
            };
            db.td_IncidentAffectedStaff.Add(staff);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public int AddVisitor(IncidentAffectedVisitorModel model)
        {
            td_IncidentAffectedVisitor visitor = new td_IncidentAffectedVisitor
            {

                VName=model.VName,
                Visible=true,
                OId=model.OId,
                IncidentId=model.IncidentId,
                VAddress=model.VAddress,
                Vid=model.Vid,
                VPhone=model.VPhone

            };
            db.td_IncidentAffectedVisitor.Add(visitor);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int DeleteStaff(int Id)
        {
            td_IncidentAffectedStaff staff = db.td_IncidentAffectedStaff.Find(Id);
            if (staff != null)
            {
                db.Entry(staff).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int DeleteVisitor(int Id)
        {
            td_IncidentAffectedVisitor Visitor = db.td_IncidentAffectedVisitor.Find(Id);
            if (Visitor !=null)
            {
                db.Entry(Visitor).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<IncidentAffectedStaffViewModel> GetStaff(int IncidentId)
        {
            List<IncidentAffectedStaffViewModel> model = db.td_IncidentAffectedStaff.Where(I => I.IncidentId == IncidentId).Select(x => new IncidentAffectedStaffViewModel {
                IncidentId = x.IncidentId,
                AffectedId = x.AffectedId,
                empId = x.empId,
                empNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                empNameEn=x.td_employee.empFNameEn+" "+ x.td_employee.empSNameEn+" "+ x.td_employee.empTNameEn,
                OId=x.OId,
                Visible=x.Visible
            }).ToList();
            return model;
        }
        public List<IncidentAffectedVisitorModel> GetVisitors(int IncidentId)
        {
            List<IncidentAffectedVisitorModel> model = db.td_IncidentAffectedVisitor.Where(I => I.IncidentId == IncidentId).
                Select(x => new IncidentAffectedVisitorModel {
                    IncidentId=x.IncidentId,
                    OId=x.OId,
                    VAddress=x.VAddress,
                    Vid=x.Vid,
                    Visible=x.Visible,
                    VName=x.VName,
                    VPhone=x.VPhone
                }).ToList();
            return model;
        }
    }
}
