﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class IncidentsAssigns : IIncidentAssign
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(IncidentAssignModel model)
        {
            td_IncidentAssign assing = new td_IncidentAssign
            {
                assDeptResDept = model.assDeptResDept,
                Comment = model.Comment,
                CreateByEmpId = model.CreateByEmpId,
                IMTId = model.IMTId,
                IncidentDeptId = model.IncidentDeptId,
                investigatorEmpId = model.investigatorEmpId,
                IncidentId = model.IncidentId,
                ISTId = model.ISTId,
                OId = model.OId,
                status = "New Assign",
                Visible=true
            };

            db.td_IncidentAssign.Add(assing);
            int res = db.SaveChanges();
            if (res > 0)
            {
                td_Incident Inc = db.td_Incident.Find(assing.IncidentId);
                Inc.status = "Assign";
                Inc.Complete = Inc.Complete + 20;
                db.Entry(Inc).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public List<IncidentAssignForViewModel> GetByEmpId(int empId)
        {
            List<IncidentAssignForViewModel> Assign = db.td_IncidentAssign.Where(e => e.investigatorEmpId == empId)
                .Select(x => new IncidentAssignForViewModel
                {
                   assId=x.assId,
                   Description=x.td_Incident.Description,
                   DiscoverDate=(DateTime)x.td_Incident.DiscoverDate,
                   IncidentId=x.IncidentId,
                   OccurrenceDate=(DateTime)x.td_Incident.OccurrenceDate,
                   ReportDate=x.td_Incident.ReportDate,
                   HideMyName=(bool)x.td_Incident.HideMyName,
                   status=x.status

                }).ToList();

            return Assign;
        }

        public IncidentAssignViewModel GetById(int assId)
        {
            td_IncidentAssign x = db.td_IncidentAssign.Find(assId);
            IncidentAssignViewModel model = new IncidentAssignViewModel
            {

                investigatorEmpId = x.investigatorEmpId,
                assDeptResDept = x.assDeptResDept,
                assDeptResDeptNameAr = x.tdManag_Department.deptNameEn,
                assDeptResDeptNameEn = x.tdManag_Department.deptNameEn,
                investigatorEmpIdNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                investigatorEmpIdNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                assId = x.assId,
                Comment = x.Comment,
                CreateByEmpId = x.CreateByEmpId,
                IMTId = x.IMTId,
                IMTNameAr = x.td_IncidentMainType.IMTNameAr,
                IMTNameEn = x.td_IncidentMainType.IMTNameEr,
                IncidentDeptId = x.IncidentDeptId,
                IncidentId = x.IncidentId,
                ISTId = x.ISTId,
                ISTNameAr = x.td_IncidentSubType.ISTNameAr,
                ISTNameEn = x.td_IncidentSubType.ISTNameEn,
                OId = x.OId,
                Visible = x.Visible,
                CreateByAr = x.td_employee1.empFNameAr + " " + x.td_employee1.empSNameAr + x.td_employee1.empTNameAr,
                CreateByEn = x.td_employee1.empFNameEn + " " + x.td_employee1.empSNameEn + x.td_employee1.empTNameEn,
                IncidentDeptNameAr = x.tdManag_Department1.deptNameAr,
                IncidentDeptNameEn = x.tdManag_Department1.deptNameEn,

            };

            return model;
        }

        public List<IncidentAssignViewModel> GetByIncidentId(int IncidentId)
        {
            List<IncidentAssignViewModel> Assign = db.td_IncidentAssign.Where(e => e.IncidentId == IncidentId)
             .Select(x => new IncidentAssignViewModel
             {
                 investigatorEmpId = x.investigatorEmpId,
                 assDeptResDept = x.assDeptResDept,
                 assDeptResDeptNameAr = x.tdManag_Department.deptNameEn,
                 assDeptResDeptNameEn = x.tdManag_Department.deptNameEn,
                 investigatorEmpIdNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                 investigatorEmpIdNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                 assId = x.assId,
                 Comment = x.Comment,
                 CreateByEmpId = x.CreateByEmpId,
                 IMTId = x.IMTId,
                 IMTNameAr = x.td_IncidentMainType.IMTNameAr,
                 IMTNameEn = x.td_IncidentMainType.IMTNameEr,
                 IncidentDeptId = x.IncidentDeptId,
                 IncidentId = x.IncidentId,
                 ISTId = x.ISTId,
                 ISTNameAr = x.td_IncidentSubType.ISTNameAr,
                 ISTNameEn = x.td_IncidentSubType.ISTNameEn,
                 OId = x.OId,
                 Visible = x.Visible,
                 CreateByAr = x.td_employee1.empFNameAr + " " + x.td_employee1.empSNameAr+" " + x.td_employee1.empTNameAr,
                 CreateByEn = x.td_employee1.empFNameEn + " " + x.td_employee1.empSNameEn +" " +x.td_employee1.empTNameEn,
                 IncidentDeptNameAr = x.tdManag_Department1.deptNameAr,
                 IncidentDeptNameEn = x.tdManag_Department1.deptNameEn,

             }).ToList();

            return Assign;
        }

        public List<IncidentAssignViewModel> GetDepartmentInvestigation(int DeptId)
        {
            List<IncidentAssignViewModel> Assign = db.td_IncidentAssign.Where(e => e.assDeptResDept == DeptId)
               .Select(x => new IncidentAssignViewModel
               {
                   investigatorEmpId = x.investigatorEmpId,
                   assDeptResDept = x.assDeptResDept,
                   assDeptResDeptNameAr = x.tdManag_Department.deptNameEn,
                   assDeptResDeptNameEn = x.tdManag_Department.deptNameEn,
                   investigatorEmpIdNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                   investigatorEmpIdNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                   assId = x.assId,
                   Comment = x.Comment,
                   CreateByEmpId = x.CreateByEmpId,
                   IMTId = x.IMTId,
                   IMTNameAr = x.td_IncidentMainType.IMTNameAr,
                   IMTNameEn = x.td_IncidentMainType.IMTNameEr,
                   IncidentDeptId = x.IncidentDeptId,
                   IncidentId = x.IncidentId,
                   ISTId = x.ISTId,
                   ISTNameAr = x.td_IncidentSubType.ISTNameAr,
                   ISTNameEn = x.td_IncidentSubType.ISTNameEn,
                   OId = x.OId,
                   Visible = x.Visible,
                   CreateByAr = x.td_employee1.empFNameAr + " " + x.td_employee1.empSNameAr + x.td_employee1.empTNameAr,
                   CreateByEn = x.td_employee1.empFNameEn + " " + x.td_employee1.empSNameEn + x.td_employee1.empTNameEn,
                   IncidentDeptNameAr = x.tdManag_Department1.deptNameAr,
                   IncidentDeptNameEn = x.tdManag_Department1.deptNameEn,

               }).ToList();

            return Assign;
        }
        public List<IncidentAssignViewModel> GetForDepartmentResponsible(int DeptId)
        {
            List<IncidentAssignViewModel> Assign = db.td_IncidentAssign.Where(e => e.IncidentDeptId == DeptId)
               .Select(x => new IncidentAssignViewModel
               {
                   investigatorEmpId = x.investigatorEmpId,
                   assDeptResDept = x.assDeptResDept,
                   assDeptResDeptNameAr = x.tdManag_Department.deptNameEn,
                   assDeptResDeptNameEn = x.tdManag_Department.deptNameEn,
                   investigatorEmpIdNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                   investigatorEmpIdNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                   assId = x.assId,
                   Comment = x.Comment,
                   CreateByEmpId = x.CreateByEmpId,
                   IMTId = x.IMTId,
                   IMTNameAr = x.td_IncidentMainType.IMTNameEr,
                   IMTNameEn = x.td_IncidentMainType.IMTNameEr,
                   IncidentDeptId = x.IncidentDeptId,
                   IncidentId = x.IncidentId,
                   ISTId = x.ISTId,
                   ISTNameAr = x.td_IncidentSubType.ISTNameAr,
                   ISTNameEn = x.td_IncidentSubType.ISTNameAr,
                   OId = x.OId,
                   Visible = x.Visible,
                   CreateByAr = x.td_employee1.empFNameAr + " " + x.td_employee1.empSNameAr + x.td_employee1.empTNameAr,
                   CreateByEn = x.td_employee1.empFNameEn + " " + x.td_employee1.empSNameEn + x.td_employee1.empTNameEn,
                   IncidentDeptNameAr = x.tdManag_Department1.deptNameAr,
                   IncidentDeptNameEn = x.tdManag_Department1.deptNameEn,
                   
               }).ToList();

            return Assign;
        }
    }
}
