﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class Management : IManagement
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddAdimn(ManagementModel model)
        {
            tdManag_Management tdM = db.tdManag_Management.Find(model.Mid);
            tdM.deptManager = model.deptManager;
            tdM.deptVicManager = model.deptManager;
            db.Entry(tdM).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
            
        }
        public int AddManagement(ManagementModel model)
        {
            tdManag_Management manag = new tdManag_Management
            {
               MNameAr = model.MNameAr,
               MName = model.MName,
               OId = model.OId,
               deptManager = model.deptManager,
               deptVicManager = model.deptVicManager
        };
            db.tdManag_Management.Add(manag);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public int DelManagement(int Id)
        {
            try
            {
                tdManag_Management manag = db.tdManag_Management.Find(Id);
                db.Entry(manag).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            catch (Exception)
            {

                return 0;
            }
           
          
        }

        public List<ManagementViewModel> Get()
        {
            List<ManagementViewModel> model = new List<ManagementViewModel>();
            List<tdManag_Management> td = db.tdManag_Management.ToList();
            foreach (var item in td)
            {
                ManagementViewModel modelItem = new ManagementViewModel
                {
                    MName = item.MName,
                    MNameAr = item.MNameAr,
                    Mid = item.Mid,
                    ONameArabic=item.tdMang_Organization.ONameAr,
                    ONameEnglish=item.tdMang_Organization.ONameEn,
                    NameManagerEnglish=item.td_employee.empFNameEn+" "+ item.td_employee.empSNameEn+" "+ item.td_employee.empTNameEn,
                    NameManagerArabic = item.td_employee.empFNameAr+" "+ item.td_employee.empSNameAr + " " + item.td_employee.empTNameAr,
                    NameVicManagerEnglish = item.td_employee1.empFNameEn + " " + item.td_employee1.empSNameEn + " " + item.td_employee1.empTNameEn,
                    NameVicManagerArbic = item.td_employee1.empFNameAr + " " + item.td_employee1.empSNameAr + " " + item.td_employee1.empTNameAr,
                    OId =item.OId,
                    deptManager = item.td_employee.empid,
                    deptVicManager = item.td_employee1.empid,
                };

                model.Add(modelItem);
            }
              
            return model;
        }

        public List<ManagementViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<ManagementViewModel> GetAllByOId(int OId)
        {
            List<ManagementViewModel> model = db.tdManag_Management.Where(u => u.OId == OId).
                Select(x => new ManagementViewModel {

                    MName = x.MName,
                    MNameAr = x.MNameAr,
                    Mid = x.Mid,
                    ONameArabic = x.tdMang_Organization.ONameAr,
                    ONameEnglish = x.tdMang_Organization.ONameEn,
                    NameManagerEnglish = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                    NameManagerArabic = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                    NameVicManagerEnglish = x.td_employee1.empFNameEn + " " + x.td_employee1.empSNameEn + " " + x.td_employee1.empTNameEn,
                    NameVicManagerArbic = x.td_employee1.empFNameAr + " " + x.td_employee1.empSNameAr + " " + x.td_employee1.empTNameAr,
                    deptManager=x.td_employee.empid,
                    deptVicManager=x.td_employee1.empid,
                    OId = x.OId,



                }).ToList();
            return model;
        }

        public ManagementModel GetManagementById(int id)
        {
            var td = db.tdManag_Management.Find(id);
            ManagementModel model = new ManagementModel();
            model.Mid = td.Mid;
            model.MName = td.MName;
            model.MNameAr = td.MNameAr;
            model.OId = td.OId;
            model.deptManager = td.deptManager;
            model.deptVicManager = td.deptVicManager;
            return model;
            

        }
        public int UpdateAdimn(ManagementModel model)
        {

            tdManag_Management tdM = db.tdManag_Management.Find(model.Mid);
            tdM.deptManager = model.deptManager;
            tdM.deptVicManager = model.deptManager;
            db.Entry(tdM).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public int UpdateManagement(ManagementModel model)
        {
            tdManag_Management manag = db.tdManag_Management.Find(model.Mid);
            manag.MNameAr = model.MNameAr;
            manag.MName = model.MName;
            manag.OId = model.OId;
            manag.deptVicManager = model.deptVicManager;
            manag.deptManager = model.deptManager;
            db.Entry(manag).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        ///    Get All Management By  Organization ID
        /// </summary>
        /// <param name="OId">Organization ID</param>
        /// <returns></returns>
        public List<ManagementelectionModel> GetSelectionByOId(int OId)
         {
                List<ManagementelectionModel> model = db.tdManag_Management.Where(u => u.OId == OId).
                    Select(x => new ManagementelectionModel {
                    OId=x.OId,
                    Mid=x.Mid,
                    MNameArabic=x.MNameAr,
                    MNameEnglish=x.MName
                }).ToList();
                return model;
         }
    }
}
