﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;

namespace hospitalErpBusinessLayer
{
    public class SelectionType : ISelectionType
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddSelectionType(SelectionTypeModel model)
        {
            td_selectionType sType = new td_selectionType
            {
                id=model.id,
                ArabicName=model.ArabicName,
                EnglishName=model.EnglishName
            };
            db.td_selectionType.Add(sType);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int DelSelectionType(SelectionTypeModel model)
        {
            throw new NotImplementedException();
        }

        public List<SelectionTypeModel> GetAll()
        {
            List<SelectionTypeModel> model = db.td_selectionType.Select(x => new SelectionTypeModel
            {
                ArabicName=x.ArabicName,
                EnglishName=x.EnglishName,
                id=x.id
            }).ToList();
            return model;
                
        }

        public SelectionTypeModel GetPyId(int Id)
        {
            td_selectionType model = db.td_selectionType.Find(Id);

            SelectionTypeModel sType = new SelectionTypeModel
            {
                ArabicName = model.ArabicName,
                EnglishName=model.EnglishName,
                id=model.id
            };

            return sType;
        }

        public int UpdaetSelectionType(SelectionTypeModel model)
        {
            throw new NotImplementedException();
        }
    }
}
