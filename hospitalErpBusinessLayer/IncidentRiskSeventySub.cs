﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class IncidentRiskSeventySub : IIncidentRiskSeventySub
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(IncidentRiskSeventySubModel model)
        {
            td_IncidentRiskSeventySub sub = new td_IncidentRiskSeventySub
            {
                OId=model.OId,
                RiskRating=model.RiskRating,
                RPMId=model.RPMId,
                RSSNameAr=model.RSSNameAr,
                RSSNameEn=model.RSSNameEn,
                Visible=true
            };
            db.td_IncidentRiskSeventySub.Add(sub);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_IncidentRiskSeventySub Sub = db.td_IncidentRiskSeventySub.Find(Id);
            Sub.Visible = false;
            db.Entry(Sub).State = EntityState.Modified;
           int res= db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
            
        }

        public IncidentRiskSeventySubModel GetById(int Id)
        {
            td_IncidentRiskSeventySub x = db.td_IncidentRiskSeventySub.Find(Id);
            IncidentRiskSeventySubModel model = new IncidentRiskSeventySubModel
            {
                Visible=x.Visible,
                RSSNameEn=x.RSSNameEn,
                OId=x.OId,
                RiskRating=x.RiskRating,
                RPMId=x.RPMId,
                RSSId=x.RSSId,
                RSSNameAr=x.RSSNameAr
            };
            return model;
        }

        public List<IncidentRiskSeventySubViewModel> GetByMainType(int RPMId)
        {
            List<IncidentRiskSeventySubViewModel> model = db.td_IncidentRiskSeventySub.Where(u => u.RPMId == RPMId && u.Visible == true)
                .Select(x => new IncidentRiskSeventySubViewModel
                {
                    Visible=x.Visible,
                    RPMId=x.RPMId,
                    OId=x.OId,
                    RiskProbabilityArabic=x.td_IncidentRiskProbabilityMain.RPMNameAr,
                    RiskProbabilityEnglish=x.td_IncidentRiskProbabilityMain.RPMINameEn,
                    RiskRating=x.RiskRating,
                    RSSId=x.RSSId,
                    RSSNameAr=x.RSSNameAr,
                    RSSNameEn=x.RSSNameEn
                }).ToList();
            return model;
        }

        public List<IncidentRiskSeventySubViewModel> GetByOId(int OId)
        {
            List<IncidentRiskSeventySubViewModel> model = db.td_IncidentRiskSeventySub.Where(u => u.OId == OId && u.Visible == true)
                .Select(x => new IncidentRiskSeventySubViewModel
                {
                    Visible = x.Visible,
                    RPMId = x.RPMId,
                    OId = x.OId,
                    RiskProbabilityArabic = x.td_IncidentRiskProbabilityMain.RPMNameAr,
                    RiskProbabilityEnglish = x.td_IncidentRiskProbabilityMain.RPMINameEn,
                    RiskRating = x.RiskRating,
                    RSSId = x.RSSId,
                    RSSNameAr = x.RSSNameAr,
                    RSSNameEn = x.RSSNameEn
                }).ToList();
            return model;
        }

        public int Update(IncidentRiskSeventySubModel model)
        {
            td_IncidentRiskSeventySub x = db.td_IncidentRiskSeventySub.Find(model.RSSId);
            x.RiskRating = model.RiskRating;
            x.RPMId = model.RPMId;
            x.RSSNameAr = model.RSSNameAr;
            x.RSSNameEn = model.RSSNameEn;
            db.Entry(x).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
}
