﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
public  class Assets : IAssets
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(AssetsModel model)
        {
            if (model.AsMaintenanceDuration == null)
            {
                model.AsMaintenanceDuration = 0;
            }
            td_Assets assets = new td_Assets
            {
                AnnualDepreciation=model.AnnualDepreciation,
                ASBill=model.ASBill,
                AsDescription=model.AsDescription,
                AsInstructionManual=model.AsInstructionManual,
                ASMaintenanceContract=model.ASMaintenanceContract,
                ASMaintenanceType=model.AsMaintenanceType,
                AsName=model.AsName,
                AsOId=model.AsOId,
                AsPrice=model.AsPrice,
                AsUseresType=model.AsUseresType,
                AsWarrantyImage=model.AsWarrantyImage,
                AsWarrantyPeriodDate=model.AsWarrantyPeriodDate,
                EnterDate=DateTime.Now,
                SubType=model.SubType,
                PurchaseDate=model.PurchaseDate,
                StopDeeperPrint=model.StopDeeperPrint,
                AsMaintenanceDuration=model.AsMaintenanceDuration,
                Status="New",
                Availability = true,
                Visible = true,
                
            };
            db.td_Assets.Add(assets);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int id)
        {
            td_Assets asst = db.td_Assets.Find(id);
            asst.Visible = false;
            db.Entry(asst).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int Edit(AssetsModel model)
        {
            td_Assets asst = db.td_Assets.Find(model.id);
            asst.AnnualDepreciation = model.AnnualDepreciation;
            asst.ASBill = model.ASBill;
            asst.AsDescription = model.AsDescription;
            asst.AsInstructionManual = model.AsInstructionManual;
            asst.ASMaintenanceContract = model.ASMaintenanceContract;
            asst.ASMaintenanceType = model.AsMaintenanceType;
            asst.AsName = model.AsName;
            asst.AsOId = model.AsOId;
            asst.AsPrice = model.AsPrice;
            asst.AsUseresType = model.AsUseresType;
            asst.AsWarrantyImage = model.AsWarrantyImage;
            asst.AsWarrantyPeriodDate = model.AsWarrantyPeriodDate;
            asst.SubType = model.SubType;
            asst.PurchaseDate = model.PurchaseDate;
            asst.StopDeeperPrint = model.StopDeeperPrint;
            asst.AsMaintenanceDuration = model.AsMaintenanceDuration;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public AssetsModel get(int id)
        {
            td_Assets asst = db.td_Assets.Find(id);

            AssetsModel model = new AssetsModel
            {
              id=asst.id,
              ASBill=asst.ASBill,
              AsDescription=asst.AsDescription,
              AsInstructionManual=asst.AsInstructionManual,
              ASMaintenanceContract=asst.ASMaintenanceContract,
              AsName=asst.AsName,
              AsOId=asst.AsOId,
              AsPrice=asst.AsPrice,
              AnnualDepreciation=asst.AnnualDepreciation,
              AsMaintenanceType = asst.ASMaintenanceType,
              AsWarrantyImage=asst.AsWarrantyImage,
              AsWarrantyPeriodDate=asst.AsWarrantyPeriodDate,
              EnterDate=asst.EnterDate,
          
              PurchaseDate=asst.PurchaseDate,
              Status=asst.Status,
              StopDeeperPrint=asst.StopDeeperPrint,
              SubType=asst.SubType,
              AsUseresType=asst.AsUseresType,
              Availability=asst.Availability,
              AsMaintenanceDuration = asst.AsMaintenanceDuration
        };

            return model;


        }

        public AssetsViewModel getById(int asstesId)
        {
            td_Assets asst = db.td_Assets.Find(asstesId);
            AssetsViewModel model = new AssetsViewModel {
                id = asst.id,
                ASBill = asst.ASBill,
                AsDescription = asst.AsDescription,
                AsInstructionManual = asst.AsInstructionManual,
                ASMaintenanceContract = asst.ASMaintenanceContract,
                AsName = asst.AsName,
                AsOId = asst.AsOId,
                AsPrice = asst.AsPrice,
                AsUseresType = asst.AsUseresType,
                Availability = asst.Availability,
                SubType = asst.SubType,
                Status = asst.Status,
                PurchaseDate =(DateTime)asst.PurchaseDate,
                AsMaintenanceType = (int)asst.ASMaintenanceType,
                EnterDate =(DateTime) asst.EnterDate,
                AsWarrantyPeriodDate = (DateTime)asst.AsWarrantyPeriodDate,
                AssetTypeNameArabic = asst.td_AsstesSubClassification.NameArbic,
                AssetTypeNameEnglish = asst.td_AsstesSubClassification.NameEnglish,
                AsWarrantyImage = asst.AsWarrantyImage,
                AsMaintenanceDuration = asst.AsMaintenanceDuration,
                Visible = asst.Visible
            };
            return model;
        }

        public List<AssetsViewModel> getByOId(int id)
        {
            List<AssetsViewModel> model = db.td_Assets.Where(u => u.AsOId == id && u.Visible==true)
                 .Select(asst => new AssetsViewModel {

                     id = asst.id,
                     ASBill = asst.ASBill,
                     AsDescription = asst.AsDescription,
                     AsInstructionManual = asst.AsInstructionManual,
                     ASMaintenanceContract = asst.ASMaintenanceContract,
                     AsName = asst.AsName,
                     AsOId = asst.AsOId,
                     AsPrice = asst.AsPrice,
                     AsUseresType = asst.AsUseresType,
                     Availability = asst.Availability,
                     SubType=asst.SubType,
                     Status=asst.Status,
                     PurchaseDate=(DateTime)asst.PurchaseDate,
                     AsMaintenanceType = (int)asst.ASMaintenanceType,
                     EnterDate=(DateTime)asst.EnterDate,
                     AsWarrantyPeriodDate=(DateTime)asst.AsWarrantyPeriodDate,
                     AssetTypeNameArabic=asst.td_AsstesSubClassification.NameArbic,
                     AssetTypeNameEnglish=asst.td_AsstesSubClassification.NameEnglish,
                     AsWarrantyImage=asst.AsWarrantyImage,
                     AsMaintenanceDuration = asst.AsMaintenanceDuration,
                     Visible =asst.Visible
                 }).ToList();

            return model;
        }
    }
}
