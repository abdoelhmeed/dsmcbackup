﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class EmployeesDepartmentManagment : IEmployeesDepartmentManagment
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(employeeDepartmentManagmentModel model)
        {
            td_employeeDepartmentManagment empDeptManag = new td_employeeDepartmentManagment
            {
                DepartmentId = model.DepartmentId,
                empid = model.empid,
                ManagmentId = model.ManagmentId,
                OId = model.OId,
                Visible = true
            };
            db.td_employeeDepartmentManagment.Add(empDeptManag);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_employeeDepartmentManagment edm = db.td_employeeDepartmentManagment.Find(Id);
            edm.Visible = false;
            db.Entry(edm).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Edit(employeeDepartmentManagmentModel model)
        {

            td_employeeDepartmentManagment empDeptManag = new td_employeeDepartmentManagment
            {
                DepartmentId = model.DepartmentId,
                empid = model.empid,
                ManagmentId = model.ManagmentId,
                OId = model.OId,
                Visible = true
            };
            db.td_employeeDepartmentManagment.Add(empDeptManag);
            int res = db.SaveChanges();
            if (res > 0)
            {
                td_employeeDepartmentManagment edm = db.td_employeeDepartmentManagment.Find(model.id);
                edm.Visible = false;
                db.Entry(edm).State = EntityState.Modified;
                db.SaveChanges();

                return 1;
            }
            else
            {
                return 0;
            }

        }

        public List<employeePositionModel> employeePosition(int OId)
        {
            List<employeePositionModel> model = db.td_employeeDepartmentManagment.Where(x => x.OId == OId && x.Visible == true && x.td_employee.UserId != null)
                .Select(x => new employeePositionModel
                {

                    DeparNameArabic = x.tdManag_Department.deptNameAr,
                    DeparNameEnglish = x.tdManag_Department.deptNameEn,
                    UserId = x.td_employee.UserId,
                    OId = (int)x.OId,
                    empActivationStatus = (bool)x.td_employee.empActivationStatus,
                    empBirthblace = x.td_employee.empBirthblace,
                    empBirthDate = (DateTime)x.td_employee.empBirthDate,
                    empBloodGroup = x.td_employee.empBloodGroup,
                    empid = x.td_employee.empid,
                    empImg = x.td_employee.empImg,
                    empNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                    empNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                    ManagEnglish = x.tdManag_Management.MName,
                    ManagArabic = x.tdManag_Management.MNameAr,
                    NameCareerLevelEn = x.td_employee.CareerLevel.selNameEnglish,
                    NameGenderEn = x.td_employee.Gender.selNameEnglish,
                    NameMaritalStatusEn = x.td_employee.MaritalStatus.selNameEnglish,
                    NameNationalityEn = x.td_employee.Nationality.selNameEnglish,
                    NameReligionEn = x.td_employee.Religion.selNameEnglish,
                    NameTitleEn = x.td_employee.Title.selNameEnglish,
                    NameCareerLevelAr = x.td_employee.CareerLevel.selNameArabic,
                    NameGenderAr = x.td_employee.Gender.selNameArabic,
                    NameMaritalStatusAr = x.td_employee.MaritalStatus.selNameArabic,
                    NameNationalityAr = x.td_employee.Nationality.selNameArabic,
                    NameReligionAr = x.td_employee.Religion.selNameArabic,
                    NameTitleAr = x.td_employee.Title.selNameArabic,

                }).ToList();
            return model;
        }

        public List<employeePositionModel> AllemployeePosition()
        {
            List<employeePositionModel> model = db.td_employeeDepartmentManagment.Where(x => x.Visible == true && x.td_employee.UserId != null)
                .Select(x => new employeePositionModel
                {

                    DeparNameArabic = x.tdManag_Department.deptNameAr,
                    DeparNameEnglish = x.tdManag_Department.deptNameEn,
                    UserId = x.td_employee.UserId,
                    OId = (int)x.OId,
                    empActivationStatus = (bool)x.td_employee.empActivationStatus,
                    empBirthblace = x.td_employee.empBirthblace,
                    empBirthDate = (DateTime)x.td_employee.empBirthDate,
                    empBloodGroup = x.td_employee.empBloodGroup,
                    empid = x.td_employee.empid,
                    empImg = x.td_employee.empImg,
                    empNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                    empNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                    ManagEnglish = x.tdManag_Management.MName,
                    ManagArabic = x.tdManag_Management.MNameAr,
                    NameCareerLevelEn = x.td_employee.CareerLevel.selNameEnglish,
                    NameGenderEn = x.td_employee.Gender.selNameEnglish,
                    NameMaritalStatusEn = x.td_employee.MaritalStatus.selNameEnglish,
                    NameNationalityEn = x.td_employee.Nationality.selNameEnglish,
                    NameReligionEn = x.td_employee.Religion.selNameEnglish,
                    NameTitleEn = x.td_employee.Title.selNameEnglish,
                    NameCareerLevelAr = x.td_employee.CareerLevel.selNameArabic,
                    NameGenderAr = x.td_employee.Gender.selNameArabic,
                    NameMaritalStatusAr = x.td_employee.MaritalStatus.selNameArabic,
                    NameNationalityAr = x.td_employee.Nationality.selNameArabic,
                    NameReligionAr = x.td_employee.Religion.selNameArabic,
                    NameTitleAr = x.td_employee.Title.selNameArabic,

                }).ToList();
            return model;
        }

        public employeeDepartmentManagmentModel GetById(int Id)
        {
            td_employeeDepartmentManagment edm = db.td_employeeDepartmentManagment.Find(Id);
            employeeDepartmentManagmentModel model = new employeeDepartmentManagmentModel();
            model.DepartmentId = edm.DepartmentId;
            model.empid = edm.empid;
            model.ManagmentId = edm.ManagmentId;
            model.id = edm.id;
            return model;

        }

        public List<employeeDepartmentManagmentViewModel> GetEmployeesByDepartmentId(int DId)
        {
            List<employeeDepartmentManagmentViewModel> model = db.td_employeeDepartmentManagment.Where(u => u.DepartmentId == DId && u.Visible == true)
                .Select(x => new employeeDepartmentManagmentViewModel
                {
                    DepartmentId = x.DepartmentId,
                    OId = x.OId,
                    DeparNameArabic = x.tdManag_Department.deptNameAr,
                    DeparNameEnglish = x.tdManag_Department.deptNameEn,
                    empid = x.empid,
                    ManagArabic = x.tdManag_Management.MNameAr,
                    ManagEnglish = x.tdManag_Management.MName,
                    id = x.id,
                    ManagmentId = x.ManagmentId,
                    empNameArabic = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                    empNameEnglish = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                    Visible = x.Visible
                }).ToList();

            return model;
        }

        public employeeDepartmentManagmentViewModel GetEmployeesByEmpId(int empId)
        {
            employeeDepartmentManagmentViewModel res = new employeeDepartmentManagmentViewModel();
            List<employeeDepartmentManagmentViewModel> modelList = db.td_employeeDepartmentManagment.Where(u => u.empid == empId && u.Visible == true)
             .Select(x => new employeeDepartmentManagmentViewModel
             {
                 DepartmentId = x.DepartmentId,
                 OId = x.OId,
                 DeparNameArabic = x.tdManag_Department.deptNameAr,
                 DeparNameEnglish = x.tdManag_Department.deptNameEn,
                 empid = x.empid,
                 ManagArabic = x.tdManag_Management.MNameAr,
                 ManagEnglish = x.tdManag_Management.MName,
                 id = x.id,
                 ManagmentId = x.ManagmentId,
                 empNameArabic = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                 empNameEnglish = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                 Visible = x.Visible
             }).ToList();
            foreach (var item in modelList)
            {
                res = item;
            }
            return res;
        }

        public List<employeeDepartmentManagmentViewModel> GetEmployeesByManagment(int MId)
        {
            List<employeeDepartmentManagmentViewModel> model = db.td_employeeDepartmentManagment.Where(u => u.ManagmentId == MId && u.Visible == true)
              .Select(x => new employeeDepartmentManagmentViewModel
              {
                  DepartmentId = x.DepartmentId,
                  OId = x.OId,
                  DeparNameArabic = x.tdManag_Department.deptNameAr,
                  DeparNameEnglish = x.tdManag_Department.deptNameEn,
                  empid = x.empid,
                  ManagArabic = x.tdManag_Management.MNameAr,
                  ManagEnglish = x.tdManag_Management.MName,
                  id = x.id,
                  ManagmentId = x.ManagmentId,
                  empNameArabic = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                  empNameEnglish = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                  Visible = x.Visible
              }).ToList();

            return model;
        }

        public List<employeeDepartmentManagmentViewModel> GetEmployeesByOId(int OId)
        {
            List<employeeDepartmentManagmentViewModel> model = db.td_employeeDepartmentManagment.Where(u => u.OId == OId && u.Visible == true)
            .Select(x => new employeeDepartmentManagmentViewModel
            {
                DepartmentId = x.DepartmentId,
                OId = x.OId,
                DeparNameArabic = x.tdManag_Department.deptNameAr,
                DeparNameEnglish = x.tdManag_Department.deptNameEn,
                empid = x.empid,
                ManagArabic = x.tdManag_Management.MNameAr,
                ManagEnglish = x.tdManag_Management.MName,
                id = x.id,
                ManagmentId = x.ManagmentId,
                empNameArabic = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                empNameEnglish = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                Visible = x.Visible

            }).ToList();
            return model;
        }

        public List<EmployeeSelectionModel> GetEmployeeSelections (int OId)
        {
            List<EmployeeSelectionModel> model = db.td_employee.Where(x => x.OId == OId).Select(u => new EmployeeSelectionModel
            {
                CareerLevelEnglish = u.CareerLevel.selNameEnglish,
                CareerLevelArabic = u.CareerLevel.selNameEnglish,
                OId = u.OId,
                empid = u.empid,
                NameArabic = u.empFNameAr + " " + u.empSNameAr + " " + u.empTNameAr,
                NameEnglish = u.empFNameEn + " " + u.empSNameEn + " " + u.empTNameEn,
            }).ToList();
            return model;
        }
    }
}
