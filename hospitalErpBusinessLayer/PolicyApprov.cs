﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity; 


namespace hospitalErpBusinessLayer
{
    public class PolicyApprov : IPolicyApprov
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int add(PolicyApprovModel model)
        {
            int nub = db.td_PoliciesApprovedBy.Where(x=>x.policiesNumbers==model.policiesNumbers).Count() ;

            td_PoliciesApprovedBy pApprov = new td_PoliciesApprovedBy
            {
                Approv=false,
                empgroup=nub+1,
                empId=model.empId,
                policiesNumbers=model.policiesNumbers,
                Status="New",
            };
            db.td_PoliciesApprovedBy.Add(pApprov);
           int res= db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<PolicyApproveViewModel> GetByPolicyNumber(string Number)
        {
            List<PolicyApproveViewModel> model = db.td_PoliciesApprovedBy.Where(u => u.policiesNumbers == Number).Select(x => new PolicyApproveViewModel
            {
                policiesNumbers=x.policiesNumbers,
                Approv=(bool)x.Approv,
                empgroup=(int)x.empgroup,
                empId=x.empId,
                id=x.id,
                empNameAr=x.td_employee.empFNameAr+" "+ x.td_employee.empSNameAr+" "+x.td_employee.empTNameAr,
                empNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                Status =x.Status

            }).ToList();

            return model;
        }

        public List<PolicyApproveViewModel> GetByPolicyNumberAndEmpId(string Number, int empId)
        {
            List<PolicyApproveViewModel> model = db.td_PoliciesApprovedBy.Where(u => u.policiesNumbers == Number && u.empId==empId && u.Status == "New").Select(x => new PolicyApproveViewModel
            {
                policiesNumbers = x.policiesNumbers,
                Approv = (bool)x.Approv,
                empgroup = (int)x.empgroup,
                empId = x.empId,
                id = x.id,
                empNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                empNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                Status = x.Status
            }).ToList();

            return model;
        }

        public List<PolicyForApproveViewModel> GetForApprove(int empId,int OId)
        {
            List<PolicyForApproveViewModel> model = new List<PolicyForApproveViewModel>();
            List<td_PoliciesApprovedBy> PoliciesApprovedBy = db.td_PoliciesApprovedBy.Where(x=> x.empId==empId).ToList();
            foreach (var item in PoliciesApprovedBy)
            {
                if (item.empgroup==1)
                {
                    model.Add(Policyfilters(item));
                }


                if (item.empgroup > 1 )
                {
                    int empgroup = (int)item.empgroup - 1;
                    List<td_PoliciesApprovedBy> L2 = PoliciesApprovedBy.Where(x => x.empgroup == empgroup && x.policiesNumbers == item.policiesNumbers && item.Approv==true).ToList();
                    if (L2.Count > 0)
                    {
                        foreach (var itemL2 in L2)
                        {
                            model.Add(Policyfilters(itemL2));
                        }
                    }

                }

            }

          
            return model;
        }

        public int Approve(int Id)
        {
            td_PoliciesApprovedBy AppPol = db.td_PoliciesApprovedBy.Find(Id);
            AppPol.Approv = true;
            AppPol.Status = "Approve";
            db.Entry(AppPol).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<PoliciesViewModel> PublishPolicy(int OId)
        {
            List<PolicyForApproveViewModel> AppPolListModel = new List<PolicyForApproveViewModel>();
            List<PoliciesViewModel> model = new List<PoliciesViewModel>();
            List<td_Policies> policy = db.td_Policies.Where(x => x.OId == OId ).ToList();
            foreach (var item in policy)
            {
                if(ChedApprov(item))
                {
                    PoliciesViewModel Pol = new PoliciesViewModel
                    {
                        CreateByEmpId = item.CreateByEmpId,
                        EnterDate = item.EnterDate,
                        isPublish = item.isPublish,
                        OId = item.OId,
                        PolBody = item.PolBody,
                        PolEffectiveDate=item.PolEffectiveDate,
                        policiesNumbers=item.policiesNumbers,
                        PolPdf=item.PolPdf,
                        polTitle=item.polTitle,
                        Status=item.Status,
                        TId=item.TId,
                        Visible=item.Visible,
                        CreateByEmpNameAr=item.td_employee.empFNameAr +" "+ item.td_employee.empSNameAr+" "+ item.td_employee.empTNameAr,
                        CreateByEmpNameEn= item.td_employee.empFNameEn+" "+item.td_employee.empSNameEn+" "+item.td_employee.empTNameEn,
                        polTypeAr=item.td_PoliciesType.NameAr,
                        polTypeEn=item.td_PoliciesType.NameEn
                    };

                    model.Add(Pol);
                }

            }
            return model;
        }

        private bool ChedApprov(td_Policies item)
        {
            List<td_PoliciesApprovedBy> AppPolList = db.td_PoliciesApprovedBy.Where(x => x.policiesNumbers == item.policiesNumbers).OrderBy(u => u.policiesNumbers).ToList();
            foreach (var AppPolItem in AppPolList)
            {
                if (AppPolItem.Approv == false)
                {
                    return false;
                }
               
            }

            return true;
        }

        public PolicyForApproveViewModel Policyfilters(td_PoliciesApprovedBy filter)
        {
            PolicyForApproveViewModel model = new PolicyForApproveViewModel();
            model.Approv = (bool)filter.Approv;
            model.empId = filter.empId;
            model.empgroup = (int)filter.empgroup;
            model.empNameAr = filter.td_employee.empFNameAr + " " + filter.td_employee.empSNameAr + " " + filter.td_employee.empTNameAr;
            model.empNameEn = filter.td_employee.empFNameEn + " " + filter.td_employee.empSNameEn + " " + filter.td_employee.empTNameEn;
            model.id = filter.id;
            model.PolEffectiveDate = (DateTime)filter.td_Policies.PolEffectiveDate;
            model.policiesNumbers = filter.policiesNumbers;
            model.PolPdf = filter.td_Policies.PolPdf;
            model.Status = filter.Status;
            model.Titel = filter.td_Policies.polTitle;
            return model;
        }
    }
}
