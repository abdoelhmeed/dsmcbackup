﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class EmployeeLicenses : IEmployeeLicenses
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddEmployeeLicenses(EmployeeLicensesModel model)
        {
            td_empLicenses emp = new td_empLicenses
            {
                LEndDate=model.LEndDate,
                empId=model.empId,
                Limg=model.Limg,
                LName=model.LName,
                LPlace=model.LPlace,
                LStartDate=model.LStartDate,
                LValidity=true,
                Visible=true,
            };
           db.td_empLicenses.Add(emp);
           int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int DelEmployeeLicenses(int Id)
        {
            td_empLicenses emp = db.td_empLicenses.Find(Id);
            emp.Visible = false;
            db.Entry(emp).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public List<EmployeeLicensesViewsModel> EmployeeLicensesByEmployeeId(int empId)
        {
            List<EmployeeLicensesViewsModel> model = db.td_empLicenses.Where(x => x.empId == empId).Select(u => new EmployeeLicensesViewsModel {
                empId = u.empId,
                empNameAr = u.td_employee.empFNameAr + u.td_employee.empSNameAr + u.td_employee.empTNameAr,
                empNameEn = u.td_employee.empFNameEn + u.td_employee.empSNameEn + u.td_employee.empTNameEn,
                LEndDate = u.LEndDate,
                Limg = u.Limg,
                LName = u.LName,
                LPlace = u.LPlace,
                LStartDate = u.LStartDate,
                LValidity = u.LValidity,
                Lid = u.Lid,
                NameAr=u.td_selectionDetails.selNameArabic,
                NameEn=u.td_selectionDetails.selNameEnglish,
                PlaceAr=u.td_selectionDetails1.selNameArabic,
                PlaceEn=u.td_selectionDetails1.selNameEnglish,
                Visible=(bool)u.Visible
            }).ToList();

            return model;
        }


       

        public EmployeeLicensesViewsModel EmployeeLicensesById(int Id)
        {
            td_empLicenses emp = db.td_empLicenses.Find(Id);
            string EmpNameAr = emp.td_employee.empFNameAr + emp.td_employee.empSNameAr + emp.td_employee.empTNameAr;
            string EmpNameEn = emp.td_employee.empFNameEn + emp.td_employee.empSNameEn + emp.td_employee.empTNameEn;
            EmployeeLicensesViewsModel model = new EmployeeLicensesViewsModel
            {
                empId=emp.empId,
                empNameEn= EmpNameEn,
                empNameAr = EmpNameAr,
                LEndDate=emp.LEndDate,
                Limg=emp.Limg,
                LName=emp.LName,
                LPlace=emp.LPlace,
                LStartDate=emp.LStartDate,
                LValidity=emp.LValidity,
                Lid=emp.Lid
            };

            return model;
        }

        public EmployeeLicensesModel EmployeeLicensesByIdForUpdate(int Id)
        {
            td_empLicenses m = db.td_empLicenses.Find(Id);
            EmployeeLicensesModel model = new EmployeeLicensesModel
            {
                empId=m.empId,
                LEndDate=m.LEndDate,
                Lid=m.Lid,
                Limg=m.Limg,
                LName=m.LName,
                LPlace=m.LPlace,
                LStartDate=m.LStartDate,
                LValidity=m.LValidity,
                Visible=(bool)m.Visible
                
            };

            return model;
        }

        public List<EmployeeLicensesViewsModel> EmployeeLicensesByOrganizationId(int OId)
        {
            List<EmployeeLicensesViewsModel> model = db.td_empLicenses.Where(x => x.td_employee.OId == OId).Select(u => new EmployeeLicensesViewsModel
            {
                empId = u.empId,
                empNameAr = u.td_employee.empFNameAr +" "+ u.td_employee.empSNameAr +" "+ u.td_employee.empTNameAr,
                empNameEn = u.td_employee.empFNameEn +" "+ u.td_employee.empSNameEn +" " +u.td_employee.empTNameEn,
                LEndDate = u.LEndDate,
                Limg = u.Limg,
                LName = u.LName,
                LPlace = u.LPlace,
                LStartDate = u.LStartDate,
                LValidity = u.LValidity,
                Lid = u.Lid,
                NameAr = u.td_selectionDetails.selNameArabic,
                NameEn = u.td_selectionDetails.selNameEnglish,
                PlaceAr = u.td_selectionDetails1.selNameArabic,
                PlaceEn = u.td_selectionDetails1.selNameEnglish,
                Visible = (bool)u.Visible
            }).ToList();

            return model;
        }

        public int EmployeeLicensesValidity(int Lid)
        {
            td_empLicenses Licenses = db.td_empLicenses.Find(Lid);
            Licenses.LValidity = false;
            db.Entry(Licenses).State = EntityState.Modified;
            int res= db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int UpdateEmployeeLicenses(EmployeeLicensesModel model)
        {
            td_empLicenses emp = db.td_empLicenses.Find(model.Lid);
            emp.Limg = model.Limg;
            emp.LStartDate = model.LStartDate;
            emp.LEndDate = model.LEndDate;
            emp.LName = model.LName;
            emp.LPlace = model.LPlace;
            db.Entry(emp).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
