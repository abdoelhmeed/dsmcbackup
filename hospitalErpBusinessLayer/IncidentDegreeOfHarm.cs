﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class IncidentDegreeOfHarm : IIncidentDegreeOfHarm
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(IncidentDegreeOfHarmModel model)
        {
            td_IncidentDegreeOfHarm Harm = new td_IncidentDegreeOfHarm
            {
                DHNameAr=model.DHNameAr,
                DHNameEn=model.DHNameEn,
                OId=model.OId,
                Visible=true
            };
            db.td_IncidentDegreeOfHarm.Add(Harm);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_IncidentDegreeOfHarm Harm = db.td_IncidentDegreeOfHarm.Find(Id);
            Harm.Visible = false;
            db.Entry(Harm).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public IncidentDegreeOfHarmModel GetById(int Id)
        {
            td_IncidentDegreeOfHarm x = db.td_IncidentDegreeOfHarm.Find(Id);
            IncidentDegreeOfHarmModel model = new IncidentDegreeOfHarmModel
            {
                DHId=x.DHId,
                DHNameAr=x.DHNameAr,
                DHNameEn=x.DHNameEn,
                OId=x.OId,
                Visible=x.Visible
            };
            return model;
        }

        public List<IncidentDegreeOfHarmModel> GetByOId(int OId)
        {
            List<IncidentDegreeOfHarmModel> model = db.td_IncidentDegreeOfHarm.Where(u => u.Visible == true && u.OId == OId).
                Select(x => new IncidentDegreeOfHarmModel
                {
                    DHId = x.DHId,
                    DHNameAr = x.DHNameAr,
                    DHNameEn = x.DHNameEn,
                    OId = x.OId,
                    Visible = x.Visible
                }).ToList();
            return model;
        }

        public int Update(IncidentDegreeOfHarmModel model)
        {
            td_IncidentDegreeOfHarm x = db.td_IncidentDegreeOfHarm.Find(model.DHId);
            x.DHNameAr = model.DHNameAr;
            x.DHNameEn = model.DHNameEn;
            db.Entry(x).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
}
