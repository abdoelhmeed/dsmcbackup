﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class MaintenOrder : IMaintenOrder
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(MaintenOrderModel model)
        {
            MaintenanceOrder MOrder = new MaintenanceOrder
            {
                asstesId=model.asstesId,
                empId=model.empId,
                MOrderDate=DateTime.Now,
                OrderNotes=model.OrderNotes,
                OrderStuts=model.OrderStuts,
                OrderType=model.OrderType,
                Done=false,
                OId=model.OId,
                Visible=true
            };
            db.MaintenanceOrders.Add(MOrder);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int Delete(int Id)
        {
            MaintenanceOrder MOrder = db.MaintenanceOrders.Find(Id);
            MOrder.Visible = false;
            db.Entry(MOrder).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public MaintenOrderViewModel GetById(int Id)
        {
            MaintenanceOrder mOrder = db.MaintenanceOrders.Find(Id);
            MaintenOrderViewModel model = new MaintenOrderViewModel();
            model.asstesId = mOrder.asstesId;
            model.Done = mOrder.Done;
            model.empId = mOrder.empId;
            model.employeeNameAr = mOrder.td_employee.empFNameAr + " " + mOrder.td_employee.empSNameAr + " " + mOrder.td_employee.empTNameAr;
            model.employeeNameEn = mOrder.td_employee.empFNameEn + " " + mOrder.td_employee.empSNameEn + " " + mOrder.td_employee.empTNameEn;
            model.MOrderDate = (DateTime)mOrder.MOrderDate;
            model.OrderNotes = mOrder.OrderNotes;
            model.OrderStuts = mOrder.OrderStuts;
            model.OrderType = mOrder.OrderType;
            model.asstesName = mOrder.td_Assets.AsName;
            model.id = mOrder.id;

            return model;

        }

        public List<MaintenOrderViewModel> GetByOEmpId(int empId)
        {
            List<MaintenOrderViewModel> model = db.MaintenanceOrders.Where(u => u.empId == empId 
            && u.Visible == true).Select(x =>new MaintenOrderViewModel
            {
                asstesId=x.asstesId,
                empId=x.empId,
                Done=x.Done,
                employeeNameAr= x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                employeeNameEn= x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                id=x.id,
                MOrderDate=(DateTime)x.MOrderDate,
                OrderNotes=x.OrderNotes,
                OrderStuts=x.OrderStuts,
                OrderType=x.OrderType,
                strMaintenanceOrderDate=x.MOrderDate.ToString(),
                asstesName = x.td_Assets.AsName
            }).ToList();

            return model;
        }


        public List<MaintenOrderViewModel> GetByAssteId(int assetsId)
        {
            List<MaintenOrderViewModel> model = db.MaintenanceOrders.Where(u => u.asstesId == assetsId
            && u.Visible == true).Select(x => new MaintenOrderViewModel
            {
                asstesId = x.asstesId,
                empId = x.empId,
                Done = x.Done,
                employeeNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                employeeNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                id = x.id,
                MOrderDate = (DateTime)x.MOrderDate,
                OrderNotes = x.OrderNotes,
                OrderStuts = x.OrderStuts,
                OrderType = x.OrderType,
                strMaintenanceOrderDate = x.MOrderDate.ToString(),
                asstesName = x.td_Assets.AsName
            }).ToList();

            return model;
        }

        public List<MaintenOrderViewModel> GetByOId(int OId)
        {
               List<MaintenOrderViewModel> model = db.MaintenanceOrders.Where(u => u.OId == OId
               && u.Visible == true).Select(x => new MaintenOrderViewModel
               {
                   asstesId = x.asstesId,
                   empId = x.empId,
                   Done = x.Done,
                   employeeNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                   employeeNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                   id = x.id,
                   MOrderDate = (DateTime)x.MOrderDate,
                   OrderNotes = x.OrderNotes,
                   OrderStuts = x.OrderStuts,
                   OrderType = x.OrderType,
                   strMaintenanceOrderDate = x.MOrderDate.ToString(),
                   asstesName = x.td_Assets.AsName
        }).ToList();

            return model;
        }

        public int Update(MaintenOrderModel model)
        {
            throw new NotImplementedException();
        }

        public int Refusal(MaintenOrderViewModel model)
        {
            MaintenOrderViewModel OrderModel= GetById(model.id);
            MaintenanceOrder MOrder = db.MaintenanceOrders.Find(model.id);

            string Notes = model.OrderNotes +Environment.NewLine+ " - Refusal By "+ OrderModel.employeeNameEn+ Environment.NewLine+ " - Refusal Date is " + DateTime.Now.ToShortDateString();
            MOrder.OrderNotes = Notes;
            MOrder.OrderStuts = "Refusal";
            MOrder.Done = true;
            db.Entry(MOrder).State = EntityState.Modified;
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public List<MaintenOrderViewModel> GetByDeptId(int DeptId)
        {
            List<MaintenOrderViewModel> model = new List<MaintenOrderViewModel>();
            List<int?> ListOfasstesId = db.td_DepartmentAsstes.Where(x=>x.DeptId==DeptId && x.Visible==true).Select(u=>u.AsId).ToList();
            foreach (var item in ListOfasstesId)
            {
                List<MaintenOrderViewModel> MaintenOrderList = db.MaintenanceOrders.Where(u => u.asstesId == item
                         && u.Visible == true).Select(x => new MaintenOrderViewModel
                         {
                             asstesId = x.asstesId,
                             empId = x.empId,
                             Done = x.Done,
                             employeeNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                             employeeNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                             id = x.id,
                             MOrderDate = (DateTime)x.MOrderDate,
                             OrderNotes = x.OrderNotes,
                             OrderStuts = x.OrderStuts,
                             OrderType = x.OrderType,
                             strMaintenanceOrderDate = x.MOrderDate.ToString(),
                             asstesName = x.td_Assets.AsName
                         }).ToList();
                model.AddRange(MaintenOrderList);
            }
          

            return model;
        }
    }
}
