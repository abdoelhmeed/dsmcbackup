﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;

namespace hospitalErpBusinessLayer
{
    public class UpdateNotifications : IUpdateNotification
    {
        erpHospitalEntities db = new erpHospitalEntities();

        public int Add()
        {
            td_UpdateNotification UpdateNotification = new td_UpdateNotification
            {
                UpdatesNotification=DateTime.Now

            };
            db.td_UpdateNotification.Add(UpdateNotification);
            db.SaveChanges();
            return 1;
        }

        public int GetNumberByDate()
        {
            DateTime date = DateTime.Now;
            List<td_UpdateNotification> model = db.td_UpdateNotification.Where(x=>x.UpdatesNotification.Day== date.Day && x.UpdatesNotification.Month==date.Month && x.UpdatesNotification.Year == date.Year).ToList();
            return model.Count;
        }
    }
}
