﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;

namespace hospitalErpBusinessLayer
{
   public class SelectionDetailsList : ISelectionDetailsList
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public List<SelectionDetailsModel> SelectionListArabic(int stId)
        {
            List<SelectionDetailsModel> model = db.td_selectionDetails.Where(u => u.sTId == stId).Select(x => new SelectionDetailsModel
            {
                sTId=x.sTId,
                id=x.id,
                selNameArabic=x.selNameArabic,
                selNameEnglish=x.selNameEnglish
            }).ToList();

            return model;
        }

        public List<SelectionDetailsModel> SelectionListEnglis(int stId)
        {
            List<SelectionDetailsModel> model = db.td_selectionDetails.Where(u => u.sTId == stId).Select(x => new SelectionDetailsModel
            {
                sTId = x.sTId,
                id = x.id,
                selNameArabic = x.selNameArabic,
                selNameEnglish=x.selNameEnglish
            }).ToList();
            return model;
        }
    }
}
