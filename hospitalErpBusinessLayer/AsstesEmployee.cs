﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
  public  class AsstesEmployee : IAsstesEmployee
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int add(AsstesEmployeeModel model)
        {
            td_EmployeeAsstes EmpAsstes = new td_EmployeeAsstes
            {
                AsempEntryDate=DateTime.Now,
                AsEmpId=model.AsEmpId,
                AsempMaintenanceDateReq= DateTime.Now,
                AsEmpReceivedDate=model.AsEmpReceivedDate,
                AsId=model.AsId,
                StatusConsumption="NeW",
                Visible=true,
                OId=model.OId,
                NotificationBefore=0
            };
            db.td_EmployeeAsstes.Add(EmpAsstes);
          int res=  db.SaveChanges();
            if (res > 0)
            {
                td_Assets asst = db.td_Assets.Find(EmpAsstes.AsId);
                asst.Availability = false;
                db.Entry(asst).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
            
        }

        public int Delete(int id)
        {
            td_EmployeeAsstes EmpAsstes = db.td_EmployeeAsstes.Find(id);
            EmpAsstes.Visible = false;
            db.Entry(EmpAsstes).State = EntityState.Modified;
           int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public AsstesEmployeeModel get(int Id)
        {
            td_EmployeeAsstes EmpAsstes = db.td_EmployeeAsstes.Find(Id);
            AsstesEmployeeModel model = new AsstesEmployeeModel();
            model.AsempEntryDate =(DateTime) EmpAsstes.AsempEntryDate;
            model.AsEmpId = model.AsEmpId;
            model.AsempMaintenanceDateReq =(DateTime)EmpAsstes.AsempMaintenanceDateReq;
            model.AsEmpReceivedDate =(DateTime) EmpAsstes.AsEmpReceivedDate;
            model.AsId =(int) EmpAsstes.AsId;
            model.StatusConsumption = EmpAsstes.StatusConsumption;
            return model;
        }

        public AsstesEmployeeViewModel getById(int id)
        {
            td_EmployeeAsstes EmpAsstes = db.td_EmployeeAsstes.Find(id);
            AsstesEmployeeViewModel model = new AsstesEmployeeViewModel();
            model.AsempEntryDate = (DateTime)EmpAsstes.AsempEntryDate;
            model.AsEmpId = (int)EmpAsstes.AsEmpId;
            model.AsempMaintenanceDateReq = (DateTime)EmpAsstes.AsempMaintenanceDateReq;
            model.AsEmpReceivedDate =(DateTime) EmpAsstes.AsEmpReceivedDate;
            model.AsId =(int) EmpAsstes.AsId;
            model.StatusConsumption = EmpAsstes.StatusConsumption;
            model.AsDescription = EmpAsstes.td_Assets.AsDescription;
            model.strAsempEntryDate = EmpAsstes.AsempEntryDate.ToString();
            model.strAsempMaintenanceDateReq = EmpAsstes.AsempMaintenanceDateReq.ToString();
            model.strAsEmpReceivedDate = EmpAsstes.AsempMaintenanceDateReq.ToString();
            model.id = EmpAsstes.id;
            model.AsName = EmpAsstes.td_Assets.AsName;
            model.EmpNameArabic = EmpAsstes.td_employee.empFNameAr + " " + EmpAsstes.td_employee.empSNameAr + " " + EmpAsstes.td_employee.empTNameAr;
            model.EmpNameEnglis= EmpAsstes.td_employee.empFNameEn + " " + EmpAsstes.td_employee.empSNameEn + " " + EmpAsstes.td_employee.empTNameEn;
            return model;
        }

        public List<AsstesEmployeeViewModel> getByOId(int Oid)
        {
            List<td_employee> model = db.td_employee.Where(x=>x.OId == Oid).ToList();
            List<td_EmployeeAsstes> EmpAsstes = new List<td_EmployeeAsstes>();
            List<AsstesEmployeeViewModel> modelAsste = new List<AsstesEmployeeViewModel>();
            foreach (var item in model)
            {
                EmpAsstes = db.td_EmployeeAsstes.Where(x => x.AsEmpId == item.empid && x.Visible==true&&x.td_Assets.Availability==true).ToList();
                foreach (var itemAssets in EmpAsstes)
                {
                    AsstesEmployeeViewModel asstesEmployeeViewModel = new AsstesEmployeeViewModel
                    {
                        AsempEntryDate = (DateTime)itemAssets.AsempEntryDate,
                        AsEmpId = (int)itemAssets.AsEmpId,
                        AsempMaintenanceDateReq = (DateTime)itemAssets.AsempMaintenanceDateReq,
                        AsEmpReceivedDate = (DateTime)itemAssets.AsEmpReceivedDate,
                        AsId = (int)itemAssets.AsId,
                        StatusConsumption = itemAssets.StatusConsumption,
                        AsDescription = itemAssets.td_Assets.AsDescription,
                        strAsempEntryDate = itemAssets.AsempEntryDate.ToString(),
                        strAsempMaintenanceDateReq = itemAssets.AsempMaintenanceDateReq.ToString(),
                        strAsEmpReceivedDate = itemAssets.AsempMaintenanceDateReq.ToString(),
                        id = itemAssets.id,
                        AsName = itemAssets.td_Assets.AsName,
                        EmpNameArabic = itemAssets.td_employee.empFNameAr + " " + itemAssets.td_employee.empSNameAr + " " + itemAssets.td_employee.empTNameAr,
                       EmpNameEnglis = itemAssets.td_employee.empFNameEn + " " + itemAssets.td_employee.empSNameEn + " " + itemAssets.td_employee.empTNameEn

                };
                    modelAsste.Add(asstesEmployeeViewModel);

                }

            }
            return modelAsste;
        }

        public List<AsstesEmployeeViewModel> getByEmpId(int EmpId)
        {
            List<AsstesEmployeeViewModel> model = db.td_EmployeeAsstes.Where(x => x.AsEmpId == EmpId && x.Visible == true && x.td_Assets.Availability == false).Select(
                u=> new AsstesEmployeeViewModel
                {
                    AsempEntryDate = (DateTime)u.AsempEntryDate,
                    AsEmpId = (int)u.AsEmpId,
                    AsempMaintenanceDateReq = (DateTime)u.AsempMaintenanceDateReq,
                    AsEmpReceivedDate = (DateTime)u.AsEmpReceivedDate,
                    AsId = (int)u.AsId,
                    StatusConsumption = u.StatusConsumption,
                    AsDescription = u.td_Assets.AsDescription,
                    strAsempEntryDate = u.AsempEntryDate.ToString(),
                    strAsempMaintenanceDateReq = u.AsempMaintenanceDateReq.ToString(),
                    strAsEmpReceivedDate = u.AsempMaintenanceDateReq.ToString(),
                    id = u.id,
                    AsName = u.td_Assets.AsName,
                    EmpNameArabic = u.td_employee.empFNameAr + " " + u.td_employee.empSNameAr + " " + u.td_employee.empTNameAr,
                    EmpNameEnglis = u.td_employee.empFNameEn + " " + u.td_employee.empSNameEn + " " + u.td_employee.empTNameEn

                }
                ).ToList();

            return model;

        }

        public int Update(AsstesEmployeeModel model)
        {
            td_EmployeeAsstes EmpAsstes = db.td_EmployeeAsstes.Find(model.id);
            EmpAsstes.AsempMaintenanceDateReq = model.AsempMaintenanceDateReq;
            EmpAsstes.AsEmpReceivedDate = model.AsEmpReceivedDate;
            EmpAsstes.AsId = model.AsId;
            db.Entry(EmpAsstes).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
