﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
  public  interface IPolicy
  {
        int add(PoliciesModel model);
        int Update(PoliciesUpdateModel model);
        PoliciesModel GetByNumber(string Number);
        PoliciesViewModel GetPolicyByNumber(string Number);
        List<PoliciesViewModel> GetPolicies(int OId);
        int PublishEmployee(string Number);




    }
}
