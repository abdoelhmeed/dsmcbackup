﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IMaintenanceHand
    {
        int add(MaintenanceHandModel model);
        int Update(MaintenanceHandModel model);
        int delete(int Id);
        MaintenanceHandModel GetUpdateById(int Id);

        int CompleteMaintenance(int Id);

        MaintenanceHandViewModel GetById(int Id);
        List<MaintenanceHandViewModel> GetByAsstesId(int asstesId);
        List<MaintenanceHandViewModel> GetByOrderId(int orderId);
        List<MaintenanceHandViewModel> GetByOId(int OId);

    }
}
