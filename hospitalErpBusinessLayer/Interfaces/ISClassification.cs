﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
  public  interface ISClassification
    {
        int Add(SClassificationModel model);
        int Update(SClassificationModel model);
        int Delete(int Id);
        SClassificationModel GetBySubId(int Id);
        List<SClassificationViewModel> GetByOId(int Id);
        List<SClassificationViewModel> GetByMainId(int Id);

    }
}
