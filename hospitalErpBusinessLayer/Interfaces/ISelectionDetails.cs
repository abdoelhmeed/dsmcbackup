﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface ISelectionDetails
   {
        int AddSelectionDetail(SelectionDetailsModel model);
        int UPdateSelectionDetail(SelectionDetailsModel model);
        int DelSelectionDetail(int id);
        SelectionDetailsModel GetByID(int id);
        List<SelectionDetailsModel> GetByTypeID(int sTId);
    }
}
