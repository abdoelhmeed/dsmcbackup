﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public  interface IEmployeeRequestsNodes
    {
        int Add(EmployeeRequestsNodesModel model);
        int Update(EmployeeRequestsNodesModel model);
        EmployeeRequestsNodesModel GetById(int Id);
        List<EmployeeRequestsNodesViewModel> GetForApprove(int empId);
        List<EmployeeRequestsNodesViewModel> GetByRequestId(int RequestId);
        int Approve(EmployeeRequestsNodesModel model);
    }
}
