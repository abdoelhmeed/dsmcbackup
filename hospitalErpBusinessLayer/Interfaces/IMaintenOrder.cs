﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
       public interface IMaintenOrder
       {

        int Add(MaintenOrderModel model);
        int Update(MaintenOrderModel model);
        List<MaintenOrderViewModel> GetByOEmpId(int empId);
        MaintenOrderViewModel GetById(int Id);
        List<MaintenOrderViewModel> GetByOId(int OId);
        List<MaintenOrderViewModel> GetByAssteId(int assetsId);
        List<MaintenOrderViewModel> GetByDeptId(int DeptId);
        int Delete(int Id);
        int Refusal(MaintenOrderViewModel model);

       }
}
