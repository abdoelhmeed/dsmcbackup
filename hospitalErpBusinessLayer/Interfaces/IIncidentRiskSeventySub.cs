﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IIncidentRiskSeventySub
    {
        int Add(IncidentRiskSeventySubModel model);
        int Update(IncidentRiskSeventySubModel model);
        int Delete(int Id);
        IncidentRiskSeventySubModel GetById(int Id);
        List<IncidentRiskSeventySubViewModel> GetByOId(int OId);
        List<IncidentRiskSeventySubViewModel> GetByMainType(int RPMId);


    }
}
