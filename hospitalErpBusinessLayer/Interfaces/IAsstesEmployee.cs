﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IAsstesEmployee
    {
        int add(AsstesEmployeeModel model);

        int Update(AsstesEmployeeModel model);

        int Delete(int id);

        AsstesEmployeeModel get(int Id);

        AsstesEmployeeViewModel getById(int id);

        List<AsstesEmployeeViewModel> getByOId(int Oid);

        List<AsstesEmployeeViewModel> getByEmpId(int EmpId);
    }
}
