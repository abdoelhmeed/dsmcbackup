﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IIncidentAffected
    {
        int AddStaff(IncidentAffectedStaffModel model);
        List<IncidentAffectedStaffViewModel> GetStaff(int IncidentId);
        int DeleteStaff(int Id);

        //Visitor
        int AddVisitor(IncidentAffectedVisitorModel model);
        List<IncidentAffectedVisitorModel> GetVisitors(int IncidentId);
        int DeleteVisitor(int Id);
    }
}
