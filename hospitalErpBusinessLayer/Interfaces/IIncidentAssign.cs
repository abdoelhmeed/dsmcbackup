﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IIncidentAssign
    {
        int Add(IncidentAssignModel model);
        IncidentAssignViewModel GetById(int assId);
        List<IncidentAssignForViewModel> GetByEmpId(int empId);
        List<IncidentAssignViewModel> GetForDepartmentResponsible(int DeptId);
        List<IncidentAssignViewModel> GetDepartmentInvestigation(int DeptId);

        List<IncidentAssignViewModel> GetByIncidentId(int IncidentId);

    }
}
