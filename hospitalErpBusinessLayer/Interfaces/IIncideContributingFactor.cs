﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public  interface IIncideContributingFactor
    {
        int Add(IncideContributingFactorModel model);
        IncideContributingFactorModel GetById(int CFId);
        List<IncideContributingFactorModel> GetByOId(int OId);
        int Update(IncideContributingFactorModel model);
        int Delete(int CFId);
    }
}
