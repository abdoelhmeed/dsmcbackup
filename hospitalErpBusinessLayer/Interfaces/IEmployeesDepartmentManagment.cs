﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
  public  interface IEmployeesDepartmentManagment
  {
        int Add(employeeDepartmentManagmentModel model);
        int Edit(employeeDepartmentManagmentModel model);
        int Delete(int Id);
        List<employeePositionModel> AllemployeePosition();
        employeeDepartmentManagmentModel GetById(int Id);
        List<employeeDepartmentManagmentViewModel>  GetEmployeesByOId(int OId);
        List<employeeDepartmentManagmentViewModel> GetEmployeesByManagment(int MId);
        List<employeeDepartmentManagmentViewModel> GetEmployeesByDepartmentId(int DId);
       employeeDepartmentManagmentViewModel GetEmployeesByEmpId(int empId);
       List<employeePositionModel> employeePosition(int OId);
    }
}
