﻿using hospitalErpDomainLayer;
using hospitalErpRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IPolicyApprov
    {
        int add(PolicyApprovModel model);
        List<PolicyApproveViewModel> GetByPolicyNumber(string Number);
        List<PolicyApproveViewModel> GetByPolicyNumberAndEmpId(string Number,int empId);
        List<PolicyForApproveViewModel> GetForApprove(int empId, int OId);
        PolicyForApproveViewModel Policyfilters(td_PoliciesApprovedBy filter);
        List<PoliciesViewModel> PublishPolicy(int OId);
        int Approve(int Id);
    }
}
