﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IPoliciesPlan
    {
        int Add(PolicyPlanModel model);
        int Update(PolicyPlanModel model);
        int Delete(int Id);
        PolicyPlanModel GetById(int Id);
        List<PolicyPlanViweModel> GetByTId(int TId);
        List<PolicyPlanViweModel> GetByMid(int Mid);
    }
}
