﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IInvestigator
    {
        int Add(InvestigatorModel model);
        List<InvestigatorModel> GetByIncidentId(int IncidentId);
        InvestigatorModel GetById(int InvId);

    }
}
