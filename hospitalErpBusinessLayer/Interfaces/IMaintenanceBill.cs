﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IMaintenanceBill
    {
        int Add(MaintenanceBillModel model);
        int Update(MaintenanceBillModel model);
        int detete(int Id);
        List<MaintenanceBillViewModel> GetBill(int OId);
        int ApprovalGeneralDirector(int Id );
        int ApprovalFinancial(int Id, string Notse);
        MaintenanceBillViewModel GetById(int Id);

    }
}
