﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpRepository;
using hospitalErpDomainLayer;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface ISelectionType
   {
        int AddSelectionType(SelectionTypeModel model);
        int UpdaetSelectionType(SelectionTypeModel model);
        int DelSelectionType(SelectionTypeModel model);
        SelectionTypeModel GetPyId(int Id);
        List<SelectionTypeModel> GetAll();


    }
}
