﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IEmployeeLicenses
   {
        int AddEmployeeLicenses(EmployeeLicensesModel model);
        int UpdateEmployeeLicenses(EmployeeLicensesModel model);
        int DelEmployeeLicenses(int Id);
        EmployeeLicensesModel EmployeeLicensesByIdForUpdate(int Id);
        List<EmployeeLicensesViewsModel> EmployeeLicensesByEmployeeId(int empId);
        EmployeeLicensesViewsModel EmployeeLicensesById(int Id);
        List<EmployeeLicensesViewsModel> EmployeeLicensesByOrganizationId(int OId);
        int EmployeeLicensesValidity(int Lid);
    }
}
