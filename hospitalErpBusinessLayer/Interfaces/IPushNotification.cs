﻿using hospitalErpDomainLayer;
using hospitalErpRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IPushNotification
    {
       
        int Add(PushNotificationModel model);
        int Update(int Id);

        void UpdateAllByEmpIdAndNType(int empId, string Ntype);

        List<PushNotificationModel> GetNotification(string UserId);
        


    }
}
