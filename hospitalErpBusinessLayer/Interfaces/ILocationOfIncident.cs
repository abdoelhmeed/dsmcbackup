﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public  interface ILocationOfIncident
    {
        int Add(LocationOfIncidentModel model);
        List<LocationOfIncidentModel> GetPyOId(int OId);
        LocationOfIncidentModel GetById(int Id);
        int Update(LocationOfIncidentModel model);
        int Delete(int Id);

    }
}
