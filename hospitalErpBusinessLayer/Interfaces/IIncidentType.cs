﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public  interface IIncidentType
    {
        //Mian
        int AddMain(IncidentMainTypeModel model);
        IncidentMainTypeModel GetMinById(int Id);
        List<IncidentMainTypeModel> GetMianList(int OId);
        int DeleteMain(int Id);
        int UpdateMain(IncidentMainTypeModel model);





        //Sub
        int AddSub(IncidentSubTypeModel model);
        IncidentSubTypeModel GetSubById(int Id);
        List<IncidentSubTypeModel> GetSubList(int OId);
        int DeleteSub(int Id);
        int UpdateSub(IncidentSubTypeModel model);

        List<IncidentSubTypeModel> GetSubTypeByMId(int MId);




    }
}
