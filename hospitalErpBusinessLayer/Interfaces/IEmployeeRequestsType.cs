﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IEmployeeRequestsType
    {
        int Add(EmployeeRequestsTypeModel model);
        EmployeeRequestsTypeModel GetToUpdate(int Id);
        int Update(EmployeeRequestsTypeModel model);

        int Delete(int Id);
        List<EmployeeRequestsTypeViewModel> GetByOId(int OId);

    }
}
