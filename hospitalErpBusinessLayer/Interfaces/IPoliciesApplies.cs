﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
  public  interface IPoliciesApplies
    {
        int add(PoliciesAppliesToModel model);
        PoliciesAppliesToViewModel GetById(int Id);
        List<PoliciesAppliesToViewModel> GetByPolicyNumbers(string Number);
    }
}
