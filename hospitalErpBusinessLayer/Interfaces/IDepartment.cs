﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IDepartment
   {
        int AddDept(DepartmentModel model);
       
        int DelDept(int id);
        int UpdateDept(DepartmentModel model);
        DepartmentModel getById(int id);
        List<DepartmentViewModel> GetAll();
        List<DepartmentViewModel> GetAllByOId(int OId);
        List<DepartmentViewModel> GetDeptByMId(int Mid);
        List<EmployeeViewModel> GetDepartmentManagers(int DeptId);


    }
}
