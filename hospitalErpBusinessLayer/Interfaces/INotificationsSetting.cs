﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public  interface INotificationsSetting
    {


      int  Add(NotificationsSettingMode model);
      int Delete(int Id);
      List<NotificationsViewSettingMode> GetByOId(int OId);

    }
}
