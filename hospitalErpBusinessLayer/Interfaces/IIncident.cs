﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
     public interface IIncident
     {
        int Add(IncidentsModel model);
        List<IncidentViewModel> GetIncidentByOId(int OId);
        IncidentViewModel GetById(int Id);
        int SubmitIncident(int Id);


    }
}
