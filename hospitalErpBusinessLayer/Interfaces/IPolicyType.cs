﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IPolicyType
    {
        int add(PoliciesTypeModel model);
        PoliciesTypeModel getById(int Id);
        int Update(PoliciesTypeModel PT);
        List<PoliciesTypeViewModel> getByOId(int OId);
    }
}
