﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IEmployeeQualifications
   {
        int addEmpeQualificat(EmployeeQualificationsModel model);
        int UpdateEmpeQualificat(EmployeeQualificationsModel model);
        int DelEmpeQualificat(int id);
        EmployeeQualificationsModel GetEmpeQualificatById(int id);
        List<EmployeeQualificationsViewModel> GetEmpQualificatByEmpId(int empId);
    }
}
