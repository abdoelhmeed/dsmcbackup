﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
 public interface IMClassification
  {
        int Add(MClassificationModel model);
        int Update(MClassificationModel model);
        int Delete(int id);
       List<MClassificationViewModel> GetAll(int OId);
        MClassificationModel GetById(int Id);

    }
}
