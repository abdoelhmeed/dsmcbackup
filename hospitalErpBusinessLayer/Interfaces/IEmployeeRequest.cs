﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
  public  interface IEmployeeRequest
  {
        int Add(EmployeeRequestModel model);
        int Update(EmployeeRequestModel model);
        EmployeeRequestModel GetById(int Id);
        List<EmployeeRequestViweModel> GetByempId(int empId);
    }
}
