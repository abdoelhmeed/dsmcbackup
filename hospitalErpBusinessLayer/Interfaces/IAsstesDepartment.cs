﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IAsstesDepartment
    {
        int Add(AsstesDepartmentModel model);
        int Update(AsstesDepartmentModel model);
        int Delete(int Id);
        AsstesDepartmentModel getById(int Id);
       List<AsstesDepartmentViewModel> getByOId(int Id);
       List<AsstesDepartmentViewModel> getByDeptID(int dept);

        List<AsstesDepartmentViewModel> GetForSystemOrderMaintenance();

        List<AsstesDepartmentModel> GetByAId(int AId);
    }
}
