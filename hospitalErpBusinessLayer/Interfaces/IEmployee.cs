﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IEmployee
    {
        int AddEmployee(EmployeeModel model);
        int DelEmployee(int Id);
        int UpdateEmployee(EmployeeModel model);
        EmployeeViewModel GetEmployeeById(int id);
        EmployeeViewModel GetEmployeeByUserId(string userId);
        List<EmployeeViewModel> GetAllEmployee();
        EmployeeModel GetFoeEditById(int id);
       int UpdateLoginEmployee(int id, string UserId);
        List<EmployeeSelectionModel> GetEmployeeSelectioByOId(int OId);
        int GetOIdByUserId(string userId);
        int Edit(EmployeeModel model);
        int GetEmpIDByUserId(string userId);

        List<EmployeeViewModel> GetEmployeeByOId(int OId);

    }
}
