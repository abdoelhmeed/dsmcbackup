﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IEmployeeIds
   {
        int AddIds(EmployeeIdsModel model);
        int UpdateIds(EmployeeIdsModel model);
        int DeleteIds(int id);
        EmployeeIdsViewsModel GegIds(int id);
        List<EmployeeIdsViewsModel> GegListIds(int empId);


    }
}
