﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
  public interface IOrganization
  {

        int AddNewOrganization(OrganizationModel model);
        int DelNewOrganization(int id);
        int UpdateNewOrganization(OrganizationModel model);
        int AddNewOrganizationMangers(OrganizationModel model);
        OrganizationModel GetOrganizationById(int id);
        OrganizationViewModel GetAllOrganizationBy(int Id);
        List<OrganizationViewModel> GetAllOrganization();
    }
}
