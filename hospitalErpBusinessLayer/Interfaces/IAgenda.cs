﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IAgenda
   {
        int Add(AgendaModel model);
        List<AgendaViewModel> GetAll(int OId);

        List<AgendaViewModel> GetEmployeeFor(int empFor);

        AgendaViewEmployeeModel GetById(int AId, int NId);
    }
}
