﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IIncidentDegreeOfHarm
    {
        int Add(IncidentDegreeOfHarmModel model);
        int Update(IncidentDegreeOfHarmModel model);
        int Delete(int Id);
        IncidentDegreeOfHarmModel GetById(int Id);
        List<IncidentDegreeOfHarmModel> GetByOId(int OId);
    }
}
