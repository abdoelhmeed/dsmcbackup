﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IOrganizationLicenses
    {
        int AddOrganizationLicenses(OrganizationLicensesModel model);
        int UpdateOrganizationLicenses(OrganizationLicensesModel OL);
        int DeleteOrganizationLicenses(int id);
        OrganizationLicensesModel GetOrganizationLicensesById( int id);
        List<OrganizationLicensesViewsModel> GetListOrganizationLicensesByOId(int Oid);
        int OrganizationLicenseValidity(int Id);
    }
}
