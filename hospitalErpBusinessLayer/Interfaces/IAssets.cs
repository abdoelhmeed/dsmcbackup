﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
   public interface IAssets
   {
        int Add(AssetsModel model);
        int Edit(AssetsModel model);
        int Delete(int id);
        AssetsModel get(int id);
        List<AssetsViewModel> getByOId(int id);
        AssetsViewModel getById(int asstesId);

    }
}
