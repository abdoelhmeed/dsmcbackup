﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public interface IIncidentRiskProbability
    {
        int Add(IncidentRiskProbabilityModel model);
        int Update(IncidentRiskProbabilityModel model);
        IncidentRiskProbabilityModel GetById(int RPMId);
        List<IncidentRiskProbabilityModel> GetByOId(int OId);
        int Delete(int RPMId);

    }
}
