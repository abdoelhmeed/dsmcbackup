﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
    public  interface IIncidentAnalystToClose
    {
        int Add(IncidentAnalystToCloseModel model);
        List<AnalystToCloseViewModel> GetByOId(int OId);
        List<AnalystToCloseViewModel> ChartGetByOId(int OId);
        List<GetCharIncidentByRiskProbabilityModel> GetCharIncidentByRiskProbability(int OId);
    }
}
