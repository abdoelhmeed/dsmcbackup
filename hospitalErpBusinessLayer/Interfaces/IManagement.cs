﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitalErpBusinessLayer.Interfaces
{
  public  interface IManagement
  {
        int AddManagement(ManagementModel model);
        int UpdateManagement(ManagementModel model);
        int DelManagement(int Id);
        ManagementModel GetManagementById(int id);
        List<ManagementViewModel> GetAllByOId(int OId);
        List<ManagementelectionModel> GetSelectionByOId(int Oid);
        List<ManagementViewModel> Get();

        //Admin
        int AddAdimn(ManagementModel model);
        int UpdateAdimn(ManagementModel model);
        List<ManagementViewModel> GetAll();
    }
}
