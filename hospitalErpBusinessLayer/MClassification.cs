﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class MClassification : IMClassification
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(MClassificationModel model)
        {
            td_AsstesMainClassification MainClassification = new td_AsstesMainClassification
            {
                NameArabic = model.NameArabic,
                NameEnglish = model.NameEnglish,
                Visible = true,
                OId=model.OId
            };

            db.td_AsstesMainClassification.Add(MainClassification);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int Delete(int id)
        {
            td_AsstesMainClassification model = db.td_AsstesMainClassification.Find(id);
            model.Visible = false;
            db.Entry(model).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<MClassificationViewModel> GetAll(int OId)
        {
            List<MClassificationViewModel> model = db.td_AsstesMainClassification.Where(x => x.Visible == true && x.OId== OId).Select(u => new MClassificationViewModel
            {
                id=u.id,
                NameArabic=u.NameArabic,
                NameEnglish=u.NameEnglish,
                ONameEnglish=u.tdMang_Organization.ONameEn,
                ONameArabic = u.tdMang_Organization.ONameAr,
                OId=u.OId
            }).ToList();

            return model;
        }

        public MClassificationModel GetById(int Id)
        {
            td_AsstesMainClassification asstes = db.td_AsstesMainClassification.Find(Id);
            MClassificationModel model = new MClassificationModel
            {
                id=asstes.id,
                NameArabic=asstes.NameArabic,
                NameEnglish=asstes.NameEnglish,
                Visible=asstes.Visible,
                OId=asstes.OId
            };

            return model;
        }

        public int Update(MClassificationModel model)
        {
            td_AsstesMainClassification asstes = db.td_AsstesMainClassification.Find(model.id);
            asstes.NameArabic = model.NameArabic;
            asstes.NameEnglish = model.NameEnglish;
            db.Entry(asstes).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
