﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;

namespace hospitalErpBusinessLayer
{
    public class MaintenanceDetail : IMaintenanceDetail
    {
        erpHospitalEntities db = new erpHospitalEntities();
       
        public List<MaintenanceDetailsModel> GetMaintenanceDetail(int asstesId)
        {
            List<MaintenanceDetailsModel> model = new List<MaintenanceDetailsModel>();
            List<td_MaintenanceAspiratBill> MaintBill = db.td_MaintenanceAspiratBill.Where(m => m.AssetsId == asstesId).ToList();
            List<MaintenanceDetailsModel> BList = new List<MaintenanceDetailsModel>();
            MaintenanceDetailsModel u = new MaintenanceDetailsModel();
            foreach (var x in MaintBill)
            {
                u.TotalPrice = x.TotalPrice;
                u.OId = (int)x.td_MaintenanceHandling.OrderMaintenanceIId;
                u.OrderNotes = x.td_MaintenanceHandling.MaintenanceOrder.OrderNotes;
                u.OMaintenanceOrderDate =x.td_MaintenanceHandling.MaintenanceOrder.MOrderDate;
                u.AspiratDetails = x.AspiratDetails;
                u.AssetsName = x.td_Assets.AsName;
                u.assetsId =(int) x.AssetsId;
                u.BId = x.id;
                u.HAspiratDetails = x.td_MaintenanceHandling.AspiratDetails;
                u.HId = x.td_MaintenanceHandling.id;
                u.HMaintenanceDetails = x.td_MaintenanceHandling.MaintenanceDetails;
                u.MaintenanceDetails = x.MaintenanceDetails;
                u.HMaintenanceEndDate = x.td_MaintenanceHandling.MaintenanceEndDate;
                u.HMaintenanceStartDate = x.td_MaintenanceHandling.MaintenanceStartDate;
                u.HStuts = x.td_MaintenanceHandling.MStuts;
                u.InvoiceImage = x.InvoiceImage;
                u.MStuts = x.MStuts;
                u.Nots = x.Nots;
                u.OrderStuts = x.td_MaintenanceHandling.MaintenanceOrder.OrderStuts;
                   model.Add(u);
            }

            List<MaintenanceDetailsModel> modelNew = model;


            foreach (var item in modelNew.ToList())
            {
                    List<MaintenanceDetailsModel> HList = db.td_MaintenanceHandling.Where(y => y.asstesId != item.assetsId && y.id!=item.HId)
                                 .Select(a => new MaintenanceDetailsModel
                                 {

                                     OId = (int)a.OrderMaintenanceIId,
                                     OrderNotes = a.MaintenanceOrder.OrderNotes,
                                     OMaintenanceOrderDate = a.MaintenanceOrder.MOrderDate,
                                     AspiratDetails = a.AspiratDetails,
                                     AssetsName = a.td_Assets.AsName,
                                     BId = a.id,
                                     HAspiratDetails = a.AspiratDetails,
                                     HId = a.id,
                                     HMaintenanceDetails = a.MaintenanceDetails,
                                     MaintenanceDetails = a.MaintenanceDetails,
                                     HMaintenanceEndDate = a.MaintenanceEndDate,
                                     HMaintenanceStartDate = a.MaintenanceStartDate,
                                     HStuts = a.MStuts,
                                     MStuts = a.MStuts,
                                     OrderStuts = a.MaintenanceOrder.OrderStuts
                                 }).ToList();
                    model.AddRange(HList);
            }

          

            return model;
        }
    }
}
