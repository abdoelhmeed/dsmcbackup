﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class MaintenanceBill : IMaintenanceBill
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(MaintenanceBillModel model)
        {

            td_MaintenanceAspiratBill MBill = new td_MaintenanceAspiratBill
            {
                AspiratDetails = model.AspiratDetails,
                AssetsId = model.AssetsId,
                
                Financial = false,
                GeneralDirector = false,
                InvoiceImage = model.InvoiceImage,
                MaintenanceDetails = model.MaintenanceDetails,
                MaintenanceHandlingId = model.MaintenanceHandlingId,
                Nots = model.Nots,
                TotalPrice = model.TotalPrice,
                Visible = true,
                MStuts = "New Request",
                MAspiratDate=DateTime.Now
            };
            db.td_MaintenanceAspiratBill.Add(MBill);
            int res=db.SaveChanges();
            if (res > 0)
            {
                td_MaintenanceHandling hand = db.td_MaintenanceHandling.Find(MBill.MaintenanceHandlingId);
                hand.MStuts = "Waiting for approval";
                db.Entry(hand).State = EntityState.Modified;
                db.SaveChanges();
                MaintenanceOrder order = db.MaintenanceOrders.Find(hand.OrderMaintenanceIId);
                order.OrderStuts = "Waiting for confirmation and Purchase of Aspirat";
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0; 
            }

        }

        public int ApprovalGeneralDirector (int Id)
        {
            td_MaintenanceAspiratBill mBill = db.td_MaintenanceAspiratBill.Find(Id);
            mBill.MStuts = "In Financial Department";
            mBill.GeneralDirector = true;
            db.Entry(mBill).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                td_MaintenanceHandling hand = db.td_MaintenanceHandling.Find(mBill.MaintenanceHandlingId);
                hand.MStuts = "In Financial Department";
                db.Entry(hand).State = EntityState.Modified;
                db.SaveChanges();
                MaintenanceOrder order = db.MaintenanceOrders.Find(hand.OrderMaintenanceIId);
                order.OrderStuts = "Financial Department";
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int ApprovalFinancial(int Id,string Notse)
        {
            td_MaintenanceAspiratBill mBill = db.td_MaintenanceAspiratBill.Find(Id);
            mBill.MStuts = "In Maintenance Department";
            mBill.Financial = true;
            mBill.Nots = Notse;
            db.Entry(mBill).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                td_MaintenanceHandling hand = db.td_MaintenanceHandling.Find(mBill.MaintenanceHandlingId);
                hand.MStuts = "In Maintenance Department";
                db.Entry(hand).State = EntityState.Modified;
                db.SaveChanges();
                MaintenanceOrder order = db.MaintenanceOrders.Find(hand.OrderMaintenanceIId);
                order.OrderStuts = "Waiting To Complete";
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
        
        public int detete(int Id)
        {
            td_MaintenanceAspiratBill mBill = db.td_MaintenanceAspiratBill.Find(Id);
            mBill.Visible = false;
            db.Entry(mBill).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<MaintenanceBillViewModel> GetBill(int OId)
        {
            List<MaintenanceBillViewModel> model = db.td_MaintenanceAspiratBill.Where(u => u.td_Assets.AsOId == OId && u.Visible == true)
                .Select(x => new MaintenanceBillViewModel
                {
                    Financial = x.Financial,
                    AssetsName = x.td_Assets.AsName,
                    AssetsId = x.AssetsId,
                    AspiratDetails = x.AspiratDetails,
                    GeneralDirector = x.GeneralDirector,
                    id = x.id,
                    InvoiceImage = x.InvoiceImage,
                    MaintenanceDetails = x.MaintenanceDetails,
                    MaintenanceHandlingId = x.MaintenanceHandlingId,
                    Nots = x.Nots,
                    MStuts=x.MStuts,
                    TotalPrice = x.TotalPrice,
                    MAspiratDate = x.MAspiratDate,
                    strMAspiratDate = x.MAspiratDate.ToString()
                }).ToList();
            return model;
        }

        public MaintenanceBillViewModel GetById(int Id)
        {
           td_MaintenanceAspiratBill x= db.td_MaintenanceAspiratBill.Find(Id);
            MaintenanceBillViewModel model = new MaintenanceBillViewModel
            {
                Financial = x.Financial,
                AssetsName = x.td_Assets.AsName,
                AssetsId = x.AssetsId,
                AspiratDetails = x.AspiratDetails,
                GeneralDirector = x.GeneralDirector,
                id = x.id,
                InvoiceImage = x.InvoiceImage,
                MaintenanceDetails = x.MaintenanceDetails,
                MaintenanceHandlingId = x.MaintenanceHandlingId,
                Nots = x.Nots,
                MStuts = x.MStuts,
                TotalPrice = x.TotalPrice,
                MAspiratDate = x.MAspiratDate,
                strMAspiratDate = x.MAspiratDate.ToString()
            };
            return model;
        }

        public int Update(MaintenanceBillModel model)
        {
            throw new NotImplementedException();
        }
    }
}
