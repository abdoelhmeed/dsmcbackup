﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class PolicyType : IPolicyType
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int add(PoliciesTypeModel model)
        {
            td_PoliciesType PT = new td_PoliciesType
            {
                description=model.description,
                EnterDate=DateTime.Now,
                MId=model.MId,
                NameAr=model.NameAr,
                NameEn=model.NameEn,
                Oid=model.Oid,
                TImg=model.TImg
            };
            db.td_PoliciesType.Add(PT);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public PoliciesTypeModel getById(int Id)
        {
            td_PoliciesType PT = db.td_PoliciesType.Find(Id);
            PoliciesTypeModel model = new PoliciesTypeModel();
            model.description = PT.description;
            model.EnterDate = PT.EnterDate;
            model.id = PT.id;
            model.MId = PT.MId;
            model.NameAr = PT.NameAr;
            model.NameEn = PT.NameEn;
            model.Oid = PT.Oid;
            model.TImg = PT.TImg;
            return model;
        }

        public List<PoliciesTypeViewModel> getByOId(int OId)
        {
            List<PoliciesTypeViewModel> model = db.td_PoliciesType.
                Where(x => x.Oid == OId)
                .Select(PT => new PoliciesTypeViewModel {
                    description = PT.description,
                    EnterDate = PT.EnterDate,
                    MId = PT.MId,
                    NameAr = PT.NameAr,
                    NameEn = PT.NameEn,
                    Oid = PT.Oid,
                    id=PT.id,
                    MNameAr=PT.tdManag_Management.MNameAr,
                    MNameEr=PT.tdManag_Management.MName,
                    TImg=PT.TImg

                }).ToList();

            return model;
        }

        public int Update(PoliciesTypeModel PT)
        {
            td_PoliciesType model = db.td_PoliciesType.Find(PT.id);
            model.description = PT.description;
            model.MId = PT.MId;
            model.NameAr = PT.NameAr;
            model.NameEn = PT.NameEn;
            if (PT.TImg != null)
            {
                model.TImg = PT.TImg;
            }
            db.Entry(model).State = EntityState.Modified;
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

    }
}
