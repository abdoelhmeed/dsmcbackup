﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class AsstesDepartment : IAsstesDepartment
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(AsstesDepartmentModel model)
        {
            if (model.NotificationBefore != 0 || model.NotificationBefore == null)
            {
                Assets astes = new Assets();
                int AsId = (int)model.AsId;
               AssetsModel assetsModel=  astes.get(AsId);
                if (assetsModel.AsMaintenanceDuration != null && assetsModel.AsMaintenanceType==1)
                {
                    int NotificationBefore = (int)assetsModel.AsMaintenanceDuration;
                    model.DeptMaintenanceDateReq = Convert.ToDateTime(model.DeptReceivedDate).AddDays(NotificationBefore);
                }
            }
           
            td_DepartmentAsstes Dept = new td_DepartmentAsstes
            {
                AsId=model.AsId,
                DeptEntryDate=DateTime.Now,
                DeptId=model.DeptId,
                DeptMaintenanceDateReq=model.DeptMaintenanceDateReq,
                DeptReceivedDate=model.DeptReceivedDate,
                StatusConsumption="New",
                NotificationBefore=model.NotificationBefore,
                Visible=true
            };
            db.td_DepartmentAsstes.Add(Dept);
            int res = db.SaveChanges();
            if (res > 0)
            {
                td_Assets asst = db.td_Assets.Find(Dept.AsId);
                asst.Availability = false;
                db.Entry(asst).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_DepartmentAsstes Dept = db.td_DepartmentAsstes.Find(Id);
            Dept.Visible = false;
            db.Entry(Dept).State = EntityState.Modified;
           int res= db.SaveChanges();
            if (res > 0)
            {
                td_Assets asst = db.td_Assets.Find(Dept.AsId);
                asst.Availability = true;
                db.Entry(asst).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<AsstesDepartmentModel> GetByAId(int AId)
        {
            List<AsstesDepartmentModel> model = db.td_DepartmentAsstes
                .Where(u=> u.AsId==AId && u.Visible==true && u.StatusConsumption== "New")
                .Select(x=> new AsstesDepartmentModel
                {
                   AsId=x.AsId,
                   DeptEntryDate=x.DeptEntryDate,
                   DeptId=x.DeptId,
                   DeptMaintenanceDateReq=x.DeptMaintenanceDateReq,
                   DeptReceivedDate=x.DeptReceivedDate,
                   id=x.id,
                   NotificationBefore=x.NotificationBefore,
                   StatusConsumption=x.StatusConsumption,
                   Visible=x.Visible,
                  
                }).ToList();
            return model;
        }

        public List<AsstesDepartmentViewModel> getByDeptID(int dept)
        {
            List<AsstesDepartmentViewModel> model = db.td_DepartmentAsstes.Where(u=>u.DeptId==dept && u.Visible==true).Select(x=> new AsstesDepartmentViewModel {
                NotificationBefore = x.NotificationBefore,
                MagaNameArabic = x.tdManag_Department.tdManag_Management.MNameAr,
                MagaNameEnglish = x.tdManag_Department.tdManag_Management.MName,
                DeptEntryDate = x.DeptEntryDate,
                id = x.id,
                AsId = x.AsId,
                AsName = x.td_Assets.AsName,
                DeptId = x.DeptId,
                DeptNameEnglish = x.tdManag_Department.deptNameEn,
                DeptNameArabic = x.tdManag_Department.deptNameAr,
                DeptMaintenanceDateReq = x.DeptMaintenanceDateReq,
                DeptReceivedDate = x.DeptReceivedDate,
                StatusConsumption = x.StatusConsumption,

            }).ToList();
            return model;
        }

        public AsstesDepartmentModel getById(int Id)
        {
           td_DepartmentAsstes dept = db.td_DepartmentAsstes.Find(Id);
            AsstesDepartmentModel model = new AsstesDepartmentModel {

                AsId=dept.AsId,
                DeptEntryDate=dept.DeptEntryDate,
                DeptId=dept.DeptId,
                DeptMaintenanceDateReq=dept.DeptMaintenanceDateReq,
                DeptReceivedDate=dept.DeptReceivedDate,
                StatusConsumption=dept.StatusConsumption,
                id=dept.id,
                NotificationBefore=dept.NotificationBefore,
                Visible=dept.Visible
            };

            return model;
        }

        public List<AsstesDepartmentViewModel> getByOId(int Id)
        {
            List<td_DepartmentAsstes> AsstItems = new List<td_DepartmentAsstes>();
            List<AsstesDepartmentViewModel> model = new List<AsstesDepartmentViewModel>();
            List<int> MIdes = db.tdManag_Management.Where(x => x.OId == Id).Select(u=> u.Mid).ToList();
            List<int> DeptIds = new List<int>();
            foreach (var item in MIdes)
            {
                List<int> DeptId = db.tdManag_Department.Where(x => x.Mid == item).Select(u=> u.deptId).ToList();
                DeptIds.AddRange(DeptId);
            }

            foreach (var item in DeptIds)
            {
                List<td_DepartmentAsstes> AsstItem = db.td_DepartmentAsstes.Where(u => u.DeptId == item && u.Visible==true && u.td_Assets.Availability==false).ToList();
                AsstItems.AddRange(AsstItem);
            }
            foreach (var item in AsstItems)
            {
                AsstesDepartmentViewModel modelItem = new AsstesDepartmentViewModel
                {
                    NotificationBefore=item.NotificationBefore,
                    MagaNameArabic=item.tdManag_Department.tdManag_Management.MNameAr,
                    MagaNameEnglish = item.tdManag_Department.tdManag_Management.MName,
                    DeptEntryDate =item.DeptEntryDate,
                    id=item.id,
                    AsId=item.AsId,
                    AsName=item.td_Assets.AsName,
                    DeptId=item.DeptId,
                    DeptNameEnglish=item.tdManag_Department.deptNameEn,
                    DeptNameArabic = item.tdManag_Department.deptNameAr,
                    DeptMaintenanceDateReq=item.DeptMaintenanceDateReq,
                    DeptReceivedDate=item.DeptReceivedDate,
                  
                    StatusConsumption=item.StatusConsumption,
                  
                };
                model.Add(modelItem);
            }
            return model;   
        }

        public  List<AsstesDepartmentViewModel> GetForSystemOrderMaintenance()
        {
                    List<AsstesDepartmentViewModel> model = db.td_DepartmentAsstes.Where(x=>x.Visible==true).Select(item=> new AsstesDepartmentViewModel {

                        NotificationBefore = item.NotificationBefore,
                        MagaNameArabic = item.tdManag_Department.tdManag_Management.MNameAr,
                        MagaNameEnglish = item.tdManag_Department.tdManag_Management.MName,
                        DeptEntryDate = item.DeptEntryDate,
                        id = item.id,
                        AsId = item.AsId,
                        AsName = item.td_Assets.AsName,
                        DeptId = item.DeptId,
                        DeptNameEnglish = item.tdManag_Department.deptNameEn,
                        DeptNameArabic = item.tdManag_Department.deptNameAr,
                        DeptMaintenanceDateReq = item.DeptMaintenanceDateReq,
                        DeptReceivedDate = item.DeptReceivedDate,
                    }).ToList();

            return model;
        }

        public int Update(AsstesDepartmentModel model)
        {
            td_DepartmentAsstes dept = db.td_DepartmentAsstes.Find(model.id);
            dept.DeptEntryDate = model.DeptEntryDate;
            dept.DeptId = model.DeptId;
            dept.DeptMaintenanceDateReq = model.DeptMaintenanceDateReq;
            dept.DeptReceivedDate = model.DeptReceivedDate;
            dept.StatusConsumption = model.StatusConsumption;
            db.Entry(dept).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
             
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
}
