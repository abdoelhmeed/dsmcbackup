﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class Employee : IEmployee
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddEmployee(EmployeeModel model)
        {
            td_employee emp = new td_employee
            {
                empActivationStatus = true,
                empBirthblace = model.empBirthblace,
                empBirthDate = model.empBirthDate,
                empBloodGroup = model.empBloodGroup,
                empCareerLevel = model.empCareerLevel,
                empFamilyNameAr = model.empFamilyNameAr,
                empFamilyNameEn = model.empFamilyNameEn,
                empFNameAr = model.empFNameAr,
                empFNameEn = model.empFNameEn,
                empGender = model.empGender,
                empImg = model.empImg,
                empMaritalStatus = model.empMaritalStatus,
                empNationality = model.empNationality,
                empNickName = model.empNickName,
                empReligion = model.empReligion,
                empSNameAr = model.empSNameAr,
                empSNameEn = model.empSNameEn,
                empTitle = model.empTitle,
                empTNameAr = model.empTNameAr,
                empTNameEn = model.empTNameEn,
                OId = model.OId


            };
            db.td_employee.Add(emp);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return emp.empid;
            }
            else
            {
                return 0;
            }

        }
        public int Edit(EmployeeModel model)
        {
            td_employee emp = db.td_employee.Find(model.empid);

            emp.empBirthblace = model.empBirthblace;
            emp.empBirthDate = model.empBirthDate;
            emp.empBloodGroup = model.empBloodGroup;
            emp.empCareerLevel = model.empCareerLevel;
            emp.empFamilyNameAr = model.empFamilyNameAr;
            emp.empFamilyNameEn = model.empFamilyNameEn;
            emp.empFNameAr = model.empFNameAr;
            emp.empFNameEn = model.empFNameEn;
            emp.empGender = model.empGender;
            emp.empImg = model.empImg;
            emp.empMaritalStatus = model.empMaritalStatus;
            emp.empNationality = model.empNationality;
            emp.empNickName = model.empNickName;
            emp.empReligion = model.empReligion;
            emp.empSNameAr = model.empSNameAr;
            emp.empSNameEn = model.empSNameEn;
            emp.empTitle = model.empTitle;
            emp.empTNameAr = model.empTNameAr;
            emp.empTNameEn = model.empTNameEn;
            db.Entry(emp).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
        public int DelEmployee(int Id)
        {

            td_employee tsd = db.td_employee.Find(Id);
            if (tsd != null)
            {
                db.td_employee.Remove(tsd);
                int res = db.SaveChanges();
                if (res > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 2;
            }

        }
        public List<EmployeeViewModel> GetAllEmployee()
        {
            List<EmployeeViewModel> model = db.td_employee.Where(x => x.UserId == null).Select(u => new EmployeeViewModel
            {

                empBirthblace = u.empBirthblace,
                empBirthDate = u.empBirthDate,
                empBloodGroup = u.empBloodGroup,
                empCareerLevel = u.empCareerLevel,
                empFamilyNameAr = u.empFamilyNameAr,
                empFamilyNameEn = u.empFamilyNameEn,
                empFNameAr = u.empFNameAr,
                empFNameEn = u.empFNameEn,
                empGender = u.empGender,
                empid = u.empid,
                empImg = u.empImg,
                empMaritalStatus = u.empMaritalStatus,
                empNationality = u.empNationality,
                empNickName = u.empNickName,
                empReligion = u.empReligion,
                empSNameAr = u.empSNameAr,
                empSNameEn = u.empSNameEn,
                empTitle = u.empTitle,
                empTNameAr = u.empTNameAr,
                empTNameEn = u.empTNameEn,

                NameCareerLevelEn = u.CareerLevel.selNameEnglish,
                NameGenderEn = u.Gender.selNameEnglish,
                NameMaritalStatusEn = u.MaritalStatus.selNameEnglish,
                NameNationalityEn = u.Nationality.selNameEnglish,
                NameReligionEn = u.Religion.selNameEnglish,
                NameTitleEn = u.Title.selNameEnglish,

                NameCareerLevelAr = u.CareerLevel.selNameArabic,
                NameGenderAr = u.Gender.selNameArabic,
                NameMaritalStatusAr = u.MaritalStatus.selNameArabic,
                NameNationalityAr = u.Nationality.selNameArabic,
                NameReligionAr = u.Religion.selNameArabic,
                NameTitleAr = u.Title.selNameArabic,


                UserId = u.UserId,
                OId = u.OId

            }).ToList();

            return model;
        }
        public List<EmployeeViewModel> GetEmployeeByOId(int OId)
        {
            List<EmployeeViewModel> model = db.td_employee.Where(x => x.OId == OId).Select(u => new EmployeeViewModel
            {

                empBirthblace = u.empBirthblace,
                empBirthDate = u.empBirthDate,
                empBloodGroup = u.empBloodGroup,
                empCareerLevel = u.empCareerLevel,
                empFamilyNameAr = u.empFamilyNameAr,
                empFamilyNameEn = u.empFamilyNameEn,
                empFNameAr = u.empFNameAr,
                empFNameEn = u.empFNameEn,
                empGender = u.empGender,
                empid = u.empid,
                empImg = u.empImg,
                empMaritalStatus = u.empMaritalStatus,
                empNationality = u.empNationality,
                empNickName = u.empNickName,
                empReligion = u.empReligion,
                empSNameAr = u.empSNameAr,
                empSNameEn = u.empSNameEn,
                empTitle = u.empTitle,
                empTNameAr = u.empTNameAr,
                empTNameEn = u.empTNameEn,

                NameCareerLevelEn = u.CareerLevel.selNameEnglish,
                NameGenderEn = u.Gender.selNameEnglish,
                NameMaritalStatusEn = u.MaritalStatus.selNameEnglish,
                NameNationalityEn = u.Nationality.selNameEnglish,
                NameReligionEn = u.Religion.selNameEnglish,
                NameTitleEn = u.Title.selNameEnglish,

                NameCareerLevelAr = u.CareerLevel.selNameArabic,
                NameGenderAr = u.Gender.selNameArabic,
                NameMaritalStatusAr = u.MaritalStatus.selNameArabic,
                NameNationalityAr = u.Nationality.selNameArabic,
                NameReligionAr = u.Religion.selNameArabic,
                NameTitleAr = u.Title.selNameArabic,


                UserId = u.UserId,
                OId = u.OId

            }).ToList();

            return model;
        }

        public EmployeeViewModel GetEmployeeById(int id)
        {
            td_employee u = db.td_employee.Find(id);


            EmployeeViewModel model = new EmployeeViewModel();


            model.empBirthblace = u.empBirthblace;
            model.empBirthDate = (DateTime)u.empBirthDate;
            model.empBloodGroup = u.empBloodGroup;
            model.empCareerLevel = u.empCareerLevel;
            model.empFamilyNameAr = u.empFamilyNameAr;
            model.empFamilyNameEn = u.empFamilyNameEn;
            model.empFNameAr = u.empFNameAr;
            model.empFNameEn = u.empFNameEn;
            model.empGender = u.empGender;
            model.empid = u.empid;
            model.empImg = u.empImg;
            model.empMaritalStatus = u.empMaritalStatus;
            model.empNationality = u.empNationality;
            model.empNickName = u.empNickName;
            model.empReligion = u.empReligion;
            model.empSNameAr = u.empSNameAr;
            model.empSNameEn = u.empSNameEn;
            model.empTitle = u.empTitle;
            model.empTNameAr = u.empTNameAr;
            model.empTNameEn = u.empTNameEn;
            model.NameCareerLevelEn = u.CareerLevel.selNameEnglish;
            model.NameGenderEn = u.Gender.selNameEnglish;
            model.NameMaritalStatusEn = u.MaritalStatus.selNameEnglish;

            model.NameReligionEn = u.Religion.selNameEnglish;
            if (model.empTitle != null)
            {
                model.NameTitleEn = u.Title.selNameEnglish;
                model.NameCareerLevelAr = u.CareerLevel.selNameArabic;
                model.NameGenderAr = u.Gender.selNameArabic;
                model.NameMaritalStatusAr = u.MaritalStatus.selNameArabic;

                model.NameReligionAr = u.Religion.selNameArabic;
                model.NameTitleAr = u.Title.selNameArabic;
            }

            model.UserId = u.UserId;

            return model;
        }
        public EmployeeModel GetFoeEditById(int id)
        {
            td_employee u = db.td_employee.Find(id);

            EmployeeModel model = new EmployeeModel
            {
                empActivationStatus = (bool)u.empActivationStatus,
                empBirthblace = u.empBirthblace,
                empBirthDate = (DateTime)u.empBirthDate,
                empBloodGroup = u.empBloodGroup,
                empCareerLevel = u.empCareerLevel,
                empFamilyNameAr = u.empFamilyNameAr,
                empFamilyNameEn = u.empFamilyNameEn,
                empFNameAr = u.empFNameAr,
                empFNameEn = u.empFNameEn,
                empGender = u.empGender,
                empid = u.empid,
                empImg = u.empImg,
                empMaritalStatus = u.empMaritalStatus,
                empNationality = u.empNationality,
                empNickName = u.empNickName,
                empReligion = u.empReligion,
                empSNameAr = u.empSNameAr,
                empSNameEn = u.empSNameEn,
                empTitle = u.empTitle,
                empTNameAr = u.empTNameAr,
                empTNameEn = u.empTNameEn,
                UserId = u.UserId,
                OId = u.OId,

            };
            return model;
        }
        public EmployeeViewModel GetEmployeeByUserId(string userId)
        {
            throw new NotImplementedException();
        }
        public int UpdateEmployee(EmployeeModel model)
        {
            throw new NotImplementedException();
        }
        public int UpdateLoginEmployee(int id, string UserId)
        {
            td_employee model = db.td_employee.Find(id);
            model.UserId = UserId;
            db.Entry(model).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public List<EmployeeSelectionModel> GetEmployeeSelectioByOId(int OId)
        {
            List<EmployeeSelectionModel> model = db.td_employee.Where(x => x.OId == OId).Select(u => new EmployeeSelectionModel
            {
                CareerLevelEnglish = u.CareerLevel.selNameEnglish,
                CareerLevelArabic = u.CareerLevel.selNameEnglish,
                OId = u.OId,
                empid = u.empid,
                NameArabic = u.empFNameAr + " " + u.empSNameAr + " " + u.empTNameAr,
                NameEnglish = u.empFNameEn + " " + u.empSNameEn + " " + u.empTNameEn,
            }).ToList();

            return model;
        }
        public int GetOIdByUserId(string userId)
        {
            int OId = (int)db.td_employee.SingleOrDefault(x => x.AspNetUser.Id == userId).OId;

            return OId;
        }
        public int GetEmpIDByUserId(string userId)
        {
            int empId = db.td_employee.SingleOrDefault(x => x.AspNetUser.Id == userId).empid;

            return empId;
        }

        public string GetUserIdByempId(int empId)
        {
            string UserId = db.td_employee.Find(empId).UserId;
            return UserId;
        }


        public void UpdateUserId(string userId,int empId) 
        {
            td_employee employee = db.td_employee.Find(empId);
            if (employee != null)
            {
                employee.UserId = userId;
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

    }
}
