﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class Organization : IOrganization
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddNewOrganization(OrganizationModel model)
        {
            tdMang_Organization tdO = new tdMang_Organization
            {
                OAddress=model.OAddress,
                OEmail=model.OEmail,
                ONameAr=model.ONameAr,
                OPhoneNumbers=model.OPhoneNumbers,
                ONameEn=model.ONameEn,
                OSupportNumbers=model.OSupportNumbers
            };
            db.tdMang_Organization.Add(tdO);
           int res =db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int AddNewOrganizationMangers(OrganizationModel model)
        {
            int res = 0;
            tdMang_Organization tdO = db.tdMang_Organization.Find(model.OId);
            if (tdO != null)
            {
                db.Entry(tdO).State = EntityState.Modified;
               res= db.SaveChanges();
                return res;
            }
            else
            {
                return res;
            }
            

        }

        public int DelNewOrganization(int id)
        {
            throw new NotImplementedException();
        }

        public List<OrganizationViewModel> GetAllOrganization()
        {
            List<OrganizationViewModel> model = new List<OrganizationViewModel>();
            List<tdMang_Organization> Tdo = db.tdMang_Organization.ToList();
            foreach (var item in Tdo)
            {

                OrganizationViewModel modelItem = new OrganizationViewModel {

                    OAddress = item.OAddress,
                    OEmail = item.OEmail,
                    ONameAr = item.ONameAr,
                    OPhoneNumbers = item.OPhoneNumbers,
                    ONameEn = item.ONameEn,
                    OSupportNumbers = item.OSupportNumbers,
                    OId=item.OId,
                    OManager=item.OManager,
                    OVicManager=item.OVicManager
                };
                model.Add(modelItem);
            }

            return model;

        }

        public OrganizationViewModel GetAllOrganizationBy(int Id)
        {
            throw new NotImplementedException();
        }

        public OrganizationModel GetOrganizationById(int id)
        {
            tdMang_Organization org = db.tdMang_Organization.Find(id);
            OrganizationModel model = new OrganizationModel
            {

                OAddress = org.OAddress,
                OEmail = org.OEmail,
                ONameAr = org.ONameAr,
                OPhoneNumbers = org.OPhoneNumbers,
                ONameEn = org.ONameEn,
                OSupportNumbers = org.OSupportNumbers,
                OManager=org.OManager,
                OVicManager=org.OVicManager,
                OId=org.OId,
                
            };
            return model;

        }

        public int UpdateNewOrganization(OrganizationModel model)
        {
            tdMang_Organization org = db.tdMang_Organization.Find(model.OId);
            org.OAddress=model.OAddress;
            org.OEmail = model.OEmail;
            org.ONameAr = model.ONameAr;
            org.OPhoneNumbers = model.OPhoneNumbers;
            org.ONameEn = model.ONameEn;
            org.OSupportNumbers = model.OSupportNumbers;
            db.Entry(org).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
}
