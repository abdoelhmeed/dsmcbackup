﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class LocationOfIncident : ILocationOfIncident
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(LocationOfIncidentModel model)
        {
            td_LocationOfIncident LOOI = new td_LocationOfIncident
            {
                LNAr = model.LNAr,
                LNEn=model.LNEn,
                OId=model.OId,
                Visible=true
            };
            db.td_LocationOfIncident.Add(LOOI);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int Id)
        {
            td_LocationOfIncident locationOfIncident = db.td_LocationOfIncident.Find(Id);
            locationOfIncident.Visible = false;
            db.Entry(locationOfIncident).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }


        }

        public LocationOfIncidentModel GetById(int Id)
        {
            td_LocationOfIncident Loct = db.td_LocationOfIncident.Find(Id);
            LocationOfIncidentModel model = new LocationOfIncidentModel {
                LId=Loct.LId,
                LNAr=Loct.LNAr,
                LNEn=Loct.LNEn,
                OId=Loct.OId,
                Visible=Loct.Visible
            };
            return model;
        }
        public List<LocationOfIncidentModel> GetPyOId(int OId)
        {
            List<LocationOfIncidentModel> model = db.td_LocationOfIncident.Where(I => I.OId == OId && I.Visible==true)
                .Select(x => new LocationOfIncidentModel
                {
                    OId=x.OId,
                    LId=x.LId,
                    LNAr=x.LNAr,
                    LNEn=x.LNEn,
                    Visible=x.Visible
                }).ToList();
            return model;
        }
        public int Update(LocationOfIncidentModel model)
        {
            td_LocationOfIncident x = db.td_LocationOfIncident.Find(model.LId);
            x.LNAr = model.LNAr;
            x.LNEn = model.LNEn;
            db.Entry(x).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
