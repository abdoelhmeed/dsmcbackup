﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class IncidentType : IIncidentType
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddMain(IncidentMainTypeModel model)
        {
            td_IncidentMainType TMain = new td_IncidentMainType
            {
                IMTNameAr=model.IMTNameAr,
                IMTNameEr=model.IMTNameEr,
                Visible=true,
                OId=model.OId
            };
            db.td_IncidentMainType.Add(TMain);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int AddSub(IncidentSubTypeModel model)
        {
            td_IncidentSubType Tsub = new td_IncidentSubType
            {
                IMTId=model.IMTId,
                ISTNameAr=model.ISTNameAr,
                ISTNameEn=model.ISTNameEn,
                OId=model.OId,
                Visible=true
            };
            db.td_IncidentSubType.Add(Tsub);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int DeleteMain(int Id)
        {
            td_IncidentMainType TMain = db.td_IncidentMainType.Find(Id);
            TMain.Visible = false;
            db.Entry(TMain).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int DeleteSub(int Id)
        {
            td_IncidentSubType sub = db.td_IncidentSubType.Find(Id);
            sub.Visible = false;
            db.Entry(sub).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<IncidentMainTypeModel> GetMianList(int OId)
        {
            List<IncidentMainTypeModel> model = db.td_IncidentMainType.Where(u => u.Visible == true && u.OId==OId)
                .Select(x => new IncidentMainTypeModel
                {
                    OId=x.OId,
                    IMTNameEr=x.IMTNameEr,
                    IMTNameAr=x.IMTNameAr,
                    Visible=x.Visible,
                    IMTId=x.IMTId

                }).ToList();
            return model;
        }

        public IncidentMainTypeModel GetMinById(int Id)
        {
            td_IncidentMainType x = db.td_IncidentMainType.Find(Id);
            IncidentMainTypeModel model = new IncidentMainTypeModel
            {
                OId = x.OId,
                IMTNameEr = x.IMTNameEr,
                IMTNameAr = x.IMTNameAr,
                Visible = x.Visible,
                IMTId = x.IMTId
            };

            return model;
        }

        public IncidentSubTypeModel GetSubById(int Id)
        {
            td_IncidentSubType x = db.td_IncidentSubType.Find(Id);
            IncidentSubTypeModel model = new IncidentSubTypeModel
            {
                IMTId=x.IMTId,
                IMTNameAr=x.td_IncidentMainType.IMTNameAr,
                OId=x.OId,
                IMTNameEn=x.td_IncidentMainType.IMTNameEr,
                ISTNameAr=x.ISTNameAr,
                ISTNameEn=x.ISTNameEn,
                Visible=x.Visible,
                ISTId=x.ISTId
            };

            return model;
        }

        public List<IncidentSubTypeModel> GetSubList(int OId)
        {
            List<IncidentSubTypeModel> model= db.td_IncidentSubType.Where(u => u.Visible == true && u.OId == OId)
                .Select(x => new IncidentSubTypeModel
                {
                    IMTId = x.IMTId,
                    IMTNameAr = x.td_IncidentMainType.IMTNameAr,
                    OId = x.OId,
                    IMTNameEn = x.td_IncidentMainType.IMTNameEr,
                    ISTNameAr = x.ISTNameAr,
                    ISTNameEn = x.ISTNameEn,
                    Visible = x.Visible,
                    ISTId = x.ISTId
                }).ToList();

            return model;
        }

        public List<IncidentSubTypeModel> GetSubTypeByMId(int MId)
        {
            List<IncidentSubTypeModel> model = db.td_IncidentSubType.Where(u => u.Visible == true && u.IMTId == MId)
                 .Select(x => new IncidentSubTypeModel
                 {
                     IMTId = x.IMTId,
                     IMTNameAr = x.td_IncidentMainType.IMTNameAr,
                     OId = x.OId,
                     IMTNameEn = x.td_IncidentMainType.IMTNameEr,
                     ISTNameAr = x.ISTNameAr,
                     ISTNameEn = x.ISTNameEn,
                     Visible = x.Visible,
                     ISTId = x.ISTId
                 }).ToList();

            return model;
        }

        public int UpdateMain(IncidentMainTypeModel model)
        {
            td_IncidentMainType TMain = db.td_IncidentMainType.Find(model.IMTId);
            TMain.IMTNameAr = model.IMTNameAr;
            TMain.IMTNameEr = model.IMTNameEr;
            db.Entry(TMain).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int UpdateSub(IncidentSubTypeModel model)
        {
            td_IncidentSubType sub = db.td_IncidentSubType.Find(model.ISTId);
            sub.ISTNameAr = model.ISTNameAr;
            sub.ISTNameEn = model.ISTNameEn;
            sub.IMTId = model.IMTId;
            db.Entry(sub).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
}
