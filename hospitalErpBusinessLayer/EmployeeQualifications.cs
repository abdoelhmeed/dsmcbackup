﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
   public class EmployeeQualifications : IEmployeeQualifications
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int addEmpeQualificat(EmployeeQualificationsModel model)
        {
            td_empQualifications emPQual = new td_empQualifications
            {
                empId=model.empId,
                QDate=model.QDate,
                Qualifications=model.Qualifications,
                QGrade=model.QGrade,
                QImg=model.QImg,
                QPlace=model.QPlace,
                Visible=true
            };
            db.td_empQualifications.Add(emPQual);
            int res=db.SaveChanges();
            if (res> 0)
            {
                return 1;
            }
            else
            {
                return 0; 
            }
            
        }
        public int DelEmpeQualificat(int id)
        {
            td_empQualifications empQ = db.td_empQualifications.Find(id);
            empQ.Visible = false;
            db.Entry(empQ).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
        public EmployeeQualificationsModel GetEmpeQualificatById(int id)
        {
            EmployeeQualificationsModel empQ = new EmployeeQualificationsModel();
            td_empQualifications model = db.td_empQualifications.Find(id);
            empQ.empId =(int)model.empId;
            empQ.QDate = model.QDate;
            empQ.Qualifications = model.Qualifications;
            empQ.QGrade = model.QGrade;
            empQ.QImg = model.QImg;
            empQ.QPlace = model.QPlace;
            empQ.Visible =(bool)model.Visible;
            return empQ;

        }
        public List<EmployeeQualificationsViewModel> GetEmpQualificatByEmpId(int empId)
        {
            List<EmployeeQualificationsViewModel> model = db.td_empQualifications.Where(u => u.empId == empId && u.Visible==true)
                .Select(x => new EmployeeQualificationsViewModel
                {
                    empId=(int)x.empId,
                    QDate=x.QDate,
                    QGrade=x.QGrade,
                    Qid=x.Qid,
                    QImg=x.QImg,
                    QNameEnglish=x.td_selectionDetails.selNameEnglish,
                    QNameArbic= x.td_selectionDetails.selNameArabic,
                    QNamePlaceArbic=x.td_selectionDetails1.selNameArabic,
                    QNamePlaceEnglish=x.td_selectionDetails1.selNameEnglish

                }).ToList();

            return model;
        }
        public int UpdateEmpeQualificat(EmployeeQualificationsModel model)
        {
            td_empQualifications empQ = db.td_empQualifications.Find(model.Qid);
            empQ.QDate = model.QDate;
            empQ.Qualifications = model.Qualifications;
            empQ.QGrade = model.QGrade;
            empQ.QImg = model.QImg;
            empQ.QPlace = model.QPlace;
            db.Entry(empQ).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
