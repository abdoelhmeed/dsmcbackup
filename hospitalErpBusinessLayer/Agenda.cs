﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;

namespace hospitalErpBusinessLayer
{
    public class Agenda : IAgenda
    {

        erpHospitalEntities db = new erpHospitalEntities();
        PushNotification notif = new PushNotification();
        public int Add(AgendaModel model)
        {
            td_Agenda agenda = new td_Agenda
            {
                FromemployeeId=model.FromemployeeId,
                History=DateTime.Now,
                isRead=false,
                Oid=model.Oid,
                Subject=model.Subject,
                Title=model.Title,
                ToEmployeeId=model.ToEmployeeId,
               
            };
            db.td_Agenda.Add(agenda);
            int res=db.SaveChanges();
            if (res > 0)
            {
                PushNotificationModel notification = new PushNotificationModel
                {
                    empId=model.FromemployeeId,
                    nBody=model.Subject,
                    nTitel=model.Title,
                    nUrl=model.nUrl,
                    NType= "agenda"
                };
                notif.Add(notification);
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public List<AgendaViewModel> GetAll(int OId)
        {
            List<AgendaViewModel> model = db.td_Agenda.Where(u=> u.Oid==OId).Select(x => new AgendaViewModel {

                isRead = x.isRead,
                EmplNameArabicTo = x.td_employee1.empFNameAr + " " + x.td_employee1.empSNameAr + " " + x.td_employee1.empTNameAr,
                EmplNameEnglishTo = x.td_employee1.empFNameEn + " " + x.td_employee1.empSNameEn + " " + x.td_employee1.empTNameEn,
                EmpNameArabicFor= x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                EmpNameEnglishFor= x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                FromemployeeId=x.FromemployeeId,
                History=x.History,
                id=x.id,
                Oid=x.Oid,
                Subject=x.Subject,
                Title=x.Title,
                ToEmployeeId=x.ToEmployeeId,
                strHistory = x.History.ToString()
            }).ToList();
            return model;
        }

        public AgendaViewEmployeeModel GetById(int AId,int NId)
        {
            AgendaViewEmployeeModel model = new AgendaViewEmployeeModel();
            td_Agenda agenda = db.td_Agenda.Find(AId);
            model.Title = agenda.Title;
            model.Subject = agenda.Subject;
            model.id = agenda.id;
            notif.Update(NId);
            return model;
        }

        public List<AgendaViewModel> GetEmployeeFor(int empFor)
        {
            List<AgendaViewModel> model = db.td_Agenda.Where(u => u.FromemployeeId == empFor).Select(x => new AgendaViewModel
            {

                isRead = x.isRead,
                EmplNameArabicTo = x.td_employee1.empFNameAr + " " + x.td_employee1.empSNameAr + " " + x.td_employee1.empTNameAr,
                EmplNameEnglishTo = x.td_employee1.empFNameEn + " " + x.td_employee1.empSNameEn + " " + x.td_employee1.empTNameEn,
                EmpNameArabicFor = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                EmpNameEnglishFor = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                FromemployeeId = x.FromemployeeId,
                History = x.History,
                id = x.id,
                Oid = x.Oid,
                Subject = x.Subject,
                Title = x.Title,
                ToEmployeeId = x.ToEmployeeId,
                strHistory = x.History.ToString()
            }).ToList();
            notif.UpdateAllByEmpIdAndNType(empFor, "agenda");
            return model;
        }
    }
}
