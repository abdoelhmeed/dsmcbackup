﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class EmployeeIds : IEmployeeIds
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddIds(EmployeeIdsModel model)
        {
            td_employeeIds EmpIds = new td_employeeIds 
            {
                empId=model.empId,
                idexpiredDate=model.idexpiredDate,
                idIssued=model.idIssued,
                idImg=model.idImg,
                idNumber=model.idNumber,
                idPlaceIssue=model.idPlaceIssue,
                idType=model.idType,
                Visible=true
            };
            db.td_employeeIds.Add(EmpIds);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public int DeleteIds(int id)
        {
            td_employeeIds empIds = db.td_employeeIds.Find(id );
            empIds.Visible = false;
            db.Entry(empIds).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public EmployeeIdsViewsModel GegIds(int id)
        {
            EmployeeIdsViewsModel model = new EmployeeIdsViewsModel();
            td_employeeIds empIds = db.td_employeeIds.Find(id);
            model.idexpiredDate = empIds.idexpiredDate;
            model.idImg = empIds.idImg;
            model.idIssued =(DateTime)empIds.idIssued;
            model.idPlaceIssue = empIds.idPlaceIssue;
            model.idType = model.idType;
            model.NameTypeEn = empIds.td_selectionDetails.selNameEnglish;
            model.NameTypeAr = empIds.td_selectionDetails.selNameArabic;
            model.stridexpiredDate = empIds.idexpiredDate.ToShortDateString();
            model.stridIssued = empIds.idIssued.ToShortDateString();
            model.id = empIds.id;
            model.empId = empIds.empId;

            return model;


        }
        public List<EmployeeIdsViewsModel> GegListIds(int empId)
        {
            List<td_employeeIds> empIdsList = db.td_employeeIds.Where(x=> x.empId==empId && x.Visible==true).ToList();
            List<EmployeeIdsViewsModel> model = new List<EmployeeIdsViewsModel>();
            foreach (var item in empIdsList)
            {
                EmployeeIdsViewsModel modelItem = new EmployeeIdsViewsModel
                {
                    idNumber=item.idNumber,
                   idexpiredDate = item.idexpiredDate,
                   idImg = item.idImg,
                   idIssued = (DateTime)item.idIssued,
                   idPlaceIssue = item.idPlaceIssue,
                   idType = item.idType,
                   NameTypeEn = item.td_selectionDetails.selNameEnglish,
                   NameTypeAr = item.td_selectionDetails.selNameArabic,
                   stridexpiredDate = item.idexpiredDate.ToShortDateString(),
                   stridIssued = item.idIssued.ToShortDateString(),
                   id =(int) item.id,
                   empId = item.empId
               };
                model.Add(modelItem);
            }
            return model;
        }
        public int UpdateIds(EmployeeIdsModel model)
        {
            throw new NotImplementedException();
        }
    }
}
