﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class SelectionDetails : ISelectionDetails
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int AddSelectionDetail(SelectionDetailsModel model)
        {
            td_selectionDetails sd = new td_selectionDetails
            {
                selNameArabic=model.selNameArabic,
                selNameEnglish=model.selNameEnglish,
                sTId=model.sTId
            };
            db.td_selectionDetails.Add(sd);
           int res =db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;       
            }
        }

        public int DelSelectionDetail(int id)
        {
            td_selectionDetails tsd = db.td_selectionDetails.Find(id);
            if (tsd != null)
            {
               db.td_selectionDetails.Remove(tsd);
                int res = db.SaveChanges();
                if (res > 0)
                {
                    return 1;
                }
                else
                {
                    return 0;
                } 
            }
            else
            {
                return 2;  
            }
         
        }

        public SelectionDetailsModel GetByID(int id)
        {

            td_selectionDetails tsd = db.td_selectionDetails.Find(id);

            SelectionDetailsModel model = new SelectionDetailsModel
            {
                id=tsd.id,
                selNameArabic=tsd.selNameArabic,
                selNameEnglish=tsd.selNameEnglish,
                sTId=tsd.sTId
            };
            return model;

        }

        public List<SelectionDetailsModel> GetByTypeID(int sTId)
        {
            List< SelectionDetailsModel> model = db.td_selectionDetails.Where(x=>x.sTId==sTId).Select(u=> new SelectionDetailsModel
            {
                id=u.id,
                sTId=u.sTId,
                selNameArabic=u.selNameArabic,
                selNameEnglish=u.selNameEnglish

            }).ToList();

            return model;

        }

        public int UPdateSelectionDetail(SelectionDetailsModel model)
        {
            td_selectionDetails tsd = db.td_selectionDetails.Find(model.id);
            tsd.selNameArabic = model.selNameArabic;
            tsd.selNameEnglish = model.selNameEnglish;
            db.Entry(tsd).State = EntityState.Modified;
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
}
