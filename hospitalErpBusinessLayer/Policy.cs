﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class Policy : IPolicy
    {
        erpHospitalEntities db = new erpHospitalEntities();
        PoliciesApplies policiesApplies = new PoliciesApplies();
        PolicyApprov policyApprov = new PolicyApprov();
        EmployeesDepartmentManagment EmpDm = new EmployeesDepartmentManagment();
        PoliciesRea read = new PoliciesRea();
        PushNotification PushNotification = new PushNotification();
        public int add(PoliciesModel model)
        {
            td_Policies policy = new td_Policies
            {
                EnterDate=DateTime.Now,
                OId=model.OId,
                PolBody=model.PolBody,
                PolEffectiveDate=model.PolEffectiveDate,
                policiesNumbers=model.policiesNumbers,
                PolPdf=model.PolPdf,
                polTitle=model.polTitle,
                Status="New",
                TId=model.TId,
                CreateByEmpId=model.CreateByEmpId,
                isPublish=false,
                Visible=true

            };
            db.td_Policies.Add(policy);
            int res=db.SaveChanges();
            if (res > 0)
            {
                foreach (var item in model.DeptId)
                {
                    PoliciesAppliesToModel pol = new PoliciesAppliesToModel
                    {
                        DeptId=item,
                        policiesNumbers= policy.policiesNumbers
                    };
                    policiesApplies.add(pol);
                }
                List<int?> MangersIdList =db.td_PolicyPlan.OrderBy(y=>y.Arrangement).Where(x=>x.TId==model.TId).Select(u=> u.tdManag_Management.deptManager).ToList();
                foreach (var item in MangersIdList)
                {
                    PolicyApprovModel approv = new PolicyApprovModel
                    {
                        empId = item,
                        policiesNumbers = policy.policiesNumbers
                    };
                    policyApprov.add(approv);
                }
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public PoliciesModel GetByNumber(string Number)
        {
            PoliciesModel model = new PoliciesModel();
            td_Policies Policy = db.td_Policies.Find(Number);
            if (Policy != null)
            {
                model.CreateByEmpId = Policy.CreateByEmpId;
                model.policiesNumbers = Policy.policiesNumbers;
                model.PolPdf = Policy.PolPdf;
                model.isPublish = Policy.isPublish;
                model.PolBody = Policy.PolBody;
                model.TId =(int)Policy.TId;
                model.polTitle = Policy.polTitle;
            }

            return model;
        }
        public List<PoliciesViewModel> GetPolicies(int OId)
        {
            List<PoliciesViewModel> model = db.td_Policies.Where(u => u.OId == OId).Select(x => new PoliciesViewModel
            {
                OId=x.OId,
                Status=x.Status,
                EnterDate=x.EnterDate,
                PolBody=x.PolBody,
                PolEffectiveDate=x.PolEffectiveDate,
                policiesNumbers=x.policiesNumbers,
                PolPdf=x.PolPdf,
                polTitle=x.polTitle,
                CreateByEmpId = x.CreateByEmpId,
                CreateByEmpNameEn = x.td_employee.empFNameEn + " " + x.td_employee.empSNameEn + " " + x.td_employee.empTNameEn,
                CreateByEmpNameAr = x.td_employee.empFNameAr + " " + x.td_employee.empSNameAr + " " + x.td_employee.empTNameAr,
                isPublish = x.isPublish,
                polTypeEn = x.td_PoliciesType.NameEn,
                polTypeAr = x.td_PoliciesType.NameAr,
                TId = x.TId,
                Visible = x.Visible
            }).ToList();

            return model;
        }
        public PoliciesViewModel GetPolicyByNumber(string Number)
        {
            td_Policies policy = db.td_Policies.Find(Number);
            if (policy != null)
            {
                PoliciesViewModel model = new PoliciesViewModel
                {
                    polTitle = policy.polTitle,
                    EnterDate = policy.EnterDate,
                    OId = policy.OId,
                    PolBody = policy.PolBody,
                    PolEffectiveDate = policy.PolEffectiveDate,
                    policiesNumbers = policy.policiesNumbers,
                    PolPdf = policy.PolPdf,
                    Status = policy.Status,
                    CreateByEmpId= policy.CreateByEmpId,
                    CreateByEmpNameEn= policy.td_employee.empFNameEn +" " + policy.td_employee.empSNameEn+" " + policy.td_employee.empTNameEn,
                    CreateByEmpNameAr= policy.td_employee.empFNameAr + " " + policy.td_employee.empSNameAr + " " + policy.td_employee.empTNameAr,
                    isPublish = policy.isPublish,
                    polTypeEn= policy.td_PoliciesType.NameEn,
                    polTypeAr = policy.td_PoliciesType.NameAr,
                    TId = policy.TId,
                    Visible= policy.Visible
                };

                return model;
            }
            else
            {
                PoliciesViewModel model = new PoliciesViewModel();
                return model;
            }
            
          
        }
        public int PublishEmployee(string Number)
        {
            td_Policies Policy = db.td_Policies.Find(Number);
            List<int> EmpIdes = new List<int>();
            if (Policy != null)
            {
                List<PoliciesAppliesToViewModel> Applies = policiesApplies.GetByPolicyNumbers(Number);
                foreach (var item in Applies)
                {
                    List<int?> EDM = db.td_employeeDepartmentManagment.Where(x => x.DepartmentId == item.DeptId && x.Visible==true).Select(u=>u.empid).ToList();
                    foreach (var empItem in EDM)
                    {
                        EmpIdes.Add((int)empItem);
                    }
                }
               
                foreach (var item in EmpIdes)
                {
                    td_employee emp = db.td_employee.Find(item);
                  
                    if (emp.UserId != null)
                    {
                            //read Policy
                            PoliciesReaModel reaModel = new PoliciesReaModel
                            {
                                empId = emp.empid,
                                isRead = false,
                                policiesNumbers = Policy.policiesNumbers,
                                RaedDate = DateTime.Now
                            };
                            read.Add(reaModel);
                            //Notifications
                            PushNotificationModel PushModel = new PushNotificationModel
                            {
                                nBody = "Policy By " + emp.empFNameEn + " " + emp.empSNameEn + " " + emp.empTNameEn,
                                nTitel = Policy.policiesNumbers,
                                nUrl = "/PoliciesModule/PoliciesRead/ReadPolicy?Number=" + Policy.policiesNumbers,
                                UserId = emp.UserId,
                                NType = "Policy",
                                empId=emp.empid
                            };
                            PushNotification.Add(PushModel);
                    }
                }
                Policy.Status = "Published in "+DateTime.Now.ToShortDateString();
                Policy.isPublish = true;
                db.Entry(Policy).State = EntityState.Modified;
                db.SaveChanges();

                return 1;
            }
            else
            {
                return 0;
            }
           
        }
        public int Update(PoliciesUpdateModel model)
        {
            td_Policies polciy = db.td_Policies.Find(model.policiesNumbers);
            if (polciy != null)
            {
                polciy.PolEffectiveDate = model.PolEffectiveDate;
                polciy.PolBody = model.PolBody;
                polciy.Status = "Updated " + DateTime.Now;
                if (model.PolPdf != null )
                {
                    polciy.PolPdf = model.PolPdf;
                }
                db.Entry(polciy).State = EntityState.Modified;
               int res= db.SaveChanges();
                if (res > 0)
                {
                    List<td_PoliciesRead> read = db.td_PoliciesRead.Where(x=> x.policiesNumbers==polciy.policiesNumbers).ToList();
                    foreach (var item in read)
                    {
                        item.isRead = false;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    List<td_Notifications> notif = db.td_Notifications.Where(x=> x.nTitel==polciy.policiesNumbers).ToList();
                    foreach (var item in notif)
                    {
                        item.isRead = false;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    return 1;
                }

                return 0;
            }
            else
            {
                return -1;
            }
        }
    }
}
