﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class IncidentAnalystToClose : IIncidentAnalystToClose
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(IncidentAnalystToCloseModel model)
        {
            td_IncidentAnalystToClose Analyst = new td_IncidentAnalystToClose
            {
                Comment=model.Comment,
                CreatDate=DateTime.Now,
                CreateByempId=model.CreateByempId,
                DHId=model.DHId,
                DOfUE_O=model.DOfUE_O,
                Miss=model.Miss,
                IncidentId=model.IncidentId,
                OId=model.OId,
                Near=model.Near,
                RPMId=model.RPMId,
                RSSId=model.RSSId,
                SentinelEvent=model.SentinelEvent,
                Valid=model.Valid,
            };
            db.td_IncidentAnalystToClose.Add(Analyst);
            int res = db.SaveChanges();
            if (res > 0)
            {
                td_Incident Inc = db.td_Incident.Find(model.IncidentId);
                Inc.status= "Closed";
                Inc.Complete = Inc.Complete + 50;
                Inc.isClosed = true;
                db.Entry(Inc).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<AnalystToCloseViewModel> GetByOId(int OId)
        {
            List<AnalystToCloseViewModel> model = new List<AnalystToCloseViewModel>();
            List<int> Ids = db.td_Incident.Where(x => x.Visible == true && x.OId == OId && x.status == "investigated").Select(u => u.IncidentId).ToList();

            foreach (var item in Ids)
            {
                List<td_Investigator> Investigator = db.td_Investigator.Where(x => x.IncidentId == item).ToList();
                foreach (var Inve in Investigator)
                {
                    AnalystToCloseViewModel AnalystToClose = new AnalystToCloseViewModel
                    {
                        IncidentId= (int) Inve.IncidentId,
                        assId= Inve.assId,
                        Description= Inve.td_Incident.Description,
                        DiscoverDate=(DateTime)Inve.td_Incident.DiscoverDate,
                        InvestigId= Inve.InvId,
                        OccurrenceDate=(DateTime)Inve.td_Incident.OccurrenceDate,
                        ReportDate=(DateTime)Inve.td_Incident.ReportDate,
                        status=Inve.td_Incident.status
                    };
                    model.Add(AnalystToClose);
                }
            }

            return model;
        }

        /// <summary>
        /// Gn Chart Incident By RiskProbability Current Years
        /// </summary>
        /// <param name="OId"></param>
        /// <returns></returns>
        public List<GetCharIncidentByRiskProbabilityModel> GetCharIncidentByRiskProbability(int OId)
        {
            List<GetCharIncidentByRiskProbabilityModel> model = new List<GetCharIncidentByRiskProbabilityModel>();
            List<td_IncidentRiskProbabilityMain> RiskProbabilityList = db.td_IncidentRiskProbabilityMain.Where(x=> x.OId==OId).ToList();
            foreach (var item in RiskProbabilityList)
            {
                int Total = ChartGetByOId(OId).Where( u=> u.RPMId == item.RPMId && u.ReportDate.Year==DateTime.Now.Year).Count();
                string Name = db.td_IncidentRiskProbabilityMain.Find(item.RPMId).RPMINameEn;
                GetCharIncidentByRiskProbabilityModel Crp = new GetCharIncidentByRiskProbabilityModel
                {
                    RPMName = Name,
                    RPMNumber = Total
                };

                model.Add(Crp);
            }
            return model;
        }


        public List<AnalystToCloseViewModel> ChartGetByOId(int OId)
        {
            List<AnalystToCloseViewModel> model = db.td_IncidentAnalystToClose.Where(u => u.OId == OId)
                .Select(x => new AnalystToCloseViewModel
                {

                    IncidentId = (int)x.IncidentId,
                    Description = x.td_Incident.Description,
                    DiscoverDate = (DateTime)x.td_Incident.DiscoverDate,
                    OccurrenceDate = (DateTime)x.td_Incident.OccurrenceDate,
                    ReportDate = (DateTime)x.td_Incident.ReportDate,
                    status = x.td_Incident.status,
                    RPMId=x.RPMId,
                    RSSId=x.RSSId

                }).ToList();
           
            return model;
        }


        public List<ColseReport> GetByIncidentId(int IncidentId)
        {
            List<ColseReport> model = db.td_IncidentAnalystToClose.Where(u => u.IncidentId == IncidentId)
                .Select(x => new ColseReport
                {
                    Comment=x.Comment,
                    CreatDate=x.CreatDate,
                    DegreeHarmNameAr=x.td_IncidentDegreeOfHarm.DHNameAr,
                    DegreeHarmNameEn=x.td_IncidentDegreeOfHarm.DHNameEn,
                    NameRiskProbabilityAR=x.td_IncidentRiskProbabilityMain.RPMNameAr,
                    NameRiskProbabilityEn=x.td_IncidentRiskProbabilityMain.RPMINameEn,
                    NameRiskSeventyAR=x.td_IncidentRiskSeventySub.RSSNameAr,
                    NameRiskSeventyEn=x.td_IncidentRiskSeventySub.RSSNameEn,
                    RiskRating=x.td_IncidentRiskSeventySub.RiskRating
                }).ToList();

            return model;
        }
    }
}
