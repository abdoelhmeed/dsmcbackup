﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class EmployeeRequestsType : IEmployeeRequestsType
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(EmployeeRequestsTypeModel model)
        {
            td_EmployeeRequestsType RTpye = new td_EmployeeRequestsType
            {
                DepartmentId = model.DepartmentId,
                OId = model.OId,
                requestNameArabic = model.requestNameArabic,
                requestNameEnglish = model.requestNameEnglish,
                Visible = true
            };
            db.td_EmployeeRequestsType.Add(RTpye);
            int res = db.SaveChanges();
            if (res>0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public int Delete(int Id)
        {
            td_EmployeeRequestsType resType = db.td_EmployeeRequestsType.Find(Id);
            resType.Visible = false;
            db.Entry(resType).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res> 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public List<EmployeeRequestsTypeViewModel> GetByOId(int OId)
        {
            List<EmployeeRequestsTypeViewModel> model = db.td_EmployeeRequestsType.Where(x => x.OId == OId&&x.Visible==true).Select(u => new EmployeeRequestsTypeViewModel
            {
                OId=u.OId,
                DepartmentId=u.DepartmentId,
                DepartmentNameEn=u.tdManag_Department.deptNameEn,
                DepartmentNameAr=u.tdManag_Department.deptNameAr,
                id=u.id,
                requestNameArabic=u.requestNameArabic,
                requestNameEnglish=u.requestNameEnglish,
                Visible=u.Visible,
            }).ToList();
            return model;
        }
        public EmployeeRequestsTypeModel GetToUpdate(int Id)
        {
            td_EmployeeRequestsType model = db.td_EmployeeRequestsType.Find(Id);
            EmployeeRequestsTypeModel rtype = new EmployeeRequestsTypeModel();
            if (model != null)
            {
                rtype.DepartmentId= model.DepartmentId;
                rtype.DepartmentId = model.DepartmentId;
                rtype.OId = model.OId;
                rtype.requestNameArabic = model.requestNameArabic;
                rtype.requestNameEnglish = model.requestNameEnglish;
                rtype.id = model.id;

            }
            return rtype;
            
        }
        public int Update(EmployeeRequestsTypeModel model)
        {
            td_EmployeeRequestsType rtype = db.td_EmployeeRequestsType.Find(model.id);
            rtype.requestNameArabic = model.requestNameArabic;
            rtype.requestNameEnglish = model.requestNameEnglish;
            rtype.DepartmentId = model.DepartmentId;
            db.Entry(rtype).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
