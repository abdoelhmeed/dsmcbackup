﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class Investigator : IInvestigator
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(InvestigatorModel model)
        {
            td_Investigator investigator = new td_Investigator
            {
                Actions=model.Actions,
                assId=model.assId,
                CFId=model.CFId,
                IncidentId=model.IncidentId,
                InvestigationDate=DateTime.Now,
                Oid=model.Oid,
                Recement=model.Recement,  
            };
            db.td_Investigator.Add(investigator);
            int res = db.SaveChanges();
            if (res > 0)
            {
                td_Incident Incident = db.td_Incident.Find(investigator.IncidentId);
                Incident.status = "investigated";
                db.Entry(Incident).State = EntityState.Modified;
                db.SaveChanges();

                td_IncidentAssign assign = db.td_IncidentAssign.Find(investigator.assId);
                assign.status= "investigated";
                db.Entry(Incident).State = EntityState.Modified;
                db.SaveChanges();


                return 1;
            }
            else
            {
                return 0;
            }
        }

        public InvestigatorModel GetById(int InvId)
        {
            td_Investigator x = db.td_Investigator.Find(InvId);
            InvestigatorModel model= new InvestigatorModel
             {

                    IncidentId = x.IncidentId,
                    Actions = x.Actions,
                    assId = x.assId,
                    CFId = x.CFId,
                    InvestigationDate = x.InvestigationDate,
                    InvId = x.InvId,
                    Oid = x.Oid,
                    Recement = x.Recement
            };

            return model;
        }

        public List<InvestigatorModel> GetByIncidentId(int IncidentId)
        {
            List<InvestigatorModel> model = db.td_Investigator.Where(u => u.IncidentId == IncidentId)
                .Select(x => new InvestigatorModel
                {
                    IncidentId=x.IncidentId,
                    Actions=x.Actions,
                    assId=x.assId,
                    CFId=x.CFId,
                    InvestigationDate=x.InvestigationDate,
                    InvId=x.InvId,
                    Oid=x.Oid,
                    Recement=x.Recement

                }).ToList();
            return model;
        }

      public  List<InvestigatorViewModel> GetByViewIncidentId(int IncidentId) 
        {
            List<InvestigatorViewModel> model = db.td_Investigator.Where(u => u.IncidentId == IncidentId)
                    .Select(x => new InvestigatorViewModel
                    {
                        IncidentId = x.IncidentId,
                        Actions = x.Actions,
                        assId = x.assId,
                        CFId = x.CFId,
                        InvestigationDate = x.InvestigationDate,
                        InvId = x.InvId,
                        Oid = x.Oid,
                        Recement = x.Recement,
                        CFNameEn=x.td_IncideContributingFactor.CFNameEn,
                        CFNameAr=x.td_IncideContributingFactor.CFNameAr
                    }).ToList();
            return model;

        }
    }
}
