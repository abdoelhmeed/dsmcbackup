﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
  public  class Department : IDepartment
    {
        erpHospitalEntities db = new erpHospitalEntities();
        EmployeesDepartmentManagment empDM = new EmployeesDepartmentManagment();
        Employee employee = new Employee();
        public int AddDept(DepartmentModel model)
        {
            tdManag_Department dept = new tdManag_Department
            {
                deptNameAr=model.deptNameAr,
                deptNameEn=model.deptNameEn,
                deptManager=model.deptManager,
                deptVicManager=model.deptVicManager,
                Mid=model.Mid
            };
            db.tdManag_Department.Add(dept);
            int res =db.SaveChanges();
            if (res > 0)
            {
                employeeDepartmentManagmentModel DeptManagers = new employeeDepartmentManagmentModel
                {
                    DepartmentId=dept.deptId,
                    empid=dept.deptManager,
                    ManagmentId=dept.Mid,
                    OId=model.OId,
                    Visible=true,

                };
                empDM.Add(DeptManagers);
                if (model.deptVicManager != model.deptManager)
                {
                    employeeDepartmentManagmentModel DeptVicManagers = new employeeDepartmentManagmentModel
                    {
                        DepartmentId = dept.deptId,
                        empid = model.deptVicManager,
                        ManagmentId = model.Mid,
                        OId = model.OId,
                        Visible = true,

                    };
                    empDM.Add(DeptVicManagers);
                }
               
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int DelDept(int id)
        {
            try
            {
                tdManag_Department dept = db.tdManag_Department.Find(id);
                db.Entry(dept).State = EntityState.Deleted;
                db.SaveChanges();
                return 1;
            }
            catch (Exception)
            {

                return 0;
            }
          
        }

        public List<DepartmentViewModel> GetAll()
        {
            List<DepartmentViewModel> model = new List<DepartmentViewModel>();
            List<tdManag_Department> dpet = db.tdManag_Department.ToList();
       
            foreach (var item in dpet)
            {
                DepartmentViewModel modelItem = new DepartmentViewModel();
                modelItem.deptId = item.deptId;
                modelItem.deptManager = item.deptManager;
                modelItem.deptVicManager = item.deptVicManager;
                modelItem.deptNameAr = item.deptNameAr;
                modelItem.deptNameEn = item.deptNameEn;
                modelItem.Mid = item.Mid;
                modelItem.NameManager = item.td_employee.empFNameEn +" "+ item.td_employee.empSNameEn + " " + item.td_employee.empTNameEn;
                modelItem.NameMidArbic = item.tdManag_Management.MNameAr;
                modelItem.NameMidEnglis = item.tdManag_Management.MName;
                modelItem.NameVicManager = item.td_employee1.empFNameEn + " " + item.td_employee1.empSNameEn + " " + item.td_employee1.empTNameEn;
                model.Add(modelItem);
            }

            return model;
        }


        public List<DepartmentViewModel> GetAllByOId(int OId)
        {
            List<DepartmentViewModel> model = new List<DepartmentViewModel>();
            List<int> DepartmentListID = new List<int>();
            List<tdManag_Department> DpetList = new List<tdManag_Department>();
            List<int> ManagementIds = db.tdManag_Management.Where(x=>x.OId==OId).Select(u=>u.Mid).ToList();
            foreach (var item in ManagementIds)
            {
                List<int> DId = db.tdManag_Department.Where(x => x.Mid == item).Select(u => u.deptId).ToList();
                DepartmentListID.AddRange(DId);
            }

            foreach (var item in DepartmentListID)
            {
                List<tdManag_Department> dpet = db.tdManag_Department.Where(x=> x.deptId==item).ToList();
                DpetList.AddRange(dpet);
            }
        

            foreach (var item in DpetList)
            {
                DepartmentViewModel modelItem = new DepartmentViewModel();
                modelItem.deptId = item.deptId;
                modelItem.deptManager = item.deptManager;
                modelItem.deptVicManager = item.deptVicManager;
                modelItem.deptNameAr = item.deptNameAr;
                modelItem.deptNameEn = item.deptNameEn;
                modelItem.Mid = item.Mid;
                modelItem.NameManager = item.td_employee.empFNameEn + " " + item.td_employee.empSNameEn + " " + item.td_employee.empTNameEn;
                modelItem.NameManagerAr = item.td_employee.empFNameAr + " " + item.td_employee.empSNameAr + " " + item.td_employee.empTNameAr;
                modelItem.NameMidArbic = item.tdManag_Management.MNameAr;
                modelItem.NameMidEnglis = item.tdManag_Management.MName;
                modelItem.NameVicManager = item.td_employee1.empFNameEn + " " + item.td_employee1.empSNameEn + " " + item.td_employee1.empTNameEn;
                modelItem.NameVicManagerAr = item.td_employee1.empFNameAr + " " + item.td_employee1.empSNameAr + " " + item.td_employee1.empTNameAr;
                model.Add(modelItem);
            }

            return model;
        }

        public DepartmentModel getById(int id)
        {
            DepartmentModel model = new DepartmentModel();
            tdManag_Department dept = db.tdManag_Department.Find(id);
            model.deptId = dept.deptId;
            model.deptManager = dept.deptManager;
            model.deptNameAr = dept.deptNameAr;
            model.deptNameEn = dept.deptNameEn;
            model.deptVicManager = dept.deptVicManager;
            model.Mid = dept.Mid;
            return model;
        }

        public List<EmployeeViewModel> GetDepartmentManagers(int DeptId)
        {
            List<EmployeeViewModel> model = new List<EmployeeViewModel>();
            tdManag_Department dept = db.tdManag_Department.Find(DeptId);
            if (dept.deptManager != null)
            {
                int Manager = (int)dept.deptManager;
                EmployeeViewModel ManagerData= employee.GetEmployeeById(Manager);
                model.Add(ManagerData);

            }

            if (dept.deptVicManager !=null)
            {
                int deptVicManager = (int)dept.deptVicManager;
                EmployeeViewModel ManagerData = employee.GetEmployeeById(deptVicManager);
                model.Add(ManagerData);
            }

            return model;
        }

        public List<DepartmentViewModel> GetDeptByMId(int Mid)
        {
            List<DepartmentViewModel> model = db.tdManag_Department.Where(x => x.Mid == Mid).Select(u =>
                  new DepartmentViewModel
                  {
                      deptNameAr = u.deptNameAr,
                      deptNameEn = u.deptNameEn,
                      Mid = u.Mid,
                      deptId = u.deptId,

                  }
            ).ToList();
            return model;

        }
        public int UpdateDept(DepartmentModel model)
        {
            tdManag_Department dept = db.tdManag_Department.Find(model.deptId);
            dept.deptNameAr = model.deptNameAr;
            dept.deptNameEn = model.deptNameEn;
            dept.deptManager = model.deptManager;
            dept.deptVicManager = model.deptVicManager;
            db.Entry(dept).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

    
    }
}
