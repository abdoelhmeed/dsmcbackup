﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class MaintenanceHand : IMaintenanceHand
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int add(MaintenanceHandModel model)
        {
            td_MaintenanceHandling maintenanceHand = new td_MaintenanceHandling
            {
                AspiratDetails=model.AspiratDetails,
                asstesId=model.asstesId,
                Finished=false,
                MaintenanceDetails=model.MaintenanceDetails,
                MaintenanceEndDate=model.MaintenanceEndDate,
                MaintenanceStartDate=model.MaintenanceStartDate,
                OrderMaintenanceIId=model.OrderMaintenanceIId,
                MStuts="New",
                Visible=true
            };
            db.td_MaintenanceHandling.Add(maintenanceHand);
           int res= db.SaveChanges();
            if (res > 0)
            {
                MaintenanceOrder MOrder = db.MaintenanceOrders.Find(model.OrderMaintenanceIId);
                MOrder.OrderStuts = "On Maintenance ";
                db.Entry(MOrder).State = EntityState.Modified;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int CompleteMaintenance(int Id)
        {
            td_MaintenanceHandling handling = db.td_MaintenanceHandling.Find(Id);
            handling.MStuts = "Completed";
            handling.Finished = true;
            handling.MaintenanceEndDate = DateTime.Now;
            db.Entry(handling).State = EntityState.Modified;
           int res= db.SaveChanges();
            if (res > 0)
            {
                MaintenanceOrder order = db.MaintenanceOrders.Find(handling.OrderMaintenanceIId);
                order.Done = true;
                order.OrderStuts = "Completed";
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                List<td_DepartmentAsstes> AssList = db.td_DepartmentAsstes.Where(x=> x.AsId==handling.asstesId).ToList();
                if (AssList.Count > 0)
                {
                    foreach (var item in AssList)
                    {
                        td_DepartmentAsstes Ass = db.td_DepartmentAsstes.Find(item.id);
                        Ass.DeptMaintenanceDateReq = DateTime.Now;
                        db.Entry(Ass).State = EntityState.Modified;
                        db.SaveChanges();

                    }

                }
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int delete(int Id)
        {
            td_MaintenanceHandling model = db.td_MaintenanceHandling.Find(Id);
            db.Entry(model).State = EntityState.Modified;
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<MaintenanceHandViewModel> GetByAsstesId(int asstesId)
        {
            List<MaintenanceHandViewModel> model = db.td_MaintenanceHandling.Where(u => u.asstesId == asstesId).Select(x =>
                  new MaintenanceHandViewModel
                  {
                      asstesId=x.asstesId,
                      AspiratDetails=x.AspiratDetails,
                      asstesName=x.td_Assets.AsName,
                      Finished=x.Finished,
                      MaintenanceDetails=x.MaintenanceDetails,
                      MaintenanceEndDate=x.MaintenanceEndDate,
                      MaintenanceStartDate=x.MaintenanceStartDate,
                      OrderMaintenanceDate=x.MaintenanceStartDate.ToString(),
                      OrderMaintenanceEmployeeEn=x.MaintenanceOrder.td_employee.empFNameEn +" "+ x.MaintenanceOrder.td_employee.empSNameEn+" "+ x.MaintenanceOrder.td_employee.empTNameEn,
                      OrderMaintenanceEmployeeAr = x.MaintenanceOrder.td_employee.empFNameEn + " " + x.MaintenanceOrder.td_employee.empSNameEn + " " + x.MaintenanceOrder.td_employee.empTNameEn,
                      id=x.id,
                      OrderMaintenanceIId=x.MaintenanceOrder.id,
                      Visible=x.Visible
                  }
            ).ToList();

            return model;
        }

        public MaintenanceHandViewModel GetById(int Id)
        {
            td_MaintenanceHandling x = db.td_MaintenanceHandling.Find(Id);
            MaintenanceHandViewModel model = new MaintenanceHandViewModel
            {
                asstesId = x.asstesId,
                AspiratDetails = x.AspiratDetails,
                asstesName = x.td_Assets.AsName,
                Finished = x.Finished,
                MaintenanceDetails = x.MaintenanceDetails,
                MaintenanceEndDate = x.MaintenanceEndDate,
                MaintenanceStartDate = x.MaintenanceStartDate,
                OrderMaintenanceDate = x.MaintenanceStartDate.ToString(),
                OrderMaintenanceEmployeeEn = x.MaintenanceOrder.td_employee.empFNameEn + " " + x.MaintenanceOrder.td_employee.empSNameEn + " " + x.MaintenanceOrder.td_employee.empTNameEn,
                OrderMaintenanceEmployeeAr = x.MaintenanceOrder.td_employee.empFNameEn + " " + x.MaintenanceOrder.td_employee.empSNameEn + " " + x.MaintenanceOrder.td_employee.empTNameEn,
                id = x.id,
                MStuts=x.MStuts,
                OrderMaintenanceIId = x.MaintenanceOrder.id,
                Visible = x.Visible
            };

            return model;
        }

        public List<MaintenanceHandViewModel> GetByOId(int OId)
        {
            List<MaintenanceHandViewModel> model = db.td_MaintenanceHandling.Where(u => u.MaintenanceOrder.OId == OId)
                .Select(x => new MaintenanceHandViewModel
                {
                    asstesId = x.asstesId,
                    AspiratDetails = x.AspiratDetails,
                    asstesName = x.td_Assets.AsName,
                    Finished = x.Finished,
                    MaintenanceDetails = x.MaintenanceDetails,
                    MaintenanceEndDate = x.MaintenanceEndDate,
                    MStuts=x.MStuts,
                    MaintenanceStartDate = x.MaintenanceStartDate,
                    OrderMaintenanceDate = x.MaintenanceStartDate.ToString(),
                    OrderMaintenanceEmployeeEn = x.MaintenanceOrder.td_employee.empFNameEn + " " + x.MaintenanceOrder.td_employee.empSNameEn + " " + x.MaintenanceOrder.td_employee.empTNameEn,
                    OrderMaintenanceEmployeeAr = x.MaintenanceOrder.td_employee.empFNameEn + " " + x.MaintenanceOrder.td_employee.empSNameEn + " " + x.MaintenanceOrder.td_employee.empTNameEn,
                    id = x.id,
                    OrderMaintenanceIId = x.MaintenanceOrder.id,
                    Visible = x.Visible
                }).ToList();
            return model;
        }

        public List<MaintenanceHandViewModel> GetByOrderId(int orderId)
        {
            List<MaintenanceHandViewModel> model = db.td_MaintenanceHandling.Where(u => u.OrderMaintenanceIId == orderId)
                .Select(x =>  new MaintenanceHandViewModel
                  {
                      asstesId = x.asstesId,
                      AspiratDetails = x.AspiratDetails,
                      asstesName = x.td_Assets.AsName,
                      Finished = x.Finished,
                      MStuts=x.MStuts,
                      MaintenanceDetails = x.MaintenanceDetails,
                      MaintenanceEndDate = x.MaintenanceEndDate,
                      MaintenanceStartDate = x.MaintenanceStartDate,
                      OrderMaintenanceDate = x.MaintenanceStartDate.ToString(),
                      OrderMaintenanceEmployeeEn = x.MaintenanceOrder.td_employee.empFNameEn + " " + x.MaintenanceOrder.td_employee.empSNameEn + " " + x.MaintenanceOrder.td_employee.empTNameEn,
                      OrderMaintenanceEmployeeAr = x.MaintenanceOrder.td_employee.empFNameEn + " " + x.MaintenanceOrder.td_employee.empSNameEn + " " + x.MaintenanceOrder.td_employee.empTNameEn,
                      id = x.id,
                      OrderMaintenanceIId = x.MaintenanceOrder.id,
                      Visible = x.Visible
                  }).ToList();
            return model;
        }

        public MaintenanceHandModel GetUpdateById(int Id)
        {
            td_MaintenanceHandling  handling= db.td_MaintenanceHandling.Find(Id);
            MaintenanceHandModel model = new MaintenanceHandModel
            {
                OrderMaintenanceIId = handling.OrderMaintenanceIId,
                AspiratDetails = handling.AspiratDetails,
                asstesId = handling.asstesId,
                Finished = handling.Finished,
                id = handling.id,
                MStuts = handling.MStuts,
                MaintenanceDetails = handling.MaintenanceDetails,
                MaintenanceEndDate=handling.MaintenanceEndDate,
                MaintenanceStartDate=handling.MaintenanceStartDate,
                Visible=handling.Visible
            };
            return model;

        }

        public int Update(MaintenanceHandModel model)
        {
            throw new NotImplementedException();
        }
    }
}
