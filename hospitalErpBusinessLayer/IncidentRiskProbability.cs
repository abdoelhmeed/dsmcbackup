﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class IncidentRiskProbability : IIncidentRiskProbability
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(IncidentRiskProbabilityModel model)
        {
            td_IncidentRiskProbabilityMain RKP = new td_IncidentRiskProbabilityMain
            {
                OId=model.OId,
                RPMINameEn=model.RPMINameEn,
                RPMNameAr=model.RPMNameAr,
                Visible=true
            };
            db.td_IncidentRiskProbabilityMain.Add(RKP);
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Delete(int RPMId)
        {
            td_IncidentRiskProbabilityMain x = db.td_IncidentRiskProbabilityMain.Find(RPMId);
            x.Visible = false;
            db.Entry(x).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
            x.Visible = false;
        }

        public IncidentRiskProbabilityModel GetById(int RPMId)
        {
            td_IncidentRiskProbabilityMain x = db.td_IncidentRiskProbabilityMain.Find(RPMId);
            IncidentRiskProbabilityModel model = new IncidentRiskProbabilityModel
            {
                OId=x.OId,
                RPMId=x.RPMId,
                RPMINameEn=x.RPMINameEn,
                RPMNameAr=x.RPMNameAr,
                Visible=x.Visible

            };
            return model;
        }

        public List<IncidentRiskProbabilityModel> GetByOId(int OId)
        {
            List<IncidentRiskProbabilityModel> model = db.td_IncidentRiskProbabilityMain.Where(u => u.Visible == true && u.OId == OId)
                .Select(x => new IncidentRiskProbabilityModel
                {
                    OId = x.OId,
                    RPMId = x.RPMId,
                    RPMINameEn = x.RPMINameEn,
                    RPMNameAr = x.RPMNameAr,
                    Visible = x.Visible
                }).ToList();

            return model;
        }

        public int Update(IncidentRiskProbabilityModel model)
        {
            td_IncidentRiskProbabilityMain x = db.td_IncidentRiskProbabilityMain.Find(model.RPMId);
            x.RPMINameEn = model.RPMINameEn;
            x.RPMNameAr = model.RPMNameAr;
            db.Entry(x).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
}
