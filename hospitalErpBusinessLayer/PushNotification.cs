﻿using hospitalErpBusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hospitalErpDomainLayer;
using hospitalErpRepository;
using System.Data.Entity;

namespace hospitalErpBusinessLayer
{
    public class PushNotification : IPushNotification
    {
        erpHospitalEntities db = new erpHospitalEntities();
        public int Add(PushNotificationModel model)
        {
            td_employee employee = db.td_employee.Find(model.empId);
            model.UserId = employee.UserId;
            td_Notifications Not = new td_Notifications
            {
                isRead=false,
                nBody=model.nBody,
                nTitel=model.nTitel,
                nUrl=model.nUrl,
                UserId=model.UserId,
                ReadDate=DateTime.Now,
                NType=model.NType
            };
            db.td_Notifications.Add(Not);
            int res=db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
            
        }

        public int Update(int Id)
        {
            td_Notifications not = db.td_Notifications.Find(Id);
            not.ReadDate = DateTime.Now;
            not.isRead = true;
            db.Entry(not).State = EntityState.Modified;
            int res = db.SaveChanges();
            if (res > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }


        }

        public void UpdateAllByEmpIdAndNType(int empId, string Ntype)
        {
            td_employee employee = db.td_employee.Find(empId);
            string UserId = employee.UserId;
            List<td_Notifications> model = db.td_Notifications.Where(x => x.NType == Ntype && x.UserId == UserId).ToList();
            foreach (var item in model)
            {
                td_Notifications not = db.td_Notifications.Find(item.id);
                not.ReadDate = DateTime.Now;
                not.isRead = true;
                db.Entry(not).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

       public List<PushNotificationModel> GetNotification(string UserId)
        {
            erpHospitalEntities db = new erpHospitalEntities();
            List<PushNotificationModel> model = db.td_Notifications.Where(x => x.UserId == UserId && x.isRead == false).Select(u => new PushNotificationModel
            {

                isRead = u.isRead,
                UserId = u.UserId,
                id = u.id,
                nBody = u.nBody,
                nTitel = u.nTitel,
                NType = u.NType,
                nUrl = u.nUrl,
                ReadDate = u.ReadDate
            }).ToList();

            return model;
        }

    }
}
