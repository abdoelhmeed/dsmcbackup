﻿using System.Web;
using System.Web.Optimization;

namespace hospitalErpSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bootstrap/Styles")
                .Include("~/assets/css/bootstrap.min.css")
                .Include("~/assets/css/jquery-ui.min.css")
                .Include("~/assets/css/simple-line-icons.css")
            
                .Include("~/assets/css/bootstrap-datetimepicker.min.css"));

            bundles.Add(new StyleBundle("~/plugin/styles")
                .Include("~/assets/css/jquery-ui.min.css")
                .Include("~/assets/css/bootstrap-datepicker3.min.css")
                .Include("~/assets/css/ui.jqgrid.min.css")
                .Include("~/assets/css/chosen.min..css"));


            bundles.Add(new StyleBundle("~/ace/Styles")
                .Include("~/assets/css/ace.min.css")
                .Include("~/assets/css/ace-skins.min.css"));


            //------------------------ #Script# ------------------------
            bundles.Add(new ScriptBundle("~/Main/Script")
               .Include("~/assets/js/jquery-2.1.4.min.js")
                .Include("~/assets/js/bootstrap.min.js")
                .Include("~/assets/js/jquery-ui.min.js")
                .Include("~/assets/js/jquery.validate.min.js")
                .Include("~/assets/js/jquery.validate.unobtrusive.min.js")
                .Include("~/assets/js/date-format.js")
                .Include("~/assets/js/chosen.jquery.min.js"));

            bundles.Add(new ScriptBundle("~/plugin/Script")
              .Include("~/assets/js/jquery-ui.custom.min.js")
              .Include("~/assets/js/jquery.ui.touch-punch.min.js")
              .Include("~/assets/js/wizard.min.js")
              .Include("~/assets/js/jquery.easypiechart.min.js")
              .Include("~/assets/js/jquery.sparkline.index.min.js")
              .Include("~/assets/js/jquery.flot.min.js")
              .Include("~/assets/js/jquery.flot.pie.min.js")
              .Include("~/assets/js/jquery.flot.resize.min.js"));

            bundles.Add(new ScriptBundle("~/ace_plugin/Script")
                .Include("~/assets/js/ace-elements.min.js")
                .Include("~/assets/js/ace.min.js")
                .Include("~/assets/js/grid.locale-en.js")
                );

            bundles.Add(new ScriptBundle("~/aceCustem_Script/Script")
                .Include("~/assets/js/bootstrap-datepicker.min.js")
                .Include("~/assets/js/jquery.jqGrid.min.js")
                .Include("~/assets/js/jsgrid.validation.js")
                );

            bundles.Add(new ScriptBundle("~/DataTable/Script")
                .Include("~/assets/js/jquery.dataTables.min.js")
                .Include("~/assets/js/jquery.dataTables.bootstrap.min.js")
                .Include("~/assets/js/dataTables.buttons.min.js")

                 .Include("~/assets/js/buttons.flash.min.js")
                 .Include("~/assets/js/buttons.html5.min.js")
                 .Include("~/assets/js/buttons.print.min.js")
                 .Include("~/assets/js/dataTables.buttons.min.js")
                );
        }
    }
}
