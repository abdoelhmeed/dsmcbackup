﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Controllers
{
    [Authorize]
    public class NotificationSettingsController : Controller
    {
        NotificationsSetting Setting = new NotificationsSetting();
        Employee emp = new Employee();
        EmployeesDepartmentManagment EmployeesDepartmentManagment = new EmployeesDepartmentManagment();
        Department Department = new Department();
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidSettingModule;
            ViewBag.SitMap = Resources.Resource.NotificationsSetting;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<NotificationsViewSettingMode> model= Setting.GetByOId(OId);
            return View(model);
        }

        public ActionResult Create()
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<DepartmentViewModel> DeptLis = Department.GetAllByOId(OId);
            ViewBag.Dept = DeptLis.ToList();
            NotificationsSettingMode model = new NotificationsSettingMode();
            model.OId = OId;
           return PartialView("Create", model);
        }

        [HttpPost]
        public JsonResult Create(NotificationsSettingMode model)
        {
            Setting.Add(model);
            return Json("Saved", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetEmployees(int DeptId)
        {
            List<employeeDepartmentManagmentViewModel> model= EmployeesDepartmentManagment.GetEmployeesByDepartmentId(DeptId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Delete(int Id)
        {
            Setting.Delete(Id);
            return RedirectToAction("Index");
        }

     
    }
}