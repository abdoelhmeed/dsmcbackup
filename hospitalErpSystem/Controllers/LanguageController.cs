﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Globalization;

namespace hospitalErpSystem.Controllers
{
    public class LanguageController : Controller
    {
        // GET: Language
        public ActionResult change(string Lang)
        {

            if (Lang!=null)
            {
                CultureInfo en = new CultureInfo(Lang);
                en.DateTimeFormat.SetAllDateTimePatterns(new string[] { "dd/MM/yyyy" }, 'd');
                Thread.CurrentThread.CurrentCulture = en;
                Thread.CurrentThread.CurrentUICulture = en;
            }

            HttpCookie cookie = new HttpCookie("lang");
            cookie.Value = Lang;
            Response.Cookies.Add(cookie);

            return RedirectToAction("Index","Home");
        }
    }
}