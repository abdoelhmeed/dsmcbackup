﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hospitalErpSystem.Models;
using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using PagedList;
using PagedList.Mvc;

namespace hospitalErpSystem.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        PushNotification push = new PushNotification();
        EmployeeLicenses Licenses = new EmployeeLicenses();
        Employee emp = new Employee();
        Assets Assets = new Assets();
        Department Department = new Department();
        Policy Policy = new Policy();
        MaintenOrder Orders = new MaintenOrder();
        Organization Organization = new Organization();
        Management Management = new Management();
        Incident Incident =new Incident();
        OrganizationLicenses OLicenses = new OrganizationLicenses();
        AsstesDepartment AsstesDepartment = new AsstesDepartment();
        public ActionResult Index()
        {
            if (!User.IsInRole("Admin"))
            {
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                int empId = emp.GetEmpIDByUserId(UserId);
                int EmpNumber = emp.GetEmployeeSelectioByOId(OId).Count;

                ViewBag.EmpNumber = EmpNumber;
                ViewBag.Assets = Assets.getByOId(OId).Count;
                ViewBag.Department = Department.GetAllByOId(OId).Count;
                ViewBag.Orders = Orders.GetByOId(OId).Count;
                ViewBag.Organization = Organization.GetAllOrganization().Count;
                ViewBag.Management = Management.GetAllByOId(OId).Count;
                ViewBag.Policy = Policy.GetPolicies(OId).Count;
                ViewBag.Incident = Incident.GetIncidentByOId(OId).Count;
            }

            return View();
        }
        [HttpGet]
        public ActionResult MyLicenses()
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            push.UpdateAllByEmpIdAndNType(empId, "Licenses  expiry");
            List<EmployeeLicensesViewsModel> model= Licenses.EmployeeLicensesByEmployeeId(empId);
            return View(model);
        }
        [HttpGet]
        public ActionResult ExpiryLicenses()
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            push.UpdateAllByEmpIdAndNType(empId, "Employees Expirytion Licenses");
            DateTime endDate = DateTime.Now.AddDays(-5);
            List<EmployeeLicensesViewsModel> model = Licenses.EmployeeLicensesByOrganizationId(OId).Where(x => x.LEndDate <= endDate).ToList();
            return View(model);

        }
        
        [HttpGet]
        public ActionResult Maintenance()
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            push.UpdateAllByEmpIdAndNType(empId, "Maintenance");
            List<AsstesDepartmentViewModel> model = new List<AsstesDepartmentViewModel>();
            List<AsstesDepartmentViewModel> modelList = AsstesDepartment.getByOId(OId);
            foreach (var item in modelList)
            {
                int MNotification = -1 * (int)item.NotificationBefore;
              if(item.DeptMaintenanceDateReq <= DateTime.Now.AddDays(MNotification))
                {
                    model.Add(item);
                }
            }


            return View(model);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Notifications(int? Page)
        {
            ViewBag.SubSystem = "Notifications";
            PushNotification Notif = new PushNotification();
            string UserId = User.Identity.GetUserId();
            List<PushNotificationModel> model = Notif.GetNotification(UserId);
            return View(model.ToPagedList(Page ?? 1, 5));
        }
        
        public  JsonResult GetNotifications()
        {
            string UserId = User.Identity.GetUserId();
            NotificationsRepository Repository = new NotificationsRepository();
            List<NotificationsModel> model = Repository.GetAllNotification(UserId).ToList();
            return Json(model,JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllNotifications()
        {
            string UserId = User.Identity.GetUserId();
            PushNotification Push = new PushNotification();
            List<PushNotificationModel> model= Push.GetNotification(UserId);
            return Json(model,JsonRequestBehavior.AllowGet);
        }

        public ActionResult OrganizationLicenses()
      {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            push.UpdateAllByEmpIdAndNType(empId, "Organization Licenses");
            DateTime endDate = DateTime.Now.AddDays(-5);
            List<OrganizationLicensesViewsModel> model = OLicenses.GetListOrganizationLicensesByOId(OId).Where(x => x.OLEndDate <= endDate && x.OLValidity == true).ToList();
            return View(model);
      }
    }
}