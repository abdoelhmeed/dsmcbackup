﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using hospitalErpSystem.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



[assembly: OwinStartupAttribute(typeof(hospitalErpSystem.Startup))]
namespace hospitalErpSystem
{

    public partial class Startup
    {
        ApplicationDbContext db = new ApplicationDbContext();
        SelectionType stype = new SelectionType();
        //Employee employee = new Employee();
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
            //EmpAccount();
            createRolesandUsers();
            AdminAccount();
            Notification();
        }
        public void AdminAccount()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var UserAdmin = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var User = new ApplicationUser();
            User.Email = "Admin@gmail.com";
            User.UserName = "Admin";
            var check = UserAdmin.Create(User, "Admin@123");
            if (check.Succeeded)
            {
                if (roleManager.RoleExists("Admin"))
                {
                    var result1 = UserAdmin.AddToRole(User.Id, "Admin");
                    AddSelectionTypeList();
                }
            }
        }
        private void createRolesandUsers()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Admin";
                roleManager.Create(role);
            }


            if (!roleManager.RoleExists("Setting Management"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Setting Management";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("HR Management"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "HR Management";
                roleManager.Create(role);
            }

            //if (!roleManager.RoleExists("Assets Management"))
            //{
            //    var role = new IdentityRole();
            //    role.Id = ID();
            //    role.Name = "Assets Management";
            //    roleManager.Create(role);
            //}

            //if (!roleManager.RoleExists("Agenda Management"))
            //{
            //    var role = new IdentityRole();
            //    role.Id = ID();
            //    role.Name = "Agenda Management";
            //    roleManager.Create(role);
            //}

            //if (!roleManager.RoleExists("Maintenance Management"))
            //{
            //    var role = new IdentityRole();
            //    role.Id = ID();
            //    role.Name = "Maintenance Management";
            //    roleManager.Create(role);
            //}


            if (!roleManager.RoleExists("Policies Management"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Policies Management";
                roleManager.Create(role);
            }

            //if (!roleManager.RoleExists("Requests Management"))
            //{
            //    var role = new IdentityRole();
            //    role.Id = ID();
            //    role.Name = "Requests Management";
            //    roleManager.Create(role);
            //}

            if (!roleManager.RoleExists("Incident Management"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Incident Management";
                roleManager.Create(role);
            }

        }
        public string ID()
        {
            StringBuilder builder = new StringBuilder();
            Enumerable
               .Range(65, 26)
                .Select(e => ((char)e).ToString())
                .Concat(Enumerable.Range(97, 26).Select(e => ((char)e).ToString()))
                .Concat(Enumerable.Range(0, 10).Select(e => e.ToString()))
                .OrderBy(e => Guid.NewGuid())
                .Take(11)
                .ToList().ForEach(e => builder.Append(e));
            string id = builder.ToString();
            return id;
        }
        public void AddSelectionTypeList()
        {
            List<SelectionTypeModel> List = new List<SelectionTypeModel> {

                new SelectionTypeModel{id=101,EnglishName="Bank Name",ArabicName="اسم البنك"},
                new SelectionTypeModel{id=102,EnglishName="City Districts",ArabicName="منطقة المدينة"},
                new SelectionTypeModel{id=103,EnglishName="Class",ArabicName="درجة"},
                new SelectionTypeModel{id=104,EnglishName="Committee",ArabicName="لجنة"},
                new SelectionTypeModel{id=105,EnglishName="Contact Type",ArabicName="نوع الاتصال"},
                new SelectionTypeModel{id=106,EnglishName="Contract Type",ArabicName="نوع العقد"},
                new SelectionTypeModel{id=107,EnglishName="ID Type",ArabicName="المعرف"},
                new SelectionTypeModel{id=108,EnglishName="Job Title",ArabicName="المسمى الوظيفي"},
                new SelectionTypeModel{id=109,EnglishName="Licenses Employee",ArabicName="تراخيص الموظفين"},
                new SelectionTypeModel{id=110,EnglishName="Marital Status",ArabicName="الحالة الاجتماعية"},
                new SelectionTypeModel{id=111,EnglishName="Military Status",ArabicName="الوضع العسكري"},
                new SelectionTypeModel{id=112,EnglishName="Nationality",ArabicName="الجنسية"},
                new SelectionTypeModel{id=113,EnglishName="Official holidays",ArabicName="العطلات الرسمية"},
                new SelectionTypeModel{id=114,EnglishName="Patient Contract Classes",ArabicName="فصول عقود المرضى" },
                new SelectionTypeModel{id=115,EnglishName="Qualification",ArabicName="الشهادات"},
                new SelectionTypeModel{id=116,EnglishName="Qualification Place",ArabicName="الجامعة"},
                new SelectionTypeModel{id=117,EnglishName="Religion",ArabicName="الديانة"},
                new SelectionTypeModel{id=118,EnglishName="Skill Sections",ArabicName="مجال المهارة"},
                new SelectionTypeModel{id=119,EnglishName="Skill",ArabicName="مهارات"},
                new SelectionTypeModel{id=120,EnglishName="Vacation Manager Reason ",ArabicName="السبب الاداري عطلة"},
                new SelectionTypeModel{id=121,EnglishName="Vacations Type ",ArabicName="نوع الاجازات"},
                new SelectionTypeModel{id=122,EnglishName="Assets  Type ",ArabicName="نوع الأصول"},
                new SelectionTypeModel{id=123,EnglishName="Licenses Organization",ArabicName="تراخيص المنظمة" },
                new SelectionTypeModel{id=124,EnglishName="Career Level",ArabicName="المستوى الوظيفي" },
                new SelectionTypeModel{id=125,EnglishName="Gender",ArabicName="النوع" },
                new SelectionTypeModel{id=126,EnglishName="Title",ArabicName="اللقب" },
                new SelectionTypeModel{id=127,EnglishName="Licenses Employee Place",ArabicName="مكان ترخيص الموظف" },
                 new SelectionTypeModel{id=128,EnglishName="Organization Licenses Place",ArabicName="مكان ترخيص المنظمة" }
            };

            foreach (var item in List)
            {
                stype.AddSelectionType(item);
            }
        }

        public void Notification()
        {
            UpdateNotifications cheked = new UpdateNotifications();
            NotificationsSetting Setting = new NotificationsSetting();
            Organization org = new Organization();

            if (cheked.GetNumberByDate() == 0)
            {
                EmployeesLicenses(Setting, org);
                ORGLicenses(Setting, org);
                //Maintenance(org);
                cheked.Add();
            }

        }

        //public void EmpAccount()
        //{

        //    List<EmployeeViewModel> ListEmp = employee.GetAllEmployee();
        //    foreach (var item in ListEmp)
        //    {
               
        //            userseLogin model = EmploysListByMail.Get((int)item.empid);
        //        if (model != null)
        //        {
        //            if (model.UserEmail !=null)
        //            {

        //                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
        //                var UserAdmin = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        //                var User = new ApplicationUser();
        //                User.Email = model.UserEmail;
        //                User.UserName = model.UserEmail;
        //                var check = UserAdmin.Create(User, "User@123");
        //                if (check.Succeeded)
        //                {
        //                    employee.UpdateUserId(User.Id, (int)item.empid);
        //                }
        //            }
                   
        //        }
              
        //    }

        //}



        /// <summary>
        /// Maintenance Notification
        /// </summary>
        /// <param name="org">Organization</param>
        //private static void Maintenance(Organization org)
        //{

        //    List<OrganizationViewModel> OrdList = org.GetAllOrganization();
        //    PushNotification pushNotification = new PushNotification();
        //    if (OrdList.Count > 0)
        //    {
        //        Assets assets = new Assets();
        //        Employee emp = new Employee();

        //        List<AsstesDepartmentModel> model = new List<AsstesDepartmentModel>();
        //        Department Department = new Department();
        //        foreach (var item in OrdList)
        //        {
        //            List<AssetsViewModel> assetModel = assets.getByOId(item.OId).Where(x => x.AsMaintenanceType == 1).ToList();
        //            if (assetModel.Count > 0)
        //            {
        //                foreach (var Aitem in assetModel)
        //                {
        //                    List<AsstesDepartmentModel> modelByAssetId = onMaintenance( Aitem);

        //                    model.AddRange(modelByAssetId);
        //                }
        //                if (model.Count > 0)
        //                {
        //                    foreach (var Pitem in model)
        //                    {

        //                        int MNotification = -1 * (int)Pitem.NotificationBefore;
        //                        if (Pitem.DeptMaintenanceDateReq <= DateTime.Now.AddDays(MNotification))
        //                        {
        //                            DepartmentModel DeptData = Department.getById((int)Pitem.DeptId);
        //                            if (emp.GetEmployeeById((int)DeptData.deptVicManager).UserId != null && emp.GetEmployeeById((int)DeptData.deptManager).UserId != null)
        //                            {
        //                                PushNotificationModel MPushModel = new PushNotificationModel
        //                                {
        //                                    empId = DeptData.deptManager,
        //                                    nBody = "Asstes in Maintenance" + Pitem.DeptMaintenanceDateReq.ToString(),
        //                                    nTitel = " Maintenance",
        //                                    ReadDate = DateTime.Now,
        //                                    NType = "Maintenance",
        //                                    nUrl = "/Home/Maintenance",
        //                                    isRead = false,
        //                                    UserId = emp.GetEmployeeById((int)DeptData.deptManager).UserId
        //                                };
        //                                pushNotification.Add(MPushModel);

        //                                PushNotificationModel PushModel = new PushNotificationModel
        //                                {
        //                                    empId = DeptData.deptVicManager,
        //                                    nBody = "Asstes in Maintenance" + Pitem.DeptMaintenanceDateReq.ToString(),
        //                                    nTitel = " Maintenance",
        //                                    ReadDate = DateTime.Now,
        //                                    NType = "Maintenance",
        //                                    nUrl = "/Home/Maintenance",
        //                                    isRead = false,
        //                                    UserId = emp.GetEmployeeById((int)DeptData.deptVicManager).UserId
        //                                };
        //                                pushNotification.Add(PushModel);
        //                            }


        //                        }
        //                    }
        //                }

        //            }
        //        }

        //    }
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MaintenOrder">Maintenance Order</param>
        /// <param name="AsstesDepartment">Asstes Department</param>
        /// <param name="Aitem">   not done</param>
        /// <returns></returns>
        //private static List<AsstesDepartmentModel> onMaintenance( AssetsViewModel Aitem)
        //{
        //    MaintenOrder MaintenOrder = new MaintenOrder();
        //    AsstesDepartment AsstesDepartment = new AsstesDepartment();
        //    List<AsstesDepartmentModel> modelByAssetId = new List<AsstesDepartmentModel>(); 
        //    List<MaintenOrderViewModel> inMaintenice = MaintenOrder.GetByAssteId(Aitem.id).Where(x => x.OrderStuts != "Refusal" && x.OrderStuts != "Completed").ToList();
        //    if (inMaintenice.Count > 0)
        //    {
        //        modelByAssetId=AsstesDepartment.GetByAId(Aitem.id);
        //    }
        //    return modelByAssetId;
        //}

        private static void ORGLicenses(NotificationsSetting Setting, Organization org)
        {
            Employee emp = new Employee();
            List<OrganizationViewModel> OrdList = org.GetAllOrganization();
            if (OrdList.Count > 0)
            {
                PushNotification pushNotification = new PushNotification();
                DateTime endDate = DateTime.Now.AddDays(-5);
                OrganizationLicenses OLicenses = new OrganizationLicenses();
                foreach (var item in OrdList)
                {
                    List<OrganizationLicensesViewsModel> Omdel = OLicenses.GetListOrganizationLicensesByOId(item.OId).Where(x => x.OLEndDate <= endDate && x.OLValidity == true).ToList();

                    if (Omdel.Count > 0)
                    {
                        List<NotificationsViewSettingMode> NList = Setting.GetByOId(item.OId).Where(x => x.OId == item.OId && x.Type == 2).ToList();
                        foreach (var nitem in Omdel)
                        {
                            foreach (var snitem in NList)
                            {
                                PushNotificationModel PushModel = new PushNotificationModel
                                {
                                    empId = snitem.empId,
                                    nBody = "Organization Licenses is expiry  in " + nitem.strOLEndDate,
                                    nTitel = " Organization Licenses",
                                    ReadDate = DateTime.Now,
                                    NType = "Organization Licenses",
                                    nUrl = "/Home/OrganizationLicenses",
                                    isRead = false,
                                    UserId = emp.GetEmployeeById(snitem.empId).UserId
                                };
                                pushNotification.Add(PushModel);
                            }
                        }
                    }

                }
            }
        }
        private static void EmployeesLicenses(NotificationsSetting Setting, Organization org)
        {
            PushNotification pushNotification = new PushNotification();
            EmployeeLicenses Licenses = new EmployeeLicenses();
            Employee emp = new Employee();
            foreach (var Oitem in org.GetAllOrganization())
            {
                List<EmployeeViewModel> Emps = emp.GetEmployeeByOId(Oitem.OId).Where(x => x.UserId != null || x.UserId != "").ToList();
                foreach (var item in Emps)
                {
                    DateTime endDate = DateTime.Now.AddDays(-5);
                    List<EmployeeLicensesViewsModel> Llist = Licenses.EmployeeLicensesByEmployeeId((int)item.empid).Where(x => x.LValidity = true && x.LEndDate <= endDate).ToList();
                    foreach (var itemL in Llist)
                    {
                        PushNotificationModel PushModel = new PushNotificationModel
                        {
                            empId = itemL.empId,
                            nBody = "Your Licenses is expiry  in " + itemL.LEndDate.ToShortDateString(),
                            nTitel = "Licenses  expiry",
                            ReadDate = DateTime.Now,
                            NType = "Licenses  expiry",
                            nUrl = "/Home/MyLicenses",
                            isRead = false,
                            UserId = emp.GetEmployeeById((int)item.empid).UserId
                        };
                        pushNotification.Add(PushModel);
                    }

                    List<NotificationsViewSettingMode> SettingList = Setting.GetByOId(Oitem.OId).Where(x => x.Type == 1).ToList();

                    if (Llist.Count > 0)
                    {
                        foreach (var Sitem in SettingList)
                        {
                            EmployeeLicensesViewsModel EmpIdformList = Llist.First(x => x.empId == item.empid);
                            PushNotificationModel PushSettingModel = new PushNotificationModel
                            {
                                empId = Sitem.empId,
                                nBody = item.empFNameEn + " " + item.empSNameEn + " " + item.empTNameEn + "Licenses is expiry  in " + EmpIdformList.LEndDate.ToShortDateString(),
                                nTitel = "Licenses  expiry",
                                ReadDate = DateTime.Now,
                                NType = "Employees Expirytion Licenses",
                                nUrl = "/Home/ExpiryLicenses",
                                isRead = false,
                                UserId = emp.GetEmployeeById(Sitem.empId).UserId
                            };
                            pushNotification.Add(PushSettingModel);
                        }
                    }


                }



            }
        }

    }



}
