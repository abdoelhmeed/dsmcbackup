﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace hospitalErpSystem
{
    public class MvcApplication : System.Web.HttpApplication
    {
        string connString = ConfigurationManager.ConnectionStrings
         ["DefaultConnection"].ConnectionString;
        protected void Application_Start()
        {
         
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            SqlDependency.Start(connString);

        }


        protected void Application_End()
        {
            //Stop SQL dependency
            SqlDependency.Stop(connString);
        }


        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["lang"];
            if (cookie != null)
            {

                CultureInfo en = new CultureInfo(cookie.Value);
                en.DateTimeFormat.SetAllDateTimePatterns(new string[] { "dd/MM/yyyy" }, 'd');
                Thread.CurrentThread.CurrentCulture = en;
                Thread.CurrentThread.CurrentUICulture = en;

            }
            else
            {
                CultureInfo en = new CultureInfo("en-IE");
                en.DateTimeFormat.SetAllDateTimePatterns(new string[] { "dd/MM/yyyy" }, 'd');
                Thread.CurrentThread.CurrentCulture = en;
                Thread.CurrentThread.CurrentUICulture = en;
            }
        }

       

    }
}
