﻿using System.Web.Mvc;

namespace hospitalErpSystem.Areas.AsstesModule
{
    public class AsstesModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AsstesModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AsstesModule_default",
                "AsstesModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}