﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.AsstesModule.Controllers
{
    [Authorize]
    public class SubClassificationController : Controller
    {

        MClassification MClass = new MClassification();
        SClassification SClass = new SClassification();
        Employee emp = new Employee();
        // GET: AsstesModule/SubClassification
        public ActionResult Index()
        {
            ViewBag.SubSystem = @Resources.Resource.SidAssetsModule;
            ViewBag.SitMap = @Resources.Resource.Category;
            ViewBag.SubSitMap = @Resources.Resource.SidSubCategory;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<SClassificationViewModel> model = SClass.GetByOId(OId);
            return View(model);
        }

        public ActionResult AddEdit(int Id)
        {
            if (Id > 0 )
            {
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                List<MClassificationViewModel> MClassification = MClass.GetAll(OId);
               SClassificationModel model = SClass.GetBySubId(Id);
                ViewBag.MClassList = MClassification.ToList();
                return PartialView("AddEdit", model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                List<MClassificationViewModel> MClassification = MClass.GetAll(OId);
                ViewBag.MClassList = MClassification.ToList();
                SClassificationModel model = new SClassificationModel(); ;
                model.OId = OId;
                return PartialView("AddEdit", model);
            }
        }

        public JsonResult UpdateCreate(SClassificationModel model)
        {
            if (model.id > 0)
            {
                SClass.Update(model);
                return Json("Updated",JsonRequestBehavior.AllowGet);
            }
            else
            {
                SClass.Add(model);
                return Json("Updated", JsonRequestBehavior.AllowGet);
            }
        }




        public JsonResult Delete(int Id)
        {
            SClass.Delete(Id);
            return Json("Deleted",JsonRequestBehavior.AllowGet);
        }
    }
}