﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.AsstesModule.Controllers
{
    [Authorize]
    public class AssteDepartmentController : Controller
    {
        Assets asset = new Assets();
        OrganizationLicenses Org = new OrganizationLicenses();
        SelectionDetailsList SList = new SelectionDetailsList();
        Employee emp = new Employee();
        SClassification SClass = new SClassification();
        AsstesDepartment DeptAsstes = new AsstesDepartment();
        Management Mg = new Management();
        Department Dept = new Department();
        // GET: AsstesModule/AssteDepartment
        public ActionResult Index()
        {
            ViewBag.SubSystem = @Resources.Resource.SidAssetsModule;
            ViewBag.SitMap = @Resources.Resource.PAssets;
            ViewBag.SubSitMap = @Resources.Resource.AssteDepartment;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
           List< AsstesDepartmentViewModel> model=  DeptAsstes.getByOId(OId);
            return View(model);
        }


        public ActionResult Add()
        {
            
              string UserId = User.Identity.GetUserId();
              int OId = emp.GetOIdByUserId(UserId);
            //GetList Management
             List<ManagementelectionModel> ManagList=Mg.GetSelectionByOId(OId);
             ViewBag.ManagList = ManagList;
            //get List Asstes 
             List<AssetsViewModel> asstesList= asset.getByOId(OId);
             ViewBag.asstesList= asstesList.Where(x=>x.Availability==true && x.AsUseresType==2);
           
              return PartialView("AddEdit");

        }

        public JsonResult Create(AsstesDepartmentModel model)
        {
            DeptAsstes.Add(model);
            return Json("Added", JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetDapt(int Id)
        {
            List<DepartmentViewModel> DeptList = Dept.GetDeptByMId(Id);
            return Json(DeptList, JsonRequestBehavior.AllowGet);
        }
    }
}