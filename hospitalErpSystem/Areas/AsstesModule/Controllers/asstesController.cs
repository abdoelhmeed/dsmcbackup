﻿using hospitalErpBusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System.IO;

namespace hospitalErpSystem.Areas.AsstesModule.Controllers
{
    [Authorize]
    public class asstesController : Controller
    {

        Assets asset = new Assets();
        OrganizationLicenses Org = new OrganizationLicenses();
        SelectionDetailsList SList = new SelectionDetailsList();
        Employee emp = new Employee();
        SClassification SClass = new SClassification();
        MClassification MClass = new MClassification();

        [HttpGet]
        public ActionResult Index()
        {
         
            ViewBag.SubSystem = @Resources.Resource.SidAssetsModule;
            ViewBag.SitMap = @Resources.Resource.PAssets;
            ViewBag.SubSitMap = @Resources.Resource.AssetsList;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List< AssetsViewModel> model= asset.getByOId(OId);
            return View(model);
        }

       [HttpGet]
        public ActionResult AddEdit(int id)
        {

            if (id > 0)
            {
                AssetsModel model = asset.get(id);
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                model.AsOId = OId;
                List<MClassificationViewModel> MList = MClass.GetAll(OId);
                ViewBag.MList = MList.ToList();
                int SubId =(int) model.SubType;
                SClassificationModel SClassificationModel = SClass.GetBySubId(SubId);
                model.MType =(int)SClassificationModel.MainClassificationId;
                List<SClassificationViewModel> SList = SClass.GetByMainId(model.MType);
                ViewBag.SList = SList;
                
                return PartialView("AddEdit", model);
            }
            else
            {
                AssetsModel model = new AssetsModel();
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                model.AsOId = OId;
                List<MClassificationViewModel> MList = MClass.GetAll(OId);
                ViewBag.MList = MList.ToList();
                List<SClassificationViewModel> SList = new List<SClassificationViewModel>();
                ViewBag.SList = SList;
                return PartialView("AddEdit", model);
            }
            
        }
        [HttpPost]
        public JsonResult getListSubClassification(int Id)
        {
            List<SClassificationViewModel> model = SClass.GetByMainId(Id);

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AddAndEdit(AssetsModel model)
        {
            if (model.AsName == null)
            {
                return Json("Please Enter Name", JsonRequestBehavior.AllowGet);
            }

            if (model.AnnualDepreciation == null)
            {
                return Json("Please Enter Depreciation Rate", JsonRequestBehavior.AllowGet);
            }

            if (model.AsDescription == null)
            {
                return Json("Please Enter Description", JsonRequestBehavior.AllowGet);
            }

            if (model.SubType== 0 || model.SubType==null)
            {
                return Json("Please Enter Sub Type",JsonRequestBehavior.AllowGet);
            }

            if (model.id > 0)
            {

                model.ASBill = AddImage(model.BillFile, "Bill");
                model.AsInstructionManual = AddImage(model.InstructionManualFile, "InstructionManual");
                model.ASMaintenanceContract = AddImage(model.MaintenanceContractFile, "MaintenanceContract");
                model.AsWarrantyImage = AddImage(model.WarrantyImageFile, "MaintrnGuarantee");
                asset.Edit(model);

                return Json("Update", JsonRequestBehavior.AllowGet);
            }
            else
            {
                model.ASBill = AddImage(model.BillFile, "Bill");
                model.AsInstructionManual = AddImage(model.InstructionManualFile, "InstructionManual");
                model.ASMaintenanceContract = AddImage(model.MaintenanceContractFile, "MaintenanceContract");
                model.AsWarrantyImage = AddImage(model.WarrantyImageFile, "WarrantyImage");
                asset.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
           
        }
        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Attachments/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Attachments/"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }

        [HttpPost]
        public JsonResult Delete(int Id)
        {
            asset.Delete(Id);
            return Json("Deleted", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int AId)
        {
            ViewBag.SubSystem = @Resources.Resource.SidAssetsModule;
            ViewBag.SitMap = @Resources.Resource.PAssets;
            ViewBag.SubSitMap = @Resources.Resource.AsstesDetails;
            AssetsViewModel model =asset.getById(AId);
            ViewBag.asstes = model;
            return View();
        }


    }
}