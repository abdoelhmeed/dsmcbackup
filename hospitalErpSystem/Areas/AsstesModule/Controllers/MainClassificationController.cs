﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.AsstesModule.Controllers
{
    [Authorize]
    public class MainClassificationController : Controller
    {
        MClassification MClass = new MClassification();
        Employee emp = new Employee();
        // GET: AsstesModule/MainClassification
        public ActionResult Index()
        {
            ViewBag.SubSystem = @Resources.Resource.SidAssetsModule;
            ViewBag.SitMap = @Resources.Resource.Category;
            ViewBag.SubSitMap = @Resources.Resource.SidMainCategory;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<MClassificationViewModel> model = MClass.GetAll(OId);
            return View(model);
        }

        public ActionResult AddEdit(int Id)
        {
            if (Id > 0)
            {
                MClassificationModel model = MClass.GetById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                MClassificationModel model = new MClassificationModel();
                model.OId = OId;
                return PartialView("AddEdit", model);
            }
        }


        [HttpPost]
        public JsonResult UpdateCreate(MClassificationModel model)
        {

            if (model.id > 0)
            {
                MClass.Update(model);
                return Json("Update",JsonRequestBehavior.AllowGet);
            }
            else
            {
                MClass.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Delete(int Id)
        {
            MClass.Delete(Id);
            return Json("Deleted",JsonRequestBehavior.AllowGet);
        }
    }
}