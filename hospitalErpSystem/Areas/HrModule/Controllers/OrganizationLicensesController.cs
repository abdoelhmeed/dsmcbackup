﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.HrModule.Controllers
{
    [Authorize]
    public class OrganizationLicensesController : Controller
    {
        OrganizationLicenses Org = new OrganizationLicenses();
        SelectionDetailsList SList = new SelectionDetailsList();
        Employee emp = new Employee();
        // GET: HrModule/OrganizationLicenses
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = Resources.Resource.SidOrganizationLicenses;
            string UserId = User.Identity.GetUserId();
           int OId= emp.GetOIdByUserId(UserId);
            List <OrganizationLicensesViewsModel>  model=  Org.GetListOrganizationLicensesByOId(OId);

            return View(model);
        }

        public ActionResult AddEdit(int id)
        {
            if (id > 0)
            {
                OrganizationLicensesModel model=Org.GetOrganizationLicensesById(id);
                ViewBag.OrganizationLicensesName = SList.SelectionListEnglis(123);
                ViewBag.OrganizationLicensesPlace = SList.SelectionListEnglis(128);
                return PartialView("AddEdit", model);
            }
            else
            {
                OrganizationLicensesModel model = new OrganizationLicensesModel();
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                ViewBag.OrganizationLicensesName = SList.SelectionListEnglis(123);
                ViewBag.OrganizationLicensesPlace = SList.SelectionListEnglis(128);
                ViewBag.OId = OId;
                return PartialView("AddEdit", model);
            }
          
        }
        [HttpPost]
        public JsonResult UpdateCreate(OrganizationLicensesModel model)
        {
            if (model.OLid > 0)
            {
                model.OLimg = AddImage(model.OLimgFile,"Org");
                Org.UpdateOrganizationLicenses(model);
                return Json("Update", JsonRequestBehavior.AllowGet);
            }
            else
            {
                model.OLimg = AddImage(model.OLimgFile, "Org");
                Org.AddOrganizationLicenses(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Validity(int Id)
        {
            Org.OrganizationLicenseValidity(Id);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int Id)
        {
            Org.DeleteOrganizationLicenses(Id);
            return RedirectToAction("Index");
        }


        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Attachments/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Attachments/"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }

    }
}