﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using hospitalErpSystem.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace hospitalErpSystem.Areas.HrModule.Controllers
{
    [Authorize]
    public class EmployeeDataController : Controller
    {

        SelectionDetailsList SList = new SelectionDetailsList();
        EmployeeLicenses empLicenses = new EmployeeLicenses();
        EmployeeQualifications Qual = new EmployeeQualifications();
        EmployeeIds empIdes = new EmployeeIds();
        Employee emp = new Employee();
        AsstesEmployee aEmp = new AsstesEmployee();
        Management Manag = new Management();
        EmployeesDepartmentManagment empPosition = new EmployeesDepartmentManagment();
        // GET: HrModule/EmployeeData
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = Resources.Resource.HrHEmployeeList;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List <EmployeeViewModel> model= emp.GetEmployeeByOId(OId).ToList();
            return View(model);
        }

        // GET: HrModule/EmployeeData/Details/5model
        public ActionResult Details(int id)
        {
            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = @Resources.Resource.HrHEmployeeDetails;
            //Licenses
            ViewBag.empLicensesName = SList.SelectionListEnglis(109);
            ViewBag.empLicensesPlace = SList.SelectionListEnglis(127);

            //Qualifications
            ViewBag.empQualificationsName = SList.SelectionListEnglis(115);
            ViewBag.empQualificationsPlace = SList.SelectionListEnglis(116);
            //employee ides
            ViewBag.IdesType = SList.SelectionListEnglis(107);

            //
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            AsstesEmployee aEmp = new AsstesEmployee();
            Assets asset = new Assets();
            List<AssetsViewModel> asstesList = asset.getByOId(OId);
            ViewBag.asstesList = asstesList.Where(x => x.Availability == true && x.AsUseresType == 1);

            EmployeeViewModel model = emp.GetEmployeeById(id);
            ViewBag.empid = model.empid;
            
           
            return View(model);
        }

        // GET: HrModule/EmployeeData/Create
        public ActionResult Create()
        {
           
            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = Resources.Resource.LCreate;
            ViewBag.empCareerLevel = new SelectList(SList.SelectionListEnglis(124), "id", Resources.Resource.selNameVal);
            ViewBag.empGender = new SelectList(SList.SelectionListEnglis(125), "id", Resources.Resource.selNameVal);
            ViewBag.empMaritalStatus = new SelectList(SList.SelectionListEnglis(110), "id", Resources.Resource.selNameVal);
            ViewBag.empNationality = new SelectList(SList.SelectionListEnglis(112), "id", Resources.Resource.selNameVal);
            ViewBag.empReligion = new SelectList(SList.SelectionListEnglis(117), "id", Resources.Resource.selNameVal);
            ViewBag.empTitle = new SelectList(SList.SelectionListEnglis(126), "id", Resources.Resource.selNameVal);
            return View();
        }

        // POST: HrModule/EmployeeData/Create
        [HttpPost]
        public ActionResult Create(EmployeeModel model)
        {

            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = Resources.Resource.LCreate;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            model.OId = OId;
            if (ModelState.IsValid)
            {
                model.empImg = AddImage(model.empImgFile, "Emp");
                int res =emp.AddEmployee(model);
                if (res> 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Status = "";
                    ViewBag.empCareerLevel = new SelectList(SList.SelectionListEnglis(124), "id", Resources.Resource.selNameVal, model.empCareerLevel);
                    ViewBag.empGender = new SelectList(SList.SelectionListEnglis(125), "id", Resources.Resource.selNameVal, model.empGender);
                    ViewBag.empMaritalStatus = new SelectList(SList.SelectionListEnglis(110), "id", Resources.Resource.selNameVal, model.empMaritalStatus);
                    ViewBag.empNationality = new SelectList(SList.SelectionListEnglis(112), "id", Resources.Resource.selNameVal, model.empNationality);
                    ViewBag.empReligion = new SelectList(SList.SelectionListEnglis(117), "id", Resources.Resource.selNameVal, model.empReligion);
                    ViewBag.empTitle = new SelectList(SList.SelectionListEnglis(126), "id", Resources.Resource.selNameVal, model.empTitle);
                    ViewBag.Status = "Not Saved";
                    return View(model);
                }
            }

            return View(model);
        }

        // GET: HrModule/EmployeeData/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = Resources.Resource.btnEdit;
            EmployeeModel model = emp.GetFoeEditById(id);

            ViewBag.empCareerLevel = new SelectList(SList.SelectionListEnglis(124), "id", Resources.Resource.selNameVal, model.empCareerLevel);
            ViewBag.empGender = new SelectList(SList.SelectionListEnglis(125), "id", Resources.Resource.selNameVal, model.empGender);
            ViewBag.empMaritalStatus = new SelectList(SList.SelectionListEnglis(110), "id", Resources.Resource.selNameVal, model.empMaritalStatus);
            ViewBag.empNationality = new SelectList(SList.SelectionListEnglis(112), "id", Resources.Resource.selNameVal, model.empNationality);
            ViewBag.empReligion = new SelectList(SList.SelectionListEnglis(117), "id", Resources.Resource.selNameVal, model.empReligion);
            ViewBag.empTitle = new SelectList(SList.SelectionListEnglis(126), "id", Resources.Resource.selNameVal, model.empTitle);

            return View(model);
        }

        // POST: HrModule/EmployeeData/Edit/5
        [HttpPost]
        public ActionResult Edit(EmployeeModel model)
        {
            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = Resources.Resource.btnEdit;
            try
            {
                if (ModelState.IsValid)
                {
                    model.empImg = AddImage(model.empImgFile, "Emp");
                    int res = emp.Edit(model);
                    if (res > 0 )
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {

                        ViewBag.empCareerLevel = new SelectList(SList.SelectionListEnglis(124), "id", Resources.Resource.selNameVal, model.empCareerLevel);
                        ViewBag.empGender = new SelectList(SList.SelectionListEnglis(125), "id", Resources.Resource.selNameVal, model.empGender);
                        ViewBag.empMaritalStatus = new SelectList(SList.SelectionListEnglis(110), "id", Resources.Resource.selNameVal, model.empMaritalStatus);
                        ViewBag.empNationality = new SelectList(SList.SelectionListEnglis(112), "id", Resources.Resource.selNameVal, model.empNationality);
                        ViewBag.empReligion = new SelectList(SList.SelectionListEnglis(117), "id", Resources.Resource.selNameVal, model.empReligion);
                        ViewBag.empTitle = new SelectList(SList.SelectionListEnglis(126), "id", Resources.Resource.selNameVal, model.empTitle);
                        return View(model);
                    }

                }
                ViewBag.empCareerLevel = new SelectList(SList.SelectionListEnglis(124), "id", Resources.Resource.selNameVal, model.empCareerLevel);
                ViewBag.empGender = new SelectList(SList.SelectionListEnglis(125), "id", Resources.Resource.selNameVal, model.empGender);
                ViewBag.empMaritalStatus = new SelectList(SList.SelectionListEnglis(110), "id", Resources.Resource.selNameVal, model.empMaritalStatus);
                ViewBag.empNationality = new SelectList(SList.SelectionListEnglis(112), "id", Resources.Resource.selNameVal, model.empNationality);
                ViewBag.empReligion = new SelectList(SList.SelectionListEnglis(117), "id", Resources.Resource.selNameVal, model.empReligion);
                ViewBag.empTitle = new SelectList(SList.SelectionListEnglis(126), "id", Resources.Resource.selNameVal, model.empTitle);
                return View(model);

            }
            catch
            {

                ViewBag.empCareerLevel = new SelectList(SList.SelectionListEnglis(124), "id", Resources.Resource.selNameVal, model.empCareerLevel);
                ViewBag.empGender = new SelectList(SList.SelectionListEnglis(125), "id", Resources.Resource.selNameVal, model.empGender);
                ViewBag.empMaritalStatus = new SelectList(SList.SelectionListEnglis(110), "id", Resources.Resource.selNameVal, model.empMaritalStatus);
                ViewBag.empNationality = new SelectList(SList.SelectionListEnglis(112), "id", Resources.Resource.selNameVal, model.empNationality);
                ViewBag.empReligion = new SelectList(SList.SelectionListEnglis(117), "id", Resources.Resource.selNameVal, model.empReligion);
                ViewBag.empTitle = new SelectList(SList.SelectionListEnglis(126), "id", Resources.Resource.selNameVal, model.empTitle);
                return View(model);
            }
        }

        // GET: HrModule/EmployeeData/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HrModule/EmployeeData/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult RegisterLogin(int Id,string Name)
        {
            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = Resources.Resource.LRegister;

            RegisterViewModel model = new RegisterViewModel();
            model.EmployId = Id;
            model.EmployName = Name;
            

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterLogin(RegisterViewModel model)
        {
            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = Resources.Resource.LRegister;

            if (ModelState.IsValid)
            {
                ApplicationDbContext dbLog = new ApplicationDbContext();
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbLog));
                var UserAdmin = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbLog));
                var User = new ApplicationUser();
                User.Email = model.Email;
                User.UserName = model.UserName;
                var check = UserAdmin.Create(User, model.Password);
                if (check.Succeeded)
                {
                    emp.UpdateLoginEmployee(model.EmployId, User.Id);
                    return RedirectToAction("Details", new { id=model.EmployId});
                }
                AddErrors(check);
            }
            return View(model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        //Licenses
        public JsonResult GetLicenseEmp(int id)
        {
            List<EmployeeLicensesViewsModel> emplicenses = empLicenses.EmployeeLicensesByEmployeeId(id).Where(x=>x.Visible ==true && x.LValidity==true).ToList();
            List<customEmpLicenses> model = new List<customEmpLicenses>();
            ViewBag.EmployeeLicenses = emplicenses;
            ViewBag.Emp = id;
            foreach (var item in emplicenses)
            {
                customEmpLicenses modelItem = new customEmpLicenses {

                    EndDate = item.LEndDate.ToShortDateString(),
                    id = item.Lid,
                    Img = item.Limg,
                    NameEn = item.NameEn,
                    PlaceEn = item.PlaceEn,
                    PlaceAr=item.PlaceAr,
                    NameAr=item.NameAr,
                    StartDate = item.LEndDate.ToShortDateString()

                };
                model.Add(modelItem);
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Validity(int Id)
        {
            empLicenses.EmployeeLicensesValidity(Id);
            return Json("Validity Delete", JsonRequestBehavior.AllowGet);
        }

       public ActionResult LNewCreate(int id,int empId)
       {
            if (id > 0)
            {
                EmployeeLicensesModel model = empLicenses.EmployeeLicensesByIdForUpdate(id);
                ViewBag.LPlace = new SelectList(SList.SelectionListEnglis(127), "id", "selNameEnglish",model.LPlace);
                ViewBag.LName = new SelectList(SList.SelectionListEnglis(127), "id", "selNameEnglish",model.LName);
                return PartialView("LNewCreate", model);
            }
            else
            {
                ViewBag.LPlace = new SelectList(SList.SelectionListEnglis(127), "id", "selNameEnglish");
                ViewBag.LName = new SelectList(SList.SelectionListEnglis(109), "id", "selNameEnglish");

                EmployeeLicensesModel model =new EmployeeLicensesModel();
                model.empId=empId;
                model.LStartDate = DateTime.Now;
                model.LEndDate = DateTime.Now.AddMonths(12);
                return PartialView("LNewCreate", model);
            }
        }
        [HttpPost]
        public JsonResult LCreate(EmployeeLicensesModel model)
        {
            string IamgPath = AddImage(model.LimgFile, "Licenses");
            model.Limg = IamgPath;
            empLicenses.AddEmployeeLicenses(model);
            return Json("Saved",JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteLicenses(int Id)
        {
            empLicenses.DelEmployeeLicenses(Id);
            return Json("Deleted", JsonRequestBehavior.AllowGet);
        }

        //Qualificat
        public JsonResult getQualificat(int id)
        {
            List<CustomEmployeeQualifications> model = new List<CustomEmployeeQualifications>();
            List<EmployeeQualificationsViewModel> res = Qual.GetEmpQualificatByEmpId(id).ToList();
            foreach (var item in res)
            {
                CustomEmployeeQualifications modelItem = new CustomEmployeeQualifications
                {
                    Qid = item.Qid,
                    QNameArbic = item.QNameArbic,
                    QNameEnglish = item.QNameEnglish,
                    QNamePlaceArbic = item.QNamePlaceArbic,
                    QNamePlaceEnglish = item.QNamePlaceEnglish,
                    QDate = item.QDate.ToShortDateString(),
                    QGrade=item.QGrade,
                    QImg=item.QImg
                };
                model.Add(modelItem);
            }
           
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateQualificat(EmployeeQualificationsModel model)
        {
            string IamgPath = AddImage(model.QimgFile, "Qualifications");
            model.QImg = IamgPath;
            Qual.addEmpeQualificat(model);
            return Json("Seved", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteQualificat(int Id)
        {
            Qual.DelEmpeQualificat(Id);
            return Json("Delete",JsonRequestBehavior.AllowGet);
        }

        //EmployeeIds
        [HttpGet]
        public JsonResult getEmployeeIds(int empId)
        {
            List<EmployeeIdsViewsModel> model= empIdes.GegListIds(empId);
            return Json(model, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult AddNewIdes(EmployeeIdsModel model)
        {
            model.idImg = AddImage(model.idImgFile,"EmpIdes");
            empIdes.AddIds(model);
            return Json("Saved", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteEmployeeIds(int Id)
        {
            empIdes.DeleteIds(Id);
            return Json("Delete", JsonRequestBehavior.AllowGet);
        }

        //Employee Assets
        [HttpGet]
        public JsonResult getAllEmployeeAsstes(int EmpId)
        {
            AsstesEmployee aEmp = new AsstesEmployee();
            List<AsstesEmployeeViewModel> model = aEmp.getByEmpId(EmpId);
            return Json(model,JsonRequestBehavior.AllowGet);
        }

        [HttpPost] 
        public JsonResult AddNewAssete(AsstesEmployeeModel model)
        {
           
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            model.OId = OId;
            aEmp.add(model);
            return Json("Added", JsonRequestBehavior.AllowGet);
        }

        // 


        //Position
        public ActionResult PositionList()
        {
            ViewBag.SubSystem = Resources.Resource.SidHRModule;
            ViewBag.SitMap = Resources.Resource.AssignStaff;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<employeeDepartmentManagmentViewModel> model= empPosition.GetEmployeesByOId(OId);

            return View(model);
        }



        public ActionResult AddPosition()
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<EmployeeSelectionModel> allEmp = empPosition.GetEmployeeSelections(OId);
            List<EmployeeSelectionModel> EmployeeList = new List<EmployeeSelectionModel>();
            List<employeeDepartmentManagmentViewModel> EmpLdePtMana = empPosition.GetEmployeesByOId(OId);
            foreach (var item in allEmp)
            {
                List<employeeDepartmentManagmentViewModel> all = EmpLdePtMana.Where(x=>x.empid == item.empid).ToList();
                if (all.Count == 0)
                {
                    EmployeeList.Add(item);
                }
            }


            ViewBag.EmployeeList = EmployeeList.ToList();
            //
            ViewBag.Management = Manag.GetSelectionByOId(OId);

            return PartialView("AddPosition");
        }

        public JsonResult GetDepartment(int MId)
        {
            Department Dept = new Department();
            List<DepartmentViewModel> DeptList = Dept.GetDeptByMId(MId);
            return Json(DeptList, JsonRequestBehavior.AllowGet);
        }

     
        public ActionResult EditPosition(int Id)
        {
            employeeDepartmentManagmentModel model= empPosition.GetById(Id);
            int empid = (int)model.empid;
            EmployeeModel Employee = emp.GetFoeEditById(empid);
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            ViewBag.Management = Manag.GetSelectionByOId(OId);
            ViewBag.Name = Employee.empFNameEn + " " 
                + Employee.empSNameEn + " " + Employee.empSNameEn + "-"
                +  Employee.empTNameAr +" "+Employee.empSNameAr +" "+Employee.empFNameAr;

            return PartialView("EditPosition", model);
        }



        [HttpPost]
        public JsonResult EditPosition(employeeDepartmentManagmentModel model)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            model.OId = OId;
            empPosition.Edit(model);
            return Json("Updated", JsonRequestBehavior.AllowGet);
        }

    
        [HttpPost]
        public JsonResult Position(employeeDepartmentManagmentModel model)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            model.OId = OId;
            empPosition.Add(model);
            return Json("Added", JsonRequestBehavior.AllowGet);
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Attachments/"+fileName;
            fileName = Path.Combine(Server.MapPath("~/Attachments/"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}
