﻿using System.Web.Mvc;

namespace hospitalErpSystem.Areas.MaintenanceModule
{
    public class MaintenanceModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MaintenanceModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MaintenanceModule_default",
                "MaintenanceModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}