﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.MaintenanceModule.Controllers
{
    [Authorize]
    public class MaintenanceHandController : Controller
    {
        MaintenanceHand hand = new MaintenanceHand();
        Employee emp = new Employee();
        MaintenanceBill bill = new MaintenanceBill();
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidMaintenanceModule;
            ViewBag.SitMap = Resources.Resource.ReceivedRequest;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<MaintenanceHandViewModel> MaintenanceHandViewList = new List<MaintenanceHandViewModel>();
            List<MaintenanceHandViewModel> model = hand.GetByOId(OId);
         
            return View(model);
        }
        public ActionResult purchaseAssets(int Id)
        {
            MaintenanceHandViewModel HandModel = hand.GetById(Id);
            MaintenanceBillModel model = new MaintenanceBillModel();
            model.MaintenanceDetails = HandModel.MaintenanceDetails;
            model.AspiratDetails = HandModel.AspiratDetails;
            model.MaintenanceHandlingId = HandModel.id;
            model.AssetsId = HandModel.asstesId;
            model.AssetsName = HandModel.asstesName;
            return PartialView("purchaseAssets", model);
        }
        [HttpPost]
        public JsonResult purchase(MaintenanceBillModel model)
        {
            model.InvoiceImage=AddImage(model.InvoiceFill, "MaintenanceInvoice");
            bill.Add(model);
            return Json("Seved",JsonRequestBehavior.AllowGet);
        }

        public ActionResult complete(int Id)
        {
            hand.CompleteMaintenance(Id);
            return RedirectToAction("Index");
        }
        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Attachments/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Attachments/"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}