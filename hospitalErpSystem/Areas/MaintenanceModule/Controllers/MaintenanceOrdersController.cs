﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.MaintenanceModule.Controllers
{
    [Authorize]
    public class MaintenanceOrdersController : Controller
    {

        MaintenOrder mOrder = new MaintenOrder();
        Employee emp = new Employee();
        Assets assets = new Assets();
        MaintenanceHand Mhand = new MaintenanceHand();
        AsstesEmployee asstesEmployee = new AsstesEmployee();

        EmployeesDepartmentManagment empDepart = new EmployeesDepartmentManagment();
        // GET: MaintenanceModule/MaintenanceOrders
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidMaintenanceModule;
            ViewBag.SitMap = Resources.Resource.Requests;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<MaintenOrderViewModel> model = mOrder.GetByOId(OId);
            return View(model);
        }

        // GET: MaintenanceModule/MaintenanceOrders/Details/5
        [HttpGet]
        public ActionResult MyMaintenanceOrders()
        {
            ViewBag.SubSystem = Resources.Resource.SidMaintenanceModule;
            ViewBag.SitMap = Resources.Resource.Requests;
            ViewBag.SubSitMap = Resources.Resource.MyMaintenanceRequest;


            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<MaintenOrderViewModel> model = mOrder.GetByOEmpId(empId);
            List<AsstesEmployeeViewModel> asstesList= asstesEmployee.getByEmpId(empId);
            ViewBag.asstesList = asstesList;
            ViewBag.empId = empId;
            ViewBag.OId = OId;
            return View(model);
        }

        [HttpPost]
        public JsonResult EmplOrder(MaintenOrderModel model)
        {

            if (model.OrderNotes ==null || model.OrderNotes=="")
            {
                model.OrderNotes = "No Notes";
            }

            model.OrderStuts = "New Order";
            model.OrderType = "Employee";
            mOrder.Add(model);
            return Json("Saved",JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DepartmentMaintenanceOrders()
        {
            AsstesDepartment AsstesDepartment = new AsstesDepartment();
            ViewBag.SubSystem = @Resources.Resource.SidMaintenanceModule;
            ViewBag.SitMap = @Resources.Resource.MaintenanceRequest;
            ViewBag.SubSitMap = @Resources.Resource.MaintenanceDepartmentRequest;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            employeeDepartmentManagmentViewModel employeeDepartmentManagment= empDepart.GetEmployeesByEmpId(empId);
            int deptId =(int)employeeDepartmentManagment.DepartmentId;
            List<AsstesDepartmentViewModel> asstesList = AsstesDepartment.getByDeptID(deptId);
            ViewBag.asstesList = asstesList;
            List<MaintenOrderViewModel> model = mOrder.GetByDeptId(deptId);
            return View(model);
        }

        [HttpPost]
        public JsonResult DepartmentOrder(MaintenOrderModel model)
        {
            if (model.OrderNotes == null || model.OrderNotes == "")
            {
                model.OrderNotes = "No Notes";
            }
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            model.OId = OId;
            model.empId = empId;
            model.OrderStuts = "New Order";
            model.OrderType = "Department";
            mOrder.Add(model);
            return Json("Saved", JsonRequestBehavior.AllowGet);
        }

       [HttpGet]
        public ActionResult AsstesMaintenancedetails(int asstesId)
        {
            ViewBag.SubSystem = Resources.Resource.SidMaintenanceModule;
            ViewBag.SitMap = Resources.Resource.AsstesInfo;
         
            MaintenanceDetail Md = new MaintenanceDetail();
           
            AssetsViewModel asstes = assets.getById(asstesId);
            ViewBag.asstes = asstes;
            List<MaintenanceDetailsModel> model = Md.GetMaintenanceDetail(asstesId);
            return View(model);
        }

      

        [HttpGet]
        public ActionResult RefusalOrder(int id)
        {
            MaintenOrderViewModel model = mOrder.GetById(id);
            return PartialView("RefusalOrder", model);
        }
        [HttpPost]
        public JsonResult RefusalOrder(MaintenOrderViewModel model)
        {
            mOrder.Refusal(model);
            return Json("",JsonRequestBehavior.AllowGet);
        }

        public ActionResult RefusalOrderList()
        {
            ViewBag.SubSystem = Resources.Resource.SidMaintenanceModule;
            ViewBag.SitMap = Resources.Resource.Rejectrequest;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<MaintenOrderViewModel> model = mOrder.GetByOId(OId);
            return View(model);
        }

        [HttpGet]
        public ActionResult Handling(int Id)
        {
            MaintenOrderViewModel model = mOrder.GetById(Id);
            ViewBag.asstesId = model.asstesId;
            ViewBag.OrderMaintenanceIId = model.id;
            return PartialView("Handling");
        }
        
        [HttpPost]
        public JsonResult Handling(MaintenanceHandModel model)
        {
            if (model.dMaintenanceStartDate !=null)
            {
                model.MaintenanceStartDate = Convert.ToDateTime( model.dMaintenanceStartDate);
                Mhand.add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Not Save", JsonRequestBehavior.AllowGet);
            }
           
        }

    }
}
