﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.MaintenanceModule.Controllers
{
    [Authorize]
    public class MaintenanceBillsController : Controller
    {
      
        // GET: MaintenanceModule/MaintenanceBills
        MaintenanceBill bill = new MaintenanceBill();
        Employee emp = new Employee();
        public ActionResult Index()
        {
         
            ViewBag.SubSystem = @Resources.Resource.SidMaintenanceModule;
            ViewBag.SitMap = @Resources.Resource.approvalInvoice;
           
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<MaintenanceBillViewModel> model= bill.GetBill(OId).Where(x=>x.GeneralDirector==false).OrderByDescending(u=>u.MAspiratDate).ToList();
            return View(model);
        }

        public ActionResult Details(int Id)
        {
            MaintenanceBillViewModel model = bill.GetById(Id);
            return PartialView("Details", model);
        }

        public ActionResult ApprovalDirector(int Id)
        {
            

            ViewBag.SubSystem = @Resources.Resource.SidMaintenanceModule;
            ViewBag.SitMap = @Resources.Resource.ApprovalDirector;


            bill.ApprovalGeneralDirector(Id);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult financialRequest()
        {
            ViewBag.SubSystem = @Resources.Resource.SidMaintenanceModule;
            ViewBag.SitMap = @Resources.Resource.FinancialInvoice;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<MaintenanceBillViewModel> model = bill.GetBill(OId).Where(x => x.GeneralDirector == true && x.Financial== false).OrderByDescending(u => u.MAspiratDate).ToList();
            return View(model);
        }

        public ActionResult financialDone(int Id)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            EmployeeViewModel model = emp.GetEmployeeById(empId);
            string None = "Date :"+DateTime.Now.ToString()+"<br/>"+"By :"+ model.empFNameEn+" "+ model.empSNameEn+" "+ model.empTNameEn;
            bill.ApprovalFinancial(Id,None);
            return RedirectToAction("financialRequest");
        }

    }
}