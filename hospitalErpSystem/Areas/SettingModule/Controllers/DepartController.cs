﻿using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hospitalErpBusinessLayer;
using Microsoft.AspNet.Identity;

namespace hospitalErpSystem.Areas.SettingModule.Controllers
{
    [Authorize]
    public class DepartController : Controller
    {
        Department department = new Department();
        Employee emp = new Employee();
        Management Manag = new Management();
        public ActionResult Index()
        {
            ViewBag.SubSystem = @Resources.Resource.SidSettingModule;
            ViewBag.SitMap = @Resources.Resource.DDepartment;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<DepartmentViewModel> model = department.GetAllByOId(OId);
            return View(model);
        }

        public ActionResult AddEditDepartment(int id)
        {
            if (id > 0)
            {
                DepartmentModel model =department.getById(id);
                string UserId = User.Identity.GetUserId();
                
                int OId = emp.GetOIdByUserId(UserId);
                List<EmployeeSelectionModel> SelectionModel = emp.GetEmployeeSelectioByOId(OId);
                ViewBag.Selection = SelectionModel;
                ViewBag.Management = Manag.GetSelectionByOId(OId);
               
                return PartialView("AddEditDepartment", model);
            }
            else
            {
                
                DepartmentModel model = new DepartmentModel();
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                List<EmployeeSelectionModel> SelectionModel = emp.GetEmployeeSelectioByOId(OId);
                ViewBag.Selection = SelectionModel;
                model.OId = OId;
                ViewBag.Management = Manag.GetSelectionByOId(OId);
                return PartialView("AddEditDepartment", model);
            }
        }

        [HttpPost]
        public JsonResult UpdateCreate(DepartmentModel model)
        {
            if (model.deptId > 0)
            {
                department.UpdateDept(model);
                return Json("Update", JsonRequestBehavior.AllowGet);
            }
            else
            {
                department.AddDept(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
           
        }
        [HttpGet]
        public ActionResult Delete(int Id)
        {
            department.DelDept(Id);
            return RedirectToAction("Index");
        }
    }
}