﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.SettingModule.Controllers
{
    [Authorize]
    public class OrganizController : Controller
    {
        Organization org = new Organization();
        // GET: SettingModule/Organiz
        public ActionResult Index()
        {
            ViewBag.SubSystem = "Setting Module";
            ViewBag.SitMap = "Organization";
            ViewBag.SubSitMap = "New";
            List<OrganizationViewModel> model = org.GetAllOrganization();
            return View(model);
        }

        // GET: SettingModule/Organiz/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SettingModule/Organiz/Create
        public ActionResult AddEdit(int id)
        {
            if (id > 0)
            {
                OrganizationModel model = org.GetOrganizationById(id);
                return PartialView("AddEditOrganization", model);
            }
            else
            {
                OrganizationModel model = new OrganizationModel();
                return PartialView("AddEditOrganization", model);
            }
           
        }

        // POST: SettingModule/Organiz/Create
        [HttpPost]
        public JsonResult UpdateCreate(OrganizationModel model)
        {
          
            if (model.OId > 0)
            {
                org.UpdateNewOrganization(model);
                return Json("is Saved",JsonRequestBehavior.AllowGet);
            }
            else
            {
                org.AddNewOrganization(model);
                return Json("Seved", JsonRequestBehavior.AllowGet);
            }

        }

        // GET: SettingModule/Organiz/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SettingModule/Organiz/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SettingModule/Organiz/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SettingModule/Organiz/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
