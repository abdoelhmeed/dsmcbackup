﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.SettingModule.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        Management manage = new Management();
        Organization org = new Organization();
        Employee emp = new Employee();
      
        // GET: SettingModule/Manage
        public ActionResult Index()
        {
            ViewBag.SubSystem = @Resources.Resource.SidSettingModule;
            ViewBag.SitMap = @Resources.Resource.DManagement;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<ManagementViewModel> model= manage.GetAllByOId(OId);
            return View(model);
        }


        public ActionResult AddEdit(int id)
        {
           
            if (id > 0)
            {
                ManagementModel model = manage.GetManagementById(id);
                int Oid = (int)model.OId;
                List<EmployeeSelectionModel> SelectionModel = emp.GetEmployeeSelectioByOId(Oid);
                ViewBag.Selection = SelectionModel;
                ViewBag.Organization = Oid;
                return PartialView("AddEdit", model);
            }
            else
            {
                ManagementModel model = new ManagementModel();
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                List<EmployeeSelectionModel> SelectionModel = emp.GetEmployeeSelectioByOId(OId);
                ViewBag.Selection = SelectionModel;
                ViewBag.Organization = OId;
                return PartialView("AddEdit", model);
            }
        } 

        [HttpPost]
        public JsonResult UpdateCreate(ManagementModel model)
        {
            if (model.Mid > 0)
            {
                manage.UpdateManagement(model);
                return Json("Updated", JsonRequestBehavior.AllowGet);
            }
            else
            {
                manage.AddManagement(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
            
        }


        [HttpGet]
        public ActionResult Delete(int Id)
        {
            manage.DelManagement(Id);
            return RedirectToAction("Index");
        }
    }
}