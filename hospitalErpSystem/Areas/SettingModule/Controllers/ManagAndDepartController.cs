﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.SettingModule.Controllers
{
    public class ManagAndDepartController : Controller
    {
        Department department = new Department();
        Employee emp = new Employee();
        Management Manag = new Management();
        Organization org = new Organization();
        EmployeesDepartmentManagment empPosition = new EmployeesDepartmentManagment();

        public ActionResult DepartAdmin()
        {
            ViewBag.SubSystem = "Setting Module";
            ViewBag.SitMap = "Department";
            ViewBag.SubSitMap = "New";
            List<DepartmentViewModel> model = department.GetAll();
            return View(model);
        }

        public  ActionResult ManagementAdmin()
        {
            ViewBag.SubSystem = "Setting Module";
            ViewBag.SitMap = "Management";
            ViewBag.SubSitMap = "New";
            List<ManagementViewModel> model = Manag.Get();
            return View(model);
        }

        public ActionResult ManagAdmin()
        {
            List<OrganizationViewModel>  OrgList=org.GetAllOrganization();
            ViewBag.Org = OrgList.ToList();
            return PartialView("ManagAdmin");
        }

        [HttpPost]
        public JsonResult AddManag(ManagementModel model)
        {
            Manag.AddManagement(model);
            return Json("Saved", JsonRequestBehavior.AllowGet);
        }

        public ActionResult NewDept()
        {
            List<OrganizationViewModel> OrgList = org.GetAllOrganization();
            ViewBag.Org = OrgList.ToList();
            return PartialView("NewDept");
        }

        [HttpPost]
        public JsonResult AddDept(DepartmentModel model)
        {
            department.AddDept(model);
            return Json("Saved", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult employees(int OId)
        {
            List<EmployeeViewModel> EmployeeList = new List<EmployeeViewModel>();
            List<EmployeeViewModel> allEmp = emp.GetEmployeeByOId(OId);
            List<employeeDepartmentManagmentViewModel> EmpLdePtMana = empPosition.GetEmployeesByOId(OId);
            foreach (var item in allEmp)
            {
                List<employeeDepartmentManagmentViewModel> all = EmpLdePtMana.Where(x => x.empid == item.empid).ToList();
                if (all.Count == 0)
                {
                    EmployeeList.Add(item);
                }
            }

            return Json(EmployeeList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult management(int OId)
        {
            List<ManagementViewModel> model=  Manag.GetAllByOId(OId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}