﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using hospitalErpSystem.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.SettingModule.Controllers
{
    public class EmployeeController : Controller
    {
        SelectionDetailsList SList = new SelectionDetailsList();
        EmployeeLicenses empLicenses = new EmployeeLicenses();
        Employee emp = new Employee();
        EmployeesDepartmentManagment empPosition = new EmployeesDepartmentManagment();
        Organization Org = new Organization();
        public ActionResult Create()
        {
          
            ViewBag.SubSystem = "Stting Module";
            ViewBag.SitMap = "Create";
            List<OrganizationViewModel> OrgList = Org.GetAllOrganization();
            ViewBag.OrgList = OrgList.ToList();
            ViewBag.empCareerLevel = new SelectList(SList.SelectionListEnglis(124), "id", "selNameEnglish");
            ViewBag.empGender = new SelectList(SList.SelectionListEnglis(125), "id", "selNameEnglish");
            ViewBag.empMaritalStatus = new SelectList(SList.SelectionListEnglis(110), "id", "selNameEnglish");
            ViewBag.empNationality = new SelectList(SList.SelectionListEnglis(112), "id", "selNameEnglish");
            ViewBag.empReligion = new SelectList(SList.SelectionListEnglis(117), "id", "selNameEnglish");
            ViewBag.empTitle = new SelectList(SList.SelectionListEnglis(126), "id", "selNameEnglish");
            return View();
        }
        // POST: HrModule/EmployeeData/Create
        [HttpPost]
        public ActionResult Create(EmployeeModel model)
        {

            ViewBag.SubSystem = "Stting Module";
            ViewBag.SitMap = "Create";
            List<OrganizationViewModel> OrgList = Org.GetAllOrganization();
            ViewBag.OrgList = OrgList.ToList();
            
           
            if (ModelState.IsValid)
            {
                model.empImg = AddImage(model.empImgFile, "Emp");
                int res = emp.AddEmployee(model);
                if (res > 0)
                {
                    return RedirectToAction("RegisterLogin", new {Id=res});
                }
                else
                {
                    ViewBag.Status = "";
                    ViewBag.empCareerLevel = new SelectList(SList.SelectionListEnglis(124), "id", "selNameEnglish", model.empCareerLevel);
                    ViewBag.empGender = new SelectList(SList.SelectionListEnglis(125), "id", "selNameEnglish", model.empGender);
                    ViewBag.empMaritalStatus = new SelectList(SList.SelectionListEnglis(110), "id", "selNameEnglish", model.empMaritalStatus);
                    ViewBag.empNationality = new SelectList(SList.SelectionListEnglis(112), "id", "selNameEnglish", model.empNationality);
                    ViewBag.empReligion = new SelectList(SList.SelectionListEnglis(117), "id", "selNameEnglish", model.empReligion);
                    ViewBag.empTitle = new SelectList(SList.SelectionListEnglis(126), "id", "selNameEnglish", model.empTitle);
                    ViewBag.Status = "Not Saved";
                    return View(model);
                }
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult RegisterLogin(int Id)
        {
            RegisterViewModel model = new RegisterViewModel();
            EmployeeViewModel eModel = emp.GetEmployeeById(Id);
            model.EmployId = Id;
            model.EmployName = eModel.empFNameEn+" "+eModel.empSNameEn+" "+eModel.empTNameEn;
          
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterLogin(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationDbContext dbLog = new ApplicationDbContext();
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbLog));
                var UserAdmin = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbLog));
                var User = new ApplicationUser();
                User.Email = model.Email;
                User.UserName = model.UserName;
                var check = UserAdmin.Create(User, model.Password);
                if (check.Succeeded)
                {
                    emp.UpdateLoginEmployee(model.EmployId, User.Id);
                    return RedirectToAction("Create");
                }
                AddErrors(check);
            }
            return View(model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Attachments/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Attachments/"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
    }
}