﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;

namespace hospitalErpSystem.Areas.SettingModule.Controllers
{
    [Authorize]
    public class SelectionsController : Controller
    {
        SelectionType Type = new SelectionType();
        SelectionDetails TDetail = new SelectionDetails();
        // GET: SettingModule/Selection
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SelectTypeIndex()
        {
            ViewBag.SubSystem = Resources.Resource.SidSettingModule;
            ViewBag.SitMap = Resources.Resource.Selections;
            List <SelectionTypeModel> model= Type.GetAll();
            return View(model);
        }

        //SelectDetail
        public ActionResult SelectDetailIndex(int Id)
        {
            ViewBag.SubSystem = Resources.Resource.SidSettingModule;
            ViewBag.SitMap = @Resources.Resource.Selections;
            List<SelectionDetailsModel> model = TDetail.GetByTypeID(Id);
            ViewBag.sTId = Id;
           ViewBag.Arabic= Type.GetPyId(Id).ArabicName;
           ViewBag.English = Type.GetPyId(Id).EnglishName;

            return View(model);
        }

        [HttpPost]
        public JsonResult SelectDetailDelete(int Id)
        {
            TDetail.DelSelectionDetail(Id);
            return Json("Delete", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEdit(int id,int sTId)
        {
            if (id <= 0)
            {
                SelectionDetailsModel model = new SelectionDetailsModel();
                model.sTId = sTId;
                return PartialView("AddOrEdit", model);
            }
            else
            {
                SelectionDetailsModel model = TDetail.GetByID(id);
                return PartialView("AddOrEdit", model);
            }
          
               
           
            
        }
        [HttpPost]
        public JsonResult AddOrEdit(SelectionDetailsModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json("",JsonRequestBehavior.AllowGet);
            }

            if (model.id > 0)
            {
               int res= TDetail.UPdateSelectionDetail(model);
                if (res > 0)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                int res = TDetail.AddSelectionDetail(model);
                if (res > 0)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}