﻿using System.Web.Mvc;

namespace hospitalErpSystem.Areas.IncidentModel
{
    public class IncidentModelAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "IncidentModel";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "IncidentModel_default",
                "IncidentModel/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}