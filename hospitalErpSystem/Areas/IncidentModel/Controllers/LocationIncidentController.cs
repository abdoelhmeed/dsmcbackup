﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class LocationIncidentController : Controller
    {
        LocationOfIncident lOfI = new LocationOfIncident();
        Employee emp = new Employee();
        public ActionResult Index()
        {
            ViewBag.SubSystem = @Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = @Resources.Resource.ComplainLocation;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<LocationOfIncidentModel> model = lOfI.GetPyOId(OId);
            return View(model);
        }
        public ActionResult AddEdit(int Id)
        {
            if (Id > 0)
            {
                LocationOfIncidentModel model = lOfI.GetById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                LocationOfIncidentModel model = new LocationOfIncidentModel();
                model.OId = OId;
                return PartialView("AddEdit", model);
            }

        }
        [HttpPost]
        public JsonResult AddEdit(LocationOfIncidentModel model)
        {
            if (model.LId > 0)
            {
                lOfI.Update(model);
                return Json("Updated", JsonRequestBehavior.AllowGet);
            }
            else
            {
                lOfI.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Delete(int Id)
        {
            lOfI.Delete(Id);
            return Json("Deleted",JsonRequestBehavior.AllowGet);
        }
    }
}