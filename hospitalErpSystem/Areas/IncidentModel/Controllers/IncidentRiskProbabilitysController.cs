﻿using hospitalErpBusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class IncidentRiskProbabilitysController : Controller
    {
        IncidentRiskProbability Irkp = new IncidentRiskProbability();
        Employee emp = new Employee();
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.RiskProbability;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<IncidentRiskProbabilityModel> model=  Irkp.GetByOId(OId);
            return View(model);
        }


        public ActionResult AddEdit(int Id)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            if (Id > 0)
            {
                IncidentRiskProbabilityModel model = Irkp.GetById(Id);
                return PartialView("AddEdit",model);
            }
            else
            {
                IncidentRiskProbabilityModel model = new IncidentRiskProbabilityModel();
                model.OId = OId;
                return PartialView("AddEdit", model);
            }
        }

        [HttpPost]
        public JsonResult AddEdit(IncidentRiskProbabilityModel model)
        {
            if (model.RPMId > 0)
            {
                Irkp.Update(model);
                return Json("Updated", JsonRequestBehavior.AllowGet);

            }
            else
            {

                Irkp.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Delete(int Id)
        {

            Irkp.Delete(Id);
            return RedirectToAction("Index");
        }

    }
}