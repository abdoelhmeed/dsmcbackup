﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class ContributingFactorsController : Controller
    {
        IncideContributingFactor Factor = new IncideContributingFactor();
        Employee emp = new Employee();
        // GET: IncidentModel/ContributingFactors
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.ContributingFactors;
           
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<IncideContributingFactorModel> model= Factor.GetByOId(OId);
            return View(model);
        }

        public ActionResult AddEdit(int CFId)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);

            if (CFId > 0)
            {
                IncideContributingFactorModel model = Factor.GetById(CFId);
                return PartialView("AddEdit", model);
            }
            else
            {
                IncideContributingFactorModel model = new IncideContributingFactorModel();
                model.OId = OId;
                return PartialView("AddEdit", model);
            }
        }

        [HttpPost]
        public JsonResult AddEdit(IncideContributingFactorModel model)
        {
            if (model.CFId > 0)
            {
                Factor.Update(model);
                return Json("Updated",JsonRequestBehavior.AllowGet);
            }
            else
            {
                Factor.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public JsonResult Delete(int CFId)
        {
            Factor.Delete(CFId);
            return Json("Delete", JsonRequestBehavior.AllowGet);
        }

    }
}