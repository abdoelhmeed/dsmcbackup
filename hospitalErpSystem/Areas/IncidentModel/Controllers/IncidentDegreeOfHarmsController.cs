﻿using hospitalErpBusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class IncidentDegreeOfHarmsController : Controller
    {
        IncidentDegreeOfHarm Harm = new IncidentDegreeOfHarm();
        Employee emp = new Employee();
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.HarmOfDegree;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<IncidentDegreeOfHarmModel> model= Harm.GetByOId(OId);
            return View(model);
        }

        public ActionResult AddEdit(int Id)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            if (Id > 0)
            {
                IncidentDegreeOfHarmModel model = Harm.GetById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                IncidentDegreeOfHarmModel model =new  IncidentDegreeOfHarmModel();
                model.OId = OId;
                return PartialView("AddEdit", model);
            }
        }
        [HttpPost]
        public JsonResult AddEdit(IncidentDegreeOfHarmModel model)
        {
            if (model.DHId > 0)
            {
                Harm.Update(model);
                return Json("Update",JsonRequestBehavior.AllowGet);
            }
            else
            {
                Harm.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Delete(int Id)
        {
            Harm.Delete(Id);
            return RedirectToAction("Index");
        }

    }
}