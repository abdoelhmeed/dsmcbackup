﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using hospitalErpSystem.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class IncidentChartController : Controller
    {


        Employee emp = new Employee();
        Incident incident = new Incident();
        LocationOfIncident lOfI = new LocationOfIncident();
        IncidentAffected Affected = new IncidentAffected();
        IncidentRiskProbability Irkp = new IncidentRiskProbability();
        IncidentAnalystToClose Coloseing = new IncidentAnalystToClose();
        public ActionResult Index()
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            ViewBag.DataPoints = JsonConvert.SerializeObject(GetForYears(OId), _jsonSetting);
            ViewBag.RiskProbability = JsonConvert.SerializeObject(GetRiskProbability(OId), _jsonSetting);
            return View();
        }
        public List<DataPoint> GetForYears(int OId)
        {
            int y;
            string label;
            List<DataPoint> DataPoint = new List<DataPoint>();
            List<DataPoint> _dataPoint = new List<DataPoint>();
            for (int i = 1; i <= 12; i++)
            {
                List<IncidentViewModel> model = incident.GetIncidentByOId(OId).Where(x => x.ReportDate.Year == DateTime.Now.Year && x.ReportDate.Month==i).ToList();
                y = model.Count;
                label = MonthString.ObtenerNumeroMes(i);
                _dataPoint.Add(new DataPoint(y, label));
            }
            DataPoint = _dataPoint;
            return DataPoint;
        }
        public List<DataPoint> GetRiskProbability(int OId)
        {
            int y;
            string label;
            List<DataPoint> DataPoint = new List<DataPoint>();
            List<DataPoint> _dataPoint = new List<DataPoint>();
            List <GetCharIncidentByRiskProbabilityModel>  Crp= Coloseing.GetCharIncidentByRiskProbability(OId);
            foreach (var item in Crp)
            {
                y = item.RPMNumber;
                label = item.RPMName;
                _dataPoint.Add(new DataPoint(y, label));
            }

            DataPoint = _dataPoint;
            return DataPoint;
        }
        JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
    }
}