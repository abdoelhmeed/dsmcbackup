﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class AnalystToCloseController : Controller
    {
        IncidentAnalystToClose Coloseing = new IncidentAnalystToClose();
        Employee emp = new Employee();
        Incident incident = new Incident();
        IncidentAffected Affected = new IncidentAffected();
        IncidentsAssigns Assings = new IncidentsAssigns();
        Investigator Investigator = new Investigator();
        IncidentRiskProbability RiskP = new IncidentRiskProbability();
        IncidentDegreeOfHarm Harm = new IncidentDegreeOfHarm();
        IncidentRiskSeventySub Sub = new IncidentRiskSeventySub();

       
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SidAnalystToClose;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<AnalystToCloseViewModel> model = Coloseing.GetByOId(OId);

            return View(model);
        }
        public ActionResult IncidentClose(int IncidentId)
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SidAnalystToClose;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);

            IncidentViewModel Incident = incident.GetById(IncidentId);
            ViewBag.IncidentId = Incident.IncidentId;
            ViewBag.DiscoverDate = Incident.DiscoverDate;
            ViewBag.ReportDate = Incident.ReportDate;
            ViewBag.Description = Incident.Description;
            ViewBag.OccurrenceDate = Incident.OccurrenceDate;
            ViewBag.Name = Incident.LNameEn + "   -   " + Incident.LNameAr;

            List<IncidentAssignViewModel> assingList = Assings.GetByIncidentId(IncidentId);
            ViewBag.assing = assingList.ToList();


            List<IncidentAffectedStaffViewModel> affectStaff = Affected.GetStaff(IncidentId);
            ViewBag.affectStaff = affectStaff.ToList();

            List<IncidentAffectedVisitorModel> affectVisitors = Affected.GetVisitors(IncidentId);
            ViewBag.affectVisitors = affectVisitors.ToList();

            List<InvestigatorModel> Inves = Investigator.GetByIncidentId(IncidentId);
            ViewBag.Investigator = Inves.ToList();
            List<IncidentRiskProbabilityModel> rp = RiskP.GetByOId(OId);
            ViewBag.RiskProbability = rp.ToList();


            List<IncidentDegreeOfHarmModel> HarmList=Harm.GetByOId(OId);
            ViewBag.Harm = HarmList.ToList();


            IncidentAnalystToCloseModel model = new IncidentAnalystToCloseModel();
            model.OId = OId;
            model.IncidentId = IncidentId;

            return View(model);
        }
        [HttpPost]
        public ActionResult IncidentClose(IncidentAnalystToCloseModel model)
        {
            int IncidentId = (int)model.IncidentId;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);

            int empId = emp.GetEmpIDByUserId(UserId);

            IncidentViewModel Incident = incident.GetById(IncidentId);
            ViewBag.IncidentId = Incident.IncidentId;
            ViewBag.DiscoverDate = Incident.DiscoverDate;
            ViewBag.ReportDate = Incident.ReportDate;
            ViewBag.Description = Incident.Description;
            ViewBag.OccurrenceDate = Incident.OccurrenceDate;
            ViewBag.Name = Incident.LNameEn + "   -   " + Incident.LNameAr;

            List<IncidentAssignViewModel> assingList = Assings.GetByIncidentId(IncidentId);
            ViewBag.assing = assingList.ToList();


            List<IncidentAffectedStaffViewModel> affectStaff = Affected.GetStaff(IncidentId);
            ViewBag.affectStaff = affectStaff.ToList();

            List<IncidentAffectedVisitorModel> affectVisitors = Affected.GetVisitors(IncidentId);
            ViewBag.affectVisitors = affectVisitors.ToList();

            List<InvestigatorModel> Inves = Investigator.GetByIncidentId(IncidentId);
            ViewBag.Investigator = Inves.ToList();
            List<IncidentRiskProbabilityModel> rp = RiskP.GetByOId(OId);
            ViewBag.RiskProbability = rp.ToList();


            List<IncidentDegreeOfHarmModel> HarmList = Harm.GetByOId(OId);
            ViewBag.Harm = HarmList.ToList();

            model.OId = OId;
            model.CreateByempId = empId;
            if (ModelState.IsValid)
            {
                Coloseing.Add(model);
                return RedirectToAction("Index");

            }
                return View(model);

        }
        public JsonResult RiskSeventy(int MId)
        {
            List<IncidentRiskSeventySubViewModel> model= Sub.GetByMainType(MId);
            return Json(model,JsonRequestBehavior.AllowGet);
        }
        public ActionResult Closed()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdClosedComplain;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);


            List<IncidentViewModel> model = incident.GetIncidentByOId(OId).Where(x => x.status == "Closed").ToList();

            return View(model);
        }


        // /IncidentModel/AnalystToClose/ComplaintDetails? IncidentId = 2
        [HttpGet]
        public ActionResult ComplaintDetails(int IncidentId)
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.ComplainDetails;

            IncidentViewModel model = incident.GetById(IncidentId);

            List<IncidentAssignViewModel> Inc = Assings.GetByIncidentId(IncidentId);
            ViewBag.IncidentAssign = Inc.ToList();

            List<InvestigatorViewModel> Inves = Investigator.GetByViewIncidentId(IncidentId);
            ViewBag.Investigator = Inves.ToList();

            List<ColseReport> colseReports =Coloseing.GetByIncidentId(IncidentId);
            ViewBag.Reports = colseReports.ToList();

            return  View(model);
        }


       
    }
}