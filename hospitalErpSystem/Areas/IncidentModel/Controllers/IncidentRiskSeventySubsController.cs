﻿using hospitalErpBusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class IncidentRiskSeventySubsController : Controller
    {
        IncidentRiskSeventySub Sub = new IncidentRiskSeventySub();
        Employee emp = new Employee();
        IncidentRiskProbability Probability = new IncidentRiskProbability();
        // GET: IncidentModel/IncidentRiskSeventySubs
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.RiskSeventy;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<IncidentRiskSeventySubViewModel> model= Sub.GetByOId(OId);
            return View(model);
        }

        public ActionResult AddEdit(int Id)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<IncidentRiskProbabilityModel> RiskProbabilityList = Probability.GetByOId(OId);
            ViewBag.RiskProbabilityList = RiskProbabilityList.ToList();
            if (Id > 0)
            {
                IncidentRiskSeventySubModel model = Sub.GetById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                IncidentRiskSeventySubModel model = new IncidentRiskSeventySubModel();
                model.OId = OId;
                return PartialView("AddEdit", model);
            }
        }

        [HttpPost]
        public JsonResult AddEdit(IncidentRiskSeventySubModel model)
        {
            if (model.RSSId > 0)
            {
                Sub.Update(model);
                return Json("Updated", JsonRequestBehavior.AllowGet);
            }
            else
            {
                Sub.Add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Delete(int Id)
        {
            Sub.Delete(Id);
            return RedirectToAction("Index");
        }

    }
}