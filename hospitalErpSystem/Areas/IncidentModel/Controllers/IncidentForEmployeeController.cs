﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hospitalErpDomainLayer;
using hospitalErpBusinessLayer;
using Microsoft.AspNet.Identity;
using System.IO;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class IncidentForEmployeeController : Controller
    {
        Employee emp = new Employee();
        Incident incident = new Incident();
        LocationOfIncident lOfI = new LocationOfIncident();
        IncidentAffected Affected = new IncidentAffected();
        // GET: IncidentModel/IncidentForEmployee
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdMyComplain;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<IncidentViewModel>model= incident.GetIncidentByOId(OId).Where(x=> x.CreateByEmpId== empId).ToList();
            return View(model);
        }
        [HttpGet]
        public ActionResult MyIncidention()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdMyComplain;
            ViewBag.SubSitMap = Resources.Resource.LCreate;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<LocationOfIncidentModel> Location = lOfI.GetPyOId(OId);
            List<EmployeeSelectionModel> EmpList = emp.GetEmployeeSelectioByOId(OId);
            ViewBag.EmpList = EmpList.ToList();
            ViewBag.Location = Location.ToList();
            return View();
        }
        [HttpPost]
        public ActionResult MyIncidention(IncidentsModel model)
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdMyComplain;
            
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<LocationOfIncidentModel> Location = lOfI.GetPyOId(OId);
            List<EmployeeSelectionModel> EmpList = emp.GetEmployeeSelectioByOId(OId);
            ViewBag.EmpList = EmpList.ToList();
            ViewBag.Location = Location.ToList();
            if (ModelState.IsValid)
            {
                int empId = emp.GetEmpIDByUserId(UserId);
                model.CreateByEmpId = empId;
                EmployeeViewModel empName = emp.GetEmployeeById(empId);
                model.CreatorName = empName.empFNameEn + " " + empName.empSNameEn + " " + empName.empTNameEn;
                model.isClosed = false;
                model.OId = OId;
                if (model.ImagesFile != null)
                {
                    model.InciImages = AddImage(model.ImagesFile, "Incident");
                }
              int Id=incident.Add(model);
                if (Id > 0)
                {
                    return RedirectToAction("IncidentAddDetails", new { Id=Id});
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult IncidentAddDetails(int Id)
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdMyComplain;
            ViewBag.SubSitMap = Resources.Resource.LCreate;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<EmployeeSelectionModel> EmpList = emp.GetEmployeeSelectioByOId(OId);
            ViewBag.EmpList = EmpList.ToList();

            IncidentViewModel model =incident.GetById(Id);
            return View(model);
        }
        [HttpGet]
        public JsonResult GetVisitor(int Id)
        {

            IncidentAffected Affected = new IncidentAffected();
            List<IncidentAffectedVisitorModel>  model=Affected.GetVisitors(Id);
            return Json(model,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AddVisitor(IncidentAffectedVisitorModel model)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            IncidentAffectedVisitorModel Visitor = new IncidentAffectedVisitorModel {
                IncidentId=model.IncidentId,
                OId=OId,
                VAddress=model.VAddress,
                Visible=true,
                VName=model.VName,
                VPhone=model.VPhone
            };
            Affected.AddVisitor(Visitor);
            return Json("Saved", JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetStaff(int Id)
        {
            IncidentAffected Affected = new IncidentAffected();
            List<IncidentAffectedStaffViewModel> model = Affected.GetStaff(Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AddStaff(IncidentAffectedStaffModel model)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            IncidentAffectedStaffModel staff = new IncidentAffectedStaffModel
            {
                OId = OId,
                empId=model.empId,
                IncidentId=model.IncidentId,
                Visible=true
            };

            Affected.AddStaff(staff);
            return Json("Seved",JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteStaff(int Id)
        {
            Affected.DeleteStaff(Id);
            return Json("Deleted", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteVisitor(int Id)
        {
            Affected.DeleteVisitor(Id);
            return Json("Deleted", JsonRequestBehavior.AllowGet);
        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Attachments/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Attachments/"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }

        public ActionResult MyIncidentionDetails(int Id)
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdMyComplain;
            ViewBag.SubSitMap = Resources.Resource.ComplainDetails;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            IncidentViewModel model = incident.GetById(Id);
            return View(model);
        }
       
        public ActionResult submitIncident(int Id)
        {
            incident.SubmitIncident(Id);
            return RedirectToAction("Index");
        }
    }
}