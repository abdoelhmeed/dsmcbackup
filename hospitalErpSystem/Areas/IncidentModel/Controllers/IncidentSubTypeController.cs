﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class IncidentSubTypeController : Controller
    {
        IncidentType InType = new IncidentType();
        Employee emp = new Employee();
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.ComplainType;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);

            List<IncidentSubTypeModel> model = InType.GetSubList(OId);

            return View(model);
        }

        public ActionResult AddEdit(int Id)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<IncidentMainTypeModel> MianList = InType.GetMianList(OId);
            ViewBag.MianList = MianList.ToList();
            if (Id > 0)
            {
                IncidentSubTypeModel model = InType.GetSubById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                IncidentSubTypeModel model = new IncidentSubTypeModel();
                model.OId = OId;
                return PartialView("AddEdit", model);
            }
        }
        [HttpPost]
        public JsonResult AddEdit(IncidentSubTypeModel model)
        {
            if (model.ISTId > 0)
            {
                InType.UpdateSub(model);
                return Json("Update", JsonRequestBehavior.AllowGet);
            }
            else
            {
                InType.AddSub(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Delete(int Id)
        {
            InType.DeleteSub(Id);
            return Json("Deleted", JsonRequestBehavior.AllowGet);
        }
    }
}