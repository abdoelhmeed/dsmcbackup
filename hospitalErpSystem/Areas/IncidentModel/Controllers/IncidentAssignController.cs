﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class IncidentAssignController : Controller
    {
        Employee emp = new Employee();
        Incident incident = new Incident();
        LocationOfIncident lOfI = new LocationOfIncident();
        IncidentAffected Affected = new IncidentAffected();
        Department department = new Department();
        IncidentType incidentType = new IncidentType();
        IncidentsAssigns IncidentsAssigns = new IncidentsAssigns();
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdSubmittedComplain;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<IncidentViewModel> model = incident.GetIncidentByOId(OId).Where(x=> x.status== "Submited").ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult SubmitedIncident(int Id)
        {

            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdSubmittedComplain;

            IncidentViewModel Incident =incident.GetById(Id);
            ViewBag.IncidentId = Incident.IncidentId;
            ViewBag.DiscoverDate = Incident.DiscoverDate;
            ViewBag.ReportDate = Incident.ReportDate;
            ViewBag.Description = Incident.Description;
            ViewBag.OccurrenceDate = Incident.OccurrenceDate;
            ViewBag.Name = Incident.LNameEn + "   -   "+Incident.LNameAr;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);

            List<IncidentMainTypeModel> MType= incidentType.GetMianList(OId);
            ViewBag.MType = MType.ToList();

            List<DepartmentViewModel> departmentList= department.GetAllByOId(OId);
            ViewBag.departmentList = departmentList.ToList();





            List<LocationOfIncidentModel> Location = lOfI.GetPyOId(OId);

            List<IncidentAffectedStaffViewModel> affectStaff= Affected.GetStaff(Id);
            ViewBag.affectStaff = affectStaff.ToList();



            List<IncidentAffectedVisitorModel> affectVisitors = Affected.GetVisitors(Id);
            ViewBag.affectVisitors = affectVisitors.ToList();

            return View();
        }
        [HttpPost]
        public ActionResult SubmitedIncident(IncidentAssignModel model)
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdSubmittedComplain;

            IncidentViewModel Incident = incident.GetById((int)model.IncidentId);
            ViewBag.IncidentId = Incident.IncidentId;
            ViewBag.DiscoverDate = Incident.DiscoverDate;
            ViewBag.ReportDate = Incident.ReportDate;
            ViewBag.Description = Incident.Description;
            ViewBag.OccurrenceDate = Incident.OccurrenceDate;
            ViewBag.Name = Incident.LNameEn + "   -   " + Incident.LNameAr;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);

            List<IncidentMainTypeModel> MType = incidentType.GetMianList(OId);
            ViewBag.MType = MType.ToList();

            List<DepartmentViewModel> departmentList = department.GetAllByOId(OId);
            ViewBag.departmentList = departmentList.ToList();

            List<LocationOfIncidentModel> Location = lOfI.GetPyOId(OId);

            List<IncidentAffectedStaffViewModel> affectStaff = Affected.GetStaff((int)model.IncidentId);
            ViewBag.affectStaff = affectStaff.ToList();



            List<IncidentAffectedVisitorModel> affectVisitors = Affected.GetVisitors((int)model.IncidentId);
            ViewBag.affectVisitors = affectVisitors.ToList();
            model.OId = OId;
            model.CreateByEmpId = empId;
            model.Visible = true;
            
            IncidentsAssigns.Add(model);


            return RedirectToAction("Index");
        }

       [HttpPost]
        public JsonResult GetSubType(int MId)
        {
            List<IncidentSubTypeModel> model= incidentType.GetSubTypeByMId(MId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetEmployee(int DeptId)
        {
            List<EmployeeViewModel> model= department.GetDepartmentManagers(DeptId);
            return Json(model,JsonRequestBehavior.AllowGet);
        }

    }
}