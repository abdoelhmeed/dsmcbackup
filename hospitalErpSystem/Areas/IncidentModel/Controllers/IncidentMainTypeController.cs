﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class IncidentMainTypeController : Controller
    {
        IncidentType InType = new IncidentType();
        Employee emp = new Employee();
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.ComplainType;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<IncidentMainTypeModel> model = InType.GetMianList(OId);
            return View(model);
        }


        public ActionResult AddEdit(int Id)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            if (Id > 0)
            {
                IncidentMainTypeModel model =InType.GetMinById(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                IncidentMainTypeModel model = new IncidentMainTypeModel();
                model.OId = OId;
                return PartialView("AddEdit", model);
            }
        }
        [HttpPost]
        public JsonResult AddEdit(IncidentMainTypeModel model)
        {
            if (model.IMTId > 0)
            {
                InType.UpdateMain(model);
                return Json("Updated", JsonRequestBehavior.AllowGet);
            }
            else
            {
                InType.AddMain(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Delete(int Id)
        {
            InType.DeleteMain(Id);
            return Json("Deleted",JsonRequestBehavior.AllowGet);
        }
    }
}