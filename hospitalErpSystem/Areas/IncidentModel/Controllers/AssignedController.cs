﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.IncidentModel.Controllers
{
    [Authorize]
    public class AssignedController : Controller
    {
        Investigator mode = new Investigator();
        Employee emp = new Employee();
        Incident incident = new Incident();
        LocationOfIncident lOfI = new LocationOfIncident();
        IncidentAffected Affected = new IncidentAffected();
        Department department = new Department();
        IncidentType incidentType = new IncidentType();
        IncidentsAssigns IncidentsAssigns = new IncidentsAssigns();
        IncideContributingFactor Factor = new IncideContributingFactor();

        Investigator Investigator = new Investigator();
        // GET: IncidentModel/Assigned
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.SIdAssignedComplain;
           

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<IncidentAssignForViewModel> model= IncidentsAssigns.GetByEmpId(empId);
            return View(model);
        }
        public ActionResult investigate(int assId)
        {
            ViewBag.SubSystem = Resources.Resource.SidIncidentModel;
            ViewBag.SitMap = Resources.Resource.investigate;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);

            IncidentAssignViewModel Inc= IncidentsAssigns.GetById(assId);
            ViewBag.IncidentAssign = Inc;
            IncidentViewModel Incident = incident.GetById((int)Inc.IncidentId);
            ViewBag.IncidentId = Incident.IncidentId;
            ViewBag.DiscoverDate = Incident.DiscoverDate;
            ViewBag.ReportDate = Incident.ReportDate;
            ViewBag.Description = Incident.Description;
            ViewBag.OccurrenceDate = Incident.OccurrenceDate;
            ViewBag.Name = Incident.LNameEn + "   -   " + Incident.LNameAr;

            List<IncidentAffectedStaffViewModel> affectStaff = Affected.GetStaff((int)Inc.IncidentId);
            ViewBag.affectStaff = affectStaff.ToList();

            List<IncidentAffectedVisitorModel> affectVisitors = Affected.GetVisitors((int)Inc.IncidentId);
            ViewBag.affectVisitors = affectVisitors.ToList();

            InvestigatorModel Getmodel = new InvestigatorModel();
            Getmodel.assId = assId;
            Getmodel.IncidentId = Inc.IncidentId;
            Getmodel.Oid = OId;

            List<IncideContributingFactorModel> ContributingFactorList= Factor.GetByOId(OId);
            ViewBag.ContributingFactorList = ContributingFactorList;

            return View(Getmodel);
        }
        [HttpPost]
        public ActionResult investigate(InvestigatorModel model)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
         
            if (ModelState.IsValid)
            {
                Investigator.Add(model);
                return RedirectToAction("Index");

            }
            else
            {
                int assId = (int)model.assId;
                IncidentAssignViewModel Inc = IncidentsAssigns.GetById(assId);
                ViewBag.IncidentAssign = Inc;
                int IncidentId = (int)Inc.IncidentId;
                IncidentViewModel Incident = incident.GetById(IncidentId);
                ViewBag.IncidentId = Incident.IncidentId;
                ViewBag.DiscoverDate = Incident.DiscoverDate;
                ViewBag.ReportDate = Incident.ReportDate;
                ViewBag.Description = Incident.Description;
                ViewBag.OccurrenceDate = Incident.OccurrenceDate;
                ViewBag.Name = Incident.LNameEn + "   -   " + Incident.LNameAr;

                List<IncidentAffectedStaffViewModel> affectStaff = Affected.GetStaff(IncidentId);
                ViewBag.affectStaff = affectStaff.ToList();

                List<IncidentAffectedVisitorModel> affectVisitors = Affected.GetVisitors(IncidentId);
                ViewBag.affectVisitors = affectVisitors.ToList();

                List<IncideContributingFactorModel> ContributingFactorList = Factor.GetByOId(OId);
                ViewBag.ContributingFactorList = ContributingFactorList;

                return View(model);
            }
       
              
        }

    }
}