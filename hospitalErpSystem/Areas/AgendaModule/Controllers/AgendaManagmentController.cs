﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;


namespace hospitalErpSystem.Areas.AgendaModule.Controllers
{
    [Authorize]
    public class AgendaManagmentController : Controller
    {

        Agenda agenda = new Agenda();
        Employee emp = new Employee();
        Department department = new Department();
        EmployeesDepartmentManagment empPosition = new EmployeesDepartmentManagment();
        // GET: AgendaModule/AgendaManagment
        public ActionResult Index()
        {
            ViewBag.SubSystem = @Resources.Resource.SidAgendaModule;
            ViewBag.SitMap = @Resources.Resource.Agenda;
            ViewBag.SubSitMap = @Resources.Resource.AgendaList;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<AgendaViewModel> model= agenda.GetAll(OId);
            return View(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
          
            ViewBag.SubSystem = @Resources.Resource.SidAgendaModule;
            ViewBag.SitMap = @Resources.Resource.Agenda;
            ViewBag.SubSitMap = @Resources.Resource.LCreate;

            ViewBag.Error = "";

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);


            List<EmployeeViewModel> model = emp.GetEmployeeByOId(OId);
            ViewBag.EmpList = model.ToList();

            List<DepartmentViewModel> DepartmentList = department.GetAllByOId(OId);
            ViewBag.DepartmentList = DepartmentList.ToList();



            return View();
        }
        public ActionResult Create(AgendaModel model)
        {
            ViewBag.SubSystem = @Resources.Resource.SidAgendaModule;
            ViewBag.SitMap = @Resources.Resource.Agenda;
            ViewBag.SubSitMap = @Resources.Resource.LCreate;

            ViewBag.Error = "";

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empIdForm = emp.GetEmpIDByUserId(UserId);

            List<EmployeeViewModel> EmployeeList = emp.GetEmployeeByOId(OId);
            ViewBag.EmpList = EmployeeList.ToList();

            List<DepartmentViewModel> DepartmentList = department.GetAllByOId(OId);
            ViewBag.DepartmentList = DepartmentList.ToList();

            if (model.Type==0 || model.Type==null)
            {
                ViewBag.Error = "Please enter the information correctly";
                return View(model);
            }

            if (model.Title == ""|| model.Title == null)
            {
                ViewBag.Error = "Please enter Title";
                return View(model);
            }

            if (model.Subject == "" || model.Subject == null)
            {
                ViewBag.Error = "Please enter Subject";
                return View(model);
            }

            if (model.Type == 1)
            {
                model.FromemployeeId = empIdForm;
                model.Oid = OId;
                agenda.Add(model);
            }

            if (model.Type == 2)
            {
                List<employeeDepartmentManagmentViewModel> empDepartmentManagment = empPosition.GetEmployeesByOId(OId);
                if (empDepartmentManagment.Count > 0)
                {
                    foreach (var item in empDepartmentManagment.Where(x => x.DepartmentId == model.DepartmentId))
                    {
                        model.FromemployeeId = empIdForm;
                        model.ToEmployeeId = item.empid;
                        model.Oid = OId;
                        agenda.Add(model);
                    }
                }
                ViewBag.Error = "Department not has Employee";
                return View(model);
            }
            return RedirectToAction("Index");
        }

        public ActionResult ReadAgenda(int AId,int NId)
        {
            AgendaViewEmployeeModel  model= agenda.GetById(AId, NId);
            return View(model);
        }

        public ActionResult AllReadAgenda(int? Page)
        {
            ViewBag.SubSystem = @Resources.Resource.SidAgendaModule;
            ViewBag.SitMap = @Resources.Resource.Agenda;
            ViewBag.SubSitMap = @Resources.Resource.MyAgenda;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empIdForm = emp.GetEmpIDByUserId(UserId);
            List<AgendaViewModel> model = agenda.GetEmployeeFor(empIdForm);
            return View(model.ToPagedList(Page ?? 1, 5));
        }
    }
}