﻿using System.Web.Mvc;

namespace hospitalErpSystem.Areas.AgendaModule
{
    public class AgendaModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AgendaModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AgendaModule_default",
                "AgendaModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}