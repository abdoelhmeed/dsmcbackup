﻿using System.Web.Mvc;

namespace hospitalErpSystem.Areas.RequestsModule
{
    public class RequestsModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RequestsModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RequestsModule_default",
                "RequestsModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}