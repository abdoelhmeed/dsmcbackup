﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.RequestsModule.Controllers
{
    [Authorize]
    public class RequestsTypeController : Controller
    {
        EmployeeRequestsType ResType = new EmployeeRequestsType();
        Employee emp = new Employee();
        Department department =new  Department();
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidRequestsModule;
            ViewBag.SitMap = Resources.Resource.SidRequestTypes;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<EmployeeRequestsTypeViewModel> model = ResType.GetByOId(OId);
            return View(model);
        }


        public ActionResult AddEdit(int Id)
        {
           
            if (Id > 0)
            {
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                List<DepartmentViewModel> DepartmentList = department.GetAllByOId(OId);
                ViewBag.Department = DepartmentList.ToList();
                EmployeeRequestsTypeModel model = ResType.GetToUpdate(Id);
                return PartialView("AddEdit", model);
            }
            else
            {
                string UserId = User.Identity.GetUserId();
                int OId = emp.GetOIdByUserId(UserId);
                List<DepartmentViewModel> DepartmentList = department.GetAllByOId(OId);
                ViewBag.Department = DepartmentList.ToList();
                EmployeeRequestsTypeModel model = new EmployeeRequestsTypeModel();
                model.OId = OId;
                return PartialView("AddEdit", model);
            }
        }

        [HttpPost]
        public JsonResult AddEdit(EmployeeRequestsTypeModel model)
        {
            if (model.id > 0)
            {
                ResType.Update(model);
                return Json("Updated",JsonRequestBehavior.AllowGet);
            }
            else
            {
                ResType.Add(model);
                return Json("Saved",JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int Id)
        {
            ResType.Delete(Id);
            return Json("Deleted", JsonRequestBehavior.AllowGet);
        }
    }
}