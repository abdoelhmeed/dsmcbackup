﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.RequestsModule.Controllers
{
    [Authorize]
    public class EmployeeRequestController : Controller
    {
        EmployeeRequest empReq = new EmployeeRequest();
        Employee emp = new Employee();
        EmployeeRequestsType Type = new EmployeeRequestsType();
        // GET: RequestsModule/EmployeeRequest
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidRequestsModule;
            ViewBag.SitMap = Resources.Resource.SIdMyRequest;
          
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<EmployeeRequestViweModel> model= empReq.GetByempId(empId);
            return View(model);
        }

        public ActionResult Create()
        {
            EmployeeRequestModel model = new EmployeeRequestModel();
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<EmployeeRequestsTypeViewModel> TypeList= Type.GetByOId(OId);
            ViewBag.Type = TypeList.ToList();
            model.Oid = OId;
            model.empId = empId;
            return PartialView("Create", model);
        }
        [HttpPost]
        public JsonResult Create(EmployeeRequestModel model)
        {
            empReq.Add(model);
            return Json("Saved",JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReadRequest(int requestId)
        {
            EmployeeRequestModel model= empReq.GetById(requestId);


            return View();
        }

    }
}