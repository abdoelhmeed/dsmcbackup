﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.RequestsModule.Controllers
{
    [Authorize]
    public class ApproveRequestController : Controller
    {
        EmployeeRequestsNodes Approve = new EmployeeRequestsNodes();
        Employee emp = new Employee();
        public ActionResult Index()
        {
            ViewBag.SubSystem = Resources.Resource.SidRequestsModule;
            ViewBag.SitMap = Resources.Resource.SIdApproveRequest;
           
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<EmployeeRequestsNodesViewModel> model = Approve.GetForApprove(empId);
            return View(model);
        }
        public ActionResult approve(int Id)
        {
            EmployeeRequestsNodesModel model=Approve.GetById(Id);
            return PartialView("approve", model);
        }
        [HttpPost]
        public ActionResult GoApprove(EmployeeRequestsNodesModel model)
        {
            Approve.Approve(model);
            return RedirectToAction("Index");
        }
        public ActionResult Reject(int Id)
        {
            EmployeeRequestsNodesModel model = Approve.GetById(Id);
            return PartialView("Reject", model);
        }
        [HttpPost]
        public JsonResult GoReject(EmployeeRequestsNodesModel model)
        {
            Approve.Reject(model);
            return Json("Rejected", JsonRequestBehavior.AllowGet);
        }
    }
}