﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;


namespace hospitalErpSystem.Areas.PoliciesModule.Controllers
{
    [Authorize]
    public class PoliciesReadController : Controller
    {
        PoliciesRea rea = new PoliciesRea();
        Employee emp = new Employee();
        Policy policy = new Policy();
        PolicyApprov policyApprov = new PolicyApprov();
        Management Manag = new Management();
        Department Dept = new Department();
        PolicyType policyType = new PolicyType();
        PoliciesApplies policiesApplies = new PoliciesApplies();
       [Authorize]
       public ActionResult Index()
       {
            return View();
       }

        public ActionResult PolicyType()
        {
            ViewBag.SubSystem = "Policies Details Module";
            ViewBag.SitMap = "Policy";
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);

            List<PoliciesTypeViewModel> models=  policyType.getByOId(OId);
            return View(models);
        }
       public ActionResult MyPolicy(int? Page)
       {

            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.SIdPolicies;
            ViewBag.SubSitMap = Resources.Resource.MyPolicy;

            string UserId = User.Identity.GetUserId();
           int empId= emp.GetEmpIDByUserId(UserId);
           List<PoliciesReaViewModel> model = rea.GetMyPolicy(empId);

            var model2= model.GroupBy(x => x.polTypeEn).ToList();

            return View(model2.ToPagedList(Page ?? 1, 8));
        }
       public ActionResult ReadPolicy(string Number)
       {
            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.SIdPolicies;
            ViewBag.SubSitMap = Resources.Resource.MyPolicy;


            PoliciesViewModel model = policy.GetPolicyByNumber(Number);
            List<PolicyApproveViewModel> ApproveList = policyApprov.GetByPolicyNumber(Number);
            ViewBag.ApproveList = ApproveList.ToList();
            List<PoliciesAppliesToViewModel> Applies = policiesApplies.GetByPolicyNumbers(Number);
            ViewBag.Applies = Applies.ToList();
            string UserId = User.Identity.GetUserId();
            int empId = emp.GetEmpIDByUserId(UserId);
            rea.PolicisRead(Number, empId, UserId);
            return View(model);
       }
    }
}