﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.PoliciesModule.Controllers
{
    [Authorize]
    public class PoliciesTypeController : Controller
    {
        PolicyType policyType = new PolicyType();
        Employee emp = new Employee();
        Management Manag = new Management();

        public ActionResult Index()
        {
            ViewBag.SubSystem =Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.SidPoliciesType;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<PoliciesTypeViewModel>model= policyType.getByOId(OId);
            return View(model);
        }

        public ActionResult AddEdit(int Id)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            ViewBag.Management = Manag.GetSelectionByOId(OId);
            if (Id > 0)
            {
                PoliciesTypeModel model = policyType.getById(Id);  
                return PartialView("AddEdit", model);
            }
            else
            {
                PoliciesTypeModel model =new PoliciesTypeModel();
                model.Oid=OId;
                return PartialView("AddEdit", model);
            }
        }
        [HttpPost]
        public JsonResult AddAndEdit(PoliciesTypeModel model)
        {
            if (model.TImgFile !=null)
            {
                model.TImg = AddImage(model.TImgFile, "PolicyType");
            }
            if (model.id > 0)
            {
                policyType.Update(model);
                return Json("Updated",JsonRequestBehavior.AllowGet);
            }
            else
            {
                policyType.add(model);
                return Json("Saved", JsonRequestBehavior.AllowGet);
            }

        }

        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Attachments/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Attachments/"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }


    }
}