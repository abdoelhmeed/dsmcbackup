﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.PoliciesModule.Controllers
{
    public class PolicyPlanController : Controller
    {
        PoliciesPlan PPlan = new PoliciesPlan();
        Employee emp = new Employee();
        Management Manag = new Management();
        // GET: PoliciesModule/PolicyPlan
        public ActionResult Index(int TId)
        {
            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.PoliciesPlan;
            List<PolicyPlanViweModel> model = PPlan.GetByTId(TId);
            ViewBag.TId = TId;
            return View(model);
        }

        public ActionResult AddEdit(int Id,int TId)
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            ViewBag.Management = Manag.GetSelectionByOId(OId);
            if (Id> 0)
            {
                PolicyPlanModel model = PPlan.GetById(Id);
                model.TId = TId;
                return  PartialView("AddEdit", model);
            }
            else
            {
                PolicyPlanModel model = new PolicyPlanModel();
                model.TId = TId;
                return PartialView("AddEdit", model);
            }
        }

        [HttpPost]
        public JsonResult AddAndEdit(PolicyPlanModel model)
        {
            string res;
            int Mid = (int)model.Mid;
            List<PolicyPlanViweModel> Ched = PPlan.GetByMid(Mid);
            int num =Ched.Where(x => x.TId == model.TId).Count();
            if (num > 0 )
            {
                res = "0";
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            if (model.id > 0)
            {
                PPlan.Update(model);
                res = "1";
                return Json(res,JsonRequestBehavior.AllowGet);
            }
            else
            {
                PPlan.Add(model);
                res = "1";
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult Delete(int Id)
        {
            PPlan.Delete(Id);
            return Json("Deleted",JsonRequestBehavior.AllowGet);
        }

    }
}