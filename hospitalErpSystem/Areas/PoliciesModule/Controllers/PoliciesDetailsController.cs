﻿using hospitalErpBusinessLayer;
using hospitalErpDomainLayer;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hospitalErpSystem.Areas.PoliciesModule.Controllers
{
    [Authorize]
    public class PoliciesDetailsController : Controller
    {
        Policy policy = new Policy();
        Employee emp = new Employee();
        Management Manag = new Management();
        Department Dept = new Department();
        PolicyType policyType = new PolicyType();
        PoliciesApplies policiesApplies = new PoliciesApplies();
        PolicyApprov policyApprov = new PolicyApprov();
        // GET: PoliciesModule/PoliciesDetails
        public ActionResult Index()
        {
            ViewBag.SubSystem = "Policies Details Module";
            ViewBag.SitMap = "Policy";
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<PoliciesViewModel> model = policy.GetPolicies(OId);
            return View(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.SIdPolicies;
            ViewBag.SubSitMap = Resources.Resource.LCreate;


            ViewBag.Error = "";
            List<DepartmentViewModel> dList = GetDepartmentByOId();
            ViewBag.Depts = dList.ToList();
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<PoliciesTypeViewModel> PTypeList = policyType.getByOId(OId);
            ViewBag.PTypeList = PTypeList.ToList();
            return View();
        }
        [HttpGet]
        public ActionResult Details(string Number)
        {
            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.PolicyDetails;
            PoliciesViewModel model = policy.GetPolicyByNumber(Number);
            List<PolicyApproveViewModel> ApproveList = policyApprov.GetByPolicyNumber(Number);
            ViewBag.ApproveList = ApproveList.ToList();
            List<PoliciesAppliesToViewModel> Applies = policiesApplies.GetByPolicyNumbers(Number);
            ViewBag.Applies = Applies.ToList();

            return View(model);
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(PoliciesModel model)
        {
            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.SIdPolicies;
            ViewBag.SubSitMap = Resources.Resource.LCreate;

            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            model.OId = OId;
            model.EnterDate = DateTime.Now;
            List<PoliciesTypeViewModel> PTypeList = policyType.getByOId(OId);
            ViewBag.PTypeList = PTypeList.ToList();
            PoliciesViewModel pol = policy.GetPolicyByNumber(model.policiesNumbers);
            if (pol.policiesNumbers != null)
            {
                List<DepartmentViewModel> dList = GetDepartmentByOId();
                ViewBag.Depts = dList.ToList();
                ViewBag.Error = " Number is Already Exists";
                return View(model);
            }

            if (ModelState.IsValid)
            {
                if (model.Type == "2")
                {
                    model.DeptId = ListDepartmentId();
                }
                model.PolPdf = AddImage(model.PolFile, "policy");
                model.CreateByEmpId = emp.GetEmpIDByUserId(UserId);
                policy.add(model);
                return RedirectToAction("Index");
            }
            else
            {
                List<DepartmentViewModel> dList = GetDepartmentByOId();
                ViewBag.Depts = dList.ToList();
                ViewBag.Error = "Data Is Not Valid";
                return View(model);
            }

        }
        private List<int> ListDepartmentId()
        {
            List<int> Id = new List<int>();
            List<DepartmentViewModel> deptIds = GetDepartmentByOId();
            foreach (var item in deptIds)
            {
                Id.Add(item.deptId);
            }
            return Id;
        }
        [HttpGet]
        public ActionResult ApprovePolicy()
        {
            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SubSitMap = Resources.Resource.SIdApprovePolicies;
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            int empId = emp.GetEmpIDByUserId(UserId);
            List<PolicyForApproveViewModel> model = policyApprov.GetForApprove(empId, OId);
            return View(model);
        }
        public ActionResult Approve(int Id)
        {
            policyApprov.Approve(Id);
            return RedirectToAction("ApprovePolicy");
        }
        [HttpGet]
        public ActionResult Publish()
        {
            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.SidPolicyPublish;
           
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<PoliciesViewModel> model = policyApprov.PublishPolicy(OId);
            int empId = emp.GetEmpIDByUserId(UserId);
            return View(model.Where(x=>x.CreateByEmpId==empId).ToList());
        }
        public ActionResult PublishForEmployee(string Number)
        {

           int res=policy.PublishEmployee(Number);
           
            return RedirectToAction("Publish");
        }
        [HttpGet]
        public ActionResult Update(string Number)
        {
            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.SIdPolicies;
            ViewBag.SubSitMap = Resources.Resource.UpdatePolicoy;

            PoliciesModel model= policy.GetByNumber(Number);
            PoliciesUpdateModel Policy = new PoliciesUpdateModel
            {
                PolBody=model.PolBody,
                PolEffectiveDate=model.PolEffectiveDate,
                policiesNumbers=model.policiesNumbers,
                PolPdf=model.PolPdf,
                polTitle=model.polTitle
            };
            return View(Policy);
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Update(PoliciesUpdateModel model)
        {
            ViewBag.SubSystem = Resources.Resource.SidPoliciesModule;
            ViewBag.SitMap = Resources.Resource.SIdPolicies;
            ViewBag.SubSitMap = Resources.Resource.UpdatePolicoy;

            if (model.PolFile != null)
            {
                model.PolPdf = AddImage(model.PolFile, "Policy");
            }
            if (ModelState.IsValid)
            {
                policy.Update(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }
        //fun
        public string AddImage(HttpPostedFileBase File, string Type)
        {
            string fileName = Path.GetFileNameWithoutExtension(File.FileName);
            string extension = Path.GetExtension(File.FileName);
            fileName = Type + DateTime.Now.ToString("yymmssfff") + extension;
            string imgname = "/Attachments/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Attachments/"), fileName);
            File.SaveAs(fileName);
            return imgname;
        }
        private List<DepartmentViewModel> GetDepartmentByOId()
        {
            List<DepartmentViewModel> dList = new List<DepartmentViewModel>();
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<ManagementelectionModel> mList = Manag.GetSelectionByOId(OId);
            foreach (var item in mList)
            {
                List<DepartmentViewModel> Depts = Dept.GetDeptByMId(item.Mid);
                dList.AddRange(Depts);
            }

            return dList;
        }
        private List<ManagementViewModel> GetMangers()
        {
            string UserId = User.Identity.GetUserId();
            int OId = emp.GetOIdByUserId(UserId);
            List<ManagementViewModel>  List= Manag.GetAllByOId(OId);
            return List;
        }
    }
}