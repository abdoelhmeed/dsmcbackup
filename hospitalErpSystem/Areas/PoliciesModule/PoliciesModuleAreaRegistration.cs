﻿using System.Web.Mvc;

namespace hospitalErpSystem.Areas.PoliciesModule
{
    public class PoliciesModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PoliciesModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PoliciesModule_default",
                "PoliciesModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}