$("#AccountTransf").validate({
	rules: {
		tranAmount: "required",
		IPIN: {
			required: true,
			minlength: 4,
			maxlength: 4,
			number: true
		}

	},
	messages: {

		IPIN: {
			required: "Please provide a IPIN",
			minlength: "Your IPIN must be at least 4 characters",
			number: "Your IPIN must be Nmuber"

		},


	},
	errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
	highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
	unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
	submitHandler: function (form) {



		///  Post
	}
});