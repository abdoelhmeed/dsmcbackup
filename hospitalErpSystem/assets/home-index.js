﻿(function ($) {
    function HomeIndex() {
        var $this = this;

        function initialize() {
            $('#PolBody').summernote({
                focus: true,
                height: 300,  
                codemirror: { 
                    theme: 'united'
                }
            });
        }

        $this.init = function () {
            initialize();
        }
    }
    $(function () {
        var self = new HomeIndex();
        self.init();
    })
}(jQuery))