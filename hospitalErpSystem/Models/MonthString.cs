﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hospitalErpSystem.Models
{
    public class MonthString
    {
        public static string ObtenerNumeroMes(int NombreMes)
        {
            string NumeroMes;
            switch (NombreMes)
            {

                case 1:
                    NumeroMes = "ENERO";
                    return NumeroMes;

                case 2:
                    NumeroMes = "FEBRERO";
                    return NumeroMes;

                case 3:
                    NumeroMes = "MARZO";
                    return NumeroMes;

                case 4:
                    NumeroMes = "ABRIL";
                    return NumeroMes;

                case 5:
                    NumeroMes = "MAYO";
                    return NumeroMes;

                case 6:
                    NumeroMes = "JUNIO";
                    return NumeroMes;

                case 7:
                    NumeroMes = "JULIO";
                    return NumeroMes;

                case 8:
                    NumeroMes = "AGOSTO";
                    return NumeroMes;

                case 9:
                    NumeroMes = "SEPTIEMBRE";
                    return NumeroMes;

                case 10:
                    NumeroMes = "OCTUBRE";
                    return NumeroMes;

                case 11:
                    NumeroMes = "NOVIEMBRE";
                    return NumeroMes;

                case 12:
                    NumeroMes = "DICIEMBRE";
                    return NumeroMes;

                default:
                    Console.WriteLine("Error");
                    return "ERROR";

            }
        }

    }
}