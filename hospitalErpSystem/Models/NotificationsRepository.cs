﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace hospitalErpSystem.Models
{
    public class NotificationsRepository
    {
        readonly string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
       
        public IEnumerable<NotificationsModel> GetAllNotification(string UserID)
        {
           
            List<NotificationsModel> Notifications = new List<NotificationsModel>();
            using (var connection = new SqlConnection(_connString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"
                      SELECT [id]
                          ,[nTitel]
                          ,[nBody]
                          ,[nUrl]
                          ,[isRead]
                          ,[ReadDate]
                          ,[UserId]
                          ,[NType]
                      FROM [dbo].[td_Notifications] where isRead='False' and UserId='"+ UserID + "'", connection))
                {
                    command.Notification = null;

                    var dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Notifications.Add(item: new NotificationsModel {
                            id = (int)reader["id"],
                            isRead = (bool)reader["isRead"],
                            UserId = (string)reader["UserId"],
                            NType = (string)reader["NType"],
                            nUrl = (string)reader["nUrl"],
                            nTitel = (string)reader["nTitel"],
                            nBody =(string)reader["nBody"],
                            ReadDate = Convert.ToDateTime(reader["ReadDate"])
                        });
                    }
                }

            }
            return Notifications;


        }

        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                Hubs.NotificationHub.SendNotification();
            }
        }
    }
}