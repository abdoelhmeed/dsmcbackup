﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hospitalErpSystem.Models
{
    public class NotificationsModel
    {
        public int id { get; set; }
        public string nTitel { get; set; }
        public string nBody { get; set; }
        public string nUrl { get; set; }
        public bool isRead { get; set; }
        public DateTime ReadDate { get; set; }
        public string UserId { get; set; }
        public string NType { get; set; }
    }
}