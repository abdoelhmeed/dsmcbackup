USE [master]
GO
/****** Object:  Database [erpHospitaldb]    Script Date: 04/03/2020 9:54:20 am ******/
CREATE DATABASE [erpHospitaldb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'erpHospitaldb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\erpHospitaldb.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'erpHospitaldb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\erpHospitaldb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [erpHospitaldb] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [erpHospitaldb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [erpHospitaldb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [erpHospitaldb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [erpHospitaldb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [erpHospitaldb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [erpHospitaldb] SET ARITHABORT OFF 
GO
ALTER DATABASE [erpHospitaldb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [erpHospitaldb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [erpHospitaldb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [erpHospitaldb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [erpHospitaldb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [erpHospitaldb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [erpHospitaldb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [erpHospitaldb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [erpHospitaldb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [erpHospitaldb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [erpHospitaldb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [erpHospitaldb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [erpHospitaldb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [erpHospitaldb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [erpHospitaldb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [erpHospitaldb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [erpHospitaldb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [erpHospitaldb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [erpHospitaldb] SET  MULTI_USER 
GO
ALTER DATABASE [erpHospitaldb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [erpHospitaldb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [erpHospitaldb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [erpHospitaldb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [erpHospitaldb] SET DELAYED_DURABILITY = DISABLED 
GO
USE [erpHospitaldb]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MaintenanceApproval]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaintenanceApproval](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[empId] [int] NULL,
	[node] [ntext] NULL,
	[MaintenanceApprovalid] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_MaintenanceApproval] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MaintenanceOrder]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaintenanceOrder](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[asstesId] [int] NULL,
	[empId] [int] NULL,
	[MOrderDate] [date] NULL,
	[OrderStuts] [nvarchar](50) NULL,
	[OrderNotes] [ntext] NULL,
	[Done] [bit] NULL,
	[OrderType] [nvarchar](50) NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_MaintenanceOrder] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_Agenda]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_Agenda](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[History] [date] NULL,
	[Subject] [ntext] NULL,
	[FromemployeeId] [int] NULL,
	[ToEmployeeId] [int] NULL,
	[Oid] [int] NULL,
	[isRead] [bit] NULL,
 CONSTRAINT [PK_td_Agenda] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_Assets]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_Assets](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AsOId] [int] NULL,
	[AsName] [nvarchar](50) NULL,
	[AsPrice] [float] NULL,
	[AnnualDepreciation] [float] NULL,
	[StopDeeperPrint] [float] NULL,
	[AsDescription] [ntext] NULL,
	[SubType] [int] NULL,
	[AsWarrantyPeriodDate] [date] NULL,
	[PurchaseDate] [date] NULL,
	[ASBill] [nvarchar](max) NULL,
	[AsWarrantyImage] [nvarchar](max) NULL,
	[AsInstructionManual] [nvarchar](max) NULL,
	[ASMaintenanceContract] [nvarchar](max) NULL,
	[Status] [nvarchar](50) NULL,
	[Availability] [bit] NULL,
	[AsUseresType] [int] NULL,
	[EnterDate] [date] NULL,
	[ASMaintenanceType] [int] NULL,
	[AsMaintenanceDuration] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_Assets] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_AsstesMainClassification]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_AsstesMainClassification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NameArabic] [nvarchar](50) NULL,
	[NameEnglish] [nvarchar](50) NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_AsstesMainClassification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_AsstesSubClassification]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_AsstesSubClassification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NameArbic] [nvarchar](50) NULL,
	[NameEnglish] [nvarchar](50) NULL,
	[MainClassificationId] [int] NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_AsstesSubClassification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_DepartmentAsstes]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_DepartmentAsstes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AsId] [int] NULL,
	[DeptReceivedDate] [date] NULL,
	[DeptId] [int] NULL,
	[DeptEntryDate] [date] NULL,
	[DeptMaintenanceDateReq] [date] NULL,
	[NotificationBefore] [int] NULL,
	[StatusConsumption] [nvarchar](50) NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_DepartmentAsstes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_empLicenses]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_empLicenses](
	[Lid] [int] IDENTITY(1,1) NOT NULL,
	[LName] [int] NOT NULL,
	[LPlace] [int] NOT NULL,
	[LStartDate] [date] NOT NULL,
	[LEndDate] [date] NOT NULL,
	[LValidity] [bit] NOT NULL,
	[Limg] [nvarchar](50) NULL,
	[empId] [int] NOT NULL,
	[Visible] [bit] NULL,
	[Notifications] [bit] NULL,
 CONSTRAINT [PK_td_empLicenses] PRIMARY KEY CLUSTERED 
(
	[Lid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_employee]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_employee](
	[empid] [int] IDENTITY(1,1) NOT NULL,
	[empFNameAr] [nvarchar](50) NULL,
	[empSNameAr] [nvarchar](50) NULL,
	[empTNameAr] [nvarchar](50) NULL,
	[empFNameEn] [nvarchar](50) NULL,
	[empSNameEn] [nvarchar](50) NULL,
	[empTNameEn] [nvarchar](50) NULL,
	[empImg] [nvarchar](50) NULL,
	[empFamilyNameEn] [nvarchar](50) NULL,
	[empFamilyNameAr] [nvarchar](50) NULL,
	[empNickName] [nvarchar](50) NULL,
	[empTitle] [int] NULL,
	[empGender] [int] NULL,
	[empNationality] [int] NULL,
	[empReligion] [int] NULL,
	[empBirthDate] [date] NULL,
	[empBirthblace] [nvarchar](50) NULL,
	[empBloodGroup] [nvarchar](50) NULL,
	[empMaritalStatus] [int] NULL,
	[empCareerLevel] [int] NULL,
	[empActivationStatus] [bit] NULL,
	[UserId] [nvarchar](128) NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_employee] PRIMARY KEY CLUSTERED 
(
	[empid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_EmployeeAsstes]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_EmployeeAsstes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AsId] [int] NULL,
	[AsEmpReceivedDate] [date] NULL,
	[AsEmpId] [int] NULL,
	[AsempEntryDate] [date] NULL,
	[AsempMaintenanceDateReq] [date] NULL,
	[StatusConsumption] [nvarchar](50) NULL,
	[NotificationBefore] [int] NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_EmployeeAsstes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_employeeDepartmentManagment]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_employeeDepartmentManagment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[empid] [int] NULL,
	[DepartmentId] [int] NULL,
	[ManagmentId] [int] NULL,
	[OId] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_employeeDepartmentManagment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_employeeIds]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_employeeIds](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idNumber] [nvarchar](50) NOT NULL,
	[idType] [int] NOT NULL,
	[idIssued] [date] NOT NULL,
	[idexpiredDate] [date] NOT NULL,
	[idPlaceIssue] [nvarchar](50) NOT NULL,
	[idImg] [nvarchar](max) NULL,
	[empId] [int] NOT NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_employeeIds] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_EmployeeRequests]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_EmployeeRequests](
	[requestId] [int] IDENTITY(1,1) NOT NULL,
	[empId] [int] NULL,
	[requestTypeId] [int] NULL,
	[requestDetails] [ntext] NULL,
	[Status] [ntext] NULL,
	[Oid] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_EmployeeRequests] PRIMARY KEY CLUSTERED 
(
	[requestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_EmployeeRequestsNodes]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_EmployeeRequestsNodes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[empId] [int] NULL,
	[requestsId] [int] NULL,
	[nodes] [ntext] NULL,
	[Status] [nvarchar](50) NULL,
	[Arrangement] [int] NULL,
	[Approve] [bit] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_EmployeeRequestsNodes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_EmployeeRequestsType]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_EmployeeRequestsType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[requestNameArabic] [nvarchar](50) NULL,
	[requestNameEnglish] [nvarchar](50) NULL,
	[DepartmentId] [int] NULL,
	[OId] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_EmployeeRequestsType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_empQualifications]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_empQualifications](
	[Qid] [int] IDENTITY(1,1) NOT NULL,
	[Qualifications] [int] NOT NULL,
	[QPlace] [int] NOT NULL,
	[QGrade] [int] NOT NULL,
	[QDate] [date] NOT NULL,
	[QImg] [nvarchar](max) NULL,
	[empId] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_empQualifications] PRIMARY KEY CLUSTERED 
(
	[Qid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncideContributingFactor]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncideContributingFactor](
	[CFId] [int] IDENTITY(1,1) NOT NULL,
	[CFNameAr] [nvarchar](50) NULL,
	[CFNameEn] [nvarchar](50) NULL,
	[OId] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_IncideContributingFactor] PRIMARY KEY CLUSTERED 
(
	[CFId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_Incident]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_Incident](
	[IncidentId] [int] IDENTITY(1,1) NOT NULL,
	[OccurrenceDate] [date] NULL,
	[DiscoverDate] [date] NULL,
	[ReportDate] [date] NULL,
	[Description] [ntext] NULL,
	[HideMyName] [bit] NULL,
	[SelfReporting] [bit] NULL,
	[Complete] [int] NULL,
	[status] [nvarchar](max) NULL,
	[CreateByEmpId] [int] NULL,
	[InciImages] [nvarchar](max) NULL,
	[CreatorName] [nvarchar](200) NULL,
	[isClosed] [bit] NULL,
	[LId] [int] NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_Incident] PRIMARY KEY CLUSTERED 
(
	[IncidentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncidentAffectedStaff]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncidentAffectedStaff](
	[AffectedId] [int] IDENTITY(1,1) NOT NULL,
	[empId] [int] NULL,
	[IncidentId] [int] NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_IncidentAffectedStaff] PRIMARY KEY CLUSTERED 
(
	[AffectedId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncidentAffectedVisitor]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncidentAffectedVisitor](
	[Vid] [int] IDENTITY(1,1) NOT NULL,
	[VName] [nvarchar](100) NULL,
	[VAddress] [nvarchar](200) NULL,
	[VPhone] [nvarchar](10) NULL,
	[IncidentId] [int] NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_IncidentAffectedVisitor] PRIMARY KEY CLUSTERED 
(
	[Vid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncidentAnalystToClose]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncidentAnalystToClose](
	[ARId] [int] IDENTITY(1,1) NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[Near] [bit] NULL,
	[Valid] [bit] NULL,
	[SentinelEvent] [bit] NULL,
	[Miss] [bit] NULL,
	[DOfUE_O] [bit] NULL,
	[OId] [int] NULL,
	[IncidentId] [int] NULL,
	[RPMId] [int] NULL,
	[RSSId] [int] NULL,
	[DHId] [int] NULL,
	[CreatDate] [date] NULL,
	[CreateByempId] [int] NULL,
 CONSTRAINT [PK_td_IncidentAnalystToClose] PRIMARY KEY CLUSTERED 
(
	[ARId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncidentAssign]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncidentAssign](
	[assId] [int] IDENTITY(1,1) NOT NULL,
	[assDeptResDept] [int] NULL,
	[IMTId] [int] NULL,
	[ISTId] [int] NULL,
	[IncidentDeptId] [int] NULL,
	[investigatorEmpId] [int] NULL,
	[Comment] [ntext] NULL,
	[IncidentId] [int] NULL,
	[CreateByEmpId] [int] NULL,
	[OId] [int] NULL,
	[Visible] [bit] NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [PK_td_IncidentAssign] PRIMARY KEY CLUSTERED 
(
	[assId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncidentDegreeOfHarm]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncidentDegreeOfHarm](
	[DHId] [int] IDENTITY(1,1) NOT NULL,
	[DHNameAr] [nvarchar](50) NULL,
	[DHNameEn] [nvarchar](50) NULL,
	[OId] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_IncidentDegreeOfHarm] PRIMARY KEY CLUSTERED 
(
	[DHId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncidentMainType]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncidentMainType](
	[IMTId] [int] IDENTITY(1,1) NOT NULL,
	[IMTNameAr] [nvarchar](100) NULL,
	[IMTNameEr] [nvarchar](100) NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_IncidentMainType] PRIMARY KEY CLUSTERED 
(
	[IMTId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncidentRiskProbabilityMain]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncidentRiskProbabilityMain](
	[RPMId] [int] IDENTITY(1,1) NOT NULL,
	[RPMNameAr] [nvarchar](50) NULL,
	[RPMINameEn] [nvarchar](50) NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_IncidentRiskProbabilityMain] PRIMARY KEY CLUSTERED 
(
	[RPMId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncidentRiskSeventySub]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncidentRiskSeventySub](
	[RSSId] [int] IDENTITY(1,1) NOT NULL,
	[RSSNameAr] [nvarchar](50) NULL,
	[RSSNameEn] [nvarchar](50) NULL,
	[RiskRating] [int] NULL,
	[RPMId] [int] NULL,
	[OId] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_IncidentRiskSeventySub] PRIMARY KEY CLUSTERED 
(
	[RSSId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_IncidentSubType]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_IncidentSubType](
	[ISTId] [int] IDENTITY(1,1) NOT NULL,
	[ISTNameAr] [nvarchar](100) NULL,
	[ISTNameEn] [nvarchar](100) NULL,
	[IMTId] [int] NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_IncidentSubType] PRIMARY KEY CLUSTERED 
(
	[ISTId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_Investigator]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_Investigator](
	[InvId] [int] IDENTITY(1,1) NOT NULL,
	[Actions] [ntext] NULL,
	[Recement] [ntext] NULL,
	[InvestigationDate] [date] NULL,
	[assId] [int] NULL,
	[IncidentId] [int] NULL,
	[CFId] [int] NULL,
	[Oid] [int] NULL,
 CONSTRAINT [PK_td_Investigator] PRIMARY KEY CLUSTERED 
(
	[InvId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_LocationOfIncident]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_LocationOfIncident](
	[LId] [int] IDENTITY(1,1) NOT NULL,
	[LNAr] [nvarchar](100) NULL,
	[LNEn] [nvarchar](100) NULL,
	[Visible] [bit] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_td_LocationOfIncident] PRIMARY KEY CLUSTERED 
(
	[LId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_MaintenanceAspiratBill]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_MaintenanceAspiratBill](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AssetsId] [int] NULL,
	[MaintenanceHandlingId] [int] NULL,
	[MaintenanceDetails] [ntext] NULL,
	[AspiratDetails] [ntext] NULL,
	[TotalPrice] [float] NULL,
	[MAspiratDate] [datetime] NULL,
	[MStuts] [nvarchar](max) NULL,
	[InvoiceImage] [nvarchar](max) NULL,
	[GeneralDirector] [bit] NULL,
	[Nots] [ntext] NULL,
	[Financial] [bit] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_MaintenanceAspiratBill] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_MaintenanceHandling]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_MaintenanceHandling](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[OrderMaintenanceIId] [int] NULL,
	[asstesId] [int] NULL,
	[MaintenanceDetails] [ntext] NULL,
	[AspiratDetails] [ntext] NULL,
	[MStuts] [nvarchar](max) NULL,
	[MaintenanceStartDate] [date] NULL,
	[MaintenanceEndDate] [date] NULL,
	[Finished] [bit] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_MaintenanceHandling] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_ManagementAssets]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_ManagementAssets](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AsId] [int] NULL,
	[ManagId] [int] NULL,
	[ManaReceivedDate] [date] NULL,
	[ManagEntryDate] [date] NULL,
	[ManageMaintenanceDateReq] [date] NULL,
	[StatusConsumption] [nvarchar](50) NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_td_MaintenanceAssets] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_Notifications]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_Notifications](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nTitel] [nvarchar](50) NULL,
	[nBody] [ntext] NULL,
	[nUrl] [nvarchar](max) NULL,
	[isRead] [bit] NULL,
	[ReadDate] [datetime] NULL,
	[UserId] [nvarchar](128) NULL,
	[NType] [nvarchar](50) NULL,
 CONSTRAINT [PK_td_Notifications] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_NotificationsSetting]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_NotificationsSetting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[deptId] [int] NOT NULL,
	[empId] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[OId] [int] NOT NULL,
 CONSTRAINT [PK_td_NotificationsSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_Policies]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_Policies](
	[policiesNumbers] [nvarchar](50) NOT NULL,
	[TId] [int] NULL,
	[polTitle] [nvarchar](100) NULL,
	[PolBody] [ntext] NULL,
	[PolPdf] [nvarchar](max) NULL,
	[PolEffectiveDate] [date] NULL,
	[OId] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[EnterDate] [date] NULL,
	[CreateByEmpId] [int] NULL,
	[Visible] [bit] NULL,
	[isPublish] [bit] NULL,
 CONSTRAINT [PK_td_Policies] PRIMARY KEY CLUSTERED 
(
	[policiesNumbers] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_PoliciesAppliesTo]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_PoliciesAppliesTo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[DeptId] [int] NULL,
	[policiesNumbers] [nvarchar](50) NULL,
 CONSTRAINT [PK_td_PoliciesAppliesTo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_PoliciesApprovedBy]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_PoliciesApprovedBy](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[policiesNumbers] [nvarchar](50) NULL,
	[empId] [int] NULL,
	[Approv] [bit] NULL,
	[empgroup] [int] NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_td_PoliciesApprovedBy] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_PoliciesRead]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_PoliciesRead](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[policiesNumbers] [nvarchar](50) NULL,
	[empId] [int] NULL,
	[isRead] [bit] NULL,
	[RaedDate] [date] NULL,
 CONSTRAINT [PK_td_PoliciesRead] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_PoliciesType]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_PoliciesType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](50) NULL,
	[NameEn] [nvarchar](50) NULL,
	[description] [ntext] NULL,
	[MId] [int] NULL,
	[TImg] [nvarchar](max) NULL,
	[EnterDate] [date] NULL,
	[Oid] [int] NULL,
 CONSTRAINT [PK_PoliciesType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_PolicyPlan]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_PolicyPlan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TId] [int] NULL,
	[Mid] [int] NULL,
	[Arrangement] [int] NULL,
 CONSTRAINT [PK_PolicyPlan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_selectionDetails]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_selectionDetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[selNameArabic] [nvarchar](50) NOT NULL,
	[selNameEnglish] [nvarchar](50) NOT NULL,
	[sTId] [int] NOT NULL,
 CONSTRAINT [PK_td_selectionDetails] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_selectionType]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_selectionType](
	[id] [int] NOT NULL,
	[ArabicName] [nvarchar](50) NULL,
	[EnglishName] [nvarchar](50) NULL,
 CONSTRAINT [PK_td_selectionType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[td_UpdateNotification]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[td_UpdateNotification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UpdatesNotification] [date] NOT NULL,
 CONSTRAINT [PK_td_UpdateNotification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tdMana_OrganizationLicenses]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tdMana_OrganizationLicenses](
	[OLid] [int] IDENTITY(1,1) NOT NULL,
	[OLName] [int] NOT NULL,
	[OLPlace] [int] NOT NULL,
	[OLStartDate] [date] NOT NULL,
	[OLEndDate] [date] NOT NULL,
	[OLEnterDate] [date] NOT NULL,
	[OLValidity] [bit] NOT NULL,
	[OLimg] [nvarchar](50) NULL,
	[OId] [int] NOT NULL,
	[Visible] [bit] NULL,
	[Notifications] [bit] NULL,
 CONSTRAINT [PK_tdMana_OrganizationLicenses] PRIMARY KEY CLUSTERED 
(
	[OLid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tdManag_Department]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tdManag_Department](
	[deptId] [int] IDENTITY(1,1) NOT NULL,
	[deptNameAr] [nvarchar](50) NULL,
	[deptNameEn] [nvarchar](50) NULL,
	[deptManager] [int] NULL,
	[deptVicManager] [int] NULL,
	[Mid] [int] NULL,
 CONSTRAINT [PK_tdManag_Department] PRIMARY KEY CLUSTERED 
(
	[deptId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tdManag_Management]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tdManag_Management](
	[Mid] [int] IDENTITY(1,1) NOT NULL,
	[MNameAr] [nvarchar](50) NULL,
	[MName] [nvarchar](50) NULL,
	[deptManager] [int] NULL,
	[deptVicManager] [int] NULL,
	[OId] [int] NULL,
 CONSTRAINT [PK_tdManag_Management] PRIMARY KEY CLUSTERED 
(
	[Mid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tdMang_Organization]    Script Date: 04/03/2020 9:54:20 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tdMang_Organization](
	[OId] [int] IDENTITY(1,1) NOT NULL,
	[ONameEn] [nvarchar](50) NULL,
	[ONameAr] [nvarchar](50) NULL,
	[OAddress] [nvarchar](150) NULL,
	[OPhoneNumbers] [nvarchar](50) NULL,
	[OSupportNumbers] [nvarchar](50) NULL,
	[OEmail] [nvarchar](50) NULL,
	[OManager] [int] NULL,
	[OVicManager] [int] NULL,
 CONSTRAINT [PK_tdMang_Organization] PRIMARY KEY CLUSTERED 
(
	[OId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 04/03/2020 9:54:20 am ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 04/03/2020 9:54:20 am ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 04/03/2020 9:54:20 am ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 04/03/2020 9:54:20 am ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 04/03/2020 9:54:20 am ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 04/03/2020 9:54:20 am ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[MaintenanceOrder]  WITH CHECK ADD  CONSTRAINT [FK_MaintenanceOrder_td_Assets] FOREIGN KEY([asstesId])
REFERENCES [dbo].[td_Assets] ([id])
GO
ALTER TABLE [dbo].[MaintenanceOrder] CHECK CONSTRAINT [FK_MaintenanceOrder_td_Assets]
GO
ALTER TABLE [dbo].[MaintenanceOrder]  WITH CHECK ADD  CONSTRAINT [FK_MaintenanceOrder_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[MaintenanceOrder] CHECK CONSTRAINT [FK_MaintenanceOrder_td_employee]
GO
ALTER TABLE [dbo].[MaintenanceOrder]  WITH CHECK ADD  CONSTRAINT [FK_MaintenanceOrder_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[MaintenanceOrder] CHECK CONSTRAINT [FK_MaintenanceOrder_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_Agenda]  WITH CHECK ADD  CONSTRAINT [Fromemployee] FOREIGN KEY([FromemployeeId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_Agenda] CHECK CONSTRAINT [Fromemployee]
GO
ALTER TABLE [dbo].[td_Agenda]  WITH CHECK ADD  CONSTRAINT [OidAgenda] FOREIGN KEY([Oid])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_Agenda] CHECK CONSTRAINT [OidAgenda]
GO
ALTER TABLE [dbo].[td_Agenda]  WITH CHECK ADD  CONSTRAINT [ToEmployee] FOREIGN KEY([ToEmployeeId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_Agenda] CHECK CONSTRAINT [ToEmployee]
GO
ALTER TABLE [dbo].[td_Assets]  WITH CHECK ADD  CONSTRAINT [FK_td_Assets_td_AsstesSubClassification] FOREIGN KEY([SubType])
REFERENCES [dbo].[td_AsstesSubClassification] ([id])
GO
ALTER TABLE [dbo].[td_Assets] CHECK CONSTRAINT [FK_td_Assets_td_AsstesSubClassification]
GO
ALTER TABLE [dbo].[td_Assets]  WITH CHECK ADD  CONSTRAINT [FK_td_Assets_tdMang_Organization] FOREIGN KEY([AsOId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_Assets] CHECK CONSTRAINT [FK_td_Assets_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_AsstesMainClassification]  WITH CHECK ADD  CONSTRAINT [FK_td_AsstesMainClassification_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_AsstesMainClassification] CHECK CONSTRAINT [FK_td_AsstesMainClassification_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_AsstesSubClassification]  WITH CHECK ADD  CONSTRAINT [FK_td_AsstesSubClassification_td_AsstesMainClassification] FOREIGN KEY([MainClassificationId])
REFERENCES [dbo].[td_AsstesMainClassification] ([id])
GO
ALTER TABLE [dbo].[td_AsstesSubClassification] CHECK CONSTRAINT [FK_td_AsstesSubClassification_td_AsstesMainClassification]
GO
ALTER TABLE [dbo].[td_AsstesSubClassification]  WITH CHECK ADD  CONSTRAINT [FK_td_AsstesSubClassification_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_AsstesSubClassification] CHECK CONSTRAINT [FK_td_AsstesSubClassification_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_DepartmentAsstes]  WITH CHECK ADD  CONSTRAINT [FK_td_DepartmentAsstes_td_Assets] FOREIGN KEY([AsId])
REFERENCES [dbo].[td_Assets] ([id])
GO
ALTER TABLE [dbo].[td_DepartmentAsstes] CHECK CONSTRAINT [FK_td_DepartmentAsstes_td_Assets]
GO
ALTER TABLE [dbo].[td_DepartmentAsstes]  WITH CHECK ADD  CONSTRAINT [FK_td_DepartmentAsstes_tdManag_Department] FOREIGN KEY([DeptId])
REFERENCES [dbo].[tdManag_Department] ([deptId])
GO
ALTER TABLE [dbo].[td_DepartmentAsstes] CHECK CONSTRAINT [FK_td_DepartmentAsstes_tdManag_Department]
GO
ALTER TABLE [dbo].[td_empLicenses]  WITH CHECK ADD  CONSTRAINT [FK_LName] FOREIGN KEY([LName])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_empLicenses] CHECK CONSTRAINT [FK_LName]
GO
ALTER TABLE [dbo].[td_empLicenses]  WITH CHECK ADD  CONSTRAINT [FK_td_empLicenses_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_empLicenses] CHECK CONSTRAINT [FK_td_empLicenses_td_employee]
GO
ALTER TABLE [dbo].[td_empLicenses]  WITH CHECK ADD  CONSTRAINT [LPlace] FOREIGN KEY([LPlace])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_empLicenses] CHECK CONSTRAINT [LPlace]
GO
ALTER TABLE [dbo].[td_employee]  WITH CHECK ADD  CONSTRAINT [FK_CareerLevel] FOREIGN KEY([empCareerLevel])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_employee] CHECK CONSTRAINT [FK_CareerLevel]
GO
ALTER TABLE [dbo].[td_employee]  WITH CHECK ADD  CONSTRAINT [FK_Gender] FOREIGN KEY([empGender])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_employee] CHECK CONSTRAINT [FK_Gender]
GO
ALTER TABLE [dbo].[td_employee]  WITH CHECK ADD  CONSTRAINT [FK_Nationality] FOREIGN KEY([empNationality])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_employee] CHECK CONSTRAINT [FK_Nationality]
GO
ALTER TABLE [dbo].[td_employee]  WITH CHECK ADD  CONSTRAINT [FK_Religion] FOREIGN KEY([empReligion])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_employee] CHECK CONSTRAINT [FK_Religion]
GO
ALTER TABLE [dbo].[td_employee]  WITH CHECK ADD  CONSTRAINT [FK_td_employee_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[td_employee] CHECK CONSTRAINT [FK_td_employee_AspNetUsers]
GO
ALTER TABLE [dbo].[td_employee]  WITH CHECK ADD  CONSTRAINT [FK_Title] FOREIGN KEY([empTitle])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_employee] CHECK CONSTRAINT [FK_Title]
GO
ALTER TABLE [dbo].[td_employee]  WITH CHECK ADD  CONSTRAINT [FKMaritalStauts] FOREIGN KEY([empMaritalStatus])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_employee] CHECK CONSTRAINT [FKMaritalStauts]
GO
ALTER TABLE [dbo].[td_EmployeeAsstes]  WITH CHECK ADD  CONSTRAINT [FK_td_EmployeeAsstes_td_Assets] FOREIGN KEY([AsId])
REFERENCES [dbo].[td_Assets] ([id])
GO
ALTER TABLE [dbo].[td_EmployeeAsstes] CHECK CONSTRAINT [FK_td_EmployeeAsstes_td_Assets]
GO
ALTER TABLE [dbo].[td_EmployeeAsstes]  WITH CHECK ADD  CONSTRAINT [FK_td_EmployeeAsstes_td_employee] FOREIGN KEY([AsEmpId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_EmployeeAsstes] CHECK CONSTRAINT [FK_td_EmployeeAsstes_td_employee]
GO
ALTER TABLE [dbo].[td_employeeDepartmentManagment]  WITH CHECK ADD  CONSTRAINT [FK_td_employeeDepartmentManagment_td_employee] FOREIGN KEY([empid])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_employeeDepartmentManagment] CHECK CONSTRAINT [FK_td_employeeDepartmentManagment_td_employee]
GO
ALTER TABLE [dbo].[td_employeeDepartmentManagment]  WITH CHECK ADD  CONSTRAINT [FK_td_employeeDepartmentManagment_tdManag_Department] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[tdManag_Department] ([deptId])
GO
ALTER TABLE [dbo].[td_employeeDepartmentManagment] CHECK CONSTRAINT [FK_td_employeeDepartmentManagment_tdManag_Department]
GO
ALTER TABLE [dbo].[td_employeeDepartmentManagment]  WITH CHECK ADD  CONSTRAINT [FK_td_employeeDepartmentManagment_tdManag_Management] FOREIGN KEY([ManagmentId])
REFERENCES [dbo].[tdManag_Management] ([Mid])
GO
ALTER TABLE [dbo].[td_employeeDepartmentManagment] CHECK CONSTRAINT [FK_td_employeeDepartmentManagment_tdManag_Management]
GO
ALTER TABLE [dbo].[td_employeeDepartmentManagment]  WITH CHECK ADD  CONSTRAINT [FK_td_employeeDepartmentManagment_tdMang_Organization1] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_employeeDepartmentManagment] CHECK CONSTRAINT [FK_td_employeeDepartmentManagment_tdMang_Organization1]
GO
ALTER TABLE [dbo].[td_employeeIds]  WITH CHECK ADD  CONSTRAINT [FK_IdType] FOREIGN KEY([idType])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_employeeIds] CHECK CONSTRAINT [FK_IdType]
GO
ALTER TABLE [dbo].[td_employeeIds]  WITH CHECK ADD  CONSTRAINT [FK_td_employeeIds_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_employeeIds] CHECK CONSTRAINT [FK_td_employeeIds_td_employee]
GO
ALTER TABLE [dbo].[td_EmployeeRequests]  WITH CHECK ADD  CONSTRAINT [FK_td_EmployeeRequests_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_EmployeeRequests] CHECK CONSTRAINT [FK_td_EmployeeRequests_td_employee]
GO
ALTER TABLE [dbo].[td_EmployeeRequests]  WITH CHECK ADD  CONSTRAINT [FK_td_EmployeeRequests_td_EmployeeRequestsType] FOREIGN KEY([requestTypeId])
REFERENCES [dbo].[td_EmployeeRequestsType] ([id])
GO
ALTER TABLE [dbo].[td_EmployeeRequests] CHECK CONSTRAINT [FK_td_EmployeeRequests_td_EmployeeRequestsType]
GO
ALTER TABLE [dbo].[td_EmployeeRequests]  WITH CHECK ADD  CONSTRAINT [FK_td_EmployeeRequests_tdMang_Organization] FOREIGN KEY([Oid])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_EmployeeRequests] CHECK CONSTRAINT [FK_td_EmployeeRequests_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_EmployeeRequestsNodes]  WITH CHECK ADD  CONSTRAINT [FK_td_EmployeeRequestsNodes_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_EmployeeRequestsNodes] CHECK CONSTRAINT [FK_td_EmployeeRequestsNodes_td_employee]
GO
ALTER TABLE [dbo].[td_EmployeeRequestsNodes]  WITH CHECK ADD  CONSTRAINT [FK_td_EmployeeRequestsNodes_td_EmployeeRequests] FOREIGN KEY([requestsId])
REFERENCES [dbo].[td_EmployeeRequests] ([requestId])
GO
ALTER TABLE [dbo].[td_EmployeeRequestsNodes] CHECK CONSTRAINT [FK_td_EmployeeRequestsNodes_td_EmployeeRequests]
GO
ALTER TABLE [dbo].[td_EmployeeRequestsType]  WITH CHECK ADD  CONSTRAINT [FK_td_EmployeeRequestsType_tdManag_Department] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[tdManag_Department] ([deptId])
GO
ALTER TABLE [dbo].[td_EmployeeRequestsType] CHECK CONSTRAINT [FK_td_EmployeeRequestsType_tdManag_Department]
GO
ALTER TABLE [dbo].[td_EmployeeRequestsType]  WITH CHECK ADD  CONSTRAINT [FK_td_EmployeeRequestsType_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_EmployeeRequestsType] CHECK CONSTRAINT [FK_td_EmployeeRequestsType_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_empQualifications]  WITH CHECK ADD  CONSTRAINT [FK_Qualifications] FOREIGN KEY([Qualifications])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_empQualifications] CHECK CONSTRAINT [FK_Qualifications]
GO
ALTER TABLE [dbo].[td_empQualifications]  WITH CHECK ADD  CONSTRAINT [FK_QualificationsPlace] FOREIGN KEY([QPlace])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[td_empQualifications] CHECK CONSTRAINT [FK_QualificationsPlace]
GO
ALTER TABLE [dbo].[td_empQualifications]  WITH CHECK ADD  CONSTRAINT [FK_td_empQualifications_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_empQualifications] CHECK CONSTRAINT [FK_td_empQualifications_td_employee]
GO
ALTER TABLE [dbo].[td_IncideContributingFactor]  WITH CHECK ADD  CONSTRAINT [FK_td_IncideContributingFactor_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncideContributingFactor] CHECK CONSTRAINT [FK_td_IncideContributingFactor_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_Incident]  WITH CHECK ADD  CONSTRAINT [FK_td_Incident_td_employee] FOREIGN KEY([CreateByEmpId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_Incident] CHECK CONSTRAINT [FK_td_Incident_td_employee]
GO
ALTER TABLE [dbo].[td_Incident]  WITH CHECK ADD  CONSTRAINT [FK_td_Incident_td_LocationOfIncident] FOREIGN KEY([LId])
REFERENCES [dbo].[td_LocationOfIncident] ([LId])
GO
ALTER TABLE [dbo].[td_Incident] CHECK CONSTRAINT [FK_td_Incident_td_LocationOfIncident]
GO
ALTER TABLE [dbo].[td_Incident]  WITH CHECK ADD  CONSTRAINT [FK_td_Incident_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_Incident] CHECK CONSTRAINT [FK_td_Incident_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_IncidentAffectedStaff]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAffectedStaff_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_IncidentAffectedStaff] CHECK CONSTRAINT [FK_td_IncidentAffectedStaff_td_employee]
GO
ALTER TABLE [dbo].[td_IncidentAffectedStaff]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAffectedStaff_td_Incident] FOREIGN KEY([IncidentId])
REFERENCES [dbo].[td_Incident] ([IncidentId])
GO
ALTER TABLE [dbo].[td_IncidentAffectedStaff] CHECK CONSTRAINT [FK_td_IncidentAffectedStaff_td_Incident]
GO
ALTER TABLE [dbo].[td_IncidentAffectedStaff]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAffectedStaff_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncidentAffectedStaff] CHECK CONSTRAINT [FK_td_IncidentAffectedStaff_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_IncidentAffectedVisitor]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAffectedVisitor_td_Incident] FOREIGN KEY([IncidentId])
REFERENCES [dbo].[td_Incident] ([IncidentId])
GO
ALTER TABLE [dbo].[td_IncidentAffectedVisitor] CHECK CONSTRAINT [FK_td_IncidentAffectedVisitor_td_Incident]
GO
ALTER TABLE [dbo].[td_IncidentAffectedVisitor]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAffectedVisitor_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncidentAffectedVisitor] CHECK CONSTRAINT [FK_td_IncidentAffectedVisitor_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAnalystToClose_td_employee] FOREIGN KEY([CreateByempId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose] CHECK CONSTRAINT [FK_td_IncidentAnalystToClose_td_employee]
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAnalystToClose_td_Incident] FOREIGN KEY([IncidentId])
REFERENCES [dbo].[td_Incident] ([IncidentId])
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose] CHECK CONSTRAINT [FK_td_IncidentAnalystToClose_td_Incident]
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAnalystToClose_td_IncidentDegreeOfHarm] FOREIGN KEY([DHId])
REFERENCES [dbo].[td_IncidentDegreeOfHarm] ([DHId])
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose] CHECK CONSTRAINT [FK_td_IncidentAnalystToClose_td_IncidentDegreeOfHarm]
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAnalystToClose_td_IncidentRiskProbabilityMain] FOREIGN KEY([RPMId])
REFERENCES [dbo].[td_IncidentRiskProbabilityMain] ([RPMId])
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose] CHECK CONSTRAINT [FK_td_IncidentAnalystToClose_td_IncidentRiskProbabilityMain]
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAnalystToClose_td_IncidentRiskSeventySub] FOREIGN KEY([RSSId])
REFERENCES [dbo].[td_IncidentRiskSeventySub] ([RSSId])
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose] CHECK CONSTRAINT [FK_td_IncidentAnalystToClose_td_IncidentRiskSeventySub]
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAnalystToClose_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncidentAnalystToClose] CHECK CONSTRAINT [FK_td_IncidentAnalystToClose_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_IncidentAssign]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAssign_td_employee] FOREIGN KEY([investigatorEmpId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_IncidentAssign] CHECK CONSTRAINT [FK_td_IncidentAssign_td_employee]
GO
ALTER TABLE [dbo].[td_IncidentAssign]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAssign_td_employee1] FOREIGN KEY([CreateByEmpId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_IncidentAssign] CHECK CONSTRAINT [FK_td_IncidentAssign_td_employee1]
GO
ALTER TABLE [dbo].[td_IncidentAssign]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAssign_td_Incident] FOREIGN KEY([IncidentId])
REFERENCES [dbo].[td_Incident] ([IncidentId])
GO
ALTER TABLE [dbo].[td_IncidentAssign] CHECK CONSTRAINT [FK_td_IncidentAssign_td_Incident]
GO
ALTER TABLE [dbo].[td_IncidentAssign]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAssign_td_IncidentMainType] FOREIGN KEY([IMTId])
REFERENCES [dbo].[td_IncidentMainType] ([IMTId])
GO
ALTER TABLE [dbo].[td_IncidentAssign] CHECK CONSTRAINT [FK_td_IncidentAssign_td_IncidentMainType]
GO
ALTER TABLE [dbo].[td_IncidentAssign]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAssign_td_IncidentSubType] FOREIGN KEY([ISTId])
REFERENCES [dbo].[td_IncidentSubType] ([ISTId])
GO
ALTER TABLE [dbo].[td_IncidentAssign] CHECK CONSTRAINT [FK_td_IncidentAssign_td_IncidentSubType]
GO
ALTER TABLE [dbo].[td_IncidentAssign]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAssign_tdManag_Department] FOREIGN KEY([assDeptResDept])
REFERENCES [dbo].[tdManag_Department] ([deptId])
GO
ALTER TABLE [dbo].[td_IncidentAssign] CHECK CONSTRAINT [FK_td_IncidentAssign_tdManag_Department]
GO
ALTER TABLE [dbo].[td_IncidentAssign]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAssign_tdManag_Department1] FOREIGN KEY([IncidentDeptId])
REFERENCES [dbo].[tdManag_Department] ([deptId])
GO
ALTER TABLE [dbo].[td_IncidentAssign] CHECK CONSTRAINT [FK_td_IncidentAssign_tdManag_Department1]
GO
ALTER TABLE [dbo].[td_IncidentAssign]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentAssign_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncidentAssign] CHECK CONSTRAINT [FK_td_IncidentAssign_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_IncidentDegreeOfHarm]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentDegreeOfHarm_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncidentDegreeOfHarm] CHECK CONSTRAINT [FK_td_IncidentDegreeOfHarm_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_IncidentMainType]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentMainType_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncidentMainType] CHECK CONSTRAINT [FK_td_IncidentMainType_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_IncidentRiskProbabilityMain]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentRiskProbabilityMain_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncidentRiskProbabilityMain] CHECK CONSTRAINT [FK_td_IncidentRiskProbabilityMain_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_IncidentRiskSeventySub]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentRiskSeventySub_td_IncidentRiskProbabilityMain] FOREIGN KEY([RPMId])
REFERENCES [dbo].[td_IncidentRiskProbabilityMain] ([RPMId])
GO
ALTER TABLE [dbo].[td_IncidentRiskSeventySub] CHECK CONSTRAINT [FK_td_IncidentRiskSeventySub_td_IncidentRiskProbabilityMain]
GO
ALTER TABLE [dbo].[td_IncidentRiskSeventySub]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentRiskSeventySub_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncidentRiskSeventySub] CHECK CONSTRAINT [FK_td_IncidentRiskSeventySub_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_IncidentSubType]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentSubType_td_IncidentMainType] FOREIGN KEY([IMTId])
REFERENCES [dbo].[td_IncidentMainType] ([IMTId])
GO
ALTER TABLE [dbo].[td_IncidentSubType] CHECK CONSTRAINT [FK_td_IncidentSubType_td_IncidentMainType]
GO
ALTER TABLE [dbo].[td_IncidentSubType]  WITH CHECK ADD  CONSTRAINT [FK_td_IncidentSubType_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_IncidentSubType] CHECK CONSTRAINT [FK_td_IncidentSubType_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_Investigator]  WITH CHECK ADD  CONSTRAINT [FK_td_Investigator_td_IncideContributingFactor] FOREIGN KEY([CFId])
REFERENCES [dbo].[td_IncideContributingFactor] ([CFId])
GO
ALTER TABLE [dbo].[td_Investigator] CHECK CONSTRAINT [FK_td_Investigator_td_IncideContributingFactor]
GO
ALTER TABLE [dbo].[td_Investigator]  WITH CHECK ADD  CONSTRAINT [FK_td_Investigator_td_Incident] FOREIGN KEY([IncidentId])
REFERENCES [dbo].[td_Incident] ([IncidentId])
GO
ALTER TABLE [dbo].[td_Investigator] CHECK CONSTRAINT [FK_td_Investigator_td_Incident]
GO
ALTER TABLE [dbo].[td_Investigator]  WITH CHECK ADD  CONSTRAINT [FK_td_Investigator_td_IncidentAssign] FOREIGN KEY([assId])
REFERENCES [dbo].[td_IncidentAssign] ([assId])
GO
ALTER TABLE [dbo].[td_Investigator] CHECK CONSTRAINT [FK_td_Investigator_td_IncidentAssign]
GO
ALTER TABLE [dbo].[td_Investigator]  WITH CHECK ADD  CONSTRAINT [FK_td_Investigator_tdMang_Organization] FOREIGN KEY([Oid])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_Investigator] CHECK CONSTRAINT [FK_td_Investigator_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_LocationOfIncident]  WITH CHECK ADD  CONSTRAINT [FK_td_LocationOfIncident_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_LocationOfIncident] CHECK CONSTRAINT [FK_td_LocationOfIncident_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_MaintenanceAspiratBill]  WITH CHECK ADD  CONSTRAINT [FK_td_MaintenanceAspiratBill_td_Assets] FOREIGN KEY([AssetsId])
REFERENCES [dbo].[td_Assets] ([id])
GO
ALTER TABLE [dbo].[td_MaintenanceAspiratBill] CHECK CONSTRAINT [FK_td_MaintenanceAspiratBill_td_Assets]
GO
ALTER TABLE [dbo].[td_MaintenanceAspiratBill]  WITH CHECK ADD  CONSTRAINT [FK_td_MaintenanceAspiratBill_td_MaintenanceHandling] FOREIGN KEY([MaintenanceHandlingId])
REFERENCES [dbo].[td_MaintenanceHandling] ([id])
GO
ALTER TABLE [dbo].[td_MaintenanceAspiratBill] CHECK CONSTRAINT [FK_td_MaintenanceAspiratBill_td_MaintenanceHandling]
GO
ALTER TABLE [dbo].[td_MaintenanceHandling]  WITH CHECK ADD  CONSTRAINT [FK_td_MaintenanceHandling_MaintenanceOrder] FOREIGN KEY([OrderMaintenanceIId])
REFERENCES [dbo].[MaintenanceOrder] ([id])
GO
ALTER TABLE [dbo].[td_MaintenanceHandling] CHECK CONSTRAINT [FK_td_MaintenanceHandling_MaintenanceOrder]
GO
ALTER TABLE [dbo].[td_MaintenanceHandling]  WITH CHECK ADD  CONSTRAINT [FK_td_MaintenanceHandling_td_Assets] FOREIGN KEY([asstesId])
REFERENCES [dbo].[td_Assets] ([id])
GO
ALTER TABLE [dbo].[td_MaintenanceHandling] CHECK CONSTRAINT [FK_td_MaintenanceHandling_td_Assets]
GO
ALTER TABLE [dbo].[td_Notifications]  WITH CHECK ADD  CONSTRAINT [FK_td_Notifications_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[td_Notifications] CHECK CONSTRAINT [FK_td_Notifications_AspNetUsers]
GO
ALTER TABLE [dbo].[td_NotificationsSetting]  WITH CHECK ADD  CONSTRAINT [FK_td_NotificationsSetting_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_NotificationsSetting] CHECK CONSTRAINT [FK_td_NotificationsSetting_td_employee]
GO
ALTER TABLE [dbo].[td_NotificationsSetting]  WITH CHECK ADD  CONSTRAINT [FK_td_NotificationsSetting_tdManag_Department] FOREIGN KEY([deptId])
REFERENCES [dbo].[tdManag_Department] ([deptId])
GO
ALTER TABLE [dbo].[td_NotificationsSetting] CHECK CONSTRAINT [FK_td_NotificationsSetting_tdManag_Department]
GO
ALTER TABLE [dbo].[td_NotificationsSetting]  WITH CHECK ADD  CONSTRAINT [FK_td_NotificationsSetting_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_NotificationsSetting] CHECK CONSTRAINT [FK_td_NotificationsSetting_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_Policies]  WITH CHECK ADD  CONSTRAINT [FK_td_Policies_td_employee] FOREIGN KEY([CreateByEmpId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_Policies] CHECK CONSTRAINT [FK_td_Policies_td_employee]
GO
ALTER TABLE [dbo].[td_Policies]  WITH CHECK ADD  CONSTRAINT [FK_td_Policies_td_PoliciesType] FOREIGN KEY([TId])
REFERENCES [dbo].[td_PoliciesType] ([id])
GO
ALTER TABLE [dbo].[td_Policies] CHECK CONSTRAINT [FK_td_Policies_td_PoliciesType]
GO
ALTER TABLE [dbo].[td_Policies]  WITH CHECK ADD  CONSTRAINT [FK_td_Policies_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_Policies] CHECK CONSTRAINT [FK_td_Policies_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_PoliciesAppliesTo]  WITH CHECK ADD  CONSTRAINT [FK_td_PoliciesAppliesTo_td_Policies] FOREIGN KEY([policiesNumbers])
REFERENCES [dbo].[td_Policies] ([policiesNumbers])
GO
ALTER TABLE [dbo].[td_PoliciesAppliesTo] CHECK CONSTRAINT [FK_td_PoliciesAppliesTo_td_Policies]
GO
ALTER TABLE [dbo].[td_PoliciesAppliesTo]  WITH CHECK ADD  CONSTRAINT [FK_td_PoliciesAppliesTo_tdManag_Department] FOREIGN KEY([DeptId])
REFERENCES [dbo].[tdManag_Department] ([deptId])
GO
ALTER TABLE [dbo].[td_PoliciesAppliesTo] CHECK CONSTRAINT [FK_td_PoliciesAppliesTo_tdManag_Department]
GO
ALTER TABLE [dbo].[td_PoliciesApprovedBy]  WITH CHECK ADD  CONSTRAINT [FK_td_PoliciesApprovedBy_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_PoliciesApprovedBy] CHECK CONSTRAINT [FK_td_PoliciesApprovedBy_td_employee]
GO
ALTER TABLE [dbo].[td_PoliciesApprovedBy]  WITH CHECK ADD  CONSTRAINT [FK_td_PoliciesApprovedBy_td_Policies] FOREIGN KEY([policiesNumbers])
REFERENCES [dbo].[td_Policies] ([policiesNumbers])
GO
ALTER TABLE [dbo].[td_PoliciesApprovedBy] CHECK CONSTRAINT [FK_td_PoliciesApprovedBy_td_Policies]
GO
ALTER TABLE [dbo].[td_PoliciesRead]  WITH CHECK ADD  CONSTRAINT [FK_td_PoliciesRead_td_employee] FOREIGN KEY([empId])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[td_PoliciesRead] CHECK CONSTRAINT [FK_td_PoliciesRead_td_employee]
GO
ALTER TABLE [dbo].[td_PoliciesRead]  WITH CHECK ADD  CONSTRAINT [FK_td_PoliciesRead_td_Policies] FOREIGN KEY([policiesNumbers])
REFERENCES [dbo].[td_Policies] ([policiesNumbers])
GO
ALTER TABLE [dbo].[td_PoliciesRead] CHECK CONSTRAINT [FK_td_PoliciesRead_td_Policies]
GO
ALTER TABLE [dbo].[td_PoliciesType]  WITH CHECK ADD  CONSTRAINT [FK_PoliciesType_tdMang_Organization] FOREIGN KEY([Oid])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[td_PoliciesType] CHECK CONSTRAINT [FK_PoliciesType_tdMang_Organization]
GO
ALTER TABLE [dbo].[td_PoliciesType]  WITH CHECK ADD  CONSTRAINT [FK_td_PoliciesType_tdManag_Management] FOREIGN KEY([MId])
REFERENCES [dbo].[tdManag_Management] ([Mid])
GO
ALTER TABLE [dbo].[td_PoliciesType] CHECK CONSTRAINT [FK_td_PoliciesType_tdManag_Management]
GO
ALTER TABLE [dbo].[td_PolicyPlan]  WITH CHECK ADD  CONSTRAINT [FK_PolicyPlan_PoliciesType] FOREIGN KEY([TId])
REFERENCES [dbo].[td_PoliciesType] ([id])
GO
ALTER TABLE [dbo].[td_PolicyPlan] CHECK CONSTRAINT [FK_PolicyPlan_PoliciesType]
GO
ALTER TABLE [dbo].[td_PolicyPlan]  WITH CHECK ADD  CONSTRAINT [FK_td_PolicyPlan_tdManag_Management] FOREIGN KEY([Mid])
REFERENCES [dbo].[tdManag_Management] ([Mid])
GO
ALTER TABLE [dbo].[td_PolicyPlan] CHECK CONSTRAINT [FK_td_PolicyPlan_tdManag_Management]
GO
ALTER TABLE [dbo].[td_selectionDetails]  WITH CHECK ADD  CONSTRAINT [FK_td_selectionDetails_td_selectionType] FOREIGN KEY([sTId])
REFERENCES [dbo].[td_selectionType] ([id])
GO
ALTER TABLE [dbo].[td_selectionDetails] CHECK CONSTRAINT [FK_td_selectionDetails_td_selectionType]
GO
ALTER TABLE [dbo].[tdMana_OrganizationLicenses]  WITH CHECK ADD  CONSTRAINT [FK_OrganizationLicensesName] FOREIGN KEY([OLName])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[tdMana_OrganizationLicenses] CHECK CONSTRAINT [FK_OrganizationLicensesName]
GO
ALTER TABLE [dbo].[tdMana_OrganizationLicenses]  WITH CHECK ADD  CONSTRAINT [FK_OrganizationLicensesPlace] FOREIGN KEY([OLPlace])
REFERENCES [dbo].[td_selectionDetails] ([id])
GO
ALTER TABLE [dbo].[tdMana_OrganizationLicenses] CHECK CONSTRAINT [FK_OrganizationLicensesPlace]
GO
ALTER TABLE [dbo].[tdMana_OrganizationLicenses]  WITH CHECK ADD  CONSTRAINT [FKOrganizationLicenses] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[tdMana_OrganizationLicenses] CHECK CONSTRAINT [FKOrganizationLicenses]
GO
ALTER TABLE [dbo].[tdManag_Department]  WITH CHECK ADD  CONSTRAINT [FK_DepartmentManger] FOREIGN KEY([deptManager])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[tdManag_Department] CHECK CONSTRAINT [FK_DepartmentManger]
GO
ALTER TABLE [dbo].[tdManag_Department]  WITH CHECK ADD  CONSTRAINT [FK_tdManag_Department_tdManag_Management] FOREIGN KEY([Mid])
REFERENCES [dbo].[tdManag_Management] ([Mid])
GO
ALTER TABLE [dbo].[tdManag_Department] CHECK CONSTRAINT [FK_tdManag_Department_tdManag_Management]
GO
ALTER TABLE [dbo].[tdManag_Department]  WITH CHECK ADD  CONSTRAINT [FK_VicManagerDepartment] FOREIGN KEY([deptVicManager])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[tdManag_Department] CHECK CONSTRAINT [FK_VicManagerDepartment]
GO
ALTER TABLE [dbo].[tdManag_Management]  WITH CHECK ADD  CONSTRAINT [FK_Manager] FOREIGN KEY([deptManager])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[tdManag_Management] CHECK CONSTRAINT [FK_Manager]
GO
ALTER TABLE [dbo].[tdManag_Management]  WITH CHECK ADD  CONSTRAINT [FK_tdManag_Management_tdMang_Organization] FOREIGN KEY([OId])
REFERENCES [dbo].[tdMang_Organization] ([OId])
GO
ALTER TABLE [dbo].[tdManag_Management] CHECK CONSTRAINT [FK_tdManag_Management_tdMang_Organization]
GO
ALTER TABLE [dbo].[tdManag_Management]  WITH CHECK ADD  CONSTRAINT [FK_VicManager] FOREIGN KEY([deptVicManager])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[tdManag_Management] CHECK CONSTRAINT [FK_VicManager]
GO
ALTER TABLE [dbo].[tdMang_Organization]  WITH CHECK ADD  CONSTRAINT [FK_ManagerOrganization] FOREIGN KEY([OManager])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[tdMang_Organization] CHECK CONSTRAINT [FK_ManagerOrganization]
GO
ALTER TABLE [dbo].[tdMang_Organization]  WITH CHECK ADD  CONSTRAINT [FK_VicOrganization] FOREIGN KEY([OVicManager])
REFERENCES [dbo].[td_employee] ([empid])
GO
ALTER TABLE [dbo].[tdMang_Organization] CHECK CONSTRAINT [FK_VicOrganization]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FromemployeeId' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'td_Agenda', @level2type=N'CONSTRAINT',@level2name=N'Fromemployee'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OidAgenda' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'td_Agenda', @level2type=N'CONSTRAINT',@level2name=N'OidAgenda'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ToEmployeeId' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'td_Agenda', @level2type=N'CONSTRAINT',@level2name=N'ToEmployee'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LName' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'td_empLicenses', @level2type=N'CONSTRAINT',@level2name=N'FK_LName'
GO
USE [master]
GO
ALTER DATABASE [erpHospitaldb] SET  READ_WRITE 
GO
